import { Route } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { SimpleAdvertisementComponent } from './simple-advertisement.component';

export const SIMPLE_ADVERTISEMENT_ROUTE: Route = {
  path: 'simple-advertisement',
  component: SimpleAdvertisementComponent,
  data: {
    authorities: ['ROLE_USER'],
    pageTitle: 'hotelManagementApp.simpleAdvertisement.title'
  },
  canActivate: [UserRouteAccessService]
};
