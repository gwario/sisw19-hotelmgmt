import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { ISimpleAdvertisement } from 'app/shared/model/simple-advertisement.model';

@Injectable({ providedIn: 'root' })
export class SimpleAdvertisementService {
  public resourceUrl = SERVER_API_URL + 'api/simple-advertisement';

  constructor(protected http: HttpClient) {}

  send(simpleAdvertisement: ISimpleAdvertisement): Observable<any> {
    return this.http.post<ISimpleAdvertisement>(this.resourceUrl, simpleAdvertisement, { observe: 'response' });
  }
}
