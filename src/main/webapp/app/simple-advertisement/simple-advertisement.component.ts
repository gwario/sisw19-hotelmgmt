import { Component, OnInit } from '@angular/core';
import { ICustomer } from 'app/shared/model/customer.model';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { SimpleAdvertisementService } from 'app/simple-advertisement/simple-advertisement.service';
import { ISimpleAdvertisement, SimpleAdvertisement } from 'app/shared/model/simple-advertisement.model';
import { JhiAlertService } from 'ng-jhipster';

@Component({
  selector: 'jhi-simple-advertisement',
  templateUrl: './simple-advertisement.component.html',
  styleUrls: ['./simple-advertisement.component.scss']
})
export class SimpleAdvertisementComponent implements OnInit {
  isSending: boolean;

  sendForm = this.fb.group({
    subject: [null, [Validators.required]],
    body: [null, [Validators.required]]
  });

  constructor(
    protected simpleAdvertisementService: SimpleAdvertisementService,
    protected jhiAlertService: JhiAlertService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSending = false;
  }

  send() {
    this.isSending = true;
    const advertisement = this.sendFromForm();
    this.subscribeToSaveResponse(this.simpleAdvertisementService.send(advertisement));
  }

  private sendFromForm(): ISimpleAdvertisement {
    return {
      ...new SimpleAdvertisement(),
      subject: this.sendForm.get(['subject']).value,
      body: this.sendForm.get(['body']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomer>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.clear();
    this.isSending = false;
  }

  protected onSaveError() {
    this.jhiAlertService.error(
      'Failed to send advertisement! If the problem persists, please contact your system administrator.',
      null,
      null
    );
    this.isSending = false;
  }

  protected clear() {
    this.sendForm.reset();
  }
}
