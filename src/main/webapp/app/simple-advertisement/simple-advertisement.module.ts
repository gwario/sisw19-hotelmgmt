import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { SimpleAdvertisementComponent } from 'app/simple-advertisement/simple-advertisement.component';
import { SIMPLE_ADVERTISEMENT_ROUTE } from 'app/simple-advertisement/simple-advertisement.route';

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild([SIMPLE_ADVERTISEMENT_ROUTE])],
  declarations: [SimpleAdvertisementComponent]
})
export class HotelManagementSimpleAdvertisementModule {}
