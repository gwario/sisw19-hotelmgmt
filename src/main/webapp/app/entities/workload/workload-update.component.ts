import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IWorkload, Workload } from 'app/shared/model/workload.model';
import { WorkloadService } from './workload.service';
import { IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from 'app/entities/employee/employee.service';

@Component({
  selector: 'jhi-workload-update',
  templateUrl: './workload-update.component.html'
})
export class WorkloadUpdateComponent implements OnInit {
  isSaving: boolean;

  employees: IEmployee[];

  editForm = this.fb.group({
    id: [],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    breakIncluded: [true, [Validators.required]],
    employeeId: [null, [Validators.required]]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected workloadService: WorkloadService,
    protected employeeService: EmployeeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ workload }) => {
      this.updateForm(workload);
    });
    this.employeeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEmployee[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEmployee[]>) => response.body)
      )
      .subscribe((res: IEmployee[]) => (this.employees = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(workload: IWorkload) {
    this.editForm.patchValue({
      id: workload.id,
      startDate: workload.startDate != null ? workload.startDate.format(DATE_TIME_FORMAT) : null,
      endDate: workload.endDate != null ? workload.endDate.format(DATE_TIME_FORMAT) : null,
      breakIncluded: workload.breakIncluded,
      employeeId: workload.employee != null ? workload.employee.id : null
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const workload = this.createFromForm();
    if (workload.id !== undefined) {
      this.subscribeToSaveResponse(this.workloadService.update(workload));
    } else {
      this.subscribeToSaveResponse(this.workloadService.create(workload));
    }
  }

  private createFromForm(): IWorkload {
    return {
      ...new Workload(),
      id: this.editForm.get(['id']).value,
      startDate:
        this.editForm.get(['startDate']).value != null ? moment(this.editForm.get(['startDate']).value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate']).value != null ? moment(this.editForm.get(['endDate']).value, DATE_TIME_FORMAT) : undefined,
      breakIncluded: this.editForm.get(['breakIncluded']).value,
      employee: { id: this.editForm.get(['employeeId']).value }
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWorkload>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackEmployeeById(index: number, item: IEmployee) {
    return item.id;
  }
}
