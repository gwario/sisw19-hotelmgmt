import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWorkload } from 'app/shared/model/workload.model';

type EntityResponseType = HttpResponse<IWorkload>;
type EntityArrayResponseType = HttpResponse<IWorkload[]>;

@Injectable({ providedIn: 'root' })
export class WorkloadService {
  public resourceUrl = SERVER_API_URL + 'api/workloads';

  constructor(protected http: HttpClient) {}

  create(workload: IWorkload): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workload);
    return this.http
      .post<IWorkload>(this.resourceUrl + '/request', copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  request(workload: IWorkload): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workload);
    return this.http
      .post<IWorkload>(this.resourceUrl + '/request', copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(workload: IWorkload): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(workload);
    return this.http
      .put<IWorkload>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWorkload>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWorkload[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  queryBoss(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWorkload[]>(this.resourceUrl + '/boss', { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  acceptWorkload(id: number) {
    return this.http
      .post<IWorkload>(this.resourceUrl + '/accept', id, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
  rejectWorkload(id: number) {
    return this.http
      .post<IWorkload>(this.resourceUrl + '/reject', id, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(workload: IWorkload): IWorkload {
    const copy: IWorkload = Object.assign({}, workload, {
      startDate: workload.startDate != null && workload.startDate.isValid() ? workload.startDate.toJSON() : null,
      endDate: workload.endDate != null && workload.endDate.isValid() ? workload.endDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate != null ? moment(res.body.startDate) : null;
      res.body.endDate = res.body.endDate != null ? moment(res.body.endDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((workload: IWorkload) => {
        workload.startDate = workload.startDate != null ? moment(workload.startDate) : null;
        workload.endDate = workload.endDate != null ? moment(workload.endDate) : null;
      });
    }
    return res;
  }
}
