import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IWorkload, Workload } from 'app/shared/model/workload.model';
import { WorkloadService } from './workload.service';
import { WorkloadComponent } from './workload.component';
import { WorkloadDetailComponent } from './workload-detail.component';
import { WorkloadUpdateComponent } from './workload-update.component';
import { WorkloadDeletePopupComponent } from './workload-delete-dialog.component';
import { WorkloadBossComponent } from 'app/entities/workload/workload-boss.component';

@Injectable({ providedIn: 'root' })
export class WorkloadResolve implements Resolve<IWorkload> {
  constructor(private service: WorkloadService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IWorkload> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Workload>) => response.ok),
        map((workload: HttpResponse<Workload>) => workload.body)
      );
    }
    return of(new Workload());
  }
}

export const workloadRoute: Routes = [
  {
    path: '',
    component: WorkloadComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hotelManagementApp.workload.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'boss',
    component: WorkloadBossComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hotelManagementApp.workload.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WorkloadDetailComponent,
    resolve: {
      workload: WorkloadResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.workload.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WorkloadUpdateComponent,
    resolve: {
      workload: WorkloadResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.workload.home.title'
    },
    canActivate: [UserRouteAccessService]
  },

  {
    path: ':id/edit',
    component: WorkloadUpdateComponent,
    resolve: {
      workload: WorkloadResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.workload.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const workloadPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: WorkloadDeletePopupComponent,
    resolve: {
      workload: WorkloadResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.workload.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
