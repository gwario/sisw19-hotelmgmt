import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IWorkload } from 'app/shared/model/workload.model';

@Component({
  selector: 'jhi-workload-detail',
  templateUrl: './workload-detail.component.html'
})
export class WorkloadDetailComponent implements OnInit {
  workload: IWorkload;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ workload }) => {
      this.workload = workload;
    });
  }

  previousState() {
    window.history.back();
  }
}
