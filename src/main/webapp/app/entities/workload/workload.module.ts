import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { WorkloadComponent } from './workload.component';
import { WorkloadDetailComponent } from './workload-detail.component';
import { WorkloadUpdateComponent } from './workload-update.component';
import { WorkloadDeletePopupComponent, WorkloadDeleteDialogComponent } from './workload-delete-dialog.component';
import { workloadRoute, workloadPopupRoute } from './workload.route';
import { WorkloadBossComponent } from 'app/entities/workload/workload-boss.component';

const ENTITY_STATES = [...workloadRoute, ...workloadPopupRoute];

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    WorkloadComponent,
    WorkloadDetailComponent,
    WorkloadUpdateComponent,
    WorkloadDeleteDialogComponent,
    WorkloadDeletePopupComponent,
    WorkloadBossComponent
  ],
  entryComponents: [WorkloadDeleteDialogComponent]
})
export class HotelManagementWorkloadModule {}
