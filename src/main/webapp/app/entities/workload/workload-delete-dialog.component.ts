import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWorkload } from 'app/shared/model/workload.model';
import { WorkloadService } from './workload.service';

@Component({
  selector: 'jhi-workload-delete-dialog',
  templateUrl: './workload-delete-dialog.component.html'
})
export class WorkloadDeleteDialogComponent {
  workload: IWorkload;

  constructor(protected workloadService: WorkloadService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.workloadService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'workloadListModification',
        content: 'Deleted an workload'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-workload-delete-popup',
  template: ''
})
export class WorkloadDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ workload }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(WorkloadDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.workload = workload;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/workload', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/workload', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
