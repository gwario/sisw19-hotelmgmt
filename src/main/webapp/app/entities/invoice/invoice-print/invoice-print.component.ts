import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IInvoice } from 'app/shared/model/invoice.model';
import { IBooking } from 'app/shared/model/booking.model';
import { TranslateService } from '@ngx-translate/core';
import { InvoicePrice } from 'app/shared/model/invoice-price.model';

@Component({
  selector: 'jhi-invoice-print',
  templateUrl: './invoice-print.component.html',
  styleUrls: ['./invoice-print.component.scss']
})
export class InvoicePrintComponent implements OnInit, OnDestroy, AfterViewInit {
  public static inPrint: boolean;
  booking: IBooking;
  invoiceId: number;
  invoice: IInvoice;

  printedRooms: string[] = [];

  constructor(protected activatedRoute: ActivatedRoute, protected translate: TranslateService) {
    InvoicePrintComponent.inPrint = true;
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ booking }) => {
      this.booking = booking;
      this.activatedRoute.paramMap.subscribe(params => {
        this.invoiceId = parseInt(params.get('id'), 10);
        this.invoice = booking.invoices.filter(invoice => invoice.id === this.invoiceId).pop();
      });
    });
  }

  ngAfterViewInit(): void {
    window.print();
  }

  ngOnDestroy(): void {
    InvoicePrintComponent.inPrint = false;
  }

  formatBillingAddress() {
    return this.invoice.customerAddress
      .split(',')
      .map(line => line.trim())
      .join('\n');
  }

  getGenderText(): string {
    return this.translate.instant('hotelManagementApp.invoice.print.gender.' + this.invoice.customerGender.toLowerCase());
  }

  getIndexOfRoomPrice(roomNumber: string): number {
    return this.invoice.prices.findIndex(value => value.roomNumber === roomNumber);
  }

  shouldShowRoomInfo(roomNumber: string) {
    if (this.printedRooms.includes(roomNumber) || roomNumber === '') {
      return false;
    } else {
      this.printedRooms.push(roomNumber);
      return true;
    }
  }

  filteredPrices() {
    const prices: InvoicePrice[] = [];
    prices.push(this.invoice.prices[0]);
    if (this.invoice.prices.length > 0) {
      for (let i = 1; i < this.invoice.prices.length; ++i) {
        const pricesElement = this.invoice.prices[i];
        if (pricesElement.discountPercent === 0) {
          if (pricesElement.note.includes('sum_int')) {
            if (
              (this.invoice.prices[i - 1].price === 0 && this.invoice.prices[i - 1].roomNumber === '') ||
              pricesElement.price === prices[prices.length - 1].price
            ) {
              continue;
            }
          }
          if (pricesElement.note.includes('discount')) {
            continue;
          }
        }
        prices.push(pricesElement);
      }
    }
    return prices;
  }

  getNoteTranslation(note: string) {
    const translation = this.translate.instant('hotelManagementApp.invoice.print.' + note);
    if (translation.startsWith('translation-not-found')) {
      return note;
    }
    return translation;
  }
}
