import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IBooking } from 'app/shared/model/booking.model';
import { IInvoice } from 'app/shared/model/invoice.model';
import { JhiEventManager } from 'ng-jhipster';
import { BookingService } from 'app/entities/booking/booking.service';

@Component({
  selector: 'jhi-invoice-detail',
  templateUrl: './invoice-detail.component.html',
  styleUrls: ['./invoice-detail.component.scss']
})
export class InvoiceDetailComponent implements OnInit {
  booking: IBooking;
  invoiceId: number;
  invoice: IInvoice;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected eventManager: JhiEventManager,
    protected bookingService: BookingService,
    protected router: Router,
    protected route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ booking }) => {
      this.booking = booking;
      this.activatedRoute.paramMap.subscribe(params => {
        this.invoiceId = parseInt(params.get('id'), 10);
        this.invoice = this.booking.invoices.filter(invoice => invoice.id === this.invoiceId).pop();
      });
    });

    this.eventManager.subscribe('invoiceListModification', () => {
      this.bookingService.findWithInvoiceId(this.invoiceId).subscribe(booking => {
        this.invoice = booking.body.invoices.filter(invoice => invoice.id === this.invoiceId).pop();
      });
    });
  }

  previousState() {
    this.router.navigate(['invoice'], { relativeTo: this.route.root });
  }
}
