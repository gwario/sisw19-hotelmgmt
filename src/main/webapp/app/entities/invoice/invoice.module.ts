import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {HotelManagementSharedModule} from 'app/shared/shared.module';
import {InvoiceComponent} from './invoice.component';
import {InvoiceDetailComponent} from './invoice-detail.component';
import {InvoiceDeletePopupComponent, InvoiceDeleteDialogComponent} from './invoice-delete-dialog.component';
import {invoiceRoute, invoicePopupRoute} from './invoice.route';
import {InvoicePrintComponent} from './invoice-print/invoice-print.component';

const ENTITY_STATES = [...invoiceRoute, ...invoicePopupRoute];

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [InvoiceComponent,
    InvoiceDetailComponent,
    InvoiceDeleteDialogComponent,
    InvoiceDeletePopupComponent,
    InvoicePrintComponent
  ],
  entryComponents: [InvoiceDeleteDialogComponent]
})
export class HotelManagementInvoiceModule {
}
