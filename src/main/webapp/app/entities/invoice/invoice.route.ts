import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { InvoiceComponent } from './invoice.component';
import { InvoiceDetailComponent } from './invoice-detail.component';
import { InvoiceDeletePopupComponent } from './invoice-delete-dialog.component';
import { BookingService } from 'app/entities/booking/booking.service';
import { Booking, IBooking } from 'app/shared/model/booking.model';
import { InvoicePrintComponent } from 'app/entities/invoice/invoice-print/invoice-print.component';

@Injectable({ providedIn: 'root' })
export class BookingResolve implements Resolve<IBooking> {
  constructor(private service: BookingService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBooking> {
    const id = route.params['id'];
    if (id) {
      return this.service.findWithInvoiceId(id).pipe(
        filter((response: HttpResponse<Booking>) => response.ok),
        map((booking: HttpResponse<Booking>) => booking.body)
      );
    }
    return of(new Booking());
  }
}

export const invoiceRoute: Routes = [
  {
    path: '',
    component: InvoiceComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hotelManagementApp.invoice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: InvoiceDetailComponent,
    resolve: {
      booking: BookingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.invoice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/print',
    component: InvoicePrintComponent,
    resolve: {
      booking: BookingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.invoice.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const invoicePopupRoute: Routes = [
  {
    path: ':id/cancel',
    component: InvoiceDeletePopupComponent,
    resolve: {
      booking: BookingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.invoice.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
