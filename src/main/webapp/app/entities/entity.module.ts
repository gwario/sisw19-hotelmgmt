import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'customer',
        loadChildren: () => import('./customer/customer.module').then(m => m.HotelManagementCustomerModule)
      },
      {
        path: 'room',
        loadChildren: () => import('./room/room.module').then(m => m.HotelManagementRoomModule)
      },
      {
        path: 'booking',
        loadChildren: () => import('./booking/booking.module').then(m => m.HotelManagementBookingModule)
      },
      {
        path: 'invoice',
        loadChildren: () => import('./invoice/invoice.module').then(m => m.HotelManagementInvoiceModule)
      },
      {
        path: 'hotel',
        loadChildren: () => import('./hotel/hotel.module').then(m => m.HotelManagementHotelModule)
      },
      {
        path: 'defect',
        loadChildren: () => import('./defect/defect.module').then(m => m.HotelManagementDefectModule)
      },
      {
        path: 'employee',
        loadChildren: () => import('./employee/employee.module').then(m => m.HotelManagementEmployeeModule)
      },
      {
        path: 'vacation',
        loadChildren: () => import('./vacation/vacation.module').then(m => m.HotelManagementVacationModule)
      },
      {
        path: 'workload',
        loadChildren: () => import('./workload/workload.module').then(m => m.HotelManagementWorkloadModule)
      },
      {
        path: 'absence-reason',
        loadChildren: () => import('./absence-reason/absence-reason.module').then(m => m.HotelManagementAbsenceReasonModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class HotelManagementEntityModule {}
