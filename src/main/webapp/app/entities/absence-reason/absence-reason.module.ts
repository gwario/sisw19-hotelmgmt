import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { AbsenceReasonComponent } from './absence-reason.component';
import { AbsenceReasonDetailComponent } from './absence-reason-detail.component';
import { AbsenceReasonUpdateComponent } from './absence-reason-update.component';
import { AbsenceReasonDeletePopupComponent, AbsenceReasonDeleteDialogComponent } from './absence-reason-delete-dialog.component';
import { absenceReasonRoute, absenceReasonPopupRoute } from './absence-reason.route';

const ENTITY_STATES = [...absenceReasonRoute, ...absenceReasonPopupRoute];

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    AbsenceReasonComponent,
    AbsenceReasonDetailComponent,
    AbsenceReasonUpdateComponent,
    AbsenceReasonDeleteDialogComponent,
    AbsenceReasonDeletePopupComponent
  ],
  entryComponents: [AbsenceReasonDeleteDialogComponent]
})
export class HotelManagementAbsenceReasonModule {}
