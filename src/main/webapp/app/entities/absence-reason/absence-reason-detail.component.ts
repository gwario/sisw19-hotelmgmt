import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAbsenceReason } from 'app/shared/model/absence-reason.model';

@Component({
  selector: 'jhi-absence-reason-detail',
  templateUrl: './absence-reason-detail.component.html'
})
export class AbsenceReasonDetailComponent implements OnInit {
  absenceReason: IAbsenceReason;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ absenceReason }) => {
      this.absenceReason = absenceReason;
    });
  }

  previousState() {
    window.history.back();
  }
}
