import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAbsenceReason } from 'app/shared/model/absence-reason.model';

type EntityResponseType = HttpResponse<IAbsenceReason>;
type EntityArrayResponseType = HttpResponse<IAbsenceReason[]>;

@Injectable({ providedIn: 'root' })
export class AbsenceReasonService {
  public resourceUrl = SERVER_API_URL + 'api/absence-reasons';

  constructor(protected http: HttpClient) {}

  create(absenceReason: IAbsenceReason): Observable<EntityResponseType> {
    return this.http.post<IAbsenceReason>(this.resourceUrl, absenceReason, { observe: 'response' });
  }

  update(absenceReason: IAbsenceReason): Observable<EntityResponseType> {
    return this.http.put<IAbsenceReason>(this.resourceUrl, absenceReason, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAbsenceReason>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAbsenceReason[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
