import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAbsenceReason } from 'app/shared/model/absence-reason.model';
import { AbsenceReasonService } from './absence-reason.service';

@Component({
  selector: 'jhi-absence-reason-delete-dialog',
  templateUrl: './absence-reason-delete-dialog.component.html'
})
export class AbsenceReasonDeleteDialogComponent {
  absenceReason: IAbsenceReason;

  constructor(
    protected absenceReasonService: AbsenceReasonService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.absenceReasonService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'absenceReasonListModification',
        content: 'Deleted an absenceReason'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-absence-reason-delete-popup',
  template: ''
})
export class AbsenceReasonDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ absenceReason }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(AbsenceReasonDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.absenceReason = absenceReason;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/absence-reason', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/absence-reason', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
