import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { AbsenceReason } from 'app/shared/model/absence-reason.model';
import { AbsenceReasonService } from './absence-reason.service';
import { AbsenceReasonComponent } from './absence-reason.component';
import { AbsenceReasonDetailComponent } from './absence-reason-detail.component';
import { AbsenceReasonUpdateComponent } from './absence-reason-update.component';
import { AbsenceReasonDeletePopupComponent } from './absence-reason-delete-dialog.component';
import { IAbsenceReason } from 'app/shared/model/absence-reason.model';

@Injectable({ providedIn: 'root' })
export class AbsenceReasonResolve implements Resolve<IAbsenceReason> {
  constructor(private service: AbsenceReasonService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAbsenceReason> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<AbsenceReason>) => response.ok),
        map((absenceReason: HttpResponse<AbsenceReason>) => absenceReason.body)
      );
    }
    return of(new AbsenceReason());
  }
}

export const absenceReasonRoute: Routes = [
  {
    path: '',
    component: AbsenceReasonComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hotelManagementApp.absenceReason.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AbsenceReasonDetailComponent,
    resolve: {
      absenceReason: AbsenceReasonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.absenceReason.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AbsenceReasonUpdateComponent,
    resolve: {
      absenceReason: AbsenceReasonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.absenceReason.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AbsenceReasonUpdateComponent,
    resolve: {
      absenceReason: AbsenceReasonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.absenceReason.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const absenceReasonPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: AbsenceReasonDeletePopupComponent,
    resolve: {
      absenceReason: AbsenceReasonResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.absenceReason.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
