import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IAbsenceReason, AbsenceReason } from 'app/shared/model/absence-reason.model';
import { AbsenceReasonService } from './absence-reason.service';

@Component({
  selector: 'jhi-absence-reason-update',
  templateUrl: './absence-reason-update.component.html'
})
export class AbsenceReasonUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    reason: [null, [Validators.required]]
  });

  constructor(protected absenceReasonService: AbsenceReasonService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ absenceReason }) => {
      this.updateForm(absenceReason);
    });
  }

  updateForm(absenceReason: IAbsenceReason) {
    this.editForm.patchValue({
      id: absenceReason.id,
      reason: absenceReason.reason
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const absenceReason = this.createFromForm();
    if (absenceReason.id !== undefined) {
      this.subscribeToSaveResponse(this.absenceReasonService.update(absenceReason));
    } else {
      this.subscribeToSaveResponse(this.absenceReasonService.create(absenceReason));
    }
  }

  private createFromForm(): IAbsenceReason {
    return {
      ...new AbsenceReason(),
      id: this.editForm.get(['id']).value,
      reason: this.editForm.get(['reason']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAbsenceReason>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
