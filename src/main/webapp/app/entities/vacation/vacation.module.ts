import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { VacationComponent } from './vacation.component';
import { VacationDetailComponent } from './vacation-detail.component';
import { VacationUpdateComponent } from './vacation-update.component';
import { VacationDeletePopupComponent, VacationDeleteDialogComponent } from './vacation-delete-dialog.component';
import { vacationRoute, vacationPopupRoute } from './vacation.route';
import { VacationRequestComponent } from 'app/entities/vacation/vacation-request.component';
import { VacationBossComponent } from 'app/entities/vacation/vacation-boss.component';

const ENTITY_STATES = [...vacationRoute, ...vacationPopupRoute];

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    VacationComponent,
    VacationDetailComponent,
    VacationUpdateComponent,
    VacationDeleteDialogComponent,
    VacationDeletePopupComponent,
    VacationRequestComponent,
    VacationBossComponent
  ],
  entryComponents: [VacationDeleteDialogComponent]
})
export class HotelManagementVacationModule {}
