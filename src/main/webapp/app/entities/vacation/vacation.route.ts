import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Vacation } from 'app/shared/model/vacation.model';
import { VacationService } from './vacation.service';
import { VacationComponent } from './vacation.component';
import { VacationDetailComponent } from './vacation-detail.component';
import { VacationUpdateComponent } from './vacation-update.component';
import { VacationDeletePopupComponent } from './vacation-delete-dialog.component';
import { IVacation } from 'app/shared/model/vacation.model';
import { VacationRequestComponent } from 'app/entities/vacation/vacation-request.component';
import { VacationBossComponent } from 'app/entities/vacation/vacation-boss.component';

@Injectable({ providedIn: 'root' })
export class VacationResolve implements Resolve<IVacation> {
  constructor(private service: VacationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IVacation> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Vacation>) => response.ok),
        map((vacation: HttpResponse<Vacation>) => vacation.body)
      );
    }
    return of(new Vacation());
  }
}

export const vacationRoute: Routes = [
  {
    path: '',
    component: VacationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hotelManagementApp.vacation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'boss',
    component: VacationBossComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hotelManagementApp.vacation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: VacationDetailComponent,
    resolve: {
      vacation: VacationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.vacation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: VacationUpdateComponent,
    resolve: {
      vacation: VacationResolve
    },
    data: {
      authorities: ['ROLE_ADMIN'],
      pageTitle: 'hotelManagementApp.vacation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'request',
    component: VacationRequestComponent,
    resolve: {
      vacation: VacationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.vacation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: VacationUpdateComponent,
    resolve: {
      vacation: VacationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.vacation.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const vacationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: VacationDeletePopupComponent,
    resolve: {
      vacation: VacationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.vacation.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
