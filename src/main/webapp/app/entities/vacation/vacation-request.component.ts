import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IVacation, Vacation } from 'app/shared/model/vacation.model';
import { VacationService } from './vacation.service';
import { IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from 'app/entities/employee/employee.service';
import { IAbsenceReason } from 'app/shared/model/absence-reason.model';
import { AbsenceReasonService } from 'app/entities/absence-reason/absence-reason.service';

@Component({
  selector: 'jhi-vacation-request',
  templateUrl: './vacation-request.component.html'
})
export class VacationRequestComponent implements OnInit {
  isSaving: boolean;

  absencereasons: IAbsenceReason[];

  editForm = this.fb.group({
    id: [],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    absenceReasonId: [null, [Validators.required]]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected vacationService: VacationService,
    protected employeeService: EmployeeService,
    protected absenceReasonService: AbsenceReasonService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ vacation }) => {
      this.updateForm(vacation);
    });
    this.absenceReasonService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IAbsenceReason[]>) => mayBeOk.ok),
        map((response: HttpResponse<IAbsenceReason[]>) => response.body)
      )
      .subscribe((res: IAbsenceReason[]) => (this.absencereasons = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(vacation: IVacation) {
    this.editForm.patchValue({
      id: vacation.id,
      startDate: vacation.startDate != null ? vacation.startDate.format(DATE_TIME_FORMAT) : null,
      endDate: vacation.endDate != null ? vacation.endDate.format(DATE_TIME_FORMAT) : null,
      // status: vacation.status,
      absenceReasonId: vacation.absenceReasonId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const vacation = this.createFromForm();

    this.subscribeToSaveResponse(this.vacationService.request(vacation));
  }

  private createFromForm(): IVacation {
    return {
      ...new Vacation(),
      id: this.editForm.get(['id']).value,
      startDate:
        this.editForm.get(['startDate']).value != null ? moment(this.editForm.get(['startDate']).value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate']).value != null ? moment(this.editForm.get(['endDate']).value, DATE_TIME_FORMAT) : undefined,
      // status: this.editForm.get(['status']).value,
      absenceReasonId: this.editForm.get(['absenceReasonId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IVacation>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackEmployeeById(index: number, item: IEmployee) {
    return item.id;
  }

  trackAbsenceReasonById(index: number, item: IAbsenceReason) {
    return item.id;
  }
}
