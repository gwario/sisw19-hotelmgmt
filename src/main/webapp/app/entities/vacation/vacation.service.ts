import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IVacation } from 'app/shared/model/vacation.model';

type EntityResponseType = HttpResponse<IVacation>;
type EntityArrayResponseType = HttpResponse<IVacation[]>;

@Injectable({ providedIn: 'root' })
export class VacationService {
  public resourceUrl = SERVER_API_URL + 'api/vacations';

  constructor(protected http: HttpClient) {}

  create(vacation: IVacation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vacation);
    return this.http
      .post<IVacation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  request(vacation: IVacation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vacation);
    return this.http
      .post<IVacation>(this.resourceUrl + '/request', copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(vacation: IVacation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(vacation);
    return this.http
      .put<IVacation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IVacation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IVacation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  queryBoss(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IVacation[]>(this.resourceUrl + '/boss', { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  acceptVacation(id: number) {
    return this.http
      .post<IVacation>(this.resourceUrl + '/accept', id, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
  rejectVacation(id: number) {
    return this.http
      .post<IVacation>(this.resourceUrl + '/reject', id, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(vacation: IVacation): IVacation {
    const copy: IVacation = Object.assign({}, vacation, {
      startDate: vacation.startDate != null && vacation.startDate.isValid() ? vacation.startDate.toJSON() : null,
      endDate: vacation.endDate != null && vacation.endDate.isValid() ? vacation.endDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate != null ? moment(res.body.startDate) : null;
      res.body.endDate = res.body.endDate != null ? moment(res.body.endDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((vacation: IVacation) => {
        vacation.startDate = vacation.startDate != null ? moment(vacation.startDate) : null;
        vacation.endDate = vacation.endDate != null ? moment(vacation.endDate) : null;
      });
    }
    return res;
  }
}
