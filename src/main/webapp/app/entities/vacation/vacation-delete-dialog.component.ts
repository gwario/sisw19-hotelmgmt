import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IVacation } from 'app/shared/model/vacation.model';
import { VacationService } from './vacation.service';

@Component({
  selector: 'jhi-vacation-delete-dialog',
  templateUrl: './vacation-delete-dialog.component.html'
})
export class VacationDeleteDialogComponent {
  vacation: IVacation;

  constructor(protected vacationService: VacationService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.vacationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'vacationListModification',
        content: 'Deleted an vacation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-vacation-delete-popup',
  template: ''
})
export class VacationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ vacation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(VacationDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.vacation = vacation;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/vacation', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/vacation', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
