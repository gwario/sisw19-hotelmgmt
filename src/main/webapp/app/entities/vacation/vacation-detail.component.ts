import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IVacation } from 'app/shared/model/vacation.model';

@Component({
  selector: 'jhi-vacation-detail',
  templateUrl: './vacation-detail.component.html'
})
export class VacationDetailComponent implements OnInit {
  vacation: IVacation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ vacation }) => {
      this.vacation = vacation;
    });
  }

  previousState() {
    window.history.back();
  }
}
