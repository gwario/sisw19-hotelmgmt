import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { HotelComponent } from './hotel.component';
import { HotelDetailComponent } from './hotel-detail.component';
import { HotelUpdateComponent } from './hotel-update.component';
import { HotelDeletePopupComponent, HotelDeleteDialogComponent } from './hotel-delete-dialog.component';
import { hotelRoute, hotelPopupRoute } from './hotel.route';

const ENTITY_STATES = [...hotelRoute, ...hotelPopupRoute];

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [HotelComponent, HotelDetailComponent, HotelUpdateComponent, HotelDeleteDialogComponent, HotelDeletePopupComponent],
  entryComponents: [HotelDeleteDialogComponent]
})
export class HotelManagementHotelModule {}
