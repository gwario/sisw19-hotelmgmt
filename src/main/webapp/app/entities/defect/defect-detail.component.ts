import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDefect } from 'app/shared/model/defect.model';

@Component({
  selector: 'jhi-defect-detail',
  templateUrl: './defect-detail.component.html'
})
export class DefectDetailComponent implements OnInit {
  defect: IDefect;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ defect }) => {
      this.defect = defect;
    });
  }

  previousState() {
    window.history.back();
  }
}
