import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IDefect, Defect } from 'app/shared/model/defect.model';
import { DefectService } from './defect.service';
import { IRoom } from 'app/shared/model/room.model';
import { RoomService } from 'app/entities/room/room.service';

@Component({
  selector: 'jhi-defect-update',
  templateUrl: './defect-update.component.html'
})
export class DefectUpdateComponent implements OnInit {
  isSaving: boolean;

  rooms: IRoom[];

  editForm = this.fb.group({
    id: [],
    description: [null, [Validators.required]],
    defectState: [null, [Validators.required]],
    discountPercent: [null, [Validators.required, Validators.min(0), Validators.max(100)]],
    roomId: [null, [Validators.required]]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected defectService: DefectService,
    protected roomService: RoomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ defect }) => {
      this.updateForm(defect);
    });
    this.roomService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRoom[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRoom[]>) => response.body)
      )
      .subscribe((res: IRoom[]) => (this.rooms = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(defect: IDefect) {
    this.editForm.patchValue({
      id: defect.id,
      description: defect.description,
      defectState: defect.defectState,
      discountPercent: defect.discountPercent,
      roomId: defect.roomId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const defect = this.createFromForm();
    if (defect.id !== undefined) {
      this.subscribeToSaveResponse(this.defectService.update(defect));
    } else {
      this.subscribeToSaveResponse(this.defectService.create(defect));
    }
  }

  private createFromForm(): IDefect {
    return {
      ...new Defect(),
      id: this.editForm.get(['id']).value,
      description: this.editForm.get(['description']).value,
      defectState: this.editForm.get(['defectState']).value,
      discountPercent: this.editForm.get(['discountPercent']).value,
      roomId: this.editForm.get(['roomId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDefect>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackRoomById(index: number, item: IRoom) {
    return item.id;
  }
}
