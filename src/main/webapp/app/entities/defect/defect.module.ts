import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { DefectComponent } from './defect.component';
import { DefectDetailComponent } from './defect-detail.component';
import { DefectUpdateComponent } from './defect-update.component';
import { DefectDeletePopupComponent, DefectDeleteDialogComponent } from './defect-delete-dialog.component';
import { defectRoute, defectPopupRoute } from './defect.route';

const ENTITY_STATES = [...defectRoute, ...defectPopupRoute];

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [DefectComponent, DefectDetailComponent, DefectUpdateComponent, DefectDeleteDialogComponent, DefectDeletePopupComponent],
  entryComponents: [DefectDeleteDialogComponent]
})
export class HotelManagementDefectModule {}
