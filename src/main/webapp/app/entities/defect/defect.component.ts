import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { IDefect } from 'app/shared/model/defect.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { DefectService } from './defect.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'jhi-defect',
  templateUrl: './defect.component.html'
})
export class DefectComponent implements OnInit, OnDestroy {
  currentAccount: any;
  defects: IDefect[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  searchTerm: string;
  predicate: any;
  previousPage: any;
  reverse: any;

  searchForm = this.fb.group({
    term: ['']
  });

  constructor(
    protected defectService: DefectService,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  private addSearchTerm(query: object): object {
    if (this.searchTerm != null && this.searchTerm.length > 0) {
      query['description.contains'] = this.searchTerm;
    }
    return query;
  }

  loadAll() {
    this.defectService
      .query(
        this.addSearchTerm({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
      )
      .subscribe((res: HttpResponse<IDefect[]>) => this.paginateDefects(res.body, res.headers));
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/defect'], {
      queryParams: this.addSearchTerm({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      })
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/defect',
      {
        pagsearchTerme: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInDefects();
  }

  onChangeSearchTerm() {
    this.searchTerm = this.searchForm.get(['term']).value;
    this.loadAll();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IDefect) {
    return item.id;
  }

  registerChangeInDefects() {
    this.eventSubscriber = this.eventManager.subscribe('defectListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateDefects(data: IDefect[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.defects = data;
  }
}
