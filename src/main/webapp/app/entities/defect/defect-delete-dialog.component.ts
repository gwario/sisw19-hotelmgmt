import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDefect } from 'app/shared/model/defect.model';
import { DefectService } from './defect.service';

@Component({
  selector: 'jhi-defect-delete-dialog',
  templateUrl: './defect-delete-dialog.component.html'
})
export class DefectDeleteDialogComponent {
  defect: IDefect;

  constructor(protected defectService: DefectService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.defectService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'defectListModification',
        content: 'Deleted an defect'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-defect-delete-popup',
  template: ''
})
export class DefectDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ defect }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DefectDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.defect = defect;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/defect', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/defect', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
