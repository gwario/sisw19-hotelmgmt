import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Defect } from 'app/shared/model/defect.model';
import { DefectService } from './defect.service';
import { DefectComponent } from './defect.component';
import { DefectDetailComponent } from './defect-detail.component';
import { DefectUpdateComponent } from './defect-update.component';
import { DefectDeletePopupComponent } from './defect-delete-dialog.component';
import { IDefect } from 'app/shared/model/defect.model';

@Injectable({ providedIn: 'root' })
export class DefectResolve implements Resolve<IDefect> {
  constructor(private service: DefectService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDefect> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Defect>) => response.ok),
        map((defect: HttpResponse<Defect>) => defect.body)
      );
    }
    return of(new Defect());
  }
}

export const defectRoute: Routes = [
  {
    path: '',
    component: DefectComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hotelManagementApp.defect.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DefectDetailComponent,
    resolve: {
      defect: DefectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.defect.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DefectUpdateComponent,
    resolve: {
      defect: DefectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.defect.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DefectUpdateComponent,
    resolve: {
      defect: DefectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.defect.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const defectPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DefectDeletePopupComponent,
    resolve: {
      defect: DefectResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hotelManagementApp.defect.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
