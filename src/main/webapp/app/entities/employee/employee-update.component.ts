import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { IEmployee, Employee } from 'app/shared/model/employee.model';
import { EmployeeService } from './employee.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-employee-update',
  templateUrl: './employee-update.component.html'
})
export class EmployeeUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  employees: IEmployee[];
  birthdayDp: any;

  editForm = this.fb.group({
    id: [],
    employeeType: [null, [Validators.required]],
    shortId: [null, [Validators.required]],
    insuranceNumber: [null, [Validators.required]],
    accountData: [null, [Validators.required]],
    contractType: [null, [Validators.required]],
    tasks: [null, [Validators.required]],
    name: [null, [Validators.required]],
    birthday: [null, [Validators.required]],
    gender: [null, [Validators.required]],
    tel: [null, [Validators.required]],
    // email: [null, [Validators.required, Validators.pattern('^[^@s]+@[^@s]+.[^@s]+$')]],
    address: [null, [Validators.required]],
    userId: [],
    bossId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected employeeService: EmployeeService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ employee }) => {
      this.updateForm(employee);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.employeeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEmployee[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEmployee[]>) => response.body)
      )
      .subscribe((res: IEmployee[]) => (this.employees = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(employee: IEmployee) {
    this.editForm.patchValue({
      id: employee.id,
      employeeType: employee.employeeType,
      shortId: employee.shortId,
      insuranceNumber: employee.insuranceNumber,
      accountData: employee.accountData,
      contractType: employee.contractType,
      tasks: employee.tasks,
      name: employee.name,
      birthday: employee.birthday,
      gender: employee.gender,
      tel: employee.tel,
      // email: employee.email,
      address: employee.address,
      userId: employee.userId,
      bossId: employee.bossId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const employee = this.createFromForm();
    if (employee.id !== undefined) {
      this.subscribeToSaveResponse(this.employeeService.update(employee));
    } else {
      this.subscribeToSaveResponse(this.employeeService.create(employee));
    }
  }

  private createFromForm(): IEmployee {
    return {
      ...new Employee(),
      id: this.editForm.get(['id']).value,
      employeeType: this.editForm.get(['employeeType']).value,
      shortId: this.editForm.get(['shortId']).value,
      insuranceNumber: this.editForm.get(['insuranceNumber']).value,
      accountData: this.editForm.get(['accountData']).value,
      contractType: this.editForm.get(['contractType']).value,
      tasks: this.editForm.get(['tasks']).value,
      name: this.editForm.get(['name']).value,
      birthday: this.editForm.get(['birthday']).value,
      gender: this.editForm.get(['gender']).value,
      tel: this.editForm.get(['tel']).value,
      // email: this.editForm.get(['email']).value,
      address: this.editForm.get(['address']).value,
      userId: this.editForm.get(['userId']).value,
      bossId: this.editForm.get(['bossId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmployee>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackEmployeeById(index: number, item: IEmployee) {
    return item.id;
  }
}
