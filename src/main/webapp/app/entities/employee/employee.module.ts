import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { EmployeeComponent } from './employee.component';
import { EmployeeDetailComponent } from './employee-detail.component';
import { EmployeeUpdateComponent } from './employee-update.component';
import { EmployeeDeleteDialogComponent, EmployeeDeletePopupComponent } from './employee-delete-dialog.component';
import { employeePopupRoute, employeeRoute } from './employee.route';
import { EmployeeCreateComponent } from 'app/entities/employee/employee-create.component';

const ENTITY_STATES = [...employeeRoute, ...employeePopupRoute];

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    EmployeeComponent,
    EmployeeDetailComponent,
    EmployeeUpdateComponent,
    EmployeeCreateComponent,
    EmployeeDeleteDialogComponent,
    EmployeeDeletePopupComponent
  ],
  entryComponents: [EmployeeDeleteDialogComponent]
})
export class HotelManagementEmployeeModule {}
