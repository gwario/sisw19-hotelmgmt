import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiLanguageService } from 'ng-jhipster';
import { Employee, IEmployee } from 'app/shared/model/employee.model';
import { EmployeeService } from './employee.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE } from 'app/shared/constants/error.constants';

@Component({
  selector: 'jhi-employee-create',
  templateUrl: './employee-create.component.html'
})
export class EmployeeCreateComponent implements OnInit {
  isSaving: boolean;

  doNotMatch: string;
  error: string;
  errorEmailExists: string;
  errorUserExists: string;
  success: boolean;

  users: IUser[];

  employees: IEmployee[];
  birthdayDp: any;

  editForm = this.fb.group({
    id: [],
    employeeType: [null, [Validators.required]],
    shortId: [null, [Validators.required]],
    insuranceNumber: [null, [Validators.required]],
    contractType: [null, [Validators.required]],
    tasks: [null, [Validators.required]],
    name: [null, [Validators.required]],
    birthday: [null, [Validators.required]],
    gender: [null, [Validators.required]],
    tel: [null, [Validators.required]],
    address: [null, [Validators.required]],
    bossId: []
  });

  registerForm = this.fb.group({
    login: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50), Validators.pattern('^[_.@A-Za-z0-9-]*$')]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]]
  });

  constructor(
    private languageService: JhiLanguageService,
    protected jhiAlertService: JhiAlertService,
    protected employeeService: EmployeeService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ employee }) => {
      this.updateForm(employee);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.employeeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IEmployee[]>) => mayBeOk.ok),
        map((response: HttpResponse<IEmployee[]>) => response.body)
      )
      .subscribe((res: IEmployee[]) => (this.employees = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(employee: IEmployee) {
    this.editForm.patchValue({
      id: employee.id,
      employeeType: employee.employeeType,
      shortId: employee.shortId,
      insuranceNumber: employee.insuranceNumber,
      accountData: employee.accountData,
      contractType: employee.contractType,
      tasks: employee.tasks,
      name: employee.name,
      birthday: employee.birthday,
      gender: employee.gender,
      tel: employee.tel,
      address: employee.address,
      bossId: employee.bossId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const employee = this.createFromForm();

    let registerAccount = {};
    const login = this.registerForm.get(['login']).value;
    const email = this.registerForm.get(['email']).value;
    const password = this.registerForm.get(['password']).value;
    if (password !== this.registerForm.get(['confirmPassword']).value) {
      this.doNotMatch = 'ERROR';
    } else {
      registerAccount = { ...registerAccount, login, email, password };
      this.doNotMatch = null;
      this.error = null;
      this.errorUserExists = null;
      this.errorEmailExists = null;

      this.languageService.getCurrent().then(langKey => {
        registerAccount = { ...registerAccount, langKey };

        this.employeeService.createUser(registerAccount).subscribe(
          userId => {
            this.success = true;
            employee.userId = userId;
            this.subscribeToSaveResponse(this.employeeService.create(employee));
          },
          response => this.processError(response)
        );
      });
    }
  }

  private createFromForm(): IEmployee {
    return {
      ...new Employee(),
      id: this.editForm.get(['id']).value,
      employeeType: this.editForm.get(['employeeType']).value,
      shortId: this.editForm.get(['shortId']).value,
      insuranceNumber: this.editForm.get(['insuranceNumber']).value,
      accountData: '{}',
      contractType: this.editForm.get(['contractType']).value,
      tasks: this.editForm.get(['tasks']).value,
      name: this.editForm.get(['name']).value,
      birthday: this.editForm.get(['birthday']).value,
      gender: this.editForm.get(['gender']).value,
      tel: this.editForm.get(['tel']).value,
      address: this.editForm.get(['address']).value,
      bossId: this.editForm.get(['bossId']).value
    };
  }

  register() {}

  private processError(response: HttpErrorResponse) {
    this.success = null;
    if (response.status === 400 && response.error.type === LOGIN_ALREADY_USED_TYPE) {
      this.errorUserExists = 'ERROR';
    } else if (response.status === 400 && response.error.type === EMAIL_ALREADY_USED_TYPE) {
      this.errorEmailExists = 'ERROR';
    } else {
      this.error = 'ERROR';
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmployee>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackEmployeeById(index: number, item: IEmployee) {
    return item.id;
  }
}
