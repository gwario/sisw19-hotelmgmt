import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { IRoom } from 'app/shared/model/room.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { RoomService } from './room.service';
import { ICustomer } from 'app/shared/model/customer.model';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'jhi-room',
  templateUrl: './room.component.html'
})
export class RoomComponent implements OnInit, OnDestroy {
  currentAccount: any;
  rooms: IRoom[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  searchTerm: string;
  predicate: any;
  previousPage: any;
  reverse: any;

  searchForm = this.fb.group({
    term: ['']
  });

  constructor(
    protected roomService: RoomService,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  private addSearchTerm(query: object): object {
    if (this.searchTerm != null && this.searchTerm.length > 0) {
      query['number.equals'] = this.searchTerm;
      query['maxCapacity.equals'] = this.searchTerm;
    }
    return query;
  }

  loadAll() {
    this.roomService
      .query(
        this.addSearchTerm({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
      )
      .subscribe((res: HttpResponse<IRoom[]>) => this.paginateRooms(res.body, res.headers));
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/room'], {
      queryParams: this.addSearchTerm({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      })
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/room',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInRooms();
  }

  onChangeSearchTerm() {
    this.searchTerm = this.searchForm.get(['term']).value;
    this.loadAll();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IRoom) {
    return item.id;
  }

  registerChangeInRooms() {
    this.eventSubscriber = this.eventManager.subscribe('roomListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateRooms(data: IRoom[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.rooms = data;
  }
}
