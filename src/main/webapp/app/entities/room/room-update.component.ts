import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IRoom, Room } from 'app/shared/model/room.model';
import { RoomService } from './room.service';

@Component({
  selector: 'jhi-room-update',
  templateUrl: './room-update.component.html'
})
export class RoomUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    number: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
    maxCapacity: [null, [Validators.required, Validators.min(1), Validators.max(3)]],
    priceOneAdult: [null, [Validators.required, Validators.min(0)]],
    priceTwoAdults: [null, [Validators.min(0)]],
    priceThreeAdults: [null, [Validators.min(0)]],
    priceOneAdultOneChild: [null, [Validators.min(0)]],
    priceTwoAdultsOneChild: [null, [Validators.min(0)]],
    priceOneAdultTwoChildren: [null, [Validators.min(0)]]
  });

  constructor(protected roomService: RoomService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ room }) => {
      this.updateForm(room);
    });
  }

  updateForm(room: IRoom) {
    this.editForm.patchValue({
      id: room.id,
      number: room.number,
      maxCapacity: room.maxCapacity,
      priceOneAdult: room.priceOneAdult,
      priceTwoAdults: room.priceTwoAdults,
      priceThreeAdults: room.priceThreeAdults,
      priceOneAdultOneChild: room.priceOneAdultOneChild,
      priceTwoAdultsOneChild: room.priceTwoAdultsOneChild,
      priceOneAdultTwoChildren: room.priceOneAdultTwoChildren
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const room = this.createFromForm();
    if (room.id !== undefined) {
      this.subscribeToSaveResponse(this.roomService.update(room));
    } else {
      this.subscribeToSaveResponse(this.roomService.create(room));
    }
  }

  private createFromForm(): IRoom {
    return {
      ...new Room(),
      id: this.editForm.get(['id']).value,
      number: this.editForm.get(['number']).value,
      maxCapacity: this.editForm.get(['maxCapacity']).value,
      priceOneAdult: this.editForm.get(['priceOneAdult']).value,
      priceTwoAdults: this.editForm.get(['priceTwoAdults']).value,
      priceThreeAdults: this.editForm.get(['priceThreeAdults']).value,
      priceOneAdultOneChild: this.editForm.get(['priceOneAdultOneChild']).value,
      priceTwoAdultsOneChild: this.editForm.get(['priceTwoAdultsOneChild']).value,
      priceOneAdultTwoChildren: this.editForm.get(['priceOneAdultTwoChildren']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRoom>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
