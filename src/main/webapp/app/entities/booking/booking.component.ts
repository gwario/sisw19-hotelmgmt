import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { concat, Observable, of, Subject, Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { IBooking } from 'app/shared/model/booking.model';
import { AccountService } from 'app/core/auth/account.service';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { BookingService } from './booking.service';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { ICustomer } from 'app/shared/model/customer.model';
import { catchError, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import { CustomerService } from 'app/entities/customer/customer.service';

@Component({
  selector: 'jhi-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit, OnDestroy {
  currentAccount: any;
  bookings: IBooking[];
  error: any;
  success: any;
  eventSubscriber: Subscription;
  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  filterId = null;
  filterStartDateFrom: Date = null;
  filterStartDateTo: Date = null;
  filterEndDateFrom: Date = null;
  filterEndDateTo: Date = null;
  filterCustomer: ICustomer = null;
  filterEvent = null;
  filterShowCancelled = false;

  selectableCustomers$: Observable<ICustomer[]>;
  customerFilter$ = new Subject<String>();
  loadingCustomers = false;

  constructor(
    protected bookingService: BookingService,
    protected customerService: CustomerService,
    protected parseLinks: JhiParseLinks,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  loadAll() {
    this.bookingService
      .query(
        this.addFilterParameters({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
      )
      .subscribe((res: HttpResponse<IBooking[]>) => this.paginateBookings(res.body, res.headers));
  }

  addFilterParameters(query: any): any {
    if (this.filterId !== null) {
      query['id.equals'] = this.filterId;
    }
    if (this.filterStartDateFrom !== null) {
      query['startDate.greaterThan'] = moment(this.filterStartDateFrom, DATE_TIME_FORMAT).format('YYYY-MM-DD') + 'T00:00:00Z';
    }
    if (this.filterStartDateTo !== null) {
      query['startDate.lessThan'] = moment(this.filterStartDateTo, DATE_TIME_FORMAT).format('YYYY-MM-DD') + 'T23:59:59Z';
    }
    if (this.filterEndDateFrom !== null) {
      query['endDate.greaterThan'] = moment(this.filterEndDateFrom, DATE_TIME_FORMAT).format('YYYY-MM-DD') + 'T00:00:00Z';
    }
    if (this.filterEndDateTo !== null) {
      query['endDate.lessThan'] = moment(this.filterEndDateTo, DATE_TIME_FORMAT).format('YYYY-MM-DD') + 'T23:59:59Z';
    }
    if (this.filterCustomer !== null) {
      query['customerId'] = this.filterCustomer.id;
    }
    if (this.filterEvent !== null && this.filterEvent !== '') {
      query['event.contains'] = this.filterEvent;
    }
    if (this.filterShowCancelled) {
      query['state.in'] = 'ACTIVE,CANCELLED';
    } else {
      query['state.in'] = 'ACTIVE';
    }

    return query;
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/booking'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadAll();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/booking',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.loadCustomers();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInBookings();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  private loadCustomers() {
    this.selectableCustomers$ = concat(
      of([]), // default items
      this.customerFilter$.pipe(
        distinctUntilChanged(),
        tap(() => (this.loadingCustomers = true)),
        switchMap(term =>
          this.customerService.query({ 'name.contains': term, 'companyName.contains': term }).pipe(
            map((response: HttpResponse<ICustomer[]>) => response.body),
            catchError(() => of([])), // empty list on error
            tap(() => (this.loadingCustomers = false))
          )
        )
      )
    );
  }

  trackId(index: number, item: IBooking) {
    return item.id;
  }

  registerChangeInBookings() {
    this.eventSubscriber = this.eventManager.subscribe('bookingListModification', response => this.loadAll());
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateBookings(data: IBooking[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.bookings = data;
  }

  createInvoice(bookingId: number) {
    this.bookingService.createInvoice(bookingId).subscribe(invoiceId => {
      this.router.navigate(['invoice/' + invoiceId + '/view']);
    });
  }
}
