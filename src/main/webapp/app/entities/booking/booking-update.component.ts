import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { concat, Observable, of, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, filter, map, switchMap, tap } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { Booking, IBooking } from 'app/shared/model/booking.model';
import { BookingService } from './booking.service';
import { ICustomer } from 'app/shared/model/customer.model';
import { CustomerService } from 'app/entities/customer/customer.service';
import { RoomService } from 'app/entities/room/room.service';
import { State } from 'app/shared/model/enumerations/state.model';
import { IRoomOccupancy, RoomOccupancy } from 'app/shared/model/room-occupancy.model';
import { IRoomBooked } from 'app/shared/model/room-booked.model';

@Component({
  selector: 'jhi-booking-update',
  templateUrl: './booking-update.component.html'
})
export class BookingUpdateComponent implements OnInit {
  isSaving: boolean;

  // used to populate dropdowns
  selectableCustomers$: Observable<ICustomer[]>;
  customerFilter$ = new Subject<String>();
  loadingCustomers = false;
  availableRooms: IRoomBooked[];
  events: String[];

  roomToAdd: IRoomBooked = null;

  roomOccupancies: IRoomOccupancy[] = [];

  editForm = this.fb.group({
    id: [],
    discountPercent: [0, [Validators.required, Validators.min(0), Validators.max(100)]],
    price: [0, [Validators.required, Validators.min(0)]],
    startDate: [null, [Validators.required]],
    endDate: [null, [Validators.required]],
    durationNights: [null],
    cancellationNoticeNights: [7, [Validators.required, Validators.min(0)]],
    bookingState: [State.ACTIVE],
    event: [null],
    customer: [null, [Validators.required]],
    occupancyNumberAdults: [1, [Validators.required, Validators.min(1), Validators.max(3)]],
    occupancyNumberChildren: [0, [Validators.required, Validators.min(0), Validators.max(2)]],
    roomToAdd: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected bookingService: BookingService,
    protected customerService: CustomerService,
    protected roomService: RoomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ booking }) => {
      this.updateForm(booking);
    });

    this.updateAvailableRooms();
    this.loadCustomers();

    this.bookingService
      .getEvents()
      .pipe(
        filter((mayBeOk: HttpResponse<String[]>) => mayBeOk.ok),
        map((response: HttpResponse<String[]>) => response.body)
      )
      .subscribe((res: String[]) => (this.events = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(booking: IBooking) {
    if (!booking.id) return;
    this.editForm.patchValue({
      id: booking.id,
      discountPercent: booking.discountPercent,
      price: booking.price,
      startDate: booking.startDate != null ? booking.startDate.format(DATE_TIME_FORMAT) : null,
      endDate: booking.endDate != null ? booking.endDate.format(DATE_TIME_FORMAT) : null,
      cancellationNoticeNights: booking.cancellationNoticeNights,
      durationNights: booking.durationNights,
      bookingState: booking.bookingState,
      event: booking.event,
      customer: booking.customer
    });
    this.roomOccupancies = booking.rooms;
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const booking = this.createFromForm();
    if (booking.id) {
      this.subscribeToSaveResponse(this.bookingService.update(booking));
    } else {
      this.subscribeToSaveResponse(this.bookingService.create(booking));
    }
  }

  private createFromForm(): IBooking {
    return {
      ...new Booking(),
      id: this.editForm.get(['id']).value,
      discountPercent: this.editForm.get(['discountPercent']).value,
      price: 0,
      startDate:
        this.editForm.get(['startDate']).value != null ? moment(this.editForm.get(['startDate']).value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate']).value != null ? moment(this.editForm.get(['endDate']).value, DATE_TIME_FORMAT) : undefined,
      cancellationNoticeNights: this.editForm.get(['cancellationNoticeNights']).value,
      bookingState: this.editForm.get(['bookingState']).value,
      event: this.editForm.get(['event']).value,
      customer: this.editForm.get(['customer']).value,
      rooms: this.roomOccupancies
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBooking>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private updateAvailableRooms() {
    const from = this.editForm.get(['startDate']).value;
    const to = this.editForm.get(['endDate']).value;

    if (!from || !to) {
      this.availableRooms = [];
      return;
    }

    this.roomService
      .findByFromToAndMinCapacity(
        this.editForm.get(['startDate']).value.substr(0, 10),
        this.editForm.get(['endDate']).value.substr(0, 10),
        1
      )
      .pipe(
        filter((mayBeOk: HttpResponse<IRoomBooked[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRoomBooked[]>) => response.body)
      )
      .subscribe(
        (res: IRoomBooked[]) => {
          res.forEach(value => {
            if (this.isOccupied(value)) value.booked = true;
          });
          this.availableRooms = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  private loadCustomers() {
    this.selectableCustomers$ = concat(
      of([]), // default items
      this.customerFilter$.pipe(
        distinctUntilChanged(),
        tap(() => (this.loadingCustomers = true)),
        switchMap(term =>
          this.customerService.query({ 'name.contains': term, 'companyName.contains': term }).pipe(
            map((response: HttpResponse<ICustomer[]>) => response.body),
            catchError(() => of([])), // empty list on error
            tap(() => (this.loadingCustomers = false))
          )
        )
      )
    );
  }

  onAddRoomOccupancy(room: IRoomBooked) {
    if (!room || room.booked) return;
    this.roomToAdd = null;
    this.roomOccupancies.push(new RoomOccupancy(null, 1, 0, room.room));
    this.updateAvailableRooms();
    this.updatePricePreview();
  }

  onRemoveOccupancy(occupancy: IRoomOccupancy) {
    const index = this.roomOccupancies.indexOf(occupancy, 0);
    if (index > -1) {
      this.roomOccupancies.splice(index, 1);
      this.updateAvailableRooms();
      this.updatePricePreview();
    }
  }

  onChangeOccupancy() {
    this.updateAvailableRooms();
    if (this.editForm.valid && this.roomOccupancies.length > 0) {
      this.updatePricePreview();
    }
  }

  updatePricePreview() {
    const booking = this.createFromForm();
    this.subscribeToPricePreviewResponse(this.bookingService.pricePreview(booking));
  }

  protected subscribeToPricePreviewResponse(result: Observable<HttpResponse<IBooking>>) {
    result.subscribe(response => this.onPricePreviewSuccess(response), () => this.onPricePreviewError());
  }

  protected onPricePreviewSuccess(result: HttpResponse<IBooking>) {
    this.isSaving = false;
    this.editForm.patchValue({
      price: result.body.price,
      durationNights: result.body.durationNights
    });
  }

  protected onPricePreviewError() {
    this.isSaving = false;
  }

  isOccupied(room: IRoomBooked): boolean {
    return this.roomOccupancies.filter(r => r.room.id === room.room.id).length > 0;
  }
}
