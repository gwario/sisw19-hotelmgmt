import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { BookingComponent } from './booking.component';
import { BookingDetailComponent } from './booking-detail.component';
import { BookingUpdateComponent } from './booking-update.component';
import { BookingDeleteDialogComponent, BookingDeletePopupComponent } from './booking-delete-dialog.component';
import { bookingPopupRoute, bookingRoute } from './booking.route';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';

const ENTITY_STATES = [...bookingRoute, ...bookingPopupRoute];

@NgModule({
  imports: [FormsModule, NgSelectModule, NgOptionHighlightModule, HotelManagementSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BookingComponent,
    BookingDetailComponent,
    BookingUpdateComponent,
    BookingDeleteDialogComponent,
    BookingDeletePopupComponent
  ],
  entryComponents: [BookingDeleteDialogComponent]
})
export class HotelManagementBookingModule {}
