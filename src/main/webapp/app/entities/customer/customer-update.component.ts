import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ICustomer, Customer } from 'app/shared/model/customer.model';
import { CustomerService } from './customer.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-customer-update',
  templateUrl: './customer-update.component.html'
})
export class CustomerUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];
  birthdayDp: any;

  editForm = this.fb.group({
    id: [],
    companyName: [],
    notes: [null, [Validators.required]],
    discountPercent: [null, [Validators.required, Validators.min(0), Validators.max(100)]],
    webpage: [],
    fax: [],
    name: [null, [Validators.required]],
    birthday: [null, [Validators.required]],
    gender: [null, [Validators.required]],
    tel: [null, [Validators.required]],
    email: [null, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    billingAddress: [null, [Validators.required]],
    userId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected customerService: CustomerService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ customer }) => {
      this.updateForm(customer);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(customer: ICustomer) {
    this.editForm.patchValue({
      id: customer.id,
      companyName: customer.companyName,
      notes: customer.notes,
      discountPercent: customer.discountPercent,
      webpage: customer.webpage,
      fax: customer.fax,
      name: customer.name,
      birthday: customer.birthday,
      gender: customer.gender,
      tel: customer.tel,
      email: customer.email,
      billingAddress: customer.billingAddress,
      userId: customer.userId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const customer = this.createFromForm();
    if (customer.id !== undefined) {
      this.subscribeToSaveResponse(this.customerService.update(customer));
    } else {
      this.subscribeToSaveResponse(this.customerService.create(customer));
    }
  }

  private createFromForm(): ICustomer {
    return {
      ...new Customer(),
      id: this.editForm.get(['id']).value,
      companyName: this.editForm.get(['companyName']).value,
      notes: this.editForm.get(['notes']).value,
      discountPercent: this.editForm.get(['discountPercent']).value,
      webpage: this.editForm.get(['webpage']).value,
      fax: this.editForm.get(['fax']).value,
      name: this.editForm.get(['name']).value,
      birthday: this.editForm.get(['birthday']).value,
      gender: this.editForm.get(['gender']).value,
      tel: this.editForm.get(['tel']).value,
      email: this.editForm.get(['email']).value,
      billingAddress: this.editForm.get(['billingAddress']).value,
      userId: this.editForm.get(['userId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomer>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
