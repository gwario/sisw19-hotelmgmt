import { Route } from '@angular/router';

import { RoomUtilizationComponent } from 'app/room-utilization/room-utilization.component';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

export const ROOM_UTILIZATION_ROUTE: Route = {
  path: 'room-utilization',
  component: RoomUtilizationComponent,
  data: {
    authorities: ['ROLE_USER'],
    pageTitle: 'hotelManagementApp.roomUtilization.title'
  },
  canActivate: [UserRouteAccessService]
};
