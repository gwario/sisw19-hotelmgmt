import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { RoomUtilizationComponent } from 'app/room-utilization/room-utilization.component';
import { ROOM_UTILIZATION_ROUTE } from 'app/room-utilization/room-utilization.route';
import { RoomStatusComponent } from './room-status/room-status.component';

@NgModule({
  imports: [HotelManagementSharedModule, RouterModule.forChild([ROOM_UTILIZATION_ROUTE])],
  declarations: [RoomUtilizationComponent, RoomStatusComponent]
})
export class HotelManagementRoomUtilizationModule {}
