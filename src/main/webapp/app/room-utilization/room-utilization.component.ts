import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RoomService } from 'app/entities/room/room.service';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IRoomBooked } from 'app/shared/model/room-booked.model';

@Component({
  selector: 'jhi-room-utilization',
  templateUrl: './room-utilization.component.html',
  styleUrls: ['./room-utilization.component.scss']
})
export class RoomUtilizationComponent implements OnInit {
  searchForm = this.fb.group({
    fromDate: [null, Validators.required],
    toDate: [null, Validators.required],
    minCapacity: [null, [Validators.required, Validators.min(1), Validators.max(3)]]
  });

  roomsFree: IRoomBooked[] = [];
  roomsBooked: IRoomBooked[] = [];

  searchSuccess = false;

  constructor(protected roomService: RoomService, private fb: FormBuilder) {}

  ngOnInit() {}

  search() {
    this.roomsFree = [];
    this.roomsBooked = [];
    this.searchSuccess = false;

    const from = moment(this.searchForm.get('fromDate').value, DATE_TIME_FORMAT).format('YYYY-MM-DD');
    const to = moment(this.searchForm.get('toDate').value, DATE_TIME_FORMAT).format('YYYY-MM-DD');
    const minCapacity = this.searchForm.get('minCapacity').value;
    this.roomService.findByFromToAndMinCapacity(from, to, minCapacity).subscribe(res => {
      res.body.forEach(roombooked => (roombooked.booked ? this.roomsBooked.push(roombooked) : this.roomsFree.push(roombooked)));
      this.searchSuccess = true;
    });
  }
}
