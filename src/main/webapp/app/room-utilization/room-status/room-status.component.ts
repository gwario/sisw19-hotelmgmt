import { Component, Input, OnInit } from '@angular/core';
import { IRoomBooked } from 'app/shared/model/room-booked.model';

@Component({
  selector: 'jhi-room-status',
  templateUrl: './room-status.component.html',
  styleUrls: ['./room-status.component.scss']
})
export class RoomStatusComponent implements OnInit {
  @Input()
  room: IRoomBooked;

  constructor() {}

  ngOnInit() {}
}
