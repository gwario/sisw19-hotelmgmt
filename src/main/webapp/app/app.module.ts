import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { HotelManagementSharedModule } from 'app/shared/shared.module';
import { HotelManagementCoreModule } from 'app/core/core.module';
import { HotelManagementAppRoutingModule } from './app-routing.module';
import { HotelManagementHomeModule } from './home/home.module';
import { HotelManagementEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { NavmenuComponent } from './layouts/navmenu/navmenu.component';
import { HotelManagementRoomUtilizationModule } from 'app/room-utilization/room-utilization.module';
import { HotelManagementSimpleAdvertisementModule } from './simple-advertisement/simple-advertisement.module';

@NgModule({
  imports: [
    BrowserModule,
    HotelManagementSharedModule,
    HotelManagementCoreModule,
    HotelManagementHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    HotelManagementEntityModule,
    HotelManagementRoomUtilizationModule,
    HotelManagementSimpleAdvertisementModule,
    HotelManagementAppRoutingModule
  ],
  declarations: [
    JhiMainComponent,
    NavbarComponent,
    ErrorComponent,
    PageRibbonComponent,
    ActiveMenuDirective,
    FooterComponent,
    NavmenuComponent
  ],
  bootstrap: [JhiMainComponent]
})
export class HotelManagementAppModule {}
