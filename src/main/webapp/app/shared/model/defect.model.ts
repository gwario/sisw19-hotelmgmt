import { DefectState } from 'app/shared/model/enumerations/defect-state.model';

export interface IDefect {
  id?: number;
  description?: string;
  defectState?: DefectState;
  discountPercent?: number;
  roomId?: number;
  roomNr?: string;
}

export class Defect implements IDefect {
  constructor(
    public id?: number,
    public description?: string,
    public defectState?: DefectState,
    public discountPercent?: number,
    public roomId?: number,
    public roomNr?: string
  ) {}
}
