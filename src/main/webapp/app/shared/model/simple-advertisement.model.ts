export interface ISimpleAdvertisement {
  subject?: string;
  body?: string;
}

export class SimpleAdvertisement implements ISimpleAdvertisement {
  constructor(public subject?: string, public body?: string) {}
}
