import { Moment } from 'moment';
import { IEmployee } from 'app/shared/model/employee.model';
import { IVacation } from 'app/shared/model/vacation.model';
import { EmployeeType } from 'app/shared/model/enumerations/employee-type.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';
import { IWorkload } from 'app/shared/model/workload.model';

export interface IEmployee {
  id?: number;
  employeeType?: EmployeeType;
  shortId?: string;
  insuranceNumber?: string;
  accountData?: string;
  contractType?: string;
  tasks?: string;
  name?: string;
  birthday?: Moment;
  gender?: Gender;
  tel?: string;
  email?: string;
  address?: string;
  userId?: number;
  userName?: string;
  userEmail?: string;
  employees?: IEmployee[];
  vacations?: IVacation[];
  workloads?: IWorkload[];
  bossName?: string;
  bossId?: number;
}

export class Employee implements IEmployee {
  constructor(
    public id?: number,
    public employeeType?: EmployeeType,
    public shortId?: string,
    public insuranceNumber?: string,
    public accountData?: string,
    public contractType?: string,
    public tasks?: string,
    public name?: string,
    public birthday?: Moment,
    public gender?: Gender,
    public tel?: string,
    public email?: string,
    public address?: string,
    public userId?: number,
    public userName?: string,
    public userEmail?: string,
    public employees?: IEmployee[],
    public vacations?: IVacation[],
    public workloads?: IWorkload[],
    public bossId?: number,
    public bossName?: string
  ) {}
}
