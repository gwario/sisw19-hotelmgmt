export interface InvoicePrice {
  price: number;
  discountPercent: number;
  note: string;
  roomNumber: string;
}
