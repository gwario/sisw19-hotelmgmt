import { IBooking } from 'app/shared/model/booking.model';
import { IRoom } from 'app/shared/model/room.model';

export interface IRoomOccupancy {
  id?: number;
  numberAdults?: number;
  numberChildren?: number;
  room?: IRoom;
}

export class RoomOccupancy implements IRoomOccupancy {
  constructor(public id?: number, public numberAdults?: number, public numberChildren?: number, public room?: IRoom) {}
}
