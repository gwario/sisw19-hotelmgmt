import { IRoom } from 'app/shared/model/room.model';

export interface IRoomBooked {
  room?: IRoom;
  booked?: boolean;
}

export class RoomBooked implements IRoomBooked {
  constructor(room?: IRoom, booked?: boolean) {}
}
