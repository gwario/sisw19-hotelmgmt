export const enum EmployeeType {
  RECEPTION = 'RECEPTION',
  KITCHEN = 'KITCHEN',
  SERVICE = 'SERVICE',
  CLEANING = 'CLEANING',
  OTHER = 'OTHER'
}
