export const enum DefectState {
  IN_PROGRESS = 'IN_PROGRESS',
  RESOLVED = 'RESOLVED',
  NEW = 'NEW'
}
