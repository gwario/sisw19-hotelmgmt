import { Moment } from 'moment';
import { IEmployee } from 'app/shared/model/employee.model';

export interface IWorkload {
  id?: number;
  startDate?: Moment;
  endDate?: Moment;
  breakIncluded: boolean;
  employee?: IEmployee;
}

export class Workload implements IWorkload {
  constructor(
    public id?: number,
    public startDate?: Moment,
    public endDate?: Moment,
    public breakIncluded: boolean = true,
    public employee?: IEmployee
  ) {}
}
