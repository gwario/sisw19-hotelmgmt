import { Moment } from 'moment';
import { VacationStatus } from 'app/shared/model/enumerations/vacation-status.model';
import { IEmployee } from 'app/shared/model/employee.model';

export interface IVacation {
  id?: number;
  startDate?: Moment;
  endDate?: Moment;
  status?: VacationStatus;
  employee?: IEmployee;
  absenceReasonReason?: string;
  absenceReasonId?: number;
}

export class Vacation implements IVacation {
  constructor(
    public id?: number,
    public startDate?: Moment,
    public endDate?: Moment,
    public status?: VacationStatus,
    public employee?: IEmployee,
    public absenceReasonReason?: string,
    public absenceReasonId?: number
  ) {}
}
