import { IVacation } from 'app/shared/model/vacation.model';

export interface IAbsenceReason {
  id?: number;
  reason?: string;
  vacations?: IVacation[];
}

export class AbsenceReason implements IAbsenceReason {
  constructor(public id?: number, public reason?: string, public vacations?: IVacation[]) {}
}
