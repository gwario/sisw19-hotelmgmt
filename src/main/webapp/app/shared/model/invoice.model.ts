import { Moment } from 'moment';
import { Gender } from 'app/shared/model/enumerations/gender.model';
import { State } from 'app/shared/model/enumerations/state.model';
import { InvoicePrice } from 'app/shared/model/invoice-price.model';
import { RoomReservationInformation } from 'app/shared/model/room-reservation-information';

export interface IInvoice {
  id?: number;
  hotelAddress?: string;
  customerAddress?: string;
  customerName?: string;
  customerGender?: Gender;
  invoiceDate?: Moment;
  startDate?: Moment;
  endDate?: Moment;
  durationNights?: number;
  invoiceState?: State;
  prices?: InvoicePrice[];
  rooms?: RoomReservationInformation[];
  discountPercent?: number;
}

export class Invoice implements IInvoice {
  constructor(
    public id?: number,
    public hotelAddress?: string,
    public customerAddress?: string,
    public customerName?: string,
    public customerGender?: Gender,
    public invoiceDate?: Moment,
    public startDate?: Moment,
    public endDate?: Moment,
    public durationNights?: number,
    public invoiceState?: State,
    public prices?: InvoicePrice[],
    public rooms?: RoomReservationInformation[],
    public discountPercent?: number
  ) {}
}
