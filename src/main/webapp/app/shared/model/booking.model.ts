import {Moment} from 'moment';
import {IInvoice} from 'app/shared/model/invoice.model';
import {IRoomOccupancy} from 'app/shared/model/room-occupancy.model';
import {State} from 'app/shared/model/enumerations/state.model';
import {ICustomer} from 'app/shared/model/customer.model';

export interface IBooking {
    id?: number;
    discountPercent?: number;
    price?: number;
    startDate?: Moment;
    endDate?: Moment;
    durationNights?: number;
    cancellationNoticeNights?: number;
    bookingState?: State;
    event?: string;
    invoices?: IInvoice[];
    customer?: ICustomer;
    rooms?: IRoomOccupancy[];
}

export class Booking implements IBooking {
    constructor(
        public id?: number,
        public discountPercent?: number,
        public price?: number,
        public startDate?: Moment,
        public endDate?: Moment,
        public durationNights?: number,
        public cancellationNoticeNights?: number,
        public bookingState?: State,
        public event?: string,
        public invoices?: IInvoice[],
        public customer?: ICustomer,
        public rooms?: IRoomOccupancy[]
    ) {
    }
}
