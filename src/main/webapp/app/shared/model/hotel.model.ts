import { IInvoice } from 'app/shared/model/invoice.model';

export interface IHotel {
  id?: number;
  hotelAddress?: string;
  defaultCancellationNoticeNights?: number;
  invoices?: IInvoice[];
}

export class Hotel implements IHotel {
  constructor(
    public id?: number,
    public hotelAddress?: string,
    public defaultCancellationNoticeNights?: number,
    public invoices?: IInvoice[]
  ) {}
}
