export interface RoomReservationInformation {
  roomNumber: string;
  numberAdults: number;
  numberChildren: number;
}
