import { Moment } from 'moment';
import { Gender } from 'app/shared/model/enumerations/gender.model';

export interface ICustomer {
  id?: number;
  companyName?: string;
  notes?: string;
  discountPercent?: number;
  webpage?: string;
  fax?: string;
  name?: string;
  birthday?: Moment;
  gender?: Gender;
  tel?: string;
  email?: string;
  billingAddress?: string;
  userId?: number;
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public companyName?: string,
    public notes?: string,
    public discountPercent?: number,
    public webpage?: string,
    public fax?: string,
    public name?: string,
    public birthday?: Moment,
    public gender?: Gender,
    public tel?: string,
    public email?: string,
    public billingAddress?: string,
    public userId?: number
  ) {}
}
