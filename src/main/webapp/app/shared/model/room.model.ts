import { IDefect } from 'app/shared/model/defect.model';

export interface IRoom {
  id?: number;
  number?: string;
  maxCapacity?: number;
  priceOneAdult?: number;
  priceTwoAdults?: number;
  priceThreeAdults?: number;
  priceOneAdultOneChild?: number;
  priceTwoAdultsOneChild?: number;
  priceOneAdultTwoChildren?: number;
  defects?: IDefect[];
}

export class Room implements IRoom {
  constructor(
    public id?: number,
    public number?: string,
    public maxCapacity?: number,
    public priceOneAdult?: number,
    public priceTwoAdults?: number,
    public priceThreeAdults?: number,
    public priceOneAdultOneChild?: number,
    public priceTwoAdultsOneChild?: number,
    public priceOneAdultTwoChildren?: number,
    public defects?: IDefect[]
  ) {}
}
