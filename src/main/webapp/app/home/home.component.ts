import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import * as jQuery from 'jquery';
import 'jquery-easing';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account;
  authSubscription: Subscription;
  modalRef: NgbModalRef;

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private eventManager: JhiEventManager
  ) {}

  ngOnInit() {
    this.accountService.identity().subscribe((account: Account) => {
      this.account = account;
    });
    this.registerAuthenticationSuccess();

    // Scroll to top button appear
    jQuery(document).on('scroll', e => {
      const scrollDistance = jQuery(e.currentTarget).scrollTop();
      if (scrollDistance > 100) {
        jQuery('.scroll-to-top').fadeIn();
      } else {
        jQuery('.scroll-to-top').fadeOut();
      }
    });

    // Enable map zooming with mouse scroll when the user clicks the map
    jQuery('.map').on('click', this.onMapClickHandler);
  }

  // Disable Google Maps scrolling
  // See http://stackoverflow.com/a/25904582/1607849
  // Disable scroll zooming and bind back the click event
  onMapMouseleaveHandler(event) {
    const that = jQuery(this);
    that.on('click', this.onMapClickHandler);
    that.off('mouseleave', this.onMapMouseleaveHandler);
    that.find('iframe').css('pointer-events', 'none');
  }

  onMapClickHandler(event) {
    const that = jQuery(this);
    // Disable the click handler until the user leaves the map area
    that.off('click', this.onMapClickHandler);
    // Enable scrolling zoom
    that.find('iframe').css('pointer-events', 'auto');
    // Handle the mouse leave event
    that.on('mouseleave', this.onMapMouseleaveHandler);
  }

  registerAuthenticationSuccess() {
    this.authSubscription = this.eventManager.subscribe('authenticationSuccess', message => {
      this.accountService.identity().subscribe(account => {
        this.account = account;
      });
    });
  }

  login() {
    this.modalRef = this.loginModalService.open();
  }

  ngOnDestroy() {
    if (this.authSubscription) {
      this.eventManager.destroy(this.authSubscription);
    }
  }
}
