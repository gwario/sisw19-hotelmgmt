import { Component, OnInit } from '@angular/core';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as jQuery from 'jquery';
import 'jquery-easing';
import { JhiLanguageService } from 'ng-jhipster';
import { SessionStorageService } from 'ngx-webstorage';
import { JhiLanguageHelper } from 'app/core/language/language.helper';

@Component({
  selector: 'jhi-navmenu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.scss']
})
export class NavmenuComponent implements OnInit {
  isMenuOpen = false;
  modalRef: NgbModalRef;
  languages: any[];

  constructor(
    private loginModalService: LoginModalService,
    private languageService: JhiLanguageService,
    private languageHelper: JhiLanguageHelper,
    private sessionStorage: SessionStorageService
  ) {}

  ngOnInit() {
    this.languages = this.languageHelper.getAll();

    // Smooth scrolling using jQuery easing
    jQuery('a.js-scroll-trigger[href*="#"]:not([href="#"])').on('click', e => {
      if (
        location.pathname.replace(/^\//, '') === (e.currentTarget as HTMLAnchorElement).pathname.replace(/^\//, '') &&
        location.hostname === (e.currentTarget as HTMLAnchorElement).hostname
      ) {
        let target = jQuery((e.currentTarget as HTMLAnchorElement).hash);
        target = target.length ? target : jQuery('[name=' + (e.currentTarget as HTMLAnchorElement).hash.slice(1) + ']');
        if (target.length) {
          jQuery('html, body').animate(
            {
              scrollTop: target.offset().top
            },
            1000,
            'easeInOutExpo'
          );
          return false;
        }
      }
    });
  }

  login() {
    this.modalRef = this.loginModalService.open();
  }

  changeLanguage(languageKey: string) {
    this.sessionStorage.store('locale', languageKey);
    this.languageService.changeLanguage(languageKey);
  }
}
