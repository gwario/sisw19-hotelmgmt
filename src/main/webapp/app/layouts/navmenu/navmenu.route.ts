import { Route } from '@angular/router';

import { NavmenuComponent } from './navmenu.component';

export const navmenuRoute: Route = {
  path: '',
  component: NavmenuComponent,
  outlet: 'navmenu'
};
