package com.sisw19.hotelmgnt.domain;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A RoomOccupancy.
 */
@Entity
@Table(name = "room_occupancy")
public class RoomOccupancy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Min(value = 1)
    @Max(value = 3)
    @Column(name = "number_adults", nullable = false)
    private Integer numberAdults;

    @NotNull
    @Min(value = 0)
    @Max(value = 2)
    @Column(name = "number_children", nullable = false)
    private Integer numberChildren;

    @NotNull
    @ManyToOne
//    @JsonIgnoreProperties("rooms")
    private Booking booking;

    @NotNull
    @ManyToOne
//    @JsonIgnoreProperties("roomOccupancies")
    private Room room;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoomOccupancy id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getNumberAdults() {
        return numberAdults;
    }

    public RoomOccupancy numberAdults(Integer numberAdults) {
        this.numberAdults = numberAdults;
        return this;
    }

    public void setNumberAdults(Integer numberAdults) {
        this.numberAdults = numberAdults;
    }

    public Integer getNumberChildren() {
        return numberChildren;
    }

    public RoomOccupancy numberChildren(Integer numberChildren) {
        this.numberChildren = numberChildren;
        return this;
    }

    public void setNumberChildren(Integer numberChildren) {
        this.numberChildren = numberChildren;
    }

    public Booking getBooking() {
        return booking;
    }

    public RoomOccupancy booking(Booking booking) {
        this.booking = booking;
        return this;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public Room getRoom() {
        return room;
    }

    public RoomOccupancy room(Room room) {
        this.room = room;
        return this;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RoomOccupancy)) {
            return false;
        }
        return id != null && id.equals(((RoomOccupancy) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RoomOccupancy{" +
            "id=" + getId() +
            ", numberAdults=" + getNumberAdults() +
            ", numberChildren=" + getNumberChildren() +
            "}";
    }
}
