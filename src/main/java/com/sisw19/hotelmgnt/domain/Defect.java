package com.sisw19.hotelmgnt.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sisw19.hotelmgnt.domain.enumeration.DefectState;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A Defect.
 */
@Entity
@Table(name = "defect")
public class Defect implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "defect_state", nullable = false)
    private DefectState defectState;

    @NotNull
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    @Column(name = "discount_percent", nullable = false)
    private Double discountPercent;

    @NotNull
    @ManyToOne
    @JsonIgnoreProperties("defects")
    private Room room;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public Defect description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DefectState getDefectState() {
        return defectState;
    }

    public Defect defectState(DefectState defectState) {
        this.defectState = defectState;
        return this;
    }

    public void setDefectState(DefectState defectState) {
        this.defectState = defectState;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public Defect discountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
        return this;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Room getRoom() {
        return room;
    }

    public Defect room(Room room) {
        this.room = room;
        return this;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Defect)) {
            return false;
        }
        return id != null && id.equals(((Defect) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Defect{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", defectState='" + getDefectState() + "'" +
            ", discountPercent=" + getDiscountPercent() +
            "}";
    }
}
