package com.sisw19.hotelmgnt.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.sisw19.hotelmgnt.domain.enumeration.Gender;

import com.sisw19.hotelmgnt.domain.enumeration.State;

/**
 * A Invoice.
 */
@Entity
@Table(name = "invoice")
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "hotel_address", nullable = false)
    private String hotelAddress;

    @NotNull
    @Column(name = "customer_address", nullable = false)
    private String customerAddress;

    @NotNull
    @Column(name = "customer_name", nullable = false)
    private String customerName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "customer_gender", nullable = false)
    private Gender customerGender;

    @NotNull
    @Column(name = "invoice_date", nullable = false)
    private ZonedDateTime invoiceDate;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private ZonedDateTime startDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private ZonedDateTime endDate;

    @NotNull
    @Column(name = "duration_nights", nullable = false)
    private Integer durationNights;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "invoice_state", nullable = false)
    private State invoiceState;

    @ElementCollection
    private List<InvoicePrice> prices = new ArrayList<>();

    @ElementCollection
    private List<RoomReservationInformation> rooms = new ArrayList<>();

    @NotNull
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    @Column(name = "discount_percent", nullable = false)
    private Double discountPercent;

    @ManyToOne
    @JsonIgnoreProperties("invoices")
    private Booking booking;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public Invoice hotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
        return this;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public Invoice customerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
        return this;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Invoice customerName(String customerName) {
        this.customerName = customerName;
        return this;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Gender getCustomerGender() {
        return customerGender;
    }

    public Invoice customerGender(Gender customerGender) {
        this.customerGender = customerGender;
        return this;
    }

    public void setCustomerGender(Gender customerGender) {
        this.customerGender = customerGender;
    }

    public ZonedDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public Invoice invoiceDate(ZonedDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    public void setInvoiceDate(ZonedDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public Invoice startDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public Invoice endDate(ZonedDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public Integer getDurationNights() {
        return durationNights;
    }

    public Invoice durationNights(Integer durationNights) {
        this.durationNights = durationNights;
        return this;
    }

    public void setDurationNights(Integer durationNights) {
        this.durationNights = durationNights;
    }

    public State getInvoiceState() {
        return invoiceState;
    }

    public Invoice invoiceState(State invoiceState) {
        this.invoiceState = invoiceState;
        return this;
    }

    public void setInvoiceState(State invoiceState) {
        this.invoiceState = invoiceState;
    }

    public Booking getBooking() {
        return booking;
    }

    public Invoice booking(Booking booking) {
        this.booking = booking;
        return this;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Invoice)) {
            return false;
        }
        return id != null && id.equals(((Invoice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Invoice{" +
            "id=" + getId() +
            ", hotelAddress='" + getHotelAddress() + "'" +
            ", customerAddress='" + getCustomerAddress() + "'" +
            ", customerName='" + getCustomerName() + "'" +
            ", customerGender='" + getCustomerGender() + "'" +
            ", invoiceDate='" + getInvoiceDate() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", durationNights=" + getDurationNights() +
            ", invoiceState='" + getInvoiceState() + "'" +
            ", discountPercent=" + getDiscountPercent() +
            "}";
    }

    public static Invoice createInvoice() {
        Invoice invoice = new Invoice();
        invoice.invoiceDate = ZonedDateTime.now();
        invoice.invoiceState = State.ACTIVE;
        return invoice;
    }

    public List<InvoicePrice> getPrices() {
        return prices;
    }

    public void setPrices(List<InvoicePrice> prices) {
        this.prices = prices;
    }

    public List<RoomReservationInformation> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomReservationInformation> rooms) {
        this.rooms = rooms;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }
}
