package com.sisw19.hotelmgnt.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.sisw19.hotelmgnt.domain.enumeration.EmployeeType;

import com.sisw19.hotelmgnt.domain.enumeration.Gender;

/**
 * A Employee.
 */
@Entity
@Table(name = "employee")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "employee_type", nullable = false)
    private EmployeeType employeeType;

    @NotNull
    @Column(name = "short_id", nullable = false, unique = true)
    private String shortId;

    @NotNull
    @Column(name = "insurance_number", nullable = false)
    private String insuranceNumber;

    @NotNull
    @Column(name = "account_data", nullable = false)
    private String accountData;

    @NotNull
    @Column(name = "contract_type", nullable = false)
    private String contractType;

    @NotNull
    @Column(name = "tasks", nullable = false)
    private String tasks;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "birthday", nullable = false)
    private LocalDate birthday;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "gender", nullable = false)
    private Gender gender;

    @NotNull
    @Column(name = "tel", nullable = false)
    private String tel;

    @Column(name = "email", nullable = true)
    private String email;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "boss")
    private Set<Employee> employees = new HashSet<>();

    @OneToMany(mappedBy = "employee")
    private Set<Vacation> vacations = new HashSet<>();

    @OneToMany(mappedBy = "employee")
    private Set<Workload> workloads = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("employees")
    private Employee boss;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmployeeType getEmployeeType() {
        return employeeType;
    }

    public Employee employeeType(EmployeeType employeeType) {
        this.employeeType = employeeType;
        return this;
    }

    public void setEmployeeType(EmployeeType employeeType) {
        this.employeeType = employeeType;
    }

    public String getShortId() {
        return shortId;
    }

    public Employee shortId(String shortId) {
        this.shortId = shortId;
        return this;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public Employee insuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
        return this;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getAccountData() {
        return accountData;
    }

    public Employee accountData(String accountData) {
        this.accountData = accountData;
        return this;
    }

    public void setAccountData(String accountData) {
        this.accountData = accountData;
    }

    public String getContractType() {
        return contractType;
    }

    public Employee contractType(String contractType) {
        this.contractType = contractType;
        return this;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getTasks() {
        return tasks;
    }

    public Employee tasks(String tasks) {
        this.tasks = tasks;
        return this;
    }

    public void setTasks(String tasks) {
        this.tasks = tasks;
    }

    public String getName() {
        return name;
    }

    public Employee name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Employee birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public Employee gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getTel() {
        return tel;
    }

    public Employee tel(String tel) {
        this.tel = tel;
        return this;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public Employee email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public Employee address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getUser() {
        return user;
    }

    public Employee user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public Employee employees(Set<Employee> employees) {
        this.employees = employees;
        return this;
    }

    public Employee addEmployee(Employee employee) {
        this.employees.add(employee);
        employee.setBoss(this);
        return this;
    }

    public Employee removeEmployee(Employee employee) {
        this.employees.remove(employee);
        employee.setBoss(null);
        return this;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public Set<Vacation> getVacations() {
        return vacations;
    }

    public Employee vacations(Set<Vacation> vacations) {
        this.vacations = vacations;
        return this;
    }

    public Employee addVacations(Vacation vacation) {
        this.vacations.add(vacation);
        vacation.setEmployee(this);
        return this;
    }

    public Employee removeVacations(Vacation vacation) {
        this.vacations.remove(vacation);
        vacation.setEmployee(null);
        return this;
    }

    public void setVacations(Set<Vacation> vacations) {
        this.vacations = vacations;
    }

    public Set<Workload> getWorkloads() {
        return workloads;
    }

    public Employee workloads(Set<Workload> workloads) {
        this.workloads = workloads;
        return this;
    }

    public Employee addWorkloads(Workload workload) {
        this.workloads.add(workload);
        workload.setEmployee(this);
        return this;
    }

    public Employee removeWorkloads(Workload workload) {
        this.workloads.remove(workload);
        workload.setEmployee(null);
        return this;
    }

    public void setWorkloads(Set<Workload> workloads) {
        this.workloads = workloads;
    }

    public Employee getBoss() {
        return boss;
    }

    public Employee boss(Employee employee) {
        this.boss = employee;
        return this;
    }

    public void setBoss(Employee employee) {
        this.boss = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employee)) {
            return false;
        }
        return id != null && id.equals(((Employee) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Employee{" +
            "id=" + getId() +
            ", employeeType='" + getEmployeeType() + "'" +
            ", shortId='" + getShortId() + "'" +
            ", insuranceNumber='" + getInsuranceNumber() + "'" +
            ", accountData='" + getAccountData() + "'" +
            ", contractType='" + getContractType() + "'" +
            ", tasks='" + getTasks() + "'" +
            ", name='" + getName() + "'" +
            ", birthday='" + getBirthday() + "'" +
            ", gender='" + getGender() + "'" +
            ", tel='" + getTel() + "'" +
            ", email='" + getEmail() + "'" +
            ", address='" + getAddress() + "'" +
            "}";
    }
}
