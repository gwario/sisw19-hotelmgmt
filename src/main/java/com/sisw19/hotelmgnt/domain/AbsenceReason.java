package com.sisw19.hotelmgnt.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A AbsenceReason.
 */
@Entity
@Table(name = "absence_reason")
public class AbsenceReason implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "reason", nullable = false)
    private String reason;

    @OneToMany(mappedBy = "absenceReason")
    private Set<Vacation> vacations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public AbsenceReason reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Set<Vacation> getVacations() {
        return vacations;
    }

    public AbsenceReason vacations(Set<Vacation> vacations) {
        this.vacations = vacations;
        return this;
    }

    public AbsenceReason addVacations(Vacation vacation) {
        this.vacations.add(vacation);
        vacation.setAbsenceReason(this);
        return this;
    }

    public AbsenceReason removeVacations(Vacation vacation) {
        this.vacations.remove(vacation);
        vacation.setAbsenceReason(null);
        return this;
    }

    public void setVacations(Set<Vacation> vacations) {
        this.vacations = vacations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbsenceReason)) {
            return false;
        }
        return id != null && id.equals(((AbsenceReason) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AbsenceReason{" +
            "id=" + getId() +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
