package com.sisw19.hotelmgnt.domain;

import com.sisw19.hotelmgnt.domain.enumeration.State;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * A Booking.
 */
@Entity
@Table(name = "booking")
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    @Column(name = "discount_percent", nullable = false)
    private Double discountPercent;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "price", precision = 21, scale = 2, nullable = false)
    private BigDecimal price;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private ZonedDateTime startDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private ZonedDateTime endDate;

    @NotNull
    @Min(value = 0)
    @Column(name = "cancellation_notice_nights", nullable = false)
    private Integer cancellationNoticeNights;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "booking_state", nullable = false)
    private State bookingState;

    @Column(name = "event")
    private String event;

    @OneToMany(mappedBy = "booking", cascade = CascadeType.ALL)
    private List<Invoice> invoices = new ArrayList<>();

    @ManyToOne
    @NotNull
    private Customer customer;

    @OneToMany(mappedBy = "booking", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<RoomOccupancy> rooms = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public Booking discountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
        return this;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Booking price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public Booking startDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public Booking endDate(ZonedDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public Integer getCancellationNoticeNights() {
        return cancellationNoticeNights;
    }

    public Booking cancellationNoticeNights(Integer cancellationNoticeNights) {
        this.cancellationNoticeNights = cancellationNoticeNights;
        return this;
    }

    public void setCancellationNoticeNights(Integer cancellationNoticeNights) {
        this.cancellationNoticeNights = cancellationNoticeNights;
    }

    public State getBookingState() {
        return bookingState;
    }

    public Booking bookingState(State bookingState) {
        this.bookingState = bookingState;
        return this;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public void setBookingState(State bookingState) {
        this.bookingState = bookingState;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Booking customer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Set<RoomOccupancy> getRooms() {
        return rooms;
    }

    public Booking rooms(Set<RoomOccupancy> roomOccupancies) {
        this.rooms = roomOccupancies;
        return this;
    }

    public Booking addRoom(RoomOccupancy roomOccupancy) {
        this.rooms.add(roomOccupancy);
        roomOccupancy.setBooking(this);
        return this;
    }

    public Booking addRooms(Set<RoomOccupancy> roomOccupancies) {
        roomOccupancies.forEach(this::addRoom);
        return this;
    }

    public void setRooms(Set<RoomOccupancy> roomOccupancies) {
        this.rooms = roomOccupancies;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Booking)) {
            return false;
        }
        return id != null && id.equals(((Booking) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Booking{" +
            "id=" + getId() +
            ", discountPercent=" + getDiscountPercent() +
            ", price=" + getPrice() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", cancellationNoticeNights=" + getCancellationNoticeNights() +
            ", bookingState='" + getBookingState() + "'" +
            "}";
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }
}
