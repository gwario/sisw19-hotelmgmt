package com.sisw19.hotelmgnt.domain.enumeration;

/**
 * The DefectState enumeration.
 */
public enum DefectState {
    IN_PROGRESS, RESOLVED, NEW
}
