package com.sisw19.hotelmgnt.domain.enumeration;

/**
 * The EmployeeType enumeration.
 */
public enum EmployeeType {
    RECEPTION, KITCHEN, SERVICE, CLEANING, OTHER
}
