package com.sisw19.hotelmgnt.domain.enumeration;

/**
 * The VacationStatus enumeration.
 */
public enum VacationStatus {
    REQUESTED, ACCEPTED, DECLINED
}
