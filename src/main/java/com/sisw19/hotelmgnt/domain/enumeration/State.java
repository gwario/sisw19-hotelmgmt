package com.sisw19.hotelmgnt.domain.enumeration;

/**
 * The State enumeration.
 */
public enum State {
    ACTIVE, CANCELLED
}
