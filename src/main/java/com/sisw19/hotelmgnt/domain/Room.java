package com.sisw19.hotelmgnt.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A Room.
 */
@Entity
@Table(name = "room")
public class Room implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Pattern(regexp = "^[0-9]*$")
    @Column(name = "number", nullable = false, unique = true)
    private String number;

    @NotNull
    @Min(value = 1)
    @Max(value = 3)
    @Column(name = "max_capacity", nullable = false)
    private Integer maxCapacity;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "price_one_adult", precision = 21, scale = 2, nullable = false)
    private BigDecimal priceOneAdult;

    @DecimalMin(value = "0")
    @Column(name = "price_two_adults", precision = 21, scale = 2)
    private BigDecimal priceTwoAdults;

    @DecimalMin(value = "0")
    @Column(name = "price_three_adults", precision = 21, scale = 2)
    private BigDecimal priceThreeAdults;

    @DecimalMin(value = "0")
    @Column(name = "price_one_adult_one_child", precision = 21, scale = 2)
    private BigDecimal priceOneAdultOneChild;

    @DecimalMin(value = "0")
    @Column(name = "price_two_adults_one_child", precision = 21, scale = 2)
    private BigDecimal priceTwoAdultsOneChild;

    @DecimalMin(value = "0")
    @Column(name = "price_one_adult_two_children", precision = 21, scale = 2)
    private BigDecimal priceOneAdultTwoChildren;

    @OneToMany(mappedBy = "room")
    private Set<Defect> defects = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public Room number(String number) {
        this.number = number;
        return this;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getMaxCapacity() {
        return maxCapacity;
    }

    public Room maxCapacity(Integer maxCapacity) {
        this.maxCapacity = maxCapacity;
        return this;
    }

    public void setMaxCapacity(Integer maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public BigDecimal getPriceOneAdult() {
        return priceOneAdult;
    }

    public Room priceOneAdult(BigDecimal priceOneAdult) {
        this.priceOneAdult = priceOneAdult;
        return this;
    }

    public void setPriceOneAdult(BigDecimal priceOneAdult) {
        this.priceOneAdult = priceOneAdult;
    }

    public BigDecimal getPriceTwoAdults() {
        return priceTwoAdults;
    }

    public Room priceTwoAdults(BigDecimal priceTwoAdults) {
        this.priceTwoAdults = priceTwoAdults;
        return this;
    }

    public void setPriceTwoAdults(BigDecimal priceTwoAdults) {
        this.priceTwoAdults = priceTwoAdults;
    }

    public BigDecimal getPriceThreeAdults() {
        return priceThreeAdults;
    }

    public Room priceThreeAdults(BigDecimal priceThreeAdults) {
        this.priceThreeAdults = priceThreeAdults;
        return this;
    }

    public void setPriceThreeAdults(BigDecimal priceThreeAdults) {
        this.priceThreeAdults = priceThreeAdults;
    }

    public BigDecimal getPriceOneAdultOneChild() {
        return priceOneAdultOneChild;
    }

    public Room priceOneAdultOneChild(BigDecimal priceOneAdultOneChild) {
        this.priceOneAdultOneChild = priceOneAdultOneChild;
        return this;
    }

    public void setPriceOneAdultOneChild(BigDecimal priceOneAdultOneChild) {
        this.priceOneAdultOneChild = priceOneAdultOneChild;
    }

    public BigDecimal getPriceTwoAdultsOneChild() {
        return priceTwoAdultsOneChild;
    }

    public Room priceTwoAdultsOneChild(BigDecimal priceTwoAdultsOneChild) {
        this.priceTwoAdultsOneChild = priceTwoAdultsOneChild;
        return this;
    }

    public void setPriceTwoAdultsOneChild(BigDecimal priceTwoAdultsOneChild) {
        this.priceTwoAdultsOneChild = priceTwoAdultsOneChild;
    }

    public BigDecimal getPriceOneAdultTwoChildren() {
        return priceOneAdultTwoChildren;
    }

    public Room priceOneAdultTwoChildren(BigDecimal priceOneAdultTwoChildren) {
        this.priceOneAdultTwoChildren = priceOneAdultTwoChildren;
        return this;
    }

    public void setPriceOneAdultTwoChildren(BigDecimal priceOneAdultTwoChildren) {
        this.priceOneAdultTwoChildren = priceOneAdultTwoChildren;
    }

    public Set<Defect> getDefects() {
        return defects;
    }

    public Room defects(Set<Defect> defects) {
        this.defects = defects;
        return this;
    }

    public Room addDefects(Defect defect) {
        this.defects.add(defect);
        defect.setRoom(this);
        return this;
    }

    public Room removeDefects(Defect defect) {
        this.defects.remove(defect);
        defect.setRoom(null);
        return this;
    }

    public void setDefects(Set<Defect> defects) {
        this.defects = defects;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Room)) {
            return false;
        }
        return id != null && id.equals(((Room) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Room{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", maxCapacity=" + getMaxCapacity() +
            ", priceOneAdult=" + getPriceOneAdult() +
            ", priceTwoAdults=" + getPriceTwoAdults() +
            ", priceThreeAdults=" + getPriceThreeAdults() +
            ", priceOneAdultOneChild=" + getPriceOneAdultOneChild() +
            ", priceTwoAdultsOneChild=" + getPriceTwoAdultsOneChild() +
            ", priceOneAdultTwoChildren=" + getPriceOneAdultTwoChildren() +
            "}";
    }
}
