package com.sisw19.hotelmgnt.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Hotel.
 */
@Entity
@Table(name = "hotel")
public class Hotel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "hotel_address", nullable = false)
    private String hotelAddress;

    @NotNull
    @Min(value = 0)
    @Column(name = "default_cancellation_notice_nights", nullable = false)
    private Integer defaultCancellationNoticeNights;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public Hotel hotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
        return this;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public Integer getDefaultCancellationNoticeNights() {
        return defaultCancellationNoticeNights;
    }

    public Hotel defaultCancellationNoticeNights(Integer defaultCancellationNoticeNights) {
        this.defaultCancellationNoticeNights = defaultCancellationNoticeNights;
        return this;
    }

    public void setDefaultCancellationNoticeNights(Integer defaultCancellationNoticeNights) {
        this.defaultCancellationNoticeNights = defaultCancellationNoticeNights;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Hotel)) {
            return false;
        }
        return id != null && id.equals(((Hotel) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Hotel{" +
            "id=" + getId() +
            ", hotelAddress='" + getHotelAddress() + "'" +
            ", defaultCancellationNoticeNights=" + getDefaultCancellationNoticeNights() +
            "}";
    }
}
