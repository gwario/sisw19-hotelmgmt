package com.sisw19.hotelmgnt.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class RoomReservationInformation implements Serializable {
    private String roomNumber;
    private Integer numberAdults;
    private Integer numberChildren;

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getNumberAdults() {
        return numberAdults;
    }

    public void setNumberAdults(Integer numberAdults) {
        this.numberAdults = numberAdults;
    }

    public Integer getNumberChildren() {
        return numberChildren;
    }

    public void setNumberChildren(Integer numberChildren) {
        this.numberChildren = numberChildren;
    }
}
