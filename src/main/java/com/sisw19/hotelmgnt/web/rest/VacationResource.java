package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.service.VacationService;
import com.sisw19.hotelmgnt.service.exception.ValidationException;
import com.sisw19.hotelmgnt.web.rest.errors.BadRequestAlertException;
import com.sisw19.hotelmgnt.service.dto.VacationDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.sisw19.hotelmgnt.domain.Vacation}.
 */
@RestController
@RequestMapping("/api")
public class VacationResource {

    private final Logger log = LoggerFactory.getLogger(VacationResource.class);

    private static final String ENTITY_NAME = "vacation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VacationService vacationService;

    public VacationResource(VacationService vacationService) {
        this.vacationService = vacationService;
    }

    @ExceptionHandler
    void handleValidationException(ValidationException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    /**
     * {@code POST  /vacations} : Create a new vacation.
     *
     * @param vacationDTO the vacationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vacationDTO, or with status {@code 400 (Bad Request)} if the vacation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vacations")
    public ResponseEntity<VacationDTO> createVacation(@Valid @RequestBody VacationDTO vacationDTO) throws URISyntaxException {
        log.debug("REST request to save Vacation : {}", vacationDTO);
        if (vacationDTO.getId() != null) {
            throw new BadRequestAlertException("A new vacation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VacationDTO result = vacationService.save(vacationDTO);
        return ResponseEntity.created(new URI("/api/vacations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /vacations} : Request vacation as an employee.
     *
     * @param vacationDTO the vacationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vacationDTO, or with status {@code 400 (Bad Request)} if the vacation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vacations/request")
    public ResponseEntity<VacationDTO> requestVacation(@RequestBody VacationDTO vacationDTO) throws URISyntaxException {
        log.debug("REST request to save Vacation : {}", vacationDTO);
        if (vacationDTO.getId() != null) {
            throw new BadRequestAlertException("A new vacation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VacationDTO result = vacationService.requestVacation(vacationDTO);
        return ResponseEntity.created(new URI("/api/vacations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /vacations} : Request vacation as an employee.
     *
     * @param id the vacationDTO to accept.
     * @return the {@link ResponseEntity} with status {@code 200 (ok)} and with body the new vacationDTO, or with status {@code 400 (Bad Request)} if the vacation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vacations/accept")
    public ResponseEntity<VacationDTO> acceptVacation(@RequestBody Long id) throws URISyntaxException {
        log.debug("REST request to accept Vacation : {}", id);
        VacationDTO result = vacationService.acceptVacation(id);
        return ResponseEntity.accepted().body(result);
    }

    /**
     * {@code POST  /vacations} : Request vacation as an employee.
     *
     * @param id the vacationDTO to reject.
     * @return the {@link ResponseEntity} with status {@code 200 (ok)} and with body the new vacationDTO, or with status {@code 400 (Bad Request)} if the vacation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vacations/reject")
    public ResponseEntity<VacationDTO> rejectVacation(@RequestBody Long id) throws URISyntaxException {
        log.debug("REST request to accept Vacation : {}", id);
        VacationDTO result = vacationService.rejectVacation(id);
        return ResponseEntity.accepted().body(result);
    }

    /**
     * {@code PUT  /vacations} : Updates an existing vacation.
     *
     * @param vacationDTO the vacationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vacationDTO,
     * or with status {@code 400 (Bad Request)} if the vacationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vacationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vacations")
    public ResponseEntity<VacationDTO> updateVacation(@Valid @RequestBody VacationDTO vacationDTO) throws URISyntaxException {
        log.debug("REST request to update Vacation : {}", vacationDTO);
        if (vacationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VacationDTO result = vacationService.save(vacationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, vacationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /vacations} : get all the vacations.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vacations in body.
     */
    @GetMapping("/vacations/boss")
    public ResponseEntity<List<VacationDTO>> getAllVacationsAsBoss(Pageable pageable) {
        log.debug("REST request to get a page of Vacations");
        Page<VacationDTO> page = vacationService.findAllByBoss(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /vacations} : get all the vacations.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vacations in body.
     */
    @GetMapping("/vacations")
    public ResponseEntity<List<VacationDTO>> getAllVacations(Pageable pageable) {
        log.debug("REST request to get a page of Vacations");
        Page<VacationDTO> page = vacationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /vacations/:id} : get the "id" vacation.
     *
     * @param id the id of the vacationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vacationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vacations/{id}")
    public ResponseEntity<VacationDTO> getVacation(@PathVariable Long id) {
        log.debug("REST request to get Vacation : {}", id);
        Optional<VacationDTO> vacationDTO = vacationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(vacationDTO);
    }

    /**
     * {@code DELETE  /vacations/:id} : delete the "id" vacation.
     *
     * @param id the id of the vacationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vacations/{id}")
    public ResponseEntity<Void> deleteVacation(@PathVariable Long id) {
        log.debug("REST request to delete Vacation : {}", id);
        vacationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
