package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.service.CustomerService;
import com.sisw19.hotelmgnt.service.MailService;
import com.sisw19.hotelmgnt.service.dto.CustomerDTO;
import com.sisw19.hotelmgnt.service.dto.SimpleAdvertisementDTO;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * REST controller for managing {@link com.sisw19.hotelmgnt.service.dto.SimpleAdvertisementDTO}.
 */
@RestController
@RequestMapping("/api")
public class SimpleAdvertisementResource {

    private final Logger log = LoggerFactory.getLogger(SimpleAdvertisementResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MailService mailService;
    private final CustomerService customerService;

    public SimpleAdvertisementResource(MailService mailService, CustomerService customerService) {
        this.mailService = mailService;
        this.customerService = customerService;
    }

    /**
     * {@code POST  /simple-advertisement} : Send a simple advertisement.
     *
     * @param simpleAdvertisementDTO the simpleAdvertisementDTO to send.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new simpleAdvertisementDTO.
     */
    @PostMapping("/simple-advertisement")
    public ResponseEntity<Void> sendSimpleAdvertisement(@Valid @RequestBody SimpleAdvertisementDTO simpleAdvertisementDTO) {
        log.debug("REST request to send SimpleAdvertisement : {}", simpleAdvertisementDTO);

        // get all users
        Page<CustomerDTO> customers = customerService.findAll(Pageable.unpaged());

        // iterate over all (eligible) users and sendEmail
        customers.forEach(customerDTO -> {
            mailService.sendEmail(customerDTO.getEmail(), simpleAdvertisementDTO.getSubject(), simpleAdvertisementDTO.getBody(), false, false);
        });
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "hotelManagementApp.simpleAdvertisement.sent", simpleAdvertisementDTO.getSubject())).build();
    }
}
