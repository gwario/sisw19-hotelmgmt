package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.service.AbsenceReasonService;
import com.sisw19.hotelmgnt.web.rest.errors.BadRequestAlertException;
import com.sisw19.hotelmgnt.service.dto.AbsenceReasonDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.sisw19.hotelmgnt.domain.AbsenceReason}.
 */
@RestController
@RequestMapping("/api")
public class AbsenceReasonResource {

    private final Logger log = LoggerFactory.getLogger(AbsenceReasonResource.class);

    private static final String ENTITY_NAME = "absenceReason";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AbsenceReasonService absenceReasonService;

    public AbsenceReasonResource(AbsenceReasonService absenceReasonService) {
        this.absenceReasonService = absenceReasonService;
    }

    /**
     * {@code POST  /absence-reasons} : Create a new absenceReason.
     *
     * @param absenceReasonDTO the absenceReasonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new absenceReasonDTO, or with status {@code 400 (Bad Request)} if the absenceReason has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/absence-reasons")
    public ResponseEntity<AbsenceReasonDTO> createAbsenceReason(@Valid @RequestBody AbsenceReasonDTO absenceReasonDTO) throws URISyntaxException {
        log.debug("REST request to save AbsenceReason : {}", absenceReasonDTO);
        if (absenceReasonDTO.getId() != null) {
            throw new BadRequestAlertException("A new absenceReason cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AbsenceReasonDTO result = absenceReasonService.save(absenceReasonDTO);
        return ResponseEntity.created(new URI("/api/absence-reasons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /absence-reasons} : Updates an existing absenceReason.
     *
     * @param absenceReasonDTO the absenceReasonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated absenceReasonDTO,
     * or with status {@code 400 (Bad Request)} if the absenceReasonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the absenceReasonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/absence-reasons")
    public ResponseEntity<AbsenceReasonDTO> updateAbsenceReason(@Valid @RequestBody AbsenceReasonDTO absenceReasonDTO) throws URISyntaxException {
        log.debug("REST request to update AbsenceReason : {}", absenceReasonDTO);
        if (absenceReasonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AbsenceReasonDTO result = absenceReasonService.save(absenceReasonDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, absenceReasonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /absence-reasons} : get all the absenceReasons.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of absenceReasons in body.
     */
    @GetMapping("/absence-reasons")
    public ResponseEntity<List<AbsenceReasonDTO>> getAllAbsenceReasons(Pageable pageable) {
        log.debug("REST request to get a page of AbsenceReasons");
        Page<AbsenceReasonDTO> page = absenceReasonService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /absence-reasons/:id} : get the "id" absenceReason.
     *
     * @param id the id of the absenceReasonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the absenceReasonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/absence-reasons/{id}")
    public ResponseEntity<AbsenceReasonDTO> getAbsenceReason(@PathVariable Long id) {
        log.debug("REST request to get AbsenceReason : {}", id);
        Optional<AbsenceReasonDTO> absenceReasonDTO = absenceReasonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(absenceReasonDTO);
    }

    /**
     * {@code DELETE  /absence-reasons/:id} : delete the "id" absenceReason.
     *
     * @param id the id of the absenceReasonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/absence-reasons/{id}")
    public ResponseEntity<Void> deleteAbsenceReason(@PathVariable Long id) {
        log.debug("REST request to delete AbsenceReason : {}", id);
        absenceReasonService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
