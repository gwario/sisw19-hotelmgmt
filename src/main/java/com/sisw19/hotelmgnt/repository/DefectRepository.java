package com.sisw19.hotelmgnt.repository;
import com.sisw19.hotelmgnt.domain.Defect;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Defect entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DefectRepository extends JpaRepository<Defect, Long>, JpaSpecificationExecutor<Defect> {

}
