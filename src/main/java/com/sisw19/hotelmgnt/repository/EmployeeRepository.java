package com.sisw19.hotelmgnt.repository;
import com.sisw19.hotelmgnt.domain.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Employee entity.
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {

    @Query("select employee from Employee employee where employee.user.id =:user_id")
    Optional<Employee> findOneByUserId(@Param("user_id") Long user_id);


    @Query("select employee from Employee employee where employee.boss.user.id =:user_id")
    Page<Employee> findAllByBoss(@Param("user_id") Long user_id, Pageable pageRequest);
}
