package com.sisw19.hotelmgnt.repository;

import com.sisw19.hotelmgnt.domain.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Room entity.
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long>, JpaSpecificationExecutor<Room> {

    //TODO query?
    List<Room> findAllByMaxCapacityGreaterThanEqual(Integer minCapacity);

    //TODO query?
    Room findOneByNumber(String number);
}
