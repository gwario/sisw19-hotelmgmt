package com.sisw19.hotelmgnt.repository;

import com.sisw19.hotelmgnt.domain.Booking;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Booking entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingRepository extends JpaRepository<Booking, Long>, JpaSpecificationExecutor<Booking> {

    @Query("SELECT distinct(b.event) FROM Booking b " +
        "WHERE b.event is not null AND b.event <> '' AND b.bookingState='ACTIVE' AND b.endDate >= CURRENT_TIME " +
        "ORDER BY b.event ASC")
    List<String> findExistingEvents();

    Page<Booking> findAllByInvoicesNotEmpty(Pageable pageable);

    Optional<Booking> findByInvoices_Id(Long id);

}
