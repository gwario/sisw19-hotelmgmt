package com.sisw19.hotelmgnt.repository;
import com.sisw19.hotelmgnt.domain.Vacation;
import com.sisw19.hotelmgnt.domain.enumeration.VacationStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the Vacation entity.
 */
@Repository
public interface VacationRepository extends JpaRepository<Vacation, Long> {


    @Query("select vacation from Vacation vacation where vacation.employee.boss.user.id =:user_id and vacation.status =:status")
    Page<Vacation> findAllByBossAndStatus(@Param("user_id") Long user_id, @Param("status") VacationStatus state, Pageable pageRequest);


    @Query("select vacation from Vacation vacation where vacation.employee.boss.user.id =:user_id or vacation.employee.user.id = :user_id")
    Page<Vacation> findAllByBossOrMine(@Param("user_id") Long user_id, Pageable pageRequest);

    /**
     * Get colliding workloads if any
     * Collisions are calculated according to the start and end time, and the start or endtime corrected by the
     * mandatory break between shifts
     *
     * @return
     */
    @Query("select vacation from Vacation vacation where vacation.employee.id =:employee_id and (vacation.startDate <= :start_date and vacation.endDate > :start_date_including_break) or (vacation.startDate > :start_date and vacation.startDate < :end_date_including_break ) order by vacation.startDate")
    List<Vacation> getCollisions(@Param("employee_id") Long employee_id, @Param("start_date") ZonedDateTime dateStart, @Param("end_date") ZonedDateTime dateEnd, @Param("start_date_including_break") ZonedDateTime start_date_including_break, @Param("end_date_including_break") ZonedDateTime end_date_including_break);


}
