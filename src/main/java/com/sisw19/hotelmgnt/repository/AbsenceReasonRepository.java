package com.sisw19.hotelmgnt.repository;

import com.sisw19.hotelmgnt.domain.AbsenceReason;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AbsenceReason entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AbsenceReasonRepository extends JpaRepository<AbsenceReason, Long> {

}
