package com.sisw19.hotelmgnt.repository;
import com.sisw19.hotelmgnt.domain.Room;
import com.sisw19.hotelmgnt.domain.RoomOccupancy;
import com.sisw19.hotelmgnt.domain.enumeration.State;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the RoomOccupancy entity.
 */
@Repository
public interface RoomOccupancyRepository extends JpaRepository<RoomOccupancy, Long> {

    @Query("select distinct room from RoomOccupancy room_occupancy join room_occupancy.booking booking join room_occupancy.room room where room.maxCapacity >= :minCapacity and booking.bookingState = :bookingState and ((booking.startDate >= :fromDate and booking.startDate <= :toDate) or (booking.startDate <= :fromDate and booking.endDate >= :fromDate) or (booking.startDate >= :fromDate and booking.endDate <= :toDate))")
    List<Room> findAllOccupiedRoomsInRangeWithState(@Param("minCapacity") Integer minCapacity, @Param("fromDate") ZonedDateTime fromDate, @Param("toDate") ZonedDateTime toDate, @Param("bookingState") State bookingState);

    @Query("select distinct room from RoomOccupancy room_occupancy join room_occupancy.booking booking join room_occupancy.room room where booking.id != :thisBookingId and room.maxCapacity >= :minCapacity and booking.bookingState = :bookingState and ((booking.startDate >= :fromDate and booking.startDate <= :toDate) or (booking.startDate <= :fromDate and booking.endDate >= :fromDate) or (booking.startDate >= :fromDate and booking.endDate <= :toDate))")
    List<Room> findAllOccupiedRoomsInRangeWithStateExcludeBooking(@Param("minCapacity") Integer minCapacity, @Param("fromDate") ZonedDateTime fromDate, @Param("toDate") ZonedDateTime toDate, @Param("bookingState") State bookingState, @Param("thisBookingId") Long thisBookingId);
}
