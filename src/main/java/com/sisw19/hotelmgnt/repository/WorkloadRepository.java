package com.sisw19.hotelmgnt.repository;

import com.sisw19.hotelmgnt.domain.Workload;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;


/**
 * Spring Data  repository for the Workload entity.
 */
@Repository
public interface WorkloadRepository extends JpaRepository<Workload, Long> {
    /**
     * Get colliding workloads if any
     * Collisions are calculated according to the start and end time, and the start or endtime corrected by the
     * mandatory break between shifts
     *
     * @return
     */
    @Query("select workload from Workload workload where workload.employee.id =:employee_id and (workload.startDate <= :start_date and workload.endDate > :start_date_including_break) or (workload.startDate > :start_date and workload.startDate < :end_date_including_break ) order by workload.startDate")
    List<Workload> getCollisions(@Param("employee_id") Long employee_id, @Param("start_date") ZonedDateTime dateStart, @Param("end_date") ZonedDateTime dateEnd, @Param("start_date_including_break") ZonedDateTime start_date_including_break, @Param("end_date_including_break") ZonedDateTime end_date_including_break);

    /**
     * Get workloads by day of week
     *
     * @return
     */
    @Query("select workload from Workload workload where workload.employee.id =:employee_id and workload.endDate > :last_year and (EXTRACT(WEEK FROM workload.startDate) = :day_of_week or EXTRACT(WEEK FROM workload.startDate) = :day_of_week)")
    List<Workload> getByDayOfWeek(@Param("employee_id") Long employee_id, @Param("day_of_week") Integer dayOfWeek, @Param("last_year") ZonedDateTime lastYear);


    /**
     * Find all workloads for a given boss
     *
     * @param user_id
     * @param pageRequest
     * @return
     */
    @Query("select workload from Workload workload where workload.employee.boss.user.id =:user_id or workload.employee.user.id = :user_id")
    Page<Workload> findAllByBoss(@Param("user_id") Long user_id, Pageable pageRequest);

    /**
     * Find all workloads for a given boss
     *
     * @param user_id
     * @param pageRequest
     * @return
     */
    @Query("select workload from Workload workload where workload.employee.boss.user.id =:user_id or workload.employee.user.id = :user_id")
    Page<Workload> findAllByBossOrSelf(@Param("user_id") Long user_id, Pageable pageRequest);
}
