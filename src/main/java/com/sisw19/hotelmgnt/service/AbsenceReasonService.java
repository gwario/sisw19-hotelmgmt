package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.domain.AbsenceReason;
import com.sisw19.hotelmgnt.repository.AbsenceReasonRepository;
import com.sisw19.hotelmgnt.service.dto.AbsenceReasonDTO;
import com.sisw19.hotelmgnt.service.mapper.AbsenceReasonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AbsenceReason}.
 */
@Service
@Transactional
public class AbsenceReasonService {

    private final Logger log = LoggerFactory.getLogger(AbsenceReasonService.class);

    private final AbsenceReasonRepository absenceReasonRepository;

    private final AbsenceReasonMapper absenceReasonMapper;

    public AbsenceReasonService(AbsenceReasonRepository absenceReasonRepository, AbsenceReasonMapper absenceReasonMapper) {
        this.absenceReasonRepository = absenceReasonRepository;
        this.absenceReasonMapper = absenceReasonMapper;
    }

    /**
     * Save a absenceReason.
     *
     * @param absenceReasonDTO the entity to save.
     * @return the persisted entity.
     */
    public AbsenceReasonDTO save(AbsenceReasonDTO absenceReasonDTO) {
        log.debug("Request to save AbsenceReason : {}", absenceReasonDTO);
        AbsenceReason absenceReason = absenceReasonMapper.toEntity(absenceReasonDTO);
        absenceReason = absenceReasonRepository.save(absenceReason);
        return absenceReasonMapper.toDto(absenceReason);
    }

    /**
     * Get all the absenceReasons.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AbsenceReasonDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AbsenceReasons");
        return absenceReasonRepository.findAll(pageable)
            .map(absenceReasonMapper::toDto);
    }


    /**
     * Get one absenceReason by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AbsenceReasonDTO> findOne(Long id) {
        log.debug("Request to get AbsenceReason : {}", id);
        return absenceReasonRepository.findById(id)
            .map(absenceReasonMapper::toDto);
    }

    /**
     * Delete the absenceReason by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AbsenceReason : {}", id);
        absenceReasonRepository.deleteById(id);
    }
}
