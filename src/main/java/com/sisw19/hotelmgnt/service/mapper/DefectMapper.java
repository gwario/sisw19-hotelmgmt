package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.*;
import com.sisw19.hotelmgnt.service.dto.DefectDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Defect} and its DTO {@link DefectDTO}.
 */
@Mapper(componentModel = "spring", uses = {RoomMapper.class})
public interface DefectMapper extends EntityMapper<DefectDTO, Defect> {

    @Mapping(source = "room.id", target = "roomId")
    @Mapping(source = "room.number", target = "roomNr")
    DefectDTO toDto(Defect defect);

    @Mapping(source = "roomId", target = "room")
    Defect toEntity(DefectDTO defectDTO);

    default Defect fromId(Long id) {
        if (id == null) {
            return null;
        }
        Defect defect = new Defect();
        defect.setId(id);
        return defect;
    }
}
