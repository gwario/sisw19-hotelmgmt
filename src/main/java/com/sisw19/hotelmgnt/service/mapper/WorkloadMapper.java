package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.Workload;
import com.sisw19.hotelmgnt.service.dto.WorkloadDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Workload} and its DTO {@link WorkloadDTO}.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class})
public interface WorkloadMapper extends EntityMapper<WorkloadDTO, Workload> {

    WorkloadDTO toDto(Workload workload);

    Workload toEntity(WorkloadDTO workloadDTO);

    default Workload fromId(Long id) {
        if (id == null) {
            return null;
        }
        Workload workload = new Workload();
        workload.setId(id);
        return workload;
    }
}
