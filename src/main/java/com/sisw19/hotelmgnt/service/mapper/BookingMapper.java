package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.Booking;
import com.sisw19.hotelmgnt.service.dto.BookingDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Booking} and its DTO {@link BookingDTO}.
 */
@Mapper(componentModel = "spring", uses = {CustomerMapper.class, RoomOccupancyMapper.class, InvoiceMapper.class})
public interface BookingMapper extends EntityMapper<BookingDTO, Booking> {

    BookingDTO toDto(Booking booking);

    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "rooms", ignore = true)
    Booking toEntity(BookingDTO bookingDTO);

    default Booking fromId(Long id) {
        if (id == null) {
            return null;
        }
        Booking booking = new Booking();
        booking.setId(id);
        return booking;
    }
}
