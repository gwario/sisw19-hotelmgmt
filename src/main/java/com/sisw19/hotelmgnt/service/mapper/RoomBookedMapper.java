package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.Room;
import com.sisw19.hotelmgnt.service.dto.RoomBookedDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RoomMapper.class})
public interface RoomBookedMapper {
    RoomBookedDto toDto(Room room, boolean booked);
}
