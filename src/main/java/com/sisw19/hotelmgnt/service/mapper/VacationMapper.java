package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.*;
import com.sisw19.hotelmgnt.service.dto.VacationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Vacation} and its DTO {@link VacationDTO}.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class, AbsenceReasonMapper.class})
public interface VacationMapper extends EntityMapper<VacationDTO, Vacation> {

    @Mapping(source = "absenceReason.id", target = "absenceReasonId")
    @Mapping(source = "absenceReason.reason", target = "absenceReasonReason")
    VacationDTO toDto(Vacation vacation);

    @Mapping(source = "absenceReasonId", target = "absenceReason")
    Vacation toEntity(VacationDTO vacationDTO);

    default Vacation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Vacation vacation = new Vacation();
        vacation.setId(id);
        return vacation;
    }
}
