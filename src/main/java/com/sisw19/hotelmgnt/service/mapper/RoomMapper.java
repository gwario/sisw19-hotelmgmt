package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.*;
import com.sisw19.hotelmgnt.service.dto.RoomDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Room} and its DTO {@link RoomDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RoomMapper extends EntityMapper<RoomDTO, Room> {


    @Mapping(target = "defects", ignore = true)
    @Mapping(target = "removeDefects", ignore = true)
    Room toEntity(RoomDTO roomDTO);

    default Room fromId(Long id) {
        if (id == null) {
            return null;
        }
        Room room = new Room();
        room.setId(id);
        return room;
    }
}
