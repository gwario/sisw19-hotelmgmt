package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.*;
import com.sisw19.hotelmgnt.service.dto.HotelDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Hotel} and its DTO {@link HotelDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface HotelMapper extends EntityMapper<HotelDTO, Hotel> {

    Hotel toEntity(HotelDTO hotelDTO);

    default Hotel fromId(Long id) {
        if (id == null) {
            return null;
        }
        Hotel hotel = new Hotel();
        hotel.setId(id);
        return hotel;
    }
}
