package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.Booking;
import com.sisw19.hotelmgnt.domain.RoomOccupancy;
import com.sisw19.hotelmgnt.service.dto.BookingDTO;
import com.sisw19.hotelmgnt.service.dto.RoomOccupancyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Booking} and its DTO {@link BookingDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RoomOccupancyMapper extends EntityMapper<RoomOccupancyDTO, RoomOccupancy> {
    
    RoomOccupancyDTO toDto(RoomOccupancy roomOccupancy);

    RoomOccupancy toEntity(RoomOccupancyDTO roomOccupancyDTO);

    default RoomOccupancy fromId(Long id) {
        if (id == null) {
            return null;
        }
        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setId(id);
        return roomOccupancy;
    }
}
