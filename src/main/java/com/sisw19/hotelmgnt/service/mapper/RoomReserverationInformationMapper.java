package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.RoomOccupancy;
import com.sisw19.hotelmgnt.domain.RoomReservationInformation;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface RoomReserverationInformationMapper {

    @Mapping(source = "room.number", target = "roomNumber")
    RoomReservationInformation roomOccupationToReservationInformation(RoomOccupancy roomOccupancy);

    @InheritConfiguration
    Collection<RoomReservationInformation> roomOccupationsToReservationInformations(Collection<RoomOccupancy> roomOccupancy);
}
