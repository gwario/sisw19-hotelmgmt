package com.sisw19.hotelmgnt.service.mapper;

import com.sisw19.hotelmgnt.domain.*;
import com.sisw19.hotelmgnt.service.dto.AbsenceReasonDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AbsenceReason} and its DTO {@link AbsenceReasonDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AbsenceReasonMapper extends EntityMapper<AbsenceReasonDTO, AbsenceReason> {


    @Mapping(target = "vacations", ignore = true)
    @Mapping(target = "removeVacations", ignore = true)
    AbsenceReason toEntity(AbsenceReasonDTO absenceReasonDTO);

    default AbsenceReason fromId(Long id) {
        if (id == null) {
            return null;
        }
        AbsenceReason absenceReason = new AbsenceReason();
        absenceReason.setId(id);
        return absenceReason;
    }
}
