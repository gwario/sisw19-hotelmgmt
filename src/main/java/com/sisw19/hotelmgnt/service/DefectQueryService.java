package com.sisw19.hotelmgnt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.sisw19.hotelmgnt.domain.Defect;
import com.sisw19.hotelmgnt.domain.*; // for static metamodels
import com.sisw19.hotelmgnt.repository.DefectRepository;
import com.sisw19.hotelmgnt.service.dto.DefectCriteria;
import com.sisw19.hotelmgnt.service.dto.DefectDTO;
import com.sisw19.hotelmgnt.service.mapper.DefectMapper;

/**
 * Service for executing complex queries for {@link Defect} entities in the database.
 * The main input is a {@link DefectCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DefectDTO} or a {@link Page} of {@link DefectDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DefectQueryService extends QueryService<Defect> {

    private final Logger log = LoggerFactory.getLogger(DefectQueryService.class);

    private final DefectRepository defectRepository;

    private final DefectMapper defectMapper;

    public DefectQueryService(DefectRepository defectRepository, DefectMapper defectMapper) {
        this.defectRepository = defectRepository;
        this.defectMapper = defectMapper;
    }

    /**
     * Return a {@link List} of {@link DefectDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DefectDTO> findByCriteria(DefectCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Defect> specification = createSpecification(criteria);
        return defectMapper.toDto(defectRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link DefectDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DefectDTO> findByCriteria(DefectCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Defect> specification = createSpecification(criteria);
        return defectRepository.findAll(specification, page)
            .map(defectMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DefectCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Defect> specification = createSpecification(criteria);
        return defectRepository.count(specification);
    }

    /**
     * Function to convert {@link DefectCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Defect> createSpecification(DefectCriteria criteria) {
        Specification<Defect> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Defect_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Defect_.description));
            }
            if (criteria.getDefectState() != null) {
                specification = specification.and(buildSpecification(criteria.getDefectState(), Defect_.defectState));
            }
            if (criteria.getDiscountPercent() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDiscountPercent(), Defect_.discountPercent));
            }
            if (criteria.getRoomId() != null) {
                specification = specification.and(buildSpecification(criteria.getRoomId(),
                    root -> root.join(Defect_.room, JoinType.LEFT).get(Room_.id)));
            }
        }
        return specification;
    }
}
