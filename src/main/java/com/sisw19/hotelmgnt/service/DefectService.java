package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.domain.Defect;
import com.sisw19.hotelmgnt.repository.DefectRepository;
import com.sisw19.hotelmgnt.service.dto.DefectDTO;
import com.sisw19.hotelmgnt.service.mapper.DefectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Defect}.
 */
@Service
@Transactional
public class DefectService {

    private final Logger log = LoggerFactory.getLogger(DefectService.class);

    private final DefectRepository defectRepository;

    private final DefectMapper defectMapper;

    public DefectService(DefectRepository defectRepository, DefectMapper defectMapper) {
        this.defectRepository = defectRepository;
        this.defectMapper = defectMapper;
    }

    /**
     * Save a defect.
     *
     * @param defectDTO the entity to save.
     * @return the persisted entity.
     */
    public DefectDTO save(DefectDTO defectDTO) {
        log.debug("Request to save Defect : {}", defectDTO);
        Defect defect = defectMapper.toEntity(defectDTO);
        defect = defectRepository.save(defect);
        return defectMapper.toDto(defect);
    }

    /**
     * Get all the defects.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DefectDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Defects");
        return defectRepository.findAll(pageable)
            .map(defectMapper::toDto);
    }


    /**
     * Get one defect by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DefectDTO> findOne(Long id) {
        log.debug("Request to get Defect : {}", id);
        return defectRepository.findById(id)
            .map(defectMapper::toDto);
    }

    /**
     * Delete the defect by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Defect : {}", id);
        defectRepository.deleteById(id);
    }
}
