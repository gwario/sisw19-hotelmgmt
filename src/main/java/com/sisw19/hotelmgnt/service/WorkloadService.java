package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.domain.Employee;
import com.sisw19.hotelmgnt.domain.User;
import com.sisw19.hotelmgnt.domain.Vacation;
import com.sisw19.hotelmgnt.domain.Workload;
import com.sisw19.hotelmgnt.repository.EmployeeRepository;
import com.sisw19.hotelmgnt.repository.UserRepository;
import com.sisw19.hotelmgnt.repository.VacationRepository;
import com.sisw19.hotelmgnt.repository.WorkloadRepository;
import com.sisw19.hotelmgnt.security.AuthoritiesConstants;
import com.sisw19.hotelmgnt.security.SecurityUtils;
import com.sisw19.hotelmgnt.service.dto.WorkloadDTO;
import com.sisw19.hotelmgnt.service.mapper.WorkloadMapper;
import com.sisw19.hotelmgnt.service.exception.ValidationException;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Workload}.
 */
@Service
@Transactional
public class WorkloadService {

    private final Logger log = LoggerFactory.getLogger(WorkloadService.class);

    private final WorkloadRepository workloadRepository;
    private final VacationRepository vacationRepository;

    private final EmployeeRepository employeeRepository;

    private final WorkloadMapper workloadMapper;

    private final UserRepository userRepository;

    public WorkloadService(WorkloadRepository workloadRepository, VacationRepository vacationRepository, WorkloadMapper workloadMapper, EmployeeRepository employeeRepository, UserRepository userRepository) {
        this.workloadRepository = workloadRepository;
        this.vacationRepository = vacationRepository;
        this.workloadMapper = workloadMapper;
        this.employeeRepository = employeeRepository;
        this.userRepository = userRepository;
    }

    /**
     * An employee is sending a workload request, which will be validated by his/her boss
     *
     * @param workloadDTO the entity to save.
     * @return the persisted entity.
     */
    public WorkloadDTO requestWorkload(WorkloadDTO workloadDTO) {
        return this.save(workloadDTO);
    }



    /**
     * Check if a user has permissions to accept or reject a workload request.
     * @param user
     * @param workload
     * @return
     */
    private boolean isValidBoss(User user, Workload workload){
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return true;
        }
        log.debug(workload.getEmployee().toString());
        if (workload.getEmployee().getBoss() != null) {

            if (workload.getEmployee().getBoss().getUser().getId() == user.getId()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Save a workload.
     *
     * @param workloadDTO the entity to save.
     * @return the persisted entity.
     */
    public WorkloadDTO save(WorkloadDTO workloadDTO) {
        log.debug("Employee Workload Request: {}", workloadDTO);
        Workload workload = workloadMapper.toEntity(workloadDTO);

        Employee employee = employeeRepository.getOne(workloadDTO.getEmployee().getId());
        workload.setEmployee(employee);

        User user = this.getCurrentUser();
        if (!this.isValidBoss(user, workload)) {
            throw new ValidationException("You are not allowed to assign work to this user.", "workload", "bad_user");
        }
        if (workload.getStartDate().isBefore(ZonedDateTime.now())) {
            throw new ValidationException("The start date of the workload must be in the future.", "workload", "bad_date");
        }
        log.debug(workload.getStartDate().toString() + " is after " + ZonedDateTime.now().toString());

        if (workload.getEndDate().isBefore(workload.getStartDate())) {
            throw new ValidationException("The end date of your workload must be after the begin of your workload.", "workload", "bad_date");
        }
        log.debug(workload.getStartDate().toString() + " is before " + workload.getEndDate().toString());

        long work_hours = Duration.between(workload.getStartDate(), workload.getEndDate()).toMinutes();
        long break_length = 30;

        work_hours -= break_length;
        if (work_hours > 60 * 12) {
            throw new ValidationException("You are not allowed to assign work longer than 10 hours.", "workload", "bad_time");
        }

        List<Workload> collistions = workloadRepository.getCollisions(workload.getEmployee().getId(), workload.getStartDate(), workload.getEndDate(), workload.getStartDate().minusHours(11), workload.getEndDate().plusHours(11));
        List<Vacation> collistions_vacation = vacationRepository.getCollisions(workload.getEmployee().getId(), workload.getStartDate(), workload.getEndDate(), workload.getStartDate().minusHours(11), workload.getEndDate().plusHours(11));

        if (!collistions.isEmpty()) {
            throw new ValidationException("This workload is breaking the governmental rules. Look for the colliding workloads", "workload", "rule_broken");
        }
        if (!collistions_vacation.isEmpty()) {
            throw new ValidationException("This workload collides with the vacation of the employee.", "workload", "rule_broken");
        }

        workload = workloadRepository.save(workload);
        return workloadMapper.toDto(workload);
    }

    /**
     * Get all the workloads.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<WorkloadDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Workloads");
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN))
            return workloadRepository.findAllByBoss(getCurrentUser().getId(), pageable).map(workloadMapper::toDto);
        return workloadRepository.findAll(pageable)
            .map(workloadMapper::toDto);
    }

    /**
     * Get all by Boss.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<WorkloadDTO> findAllByBoss(Pageable pageable) {
        log.debug("Request to get all Workloads by Boss");
        User user = this.getCurrentUser();
        log.debug("user id {}", user.getId());

        return workloadRepository.findAllByBoss(user.getId(), pageable)
            .map(workloadMapper::toDto);
    }


    /**
     * Get all by Boss.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<WorkloadDTO> findAllByBossOrSelf(Pageable pageable) {
        log.debug("Request to get all Workloads by Boss");
        User user = this.getCurrentUser();
        log.debug("user id {}", user.getId());

        return workloadRepository.findAllByBossOrSelf(user.getId(), pageable)
            .map(workloadMapper::toDto);
    }

    /**
     * Get one workload by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<WorkloadDTO> findOne(Long id) {
        log.debug("Request to get Workload : {}", id);
        return workloadRepository.findById(id)
            .map(workloadMapper::toDto);
    }

    /**
     * Delete the workload by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Workload : {}", id);
        Workload workload = workloadRepository.getOne(id);
        if (!isValidBoss(this.getCurrentUser(), workload)) {
            throw new ValidationException("You are not allowed to manage work for this user.", "workload", "bad_user");
        }
        workloadRepository.deleteById(id);
    }

    private User getCurrentUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findOneByLogin(authentication.getName()).orElse(null);

        if (user == null) {
            throw new ValidationException("Cannot find the user for current request", "workload", "no_user");
        }
        return user;
    }
}
