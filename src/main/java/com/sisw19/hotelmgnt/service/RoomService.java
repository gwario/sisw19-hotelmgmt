package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.domain.Room;
import com.sisw19.hotelmgnt.domain.enumeration.State;
import com.sisw19.hotelmgnt.repository.RoomOccupancyRepository;
import com.sisw19.hotelmgnt.repository.RoomRepository;
import com.sisw19.hotelmgnt.service.dto.RoomBookedDto;
import com.sisw19.hotelmgnt.service.dto.RoomDTO;
import com.sisw19.hotelmgnt.service.mapper.RoomBookedMapper;
import com.sisw19.hotelmgnt.service.mapper.RoomMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Room}.
 */
@Service
@Transactional
public class RoomService {

    private final Logger log = LoggerFactory.getLogger(RoomService.class);

    private final RoomRepository roomRepository;
    private final RoomOccupancyRepository roomOccupancyRepository;

    private final RoomMapper roomMapper;
    private final RoomBookedMapper roomBookedMapper;

    public RoomService(RoomRepository roomRepository, RoomOccupancyRepository roomOccupancyRepository, RoomMapper roomMapper, RoomBookedMapper roomBookedMapper) {
        this.roomRepository = roomRepository;
        this.roomOccupancyRepository = roomOccupancyRepository;
        this.roomMapper = roomMapper;
        this.roomBookedMapper = roomBookedMapper;
    }

    /**
     * Save a room.
     *
     * @param roomDTO the entity to save.
     * @return the persisted entity.
     */
    public RoomDTO save(RoomDTO roomDTO) {
        log.debug("Request to save Room : {}", roomDTO);
        Room room = roomMapper.toEntity(roomDTO);
        room = roomRepository.save(room);
        return roomMapper.toDto(room);
    }

    /**
     * Get all the rooms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<RoomDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Rooms");
        return roomRepository.findAll(pageable)
            .map(roomMapper::toDto);
    }

    public List<RoomBookedDto> getRoomOccupationBetweenFromToAndMinCapacity(Date from, Date to, Integer minCapacity) {
        List<Room> allRooms = roomRepository.findAllByMaxCapacityGreaterThanEqual(minCapacity);
        List<Room> occupiedRooms = getAllRoomsBetweenFromToAndMinCapacityAndBookingStateActive(from, to, minCapacity);

        List<RoomBookedDto> roomBooked = new ArrayList<>();
        for (Room room : allRooms) {
            boolean booked = false;
            for (Room occupied : occupiedRooms) {
                if (room.getId().equals(occupied.getId())) {
                    roomBooked.add(roomBookedMapper.toDto(room, true));
                    booked = true;
                    break;
                }
            }
            if (!booked) {
                roomBooked.add(roomBookedMapper.toDto(room, false));
            }
        }
        return roomBooked;
    }

    private List<Room> getAllRoomsBetweenFromToAndMinCapacityAndBookingStateActive(Date from, Date to, Integer minCapacity) {
        ZonedDateTime zonedFrom = ZonedDateTime.ofInstant(from.toInstant(), ZoneId.systemDefault());
        ZonedDateTime zonedTo = ZonedDateTime.ofInstant(to.toInstant(), ZoneId.systemDefault());
        return roomOccupancyRepository.findAllOccupiedRoomsInRangeWithState(minCapacity, zonedFrom, zonedTo, State.ACTIVE);
    }

    /**
     * Get one room by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RoomDTO> findOne(Long id) {
        log.debug("Request to get Room : {}", id);
        return roomRepository.findById(id)
            .map(roomMapper::toDto);
    }

    /**
     * Delete the room by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Room : {}", id);
        roomRepository.deleteById(id);
    }

    public Room getRoomByNumber(String number) {
        return this.roomRepository.findOneByNumber(number);
    }
}
