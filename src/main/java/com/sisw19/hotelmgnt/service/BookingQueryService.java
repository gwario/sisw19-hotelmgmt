package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.domain.Booking;
import com.sisw19.hotelmgnt.domain.Booking_;
import com.sisw19.hotelmgnt.domain.Customer;
import com.sisw19.hotelmgnt.repository.BookingRepository;
import com.sisw19.hotelmgnt.service.dto.BookingCriteria;
import com.sisw19.hotelmgnt.service.dto.BookingDTO;
import com.sisw19.hotelmgnt.service.mapper.BookingMapper;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(readOnly = true)
public class BookingQueryService extends QueryService<Booking> {

    private final Logger log = LoggerFactory.getLogger(BookingQueryService.class);

    private final BookingRepository bookingRepository;

    private final BookingMapper bookingMapper;

    public BookingQueryService(BookingRepository bookingRepository, BookingMapper bookingMapper) {
        this.bookingRepository = bookingRepository;
        this.bookingMapper = bookingMapper;
    }

    @Transactional(readOnly = true)
    public Page<BookingDTO> findByCriteria(BookingCriteria criteria, Long customerId, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Booking> specification = createSpecification(criteria, customerId);
        return bookingRepository.findAll(specification, page)
            .map(bookingMapper::toDto);
    }

    protected Specification<Booking> createSpecification(BookingCriteria criteria, Long customerId) {
        Specification<Booking> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Booking_.id));
            }
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), Booking_.startDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), Booking_.endDate));
            }
            if (criteria.getEvent() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEvent(), Booking_.event));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildSpecification(criteria.getState(), Booking_.bookingState));
            }

            if (customerId != null) {
                Filter<Customer> customerFilter = new Filter<>();
                customerFilter.setEquals(new Customer().id(customerId));
                customerFilter.setSpecified(true);
                specification = specification.and(buildSpecification(customerFilter, Booking_.customer));
            }
        }
        return specification;
    }
}
