package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.domain.*;
import com.sisw19.hotelmgnt.domain.enumeration.DefectState;
import com.sisw19.hotelmgnt.domain.enumeration.State;
import com.sisw19.hotelmgnt.repository.*;
import com.sisw19.hotelmgnt.service.dto.BookingDTO;
import com.sisw19.hotelmgnt.service.exception.ValidationException;
import com.sisw19.hotelmgnt.service.mapper.BookingMapper;
import com.sisw19.hotelmgnt.service.mapper.RoomReserverationInformationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Period;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Booking}.
 */
@Service
@Transactional
@Validated
public class BookingService {

    private final Logger log = LoggerFactory.getLogger(BookingService.class);

    private final BookingRepository bookingRepository;
    private final InvoiceRepository invoiceRepository;
    private final CustomerRepository customerRepository;
    private final RoomOccupancyRepository roomOccupancyRepository;
    private final RoomRepository roomRepository;
    private final HotelRepository hotelRepository;

    private final BookingMapper bookingMapper;
    private final RoomReserverationInformationMapper roomReserverationInformationMapper;

    public BookingService(BookingRepository bookingRepository,
                          CustomerRepository customerRepository,
                          RoomOccupancyRepository roomOccupancyRepository,
                          InvoiceRepository invoiceRepository,
                          RoomRepository roomRepository,
                          HotelRepository hotelRepository,
                          BookingMapper bookingMapper,
                          RoomReserverationInformationMapper roomReserverationInformationMapper) {
        this.bookingRepository = bookingRepository;
        this.invoiceRepository = invoiceRepository;
        this.customerRepository = customerRepository;
        this.roomOccupancyRepository = roomOccupancyRepository;
        this.roomRepository = roomRepository;
        this.hotelRepository = hotelRepository;
        this.bookingMapper = bookingMapper;
        this.roomReserverationInformationMapper = roomReserverationInformationMapper;
    }

    /**
     * Returns the current price of the booking.
     * The price is calculated as follows:
     *
     * Discounts can come from the customer, from defects on rooms and via the reservation (discount code).
     * First the price for each room is multiplied by the booking duration, then the sum of the discounts from defects is calculated and subtracted.
     * The results over all rooms are added and the customer discount is subtracted. Then the booking discount is subtracted.
     * @param booking
     * @return
     */
    private BigDecimal getPrice(Booking booking) {
        List<BillPosition> bill = getDetailedPrice(booking);
        return bill.get(bill.size() - 1).value;
    }

    /**
     * Returns the current price of the booking.
     * The price is calculated as follows:
     *
     * Discounts can come from the customer, from defects on rooms and via the reservation (discount code).
     * First the price for each room is multiplied by the booking duration, then the sum of the discounts from defects is calculated and subtracted.
     * The results over all rooms are added and the customer discount is subtracted. Then the booking discount is subtracted.
     * @param booking
     * @return list of bill positions. The last item contains the overall costs/the price.
     */
    private List<BillPosition> getDetailedPrice(Booking booking) {

        List<BillPosition> bill = new ArrayList<>();

        long bookingDurationDays = Duration.between(booking.getEndDate(), booking.getStartDate()).abs().toDays();

        BigDecimal roomsPrice = booking.getRooms().stream()
            .map(roomOccupancy -> {
                BigDecimal price = getPriceForOccupancy(roomOccupancy);
                price = price.multiply(BigDecimal.valueOf(bookingDurationDays));
                bill.add(new BillPosition(roomOccupancy, BillPosition.PositionKey.RoomCost, price));

                BigDecimal discountDefectsPercent = getDiscountPercentForDefects(roomOccupancy.getRoom());
                BigDecimal discountDefectsAbsolute = getAbsoluteDiscount(price, discountDefectsPercent);
                bill.add(new BillPosition(roomOccupancy, BillPosition.PositionKey.DiscountDefects, discountDefectsAbsolute.negate(), discountDefectsPercent));

                price = price.subtract(discountDefectsAbsolute);
                return price;
            })
            .reduce(BigDecimal.ZERO, BigDecimal::add);

        bill.add(new BillPosition(BillPosition.PositionKey.SumIntermediate, roomsPrice));

        BigDecimal discountCustomerPercent = BigDecimal.valueOf(booking.getCustomer().getDiscountPercent());
        BigDecimal discountCustomerAbsolute = getAbsoluteDiscount(roomsPrice, discountCustomerPercent);
        bill.add(new BillPosition(BillPosition.PositionKey.DiscountCustomer, discountCustomerAbsolute.negate(), discountCustomerPercent));

        BigDecimal roomsPriceWithCustomerDiscount = roomsPrice.subtract(discountCustomerAbsolute);
        bill.add(new BillPosition(BillPosition.PositionKey.SumIntermediate, roomsPriceWithCustomerDiscount));


        BigDecimal discountBookingPercent = BigDecimal.valueOf(booking.getDiscountPercent());
        BigDecimal discountBookingAbsolute = getAbsoluteDiscount(roomsPriceWithCustomerDiscount, discountBookingPercent);
        bill.add(new BillPosition(BillPosition.PositionKey.DiscountBooking, discountBookingAbsolute.negate(), discountBookingPercent));

        BigDecimal roomsPriceWithCustomerAndBookingDiscount = roomsPriceWithCustomerDiscount.subtract(discountBookingAbsolute);
        bill.add(new BillPosition(BillPosition.PositionKey.SumFinal, roomsPriceWithCustomerAndBookingDiscount));

        return bill;
    }

    /**
     * Returns the current price of the booking.
     * The price is calculated as follows:
     *
     * Discounts can come from the customer, from defects on rooms and via the reservation (discount code).
     * First the price for each room is multiplied by the booking duration, then the sum of the discounts from defects is calculated and subtracted.
     * The results over all rooms are added and the customer discount is subtracted. Then the booking discount is subtracted.
     *
     * Uses {@link BookingService#getPrice(Booking)}
     * @param bookingDTO the booking
     * @return
     */
    private BigDecimal getPrice(BookingDTO bookingDTO) {
            return getPrice(bookingMapper.toEntity(bookingDTO));
    }

    /**
     * Returns the absolute value of a discount.
     * @param price
     * @param discountPercent
     * @return
     */
    private BigDecimal getAbsoluteDiscount(BigDecimal price, BigDecimal discountPercent) {
        return price.multiply(discountPercent).divide(BigDecimal.valueOf(100));
    }


    /**
     * Returns the sum of all discounts of the defects (with {@link com.sisw19.hotelmgnt.domain.enumeration.DefectState#NEW} and {@link com.sisw19.hotelmgnt.domain.enumeration.DefectState#IN_PROGRESS) of the room.
     *
     * @param room
     * @return the descount in percent
     */
    private BigDecimal getDiscountPercentForDefects(Room room) {
        return BigDecimal.valueOf(room.getDefects().stream()
            .filter(defect -> defect.getDefectState() == DefectState.IN_PROGRESS || defect.getDefectState() == DefectState.NEW)
            .map(Defect::getDiscountPercent)
            .reduce(0.0, Double::sum));
    }

    /**
     * Returns the actual price of the room depending on the occupancy.
     *
     * @param roomOccupancy
     * @return
     */
    private BigDecimal getPriceForOccupancy(RoomOccupancy roomOccupancy) {

        Integer numberAdults = roomOccupancy.getNumberAdults();
        Integer numberChildren = roomOccupancy.getNumberChildren();
        Room room = roomOccupancy.getRoom();

        //System.out.println(String.format("===Occupancy Ad:%d Ch:%d================", numberAdults, numberChildren));

        switch (numberAdults) {
            case 1:
                switch (numberChildren) {
                    case 0:
                        //System.out.println(String.format("===Room price getPriceOneAdult: %f================", room.getPriceOneAdult()));
                        return room.getPriceOneAdult();
                    case 1:
                        //System.out.println(String.format("===Room price getPriceOneAdultOneChild: %f================", room.getPriceOneAdultOneChild()));
                        return room.getPriceOneAdultOneChild();
                    case 2:
                        //System.out.println(String.format("===Room price getPriceOneAdultTwoChildren: %f================", room.getPriceOneAdultTwoChildren()));
                        return room.getPriceOneAdultTwoChildren();
                    default:
                        throw new IllegalArgumentException("Invalid room occupancy (for 1 adults, number of children has to be from the interval [0,2])!");
                }
            case 2:
                switch (numberChildren) {
                    case 0:
                        //System.out.println(String.format("===Room price getPriceTwoAdults: %f================", room.getPriceTwoAdults()));
                        return room.getPriceTwoAdults();
                    case 1:
                        //System.out.println(String.format("===Room price getPriceTwoAdultsOneChild: %f================", room.getPriceTwoAdultsOneChild()));
                        return room.getPriceTwoAdultsOneChild();
                    default:
                        throw new IllegalArgumentException("Invalid room occupancy (for 2 adults, number of children has to be from the interval [0,1])!");
                }
            case 3:
                switch (numberChildren) {
                    case 0:
                        //System.out.println(String.format("===Room price getPriceThreeAdults: %f================", room.getPriceThreeAdults()));
                        return room.getPriceThreeAdults();
                    default:
                        throw new IllegalArgumentException("Invalid room occupancy (for 3 adults, number of children has to be 0)!");
                }
            default:
                throw new IllegalArgumentException("Invalid room occupancy (number of adults has to be from the interval [1,3])!");
        }
    }

    public BookingDTO setCurrentPrice(BookingDTO bookingDTO) {
        log.debug("Request to set price preview on Booking : {}", bookingDTO);
        Booking booking = bookingMapper.toEntity(bookingDTO);

        fetchRelations(booking, bookingDTO);
        validate(booking, bookingDTO);

        //New booking, calculate price
        booking.price(getPrice(booking));

        return bookingMapper.toDto(booking);
    }


    /**
     * Save a booking.
     *
     * @param bookingDTO the entity to save.
     * @return the persisted entity.
     */
    public BookingDTO save(BookingDTO bookingDTO) {
        log.debug("Request to save Booking : {}", bookingDTO);
        Booking booking = bookingMapper.toEntity(bookingDTO);

        fetchRelations(booking, bookingDTO);
        validate(booking, bookingDTO);

        //recalculate price
        booking.price(getPrice(booking));

        booking = bookingRepository.save(booking);
        return bookingMapper.toDto(booking);
    }

    private void fetchRelations(Booking booking, BookingDTO bookingDTO) {
        Customer customer = customerRepository.findById(bookingDTO.getCustomer().getId())
            .orElseThrow(() -> new NoSuchElementException("Customer " + bookingDTO.getCustomer().getId()));
        List<Room> rooms = roomRepository.findAllById(bookingDTO.getRooms().stream().map(r -> r.getRoom().getId()).collect(Collectors.toList()));
        Map<Long, Room> roomMap = rooms.stream().collect(Collectors.toMap(Room::getId, room -> room));

        booking.setCustomer(customer);

        Set<RoomOccupancy> roomsToPersist;

        // delete any existing roomOccupancies that have been removed
        roomsToPersist = booking.getRooms().stream()
            .filter(r -> bookingDTO.getRooms().stream().noneMatch(dto -> r.getId().equals(dto.getId())))
            .collect(Collectors.toSet());

        // map new roomOccupancies
        bookingDTO.getRooms().stream()
            .filter(r -> r.getId() == null)
            .forEach(roomOccupancyDTO ->
                roomsToPersist.add(new RoomOccupancy()
                    .room(roomMap.get(roomOccupancyDTO.getRoom().getId()))
                    .booking(booking)
                    .numberAdults(roomOccupancyDTO.getNumberAdults())
                    .numberChildren(roomOccupancyDTO.getNumberChildren())));

        // update existing roomOccupancies
        bookingDTO.getRooms().stream()
            .filter(r -> r.getId() != null)
            .forEach(roomOccupancyDTO ->
                roomsToPersist.add(new RoomOccupancy()
                    .id(roomOccupancyDTO.getId())
                    .room(roomMap.get(roomOccupancyDTO.getRoom().getId()))
                    .booking(booking)
                    .numberAdults(roomOccupancyDTO.getNumberAdults())
                    .numberChildren(roomOccupancyDTO.getNumberChildren())));

        booking.getRooms().clear();
        booking.addRooms(roomsToPersist);
    }

    private void validate(Booking booking, BookingDTO bookingDTO) {
        if (bookingDTO.getRooms().size() != booking.getRooms().size()) {
            throw new NoSuchElementException("Number of submitted rooms and valid rooms does not match!");
        }

        // validate unique rooms
        Set<Long> roomIds = new HashSet<>();
        boolean noDuplicateRoomIds = booking.getRooms().stream().map(RoomOccupancy::getRoom).map(Room::getId).map(roomIds::add).allMatch(roomWasNew -> roomWasNew);
        if (!noDuplicateRoomIds) {
            throw new ValidationException("Booking contains the same room multiple times!", "RoomOccupancy", "business");
        }

        // check rooms to be free during period
        List<Room> occupiedRooms;
        if (booking.getId() != null) {
            //exclude this booking for update operations
            occupiedRooms = roomOccupancyRepository.findAllOccupiedRoomsInRangeWithStateExcludeBooking(0, booking.getStartDate(), booking.getEndDate(), State.ACTIVE, booking.getId());
        } else {
            occupiedRooms = roomOccupancyRepository.findAllOccupiedRoomsInRangeWithState(0, booking.getStartDate(), booking.getEndDate(), State.ACTIVE);
        }
        List<Long> occupiedRoomIds = occupiedRooms.stream().map(Room::getId).collect(Collectors.toList());
        List<Long> alreadyOccupiedRoomIds = booking.getRooms().stream().map(RoomOccupancy::getRoom).map(Room::getId).filter(occupiedRoomIds::contains).collect(Collectors.toList());
        if (!alreadyOccupiedRoomIds.isEmpty()) {
            throw new ValidationException("Booking contains unavailable rooms!", "RoomOccupancy", "business");
        }
        // validate price of booking or remove price field -> price is always (re)calculated on create or update in the backend

        // validate not exceeding room's max guest number
        booking.getRooms()
            .forEach(roomOccupancy -> {
                Room room = roomOccupancy.getRoom();
                if (room.getMaxCapacity() < roomOccupancy.getNumberAdults() + roomOccupancy.getNumberChildren()) {
                    throw new ValidationException("Exceeding capacity (" + room.getMaxCapacity() + ") of room " + room.getNumber(), "RoomOccupancy", "business");
                }
            });

//        System.out.println("=====Start: "+booking.getStartDate()+"=========");
//        System.out.println("=====End: "+booking.getEndDate()+"=========");
//        System.out.println("=====AbsDiffSec: "+Math.abs(booking.getEndDate().toEpochSecond() - booking.getStartDate().toEpochSecond())+"=========");
//        System.out.flush();
        // validate endDate after startDate
        if (booking.getEndDate().isBefore(booking.getStartDate())) {
            throw new ValidationException("End date is before start date!", "Booking", "business");
        }
        // validate duration is at least one day
        if (Math.abs(booking.getEndDate().toEpochSecond() - booking.getStartDate().toEpochSecond()) < 24 * 60 * 60) {
            throw new ValidationException("Duration is less than a day!", "Booking", "business");
        }
    }

    /**
     * Get all the bookings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BookingDTO> findAll(Pageable pageable, boolean onlyWithInvoices) {
        log.debug("Request to get all Bookings");
        if (onlyWithInvoices) {
            log.debug("Request to get all Bookings with invoices");
            Page<Booking> b = bookingRepository.findAllByInvoicesNotEmpty(pageable);
            return bookingRepository.findAllByInvoicesNotEmpty(pageable)
                .map(bookingMapper::toDto);
        }
        return bookingRepository.findAll(pageable)
            .map(bookingMapper::toDto);
    }

    /**
     * Get all booking events.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<String> getEvents() {
        log.debug("Request to get all booking events");
        return bookingRepository.findExistingEvents();
    }

    /**
     * Get one booking by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BookingDTO> findOne(Long id) {
        log.debug("Request to get Booking : {}", id);
        return bookingRepository.findById(id)//findWithEagerRelationships(id)
            .map(bookingMapper::toDto);
    }

    /**
     * Cancel the booking by id.
     *
     * @param id the id of the entity.
     */
    public void cancel(Long id) {
        log.debug("Request to cancel Booking : {}", id);
        Booking booking = bookingRepository.findById(id)
            .orElseThrow(() -> new NoSuchElementException("Booking " + id));

        // check cancellation notice period respected
        if (ZonedDateTime.now().isAfter(booking.getStartDate().minusDays(booking.getCancellationNoticeNights()))) {
            throw new ValidationException("Cancellation not possible anymore!", "Booking", "business");
        }

        booking.setBookingState(State.CANCELLED);
        bookingRepository.save(booking);
    }

    /**
     * Create Invoice for Booking
     *
     * @param id the id of the booking
     */
    @Transactional
    public Long createInvoice(Long id) {
        Invoice invoice = Invoice.createInvoice();

        // It would have been better to have a "Hotel" field for bookings...
        Hotel hotel = this.hotelRepository.findAll().get(0);
        invoice.setHotelAddress(hotel.getHotelAddress());

        Booking booking = bookingRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Booking " + id));
        invoice.setStartDate(booking.getStartDate());
        invoice.setEndDate(booking.getEndDate());
        Period period = Period.between(invoice.getStartDate().toLocalDate(), invoice.getEndDate().toLocalDate());
        invoice.setDurationNights(period.getDays());
        invoice.setRooms(new ArrayList<>(roomReserverationInformationMapper.roomOccupationsToReservationInformations(booking.getRooms())));
        invoice.setDiscountPercent(booking.getDiscountPercent());

        Customer customer = booking.getCustomer();
        invoice.setCustomerAddress(customer.getBillingAddress());
        invoice.setCustomerGender(customer.getGender());
        invoice.setCustomerName(customer.getName());

        List<BillPosition> bill = getDetailedPrice(booking);
        List<InvoicePrice> prices = bill.stream().map(billPosition -> {
            InvoicePrice price = new InvoicePrice();
            price.setDiscountPercent(billPosition.getDiscountPercent() == null ? new BigDecimal(0) : billPosition.getDiscountPercent());
            price.setPrice(billPosition.value);
            if (billPosition.getRoom() != null) {
                price.setRoomNumber(billPosition.getRoom().getRoom().getNumber());
            }
            else {
                price.setRoomNumber("");
            }
            switch (billPosition.getPositionKey()) {
                case RoomCost:
                    String note = "";
                    RoomOccupancy room = billPosition.getRoom();
                    if (room.getNumberAdults() == 1) {
                        note += "EZ";
                    }
                    if (room.getNumberAdults() == 2) {
                        note += "DZ";
                    }
                    if (room.getNumberChildren() == 1) {
                        note += "+Kind";
                    }
                    price.setNote(note);
                    break;
                default:
                    price.setNote(billPosition.getPositionKey().getKey());
            }

            return price;
        }).collect(Collectors.toList());

        invoice.setPrices(prices);
        invoice.setBooking(booking);
        invoice.setDiscountPercent(booking.getDiscountPercent());
        List<Invoice> invoices = booking.getInvoices();

        // Set previous invoice to 'CANCELLED' state
        if (!invoices.isEmpty()) {
            invoices.get(invoices.size() - 1).setInvoiceState(State.CANCELLED);
        }
        invoices.add(invoice);

        invoiceRepository.save(invoice);
        bookingRepository.save(booking);
        invoices = bookingRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Booking " + id)).getInvoices();

        return invoices.get(invoices.size() - 1).getId();
    }

    /**
     * Get one booking by invoice id.
     *
     * @param id the id of the invoice entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BookingDTO> findOneWithInvoiceId(Long id) {

        log.debug("Request to get Booking : {}", id);
        return bookingRepository.findByInvoices_Id(id)//findWithEagerRelationships(id)
            .map(bookingMapper::toDto);
    }

    /**
     * Holds a single position of the bill/invoice
     */
    static class BillPosition {
        /**
         * the position key identifies the type of the position (might be used in the frontend for translation)
         */
        enum PositionKey {
            /** room_cost: room price (per night) * duration of stay (one item for every room) */
            RoomCost("room_cost"),
            /** discount_defects: value of sum of defects (one item for every room) (negative or zero) */
            DiscountDefects("discount_defects"),
            /** discount_customer: value of the customer discount (negative or zero) */
            DiscountCustomer("discount_customer"),
            /** discount_booking: value of the booking discount (negative or zero) */
            DiscountBooking("discount_booking"),
            /** sum_int: intermediary sum (one item after all rooms and one after the customer discount) */
            SumIntermediate("sum_int"),
            /** * sum_fin: the final costs. (only one item at the very end of the bill) */
            SumFinal("sum_fin");

            private String key;

            PositionKey(String key) {
                this.key = key;
            }

            public String getKey() {
                return key;
            }
        }
        /** room and occupancy reference might be null if booking discount/sum or customer discount/sum */
        private RoomOccupancy room;
        /** the value which is supposed to be added or subtracted, depending on its sign */
        private BigDecimal value;

        /** the discount percentage, might null in case of price or sum */
        private BigDecimal discountPercent;

        /** the position key identifies the type of the position (might be used in the frontend for translation) */
        private PositionKey positionKey;

        public RoomOccupancy getRoom() {
            return room;
        }

        public BigDecimal getValue() {
            return value;
        }

        public PositionKey getPositionKey() {
            return positionKey;
        }

        public BigDecimal getDiscountPercent() {
            return discountPercent;
        }

        private BillPosition() {

        }

        public BillPosition(RoomOccupancy room, PositionKey positionKey, BigDecimal value) {
            assert positionKey.equals(PositionKey.RoomCost);
            this.room = room;
            this.positionKey = positionKey;
            this.value = value;
            this.discountPercent = null;
        }

        public BillPosition(RoomOccupancy room, PositionKey positionKey, BigDecimal value, BigDecimal discountPercent) {
            assert positionKey.equals(PositionKey.DiscountDefects);
            this.room = room;
            this.positionKey = positionKey;
            this.value = value;
            this.discountPercent = discountPercent;
        }

        public BillPosition(PositionKey positionKey, BigDecimal value) {
            assert positionKey.equals(PositionKey.SumIntermediate) || positionKey.equals(PositionKey.SumFinal);
            this.room = null;
            this.positionKey = positionKey;
            this.value = value;
            this.discountPercent = null;
        }

        public BillPosition(PositionKey positionKey, BigDecimal value, BigDecimal discountPercent) {
            assert positionKey.equals(PositionKey.DiscountBooking) || positionKey.equals(PositionKey.DiscountCustomer);
            this.room = null;
            this.positionKey = positionKey;
            this.value = value;
            this.discountPercent = discountPercent;
        }
    }
}
