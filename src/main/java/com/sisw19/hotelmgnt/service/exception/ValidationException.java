package com.sisw19.hotelmgnt.service.exception;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class ValidationException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    private final String entityName;

    private final String errorKey;

    public ValidationException(String defaultMessage, String entityName, String errorKey) {
        super(URI.create("https://www.jhipster.tech/problem/email-not-found"), defaultMessage, Status.BAD_REQUEST);
        this.errorKey = errorKey;
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getErrorKey() {
        return errorKey;
    }

    private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);

        parameters.put("params", entityName);
        return parameters;
    }
}
