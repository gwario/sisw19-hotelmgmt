package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.domain.Employee;
import com.sisw19.hotelmgnt.domain.User;
import com.sisw19.hotelmgnt.domain.Vacation;
import com.sisw19.hotelmgnt.domain.enumeration.VacationStatus;
import com.sisw19.hotelmgnt.repository.EmployeeRepository;
import com.sisw19.hotelmgnt.repository.UserRepository;
import com.sisw19.hotelmgnt.repository.VacationRepository;
import com.sisw19.hotelmgnt.security.AuthoritiesConstants;
import com.sisw19.hotelmgnt.security.SecurityUtils;
import com.sisw19.hotelmgnt.service.dto.VacationDTO;
import com.sisw19.hotelmgnt.service.exception.ValidationException;
import com.sisw19.hotelmgnt.service.mapper.VacationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Vacation}.
 */
@Service
@Transactional
public class VacationService {

    private final Logger log = LoggerFactory.getLogger(VacationService.class);

    private final VacationRepository vacationRepository;

    private final EmployeeRepository employeeRepository;

    private final VacationMapper vacationMapper;

    private final UserRepository userRepository;

    public VacationService(VacationRepository vacationRepository, VacationMapper vacationMapper, EmployeeRepository employeeRepository, UserRepository userRepository) {
        this.vacationRepository = vacationRepository;
        this.vacationMapper = vacationMapper;
        this.employeeRepository = employeeRepository;
        this.userRepository = userRepository;
    }

    /**
     * An employee is sending a vacation request, which will be validated by his/her boss
     *
     * @param vacationDTO the entity to save.
     * @return the persisted entity.
     */
    public VacationDTO requestVacation(VacationDTO vacationDTO) {
        log.debug("Employee Vacation Request: {}", vacationDTO);
        Vacation vacation = vacationMapper.toEntity(vacationDTO);

        User user = this.getCurrentUser();

        if (vacation.getStartDate().isBefore(ZonedDateTime.now())) {
            throw new ValidationException("The start date of the vacation must be in the future.", "vacation", "bad_date");
        }

        if (vacation.getEndDate().isBefore(vacation.getStartDate())) {
            throw new ValidationException("The end date of your vacation must be after the begin of your vacation.", "vacation", "bad_date");
        }
        log.debug("validated");
        vacation.setStatus(VacationStatus.REQUESTED);
        Employee employee = employeeRepository.findOneByUserId(user.getId()).orElse(null);
        if (employee == null) {
            throw new ValidationException("Cannot find employee for current user.", "vacation", "no_such_employee");
        }
        vacation.setEmployee(employee);

        vacation = vacationRepository.save(vacation);
        return vacationMapper.toDto(vacation);
    }

    /**
     * Accept vacation request as a boss.
     * @param id
     * @return
     */
    public VacationDTO acceptVacation(Long id){
        log.debug("Request to accept Vacation : {}", id);
        Vacation vacation = vacationRepository.getOne(id);
        if(vacation == null){
            throw new ValidationException("Cannot find the vacation to accept.", "vacation", "no_such_vacation");
        }
        if (vacation.getStatus() != VacationStatus.REQUESTED){
            throw new ValidationException("This vacation was already reviewed.", "vacation", "bad_status");
        }
        User user = this.getCurrentUser();
        //check if the user is the boss with permissions to accept
        this.isValidBoss(user, vacation);
        vacation.setStatus(VacationStatus.ACCEPTED);
        vacationRepository.save(vacation);

        return vacationMapper.toDto(vacation);
    }

    /**
     * Reject vacation request as a boss.
     * @param id
     * @return
     */
    public VacationDTO rejectVacation(Long id){
        log.debug("Request to reject Vacation : {}", id);
        Vacation vacation = vacationRepository.getOne(id);
        if(vacation == null){
            throw new ValidationException("Cannot find the vacation to reject.", "vacation", "no_such_vacation");
        }
        if (vacation.getStatus() != VacationStatus.REQUESTED){
            throw new ValidationException("This vacation was already reviewed.", "vacation", "bad_status");
        }
        User user = this.getCurrentUser();
        //check if the user is the boss with permissions to accept
        this.isValidBoss(user, vacation);
        vacation.setStatus(VacationStatus.DECLINED);
        vacationRepository.save(vacation);

        return vacationMapper.toDto(vacation);
    }

    /**
     * Check if a user has permissions to accept or reject a vacation request.
     * @param user
     * @param vacation
     * @return
     */
    private boolean isValidBoss(User user, Vacation vacation){

        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return true;
        }

        if (vacation.getEmployee().getBoss() != null) {
            if (vacation.getEmployee().getBoss().getUser().getId() == user.getId()) {
                return true;
            } else {
                throw new ValidationException("You are not allowed to manage this vacation.", "vacation", "unauthorized");
            }
        }
        return false;
    }

    /**
     * Save a vacation.
     *
     * @param vacationDTO the entity to save.
     * @return the persisted entity.
     */
    public VacationDTO save(VacationDTO vacationDTO) {
        log.debug("Request to save Vacation : {}", vacationDTO);
        Vacation vacation = vacationMapper.toEntity(vacationDTO);
        vacation = vacationRepository.save(vacation);
        return vacationMapper.toDto(vacation);
    }

    /**
     * Get all the vacations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<VacationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Vacations");
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            User user = getCurrentUser();
            if (user == null){
                return new PageImpl<VacationDTO>(new ArrayList<>());
            }
            return vacationRepository.findAllByBossOrMine(user.getId(), pageable).map(vacationMapper::toDto);
        }
        return vacationRepository.findAll(pageable)
            .map(vacationMapper::toDto);
    }

    /**
     * Get all by Boss.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<VacationDTO> findAllByBoss(Pageable pageable) {
        log.debug("Request to get all Vacations by Boss");
        User user = this.getCurrentUser();
        log.debug("user id {}", user.getId());
        return vacationRepository.findAllByBossAndStatus(user.getId(), VacationStatus.REQUESTED, pageable)
            .map(vacationMapper::toDto);
    }


    /**
     * Get one vacation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<VacationDTO> findOne(Long id) {
        log.debug("Request to get Vacation : {}", id);
        return vacationRepository.findById(id)
            .map(vacationMapper::toDto);
    }

    /**
     * Delete the vacation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Vacation : {}", id);
        vacationRepository.deleteById(id);
    }

    private User getCurrentUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null){
            return null;
        }
        log.debug(authentication.getName());
        User user = userRepository.findOneByLogin(authentication.getName()).orElse(null);
        log.debug(user.toString());
        log.debug("Cannot find the authenticated user");
        if (user == null) {
            throw new ValidationException("Cannot find the user for current request", "vacation", "no_user");
        }
        return user;
    }
}
