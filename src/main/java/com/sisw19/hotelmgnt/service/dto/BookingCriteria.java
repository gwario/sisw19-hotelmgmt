package com.sisw19.hotelmgnt.service.dto;

import com.sisw19.hotelmgnt.domain.enumeration.State;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;
import java.io.Serializable;
import java.util.Objects;


public class BookingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private ZonedDateTimeFilter startDate;

    private ZonedDateTimeFilter endDate;

    private StringFilter event;

    private StateFilter state;

    public BookingCriteria() {
    }

    public BookingCriteria(BookingCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.startDate = other.startDate == null ? null : other.startDate.copy();
        this.endDate = other.endDate == null ? null : other.endDate.copy();
        this.event = other.event == null ? null : other.event.copy();
        this.state = other.state == null ? null : other.state.copy();
    }

    @Override
    public BookingCriteria copy() {
        return new BookingCriteria(this);
    }


    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTimeFilter startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTimeFilter getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTimeFilter endDate) {
        this.endDate = endDate;
    }

    public StringFilter getEvent() {
        return event;
    }

    public void setEvent(StringFilter event) {
        this.event = event;
    }

    public StateFilter getState() {
        return state;
    }

    public void setState(StateFilter state) {
        this.state = state;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BookingCriteria that = (BookingCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(event, that.event) &&
                Objects.equals(state, that.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            startDate,
            endDate,
            event,
            state
        );
    }

    @Override
    public String toString() {
        return "BookingCriteria{" +
            "id=" + id +
            ", startDate=" + startDate +
            ", endDate=" + endDate +
            ", event=" + event +
            ", state=" + state +
            '}';
    }

    public static class StateFilter extends Filter<State> {
        public StateFilter() {
        }

        public StateFilter(StateFilter filter) {
            super(filter);
        }

        @Override
        public StateFilter copy() {
            return new StateFilter(this);
        }
    }
}
