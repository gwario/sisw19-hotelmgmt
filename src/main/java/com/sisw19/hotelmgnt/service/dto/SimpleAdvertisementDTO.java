package com.sisw19.hotelmgnt.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for a simple advertisement.
 */
public class SimpleAdvertisementDTO implements Serializable {

    @NotNull
    private String subject;

    @NotNull
    private String body;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
            "subject=" + getSubject() +
            ", body=" + getBody() +
            "}";
    }
}
