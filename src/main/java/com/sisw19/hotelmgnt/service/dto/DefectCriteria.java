package com.sisw19.hotelmgnt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.sisw19.hotelmgnt.domain.enumeration.DefectState;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.sisw19.hotelmgnt.domain.Defect} entity. This class is used
 * in {@link com.sisw19.hotelmgnt.web.rest.DefectResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /defects?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DefectCriteria implements Serializable, Criteria {
    /**
     * Class for filtering DefectState
     */
    public static class DefectStateFilter extends Filter<DefectState> {

        public DefectStateFilter() {
        }

        public DefectStateFilter(DefectStateFilter filter) {
            super(filter);
        }

        @Override
        public DefectStateFilter copy() {
            return new DefectStateFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter description;

    private DefectStateFilter defectState;

    private DoubleFilter discountPercent;

    private LongFilter roomId;

    public DefectCriteria(){
    }

    public DefectCriteria(DefectCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.defectState = other.defectState == null ? null : other.defectState.copy();
        this.discountPercent = other.discountPercent == null ? null : other.discountPercent.copy();
        this.roomId = other.roomId == null ? null : other.roomId.copy();
    }

    @Override
    public DefectCriteria copy() {
        return new DefectCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public DefectStateFilter getDefectState() {
        return defectState;
    }

    public void setDefectState(DefectStateFilter defectState) {
        this.defectState = defectState;
    }

    public DoubleFilter getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(DoubleFilter discountPercent) {
        this.discountPercent = discountPercent;
    }

    public LongFilter getRoomId() {
        return roomId;
    }

    public void setRoomId(LongFilter roomId) {
        this.roomId = roomId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DefectCriteria that = (DefectCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(description, that.description) &&
            Objects.equals(defectState, that.defectState) &&
            Objects.equals(discountPercent, that.discountPercent) &&
            Objects.equals(roomId, that.roomId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        description,
        defectState,
        discountPercent,
        roomId
        );
    }

    @Override
    public String toString() {
        return "DefectCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (defectState != null ? "defectState=" + defectState + ", " : "") +
                (discountPercent != null ? "discountPercent=" + discountPercent + ", " : "") +
                (roomId != null ? "roomId=" + roomId + ", " : "") +
            "}";
    }

}
