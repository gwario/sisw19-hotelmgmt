package com.sisw19.hotelmgnt.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.sisw19.hotelmgnt.domain.AbsenceReason} entity.
 */
public class AbsenceReasonDTO implements Serializable {

    private Long id;

    @NotNull
    private String reason;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AbsenceReasonDTO absenceReasonDTO = (AbsenceReasonDTO) o;
        if (absenceReasonDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), absenceReasonDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AbsenceReasonDTO{" +
            "id=" + getId() +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
