package com.sisw19.hotelmgnt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.sisw19.hotelmgnt.domain.enumeration.Gender;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.sisw19.hotelmgnt.domain.Customer} entity. This class is used
 * in {@link com.sisw19.hotelmgnt.web.rest.CustomerResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /customers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CustomerCriteria implements Serializable, Criteria {
    /**
     * Class for filtering Gender
     */
    public static class GenderFilter extends Filter<Gender> {

        public GenderFilter() {
        }

        public GenderFilter(GenderFilter filter) {
            super(filter);
        }

        @Override
        public GenderFilter copy() {
            return new GenderFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter companyName;

    private StringFilter notes;

    private DoubleFilter discountPercent;

    private StringFilter webpage;

    private StringFilter fax;

    private StringFilter name;

    private LocalDateFilter birthday;

    private GenderFilter gender;

    private StringFilter tel;

    private StringFilter email;

    private StringFilter billingAddress;

    private LongFilter userId;

    public CustomerCriteria(){
    }

    public CustomerCriteria(CustomerCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.companyName = other.companyName == null ? null : other.companyName.copy();
        this.notes = other.notes == null ? null : other.notes.copy();
        this.discountPercent = other.discountPercent == null ? null : other.discountPercent.copy();
        this.webpage = other.webpage == null ? null : other.webpage.copy();
        this.fax = other.fax == null ? null : other.fax.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.birthday = other.birthday == null ? null : other.birthday.copy();
        this.gender = other.gender == null ? null : other.gender.copy();
        this.tel = other.tel == null ? null : other.tel.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.billingAddress = other.billingAddress == null ? null : other.billingAddress.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public CustomerCriteria copy() {
        return new CustomerCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCompanyName() {
        return companyName;
    }

    public void setCompanyName(StringFilter companyName) {
        this.companyName = companyName;
    }

    public StringFilter getNotes() {
        return notes;
    }

    public void setNotes(StringFilter notes) {
        this.notes = notes;
    }

    public DoubleFilter getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(DoubleFilter discountPercent) {
        this.discountPercent = discountPercent;
    }

    public StringFilter getWebpage() {
        return webpage;
    }

    public void setWebpage(StringFilter webpage) {
        this.webpage = webpage;
    }

    public StringFilter getFax() {
        return fax;
    }

    public void setFax(StringFilter fax) {
        this.fax = fax;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LocalDateFilter getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateFilter birthday) {
        this.birthday = birthday;
    }

    public GenderFilter getGender() {
        return gender;
    }

    public void setGender(GenderFilter gender) {
        this.gender = gender;
    }

    public StringFilter getTel() {
        return tel;
    }

    public void setTel(StringFilter tel) {
        this.tel = tel;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(StringFilter billingAddress) {
        this.billingAddress = billingAddress;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CustomerCriteria that = (CustomerCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(companyName, that.companyName) &&
            Objects.equals(notes, that.notes) &&
            Objects.equals(discountPercent, that.discountPercent) &&
            Objects.equals(webpage, that.webpage) &&
            Objects.equals(fax, that.fax) &&
            Objects.equals(name, that.name) &&
            Objects.equals(birthday, that.birthday) &&
            Objects.equals(gender, that.gender) &&
            Objects.equals(tel, that.tel) &&
            Objects.equals(email, that.email) &&
            Objects.equals(billingAddress, that.billingAddress) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        companyName,
        notes,
        discountPercent,
        webpage,
        fax,
        name,
        birthday,
        gender,
        tel,
        email,
        billingAddress,
        userId
        );
    }

    @Override
    public String toString() {
        return "CustomerCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (companyName != null ? "companyName=" + companyName + ", " : "") +
                (notes != null ? "notes=" + notes + ", " : "") +
                (discountPercent != null ? "discountPercent=" + discountPercent + ", " : "") +
                (webpage != null ? "webpage=" + webpage + ", " : "") +
                (fax != null ? "fax=" + fax + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (birthday != null ? "birthday=" + birthday + ", " : "") +
                (gender != null ? "gender=" + gender + ", " : "") +
                (tel != null ? "tel=" + tel + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (billingAddress != null ? "billingAddress=" + billingAddress + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
