package com.sisw19.hotelmgnt.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.sisw19.hotelmgnt.domain.enumeration.VacationStatus;

/**
 * A DTO for the {@link com.sisw19.hotelmgnt.domain.Vacation} entity.
 */
public class VacationDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime startDate;

    @NotNull
    private ZonedDateTime endDate;

    @NotNull
    private VacationStatus status;


    private EmployeeDTO employee;

    private Long absenceReasonId;

    private String absenceReasonReason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public VacationStatus getStatus() {
        return status;
    }

    public void setStatus(VacationStatus status) {
        this.status = status;
    }

    public Long getAbsenceReasonId() {
        return absenceReasonId;
    }

    public void setAbsenceReasonId(Long absenceReasonId) {
        this.absenceReasonId = absenceReasonId;
    }

    public String getAbsenceReasonReason() {
        return absenceReasonReason;
    }

    public void setAbsenceReasonReason(String absenceReasonReason) {
        this.absenceReasonReason = absenceReasonReason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VacationDTO vacationDTO = (VacationDTO) o;
        if (vacationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), vacationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VacationDTO{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", status='" + getStatus() + "'" +
            ", employee=" + (getEmployee() != null ? employee.getName() : null) +
            ", absenceReason=" + getAbsenceReasonId() +
            ", absenceReason='" + getAbsenceReasonReason() + "'" +
            "}";
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }
}
