package com.sisw19.hotelmgnt.service.dto;

import com.sisw19.hotelmgnt.domain.enumeration.State;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Period;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the {@link com.sisw19.hotelmgnt.domain.Booking} entity.
 */
public class BookingDTO implements Serializable {

    private Long id;

    @NotNull
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    private Double discountPercent;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal price;

    @NotNull
    private ZonedDateTime startDate;

    @NotNull
    private ZonedDateTime endDate;

    @NotNull
    private State bookingState;

    private String event;

    private Integer cancellationNoticeNights; // optional, default set by business logic

    @NotNull
    private CustomerDTO customer;

    @NotNull
    @Size(min = 1)
    @Valid
    private Set<RoomOccupancyDTO> rooms = new HashSet<>();

    private List<InvoiceDTO> invoices;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public Integer getCancellationNoticeNights() {
        return cancellationNoticeNights;
    }

    public void setCancellationNoticeNights(Integer cancellationNoticeNights) {
        this.cancellationNoticeNights = cancellationNoticeNights;
    }

    public State getBookingState() {
        return bookingState;
    }

    public void setBookingState(State bookingState) {
        this.bookingState = bookingState;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public Integer getDurationNights() {
        if (this.startDate == null || this.endDate == null) return null;
        Period period = Period.between(this.startDate.toLocalDate(), this.endDate.toLocalDate());
        return period.getDays();
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public Set<RoomOccupancyDTO> getRooms() {
        return rooms;
    }

    public void setRooms(Set<RoomOccupancyDTO> rooms) {
        this.rooms = rooms;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BookingDTO bookingDTO = (BookingDTO) o;
        if (bookingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bookingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BookingDTO{" +
            "id=" + getId() +
            ", discountPercent=" + getDiscountPercent() +
            ", price=" + getPrice() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", cancellationNoticeNights=" + getCancellationNoticeNights() +
            ", bookingState='" + getBookingState() + "'" +
            "}";
    }

    public List<InvoiceDTO> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<InvoiceDTO> invoices) {
        this.invoices = invoices;
    }
}
