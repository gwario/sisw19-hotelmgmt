package com.sisw19.hotelmgnt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.sisw19.hotelmgnt.domain.enumeration.EmployeeType;
import com.sisw19.hotelmgnt.domain.enumeration.Gender;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.sisw19.hotelmgnt.domain.Employee} entity. This class is used
 * in {@link com.sisw19.hotelmgnt.web.rest.EmployeeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /employees?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EmployeeCriteria implements Serializable, Criteria {
    /**
     * Class for filtering EmployeeType
     */
    public static class EmployeeTypeFilter extends Filter<EmployeeType> {

        public EmployeeTypeFilter() {
        }

        public EmployeeTypeFilter(EmployeeTypeFilter filter) {
            super(filter);
        }

        @Override
        public EmployeeTypeFilter copy() {
            return new EmployeeTypeFilter(this);
        }

    }
    /**
     * Class for filtering Gender
     */
    public static class GenderFilter extends Filter<Gender> {

        public GenderFilter() {
        }

        public GenderFilter(GenderFilter filter) {
            super(filter);
        }

        @Override
        public GenderFilter copy() {
            return new GenderFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private EmployeeTypeFilter employeeType;

    private StringFilter shortId;

    private StringFilter insuranceNumber;

    private StringFilter accountData;

    private StringFilter contractType;

    private StringFilter tasks;

    private StringFilter name;

    private LocalDateFilter birthday;

    private GenderFilter gender;

    private StringFilter tel;

    private StringFilter email;

    private StringFilter address;

    private LongFilter userId;

    private LongFilter employeeId;

    private LongFilter vacationsId;

    private LongFilter bossId;

    public EmployeeCriteria(){
    }

    public EmployeeCriteria(EmployeeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.employeeType = other.employeeType == null ? null : other.employeeType.copy();
        this.shortId = other.shortId == null ? null : other.shortId.copy();
        this.insuranceNumber = other.insuranceNumber == null ? null : other.insuranceNumber.copy();
        this.accountData = other.accountData == null ? null : other.accountData.copy();
        this.contractType = other.contractType == null ? null : other.contractType.copy();
        this.tasks = other.tasks == null ? null : other.tasks.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.birthday = other.birthday == null ? null : other.birthday.copy();
        this.gender = other.gender == null ? null : other.gender.copy();
        this.tel = other.tel == null ? null : other.tel.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.employeeId = other.employeeId == null ? null : other.employeeId.copy();
        this.vacationsId = other.vacationsId == null ? null : other.vacationsId.copy();
        this.bossId = other.bossId == null ? null : other.bossId.copy();
    }

    @Override
    public EmployeeCriteria copy() {
        return new EmployeeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public EmployeeTypeFilter getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(EmployeeTypeFilter employeeType) {
        this.employeeType = employeeType;
    }

    public StringFilter getShortId() {
        return shortId;
    }

    public void setShortId(StringFilter shortId) {
        this.shortId = shortId;
    }

    public StringFilter getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(StringFilter insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public StringFilter getAccountData() {
        return accountData;
    }

    public void setAccountData(StringFilter accountData) {
        this.accountData = accountData;
    }

    public StringFilter getContractType() {
        return contractType;
    }

    public void setContractType(StringFilter contractType) {
        this.contractType = contractType;
    }

    public StringFilter getTasks() {
        return tasks;
    }

    public void setTasks(StringFilter tasks) {
        this.tasks = tasks;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LocalDateFilter getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateFilter birthday) {
        this.birthday = birthday;
    }

    public GenderFilter getGender() {
        return gender;
    }

    public void setGender(GenderFilter gender) {
        this.gender = gender;
    }

    public StringFilter getTel() {
        return tel;
    }

    public void setTel(StringFilter tel) {
        this.tel = tel;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(LongFilter employeeId) {
        this.employeeId = employeeId;
    }

    public LongFilter getVacationsId() {
        return vacationsId;
    }

    public void setVacationsId(LongFilter vacationsId) {
        this.vacationsId = vacationsId;
    }

    public LongFilter getBossId() {
        return bossId;
    }

    public void setBossId(LongFilter bossId) {
        this.bossId = bossId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EmployeeCriteria that = (EmployeeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(employeeType, that.employeeType) &&
            Objects.equals(shortId, that.shortId) &&
            Objects.equals(insuranceNumber, that.insuranceNumber) &&
            Objects.equals(accountData, that.accountData) &&
            Objects.equals(contractType, that.contractType) &&
            Objects.equals(tasks, that.tasks) &&
            Objects.equals(name, that.name) &&
            Objects.equals(birthday, that.birthday) &&
            Objects.equals(gender, that.gender) &&
            Objects.equals(tel, that.tel) &&
            Objects.equals(email, that.email) &&
            Objects.equals(address, that.address) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(employeeId, that.employeeId) &&
            Objects.equals(vacationsId, that.vacationsId) &&
            Objects.equals(bossId, that.bossId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        employeeType,
        shortId,
        insuranceNumber,
        accountData,
        contractType,
        tasks,
        name,
        birthday,
        gender,
        tel,
        email,
        address,
        userId,
        employeeId,
        vacationsId,
        bossId
        );
    }

    @Override
    public String toString() {
        return "EmployeeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (employeeType != null ? "employeeType=" + employeeType + ", " : "") +
                (shortId != null ? "shortId=" + shortId + ", " : "") +
                (insuranceNumber != null ? "insuranceNumber=" + insuranceNumber + ", " : "") +
                (accountData != null ? "accountData=" + accountData + ", " : "") +
                (contractType != null ? "contractType=" + contractType + ", " : "") +
                (tasks != null ? "tasks=" + tasks + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (birthday != null ? "birthday=" + birthday + ", " : "") +
                (gender != null ? "gender=" + gender + ", " : "") +
                (tel != null ? "tel=" + tel + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (employeeId != null ? "employeeId=" + employeeId + ", " : "") +
                (vacationsId != null ? "vacationsId=" + vacationsId + ", " : "") +
                (bossId != null ? "bossId=" + bossId + ", " : "") +
            "}";
    }

}
