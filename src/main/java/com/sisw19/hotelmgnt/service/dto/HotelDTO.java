package com.sisw19.hotelmgnt.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.sisw19.hotelmgnt.domain.Hotel} entity.
 */
public class HotelDTO implements Serializable {

    private Long id;

    @NotNull
    private String hotelAddress;

    @NotNull
    @Min(value = 0)
    private Integer defaultCancellationNoticeNights;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public Integer getDefaultCancellationNoticeNights() {
        return defaultCancellationNoticeNights;
    }

    public void setDefaultCancellationNoticeNights(Integer defaultCancellationNoticeNights) {
        this.defaultCancellationNoticeNights = defaultCancellationNoticeNights;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HotelDTO hotelDTO = (HotelDTO) o;
        if (hotelDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hotelDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HotelDTO{" +
            "id=" + getId() +
            ", hotelAddress='" + getHotelAddress() + "'" +
            ", defaultCancellationNoticeNights=" + getDefaultCancellationNoticeNights() +
            "}";
    }
}
