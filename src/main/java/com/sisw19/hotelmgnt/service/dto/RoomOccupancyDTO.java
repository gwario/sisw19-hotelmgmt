package com.sisw19.hotelmgnt.service.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import javax.validation.constraints.Size;

/**
 * A DTO for the {@link com.sisw19.hotelmgnt.domain.RoomOccupancy} entity.
 */
public class RoomOccupancyDTO implements Serializable {

    private Long id;

    @NotNull
    @Min(value = 1)
    @Max(value = 3)
    private Integer numberAdults;

    @NotNull
    @Min(value = 0)
    @Max(value = 2)
    private Integer numberChildren;

    @NotNull
    private RoomDTO room;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumberAdults() {
        return numberAdults;
    }

    public RoomOccupancyDTO numberAdults(Integer numberAdults) {
        this.numberAdults = numberAdults;
        return this;
    }

    public void setNumberAdults(Integer numberAdults) {
        this.numberAdults = numberAdults;
    }

    public Integer getNumberChildren() {
        return numberChildren;
    }

    public RoomOccupancyDTO numberChildren(Integer numberChildren) {
        this.numberChildren = numberChildren;
        return this;
    }

    public void setNumberChildren(Integer numberChildren) {
        this.numberChildren = numberChildren;
    }

    public RoomDTO getRoom() {
        return room;
    }

    public void setRoom(RoomDTO room) {
        this.room = room;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RoomOccupancyDTO)) {
            return false;
        }
        return id != null && id.equals(((RoomOccupancyDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RoomOccupancy{" +
            "id=" + getId() +
            ", numberAdults=" + getNumberAdults() +
            ", numberChildren=" + getNumberChildren() +
            ", room=" + getRoom() +
            "}";
    }

}
