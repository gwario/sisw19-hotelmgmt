package com.sisw19.hotelmgnt.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link com.sisw19.hotelmgnt.domain.Room} entity.
 */
public class RoomDTO implements Serializable {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[0-9]*$")
    private String number;

    @NotNull
    @Min(value = 1)
    @Max(value = 3)
    private Integer maxCapacity;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal priceOneAdult;

    @DecimalMin(value = "0")
    private BigDecimal priceTwoAdults;

    @DecimalMin(value = "0")
    private BigDecimal priceThreeAdults;

    @DecimalMin(value = "0")
    private BigDecimal priceOneAdultOneChild;

    @DecimalMin(value = "0")
    private BigDecimal priceTwoAdultsOneChild;

    @DecimalMin(value = "0")
    private BigDecimal priceOneAdultTwoChildren;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoomDTO id(Long id) {
        this.id = id;
        return this;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(Integer maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public BigDecimal getPriceOneAdult() {
        return priceOneAdult;
    }

    public void setPriceOneAdult(BigDecimal priceOneAdult) {
        this.priceOneAdult = priceOneAdult;
    }

    public BigDecimal getPriceTwoAdults() {
        return priceTwoAdults;
    }

    public void setPriceTwoAdults(BigDecimal priceTwoAdults) {
        this.priceTwoAdults = priceTwoAdults;
    }

    public BigDecimal getPriceThreeAdults() {
        return priceThreeAdults;
    }

    public void setPriceThreeAdults(BigDecimal priceThreeAdults) {
        this.priceThreeAdults = priceThreeAdults;
    }

    public BigDecimal getPriceOneAdultOneChild() {
        return priceOneAdultOneChild;
    }

    public void setPriceOneAdultOneChild(BigDecimal priceOneAdultOneChild) {
        this.priceOneAdultOneChild = priceOneAdultOneChild;
    }

    public BigDecimal getPriceTwoAdultsOneChild() {
        return priceTwoAdultsOneChild;
    }

    public void setPriceTwoAdultsOneChild(BigDecimal priceTwoAdultsOneChild) {
        this.priceTwoAdultsOneChild = priceTwoAdultsOneChild;
    }

    public BigDecimal getPriceOneAdultTwoChildren() {
        return priceOneAdultTwoChildren;
    }

    public void setPriceOneAdultTwoChildren(BigDecimal priceOneAdultTwoChildren) {
        this.priceOneAdultTwoChildren = priceOneAdultTwoChildren;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RoomDTO roomDTO = (RoomDTO) o;
        if (roomDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roomDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
            "id=" + getId() +
            ", number='" + getNumber() + "'" +
            ", maxCapacity=" + getMaxCapacity() +
            ", priceOneAdult=" + getPriceOneAdult() +
            ", priceTwoAdults=" + getPriceTwoAdults() +
            ", priceThreeAdults=" + getPriceThreeAdults() +
            ", priceOneAdultOneChild=" + getPriceOneAdultOneChild() +
            ", priceTwoAdultsOneChild=" + getPriceTwoAdultsOneChild() +
            ", priceOneAdultTwoChildren=" + getPriceOneAdultTwoChildren() +
            "}";
    }
}
