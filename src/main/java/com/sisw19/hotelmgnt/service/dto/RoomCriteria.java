package com.sisw19.hotelmgnt.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link com.sisw19.hotelmgnt.domain.Room} entity. This class is used
 * in {@link com.sisw19.hotelmgnt.web.rest.RoomResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /rooms?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class RoomCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter number;

    private IntegerFilter maxCapacity;

    private BigDecimalFilter priceOneAdult;

    private BigDecimalFilter priceTwoAdults;

    private BigDecimalFilter priceThreeAdults;

    private BigDecimalFilter priceOneAdultOneChild;

    private BigDecimalFilter priceTwoAdultsOneChild;

    private BigDecimalFilter priceOneAdultTwoChildren;

    private LongFilter defectsId;

    public RoomCriteria(){
    }

    public RoomCriteria(RoomCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.number = other.number == null ? null : other.number.copy();
        this.maxCapacity = other.maxCapacity == null ? null : other.maxCapacity.copy();
        this.priceOneAdult = other.priceOneAdult == null ? null : other.priceOneAdult.copy();
        this.priceTwoAdults = other.priceTwoAdults == null ? null : other.priceTwoAdults.copy();
        this.priceThreeAdults = other.priceThreeAdults == null ? null : other.priceThreeAdults.copy();
        this.priceOneAdultOneChild = other.priceOneAdultOneChild == null ? null : other.priceOneAdultOneChild.copy();
        this.priceTwoAdultsOneChild = other.priceTwoAdultsOneChild == null ? null : other.priceTwoAdultsOneChild.copy();
        this.priceOneAdultTwoChildren = other.priceOneAdultTwoChildren == null ? null : other.priceOneAdultTwoChildren.copy();
        this.defectsId = other.defectsId == null ? null : other.defectsId.copy();
    }

    @Override
    public RoomCriteria copy() {
        return new RoomCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNumber() {
        return number;
    }

    public void setNumber(StringFilter number) {
        this.number = number;
    }

    public IntegerFilter getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(IntegerFilter maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public BigDecimalFilter getPriceOneAdult() {
        return priceOneAdult;
    }

    public void setPriceOneAdult(BigDecimalFilter priceOneAdult) {
        this.priceOneAdult = priceOneAdult;
    }

    public BigDecimalFilter getPriceTwoAdults() {
        return priceTwoAdults;
    }

    public void setPriceTwoAdults(BigDecimalFilter priceTwoAdults) {
        this.priceTwoAdults = priceTwoAdults;
    }

    public BigDecimalFilter getPriceThreeAdults() {
        return priceThreeAdults;
    }

    public void setPriceThreeAdults(BigDecimalFilter priceThreeAdults) {
        this.priceThreeAdults = priceThreeAdults;
    }

    public BigDecimalFilter getPriceOneAdultOneChild() {
        return priceOneAdultOneChild;
    }

    public void setPriceOneAdultOneChild(BigDecimalFilter priceOneAdultOneChild) {
        this.priceOneAdultOneChild = priceOneAdultOneChild;
    }

    public BigDecimalFilter getPriceTwoAdultsOneChild() {
        return priceTwoAdultsOneChild;
    }

    public void setPriceTwoAdultsOneChild(BigDecimalFilter priceTwoAdultsOneChild) {
        this.priceTwoAdultsOneChild = priceTwoAdultsOneChild;
    }

    public BigDecimalFilter getPriceOneAdultTwoChildren() {
        return priceOneAdultTwoChildren;
    }

    public void setPriceOneAdultTwoChildren(BigDecimalFilter priceOneAdultTwoChildren) {
        this.priceOneAdultTwoChildren = priceOneAdultTwoChildren;
    }

    public LongFilter getDefectsId() {
        return defectsId;
    }

    public void setDefectsId(LongFilter defectsId) {
        this.defectsId = defectsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RoomCriteria that = (RoomCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(number, that.number) &&
            Objects.equals(maxCapacity, that.maxCapacity) &&
            Objects.equals(priceOneAdult, that.priceOneAdult) &&
            Objects.equals(priceTwoAdults, that.priceTwoAdults) &&
            Objects.equals(priceThreeAdults, that.priceThreeAdults) &&
            Objects.equals(priceOneAdultOneChild, that.priceOneAdultOneChild) &&
            Objects.equals(priceTwoAdultsOneChild, that.priceTwoAdultsOneChild) &&
            Objects.equals(priceOneAdultTwoChildren, that.priceOneAdultTwoChildren) &&
            Objects.equals(defectsId, that.defectsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        number,
        maxCapacity,
        priceOneAdult,
        priceTwoAdults,
        priceThreeAdults,
        priceOneAdultOneChild,
        priceTwoAdultsOneChild,
        priceOneAdultTwoChildren,
        defectsId
        );
    }

    @Override
    public String toString() {
        return "RoomCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (number != null ? "number=" + number + ", " : "") +
                (maxCapacity != null ? "maxCapacity=" + maxCapacity + ", " : "") +
                (priceOneAdult != null ? "priceOneAdult=" + priceOneAdult + ", " : "") +
                (priceTwoAdults != null ? "priceTwoAdults=" + priceTwoAdults + ", " : "") +
                (priceThreeAdults != null ? "priceThreeAdults=" + priceThreeAdults + ", " : "") +
                (priceOneAdultOneChild != null ? "priceOneAdultOneChild=" + priceOneAdultOneChild + ", " : "") +
                (priceTwoAdultsOneChild != null ? "priceTwoAdultsOneChild=" + priceTwoAdultsOneChild + ", " : "") +
                (priceOneAdultTwoChildren != null ? "priceOneAdultTwoChildren=" + priceOneAdultTwoChildren + ", " : "") +
                (defectsId != null ? "defectsId=" + defectsId + ", " : "") +
            "}";
    }

}
