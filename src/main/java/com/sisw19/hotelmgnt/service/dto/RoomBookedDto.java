package com.sisw19.hotelmgnt.service.dto;

public class RoomBookedDto {

    private RoomDTO room;
    private Boolean Booked;

    public RoomDTO getRoom() {
        return room;
    }

    public void setRoom(RoomDTO room) {
        this.room = room;
    }

    public Boolean getBooked() {
        return Booked;
    }

    public void setBooked(Boolean booked) {
        Booked = booked;
    }
}
