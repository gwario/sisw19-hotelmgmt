package com.sisw19.hotelmgnt.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.sisw19.hotelmgnt.domain.enumeration.DefectState;

/**
 * A DTO for the {@link com.sisw19.hotelmgnt.domain.Defect} entity.
 */
public class DefectDTO implements Serializable {

    private Long id;

    @NotNull
    private String description;

    @NotNull
    private DefectState defectState;

    @NotNull
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    private Double discountPercent;


    private Long roomId;

    private String roomNr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DefectState getDefectState() {
        return defectState;
    }

    public void setDefectState(DefectState defectState) {
        this.defectState = defectState;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getRoomNr() {
        return roomNr;
    }

    public void setRoomNr(String roomNr) {
        this.roomNr = roomNr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DefectDTO defectDTO = (DefectDTO) o;
        if (defectDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), defectDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DefectDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", defectState='" + getDefectState() + "'" +
            ", discountPercent=" + getDiscountPercent() +
            ", room=" + getRoomId() +
            "}";
    }
}
