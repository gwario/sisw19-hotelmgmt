package com.sisw19.hotelmgnt.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import com.sisw19.hotelmgnt.domain.InvoicePrice;
import com.sisw19.hotelmgnt.domain.RoomReservationInformation;
import com.sisw19.hotelmgnt.domain.enumeration.Gender;
import com.sisw19.hotelmgnt.domain.enumeration.State;

/**
 * A DTO for the {@link com.sisw19.hotelmgnt.domain.Invoice} entity.
 */
public class InvoiceDTO implements Serializable {

    private Long id;

    @NotNull
    private String hotelAddress;

    @NotNull
    private String customerAddress;

    @NotNull
    private String customerName;

    @NotNull
    private Gender customerGender;

    @NotNull
    private ZonedDateTime invoiceDate;

    @NotNull
    private ZonedDateTime startDate;

    @NotNull
    private ZonedDateTime endDate;

    @NotNull
    private Integer durationNights;

    @NotNull
    private State invoiceState;

    @NotNull
    private List<InvoicePrice> prices;

    @NotNull
    private List<RoomReservationInformation> rooms;

    @NotNull
    @DecimalMin(value = "0")
    @DecimalMax(value = "100")
    private Double discountPercent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Gender getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(Gender customerGender) {
        this.customerGender = customerGender;
    }

    public ZonedDateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(ZonedDateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public Integer getDurationNights() {
        return durationNights;
    }

    public void setDurationNights(Integer durationNights) {
        this.durationNights = durationNights;
    }

    public State getInvoiceState() {
        return invoiceState;
    }

    public void setInvoiceState(State invoiceState) {
        this.invoiceState = invoiceState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InvoiceDTO invoiceDTO = (InvoiceDTO) o;
        if (invoiceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoiceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvoiceDTO{" +
            "id=" + getId() +
            ", hotelAddress='" + getHotelAddress() + "'" +
            ", customerAddress='" + getCustomerAddress() + "'" +
            ", customerName='" + getCustomerName() + "'" +
            ", customerGender='" + getCustomerGender() + "'" +
            ", invoiceDate='" + getInvoiceDate() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", durationNights=" + getDurationNights() +
            ", invoiceState='" + getInvoiceState() + "'" +
            ", discountPercent='" + getDiscountPercent() + "'" +
            "}";
    }

    public List<InvoicePrice> getPrices() {
        return prices;
    }

    public void setPrices(List<InvoicePrice> prices) {
        this.prices = prices;
    }

    public List<RoomReservationInformation> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomReservationInformation> rooms) {
        this.rooms = rooms;
    }

    public Double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }
}
