package com.sisw19.hotelmgnt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.sisw19.hotelmgnt.domain.Room;
import com.sisw19.hotelmgnt.domain.*; // for static metamodels
import com.sisw19.hotelmgnt.repository.RoomRepository;
import com.sisw19.hotelmgnt.service.dto.RoomCriteria;
import com.sisw19.hotelmgnt.service.dto.RoomDTO;
import com.sisw19.hotelmgnt.service.mapper.RoomMapper;

/**
 * Service for executing complex queries for {@link Room} entities in the database.
 * The main input is a {@link RoomCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RoomDTO} or a {@link Page} of {@link RoomDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RoomQueryService extends QueryService<Room> {

    private final Logger log = LoggerFactory.getLogger(RoomQueryService.class);

    private final RoomRepository roomRepository;

    private final RoomMapper roomMapper;

    public RoomQueryService(RoomRepository roomRepository, RoomMapper roomMapper) {
        this.roomRepository = roomRepository;
        this.roomMapper = roomMapper;
    }

    /**
     * Return a {@link List} of {@link RoomDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RoomDTO> findByCriteria(RoomCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Room> specification = createSpecification(criteria);
        return roomMapper.toDto(roomRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RoomDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RoomDTO> findByCriteria(RoomCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Room> specification = createSpecification(criteria);
        return roomRepository.findAll(specification, page)
            .map(roomMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RoomCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Room> specification = createSpecification(criteria);
        return roomRepository.count(specification);
    }

    /**
     * Function to convert {@link RoomCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Room> createSpecification(RoomCriteria criteria) {
        Specification<Room> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Room_.id));
            }
            if (criteria.getNumber() != null) {
                specification = specification.or(buildStringSpecification(criteria.getNumber(), Room_.number));
            }
            if (criteria.getMaxCapacity() != null) {
                specification = specification.or(buildRangeSpecification(criteria.getMaxCapacity(), Room_.maxCapacity));
            }
            if (criteria.getPriceOneAdult() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPriceOneAdult(), Room_.priceOneAdult));
            }
            if (criteria.getPriceTwoAdults() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPriceTwoAdults(), Room_.priceTwoAdults));
            }
            if (criteria.getPriceThreeAdults() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPriceThreeAdults(), Room_.priceThreeAdults));
            }
            if (criteria.getPriceOneAdultOneChild() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPriceOneAdultOneChild(), Room_.priceOneAdultOneChild));
            }
            if (criteria.getPriceTwoAdultsOneChild() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPriceTwoAdultsOneChild(), Room_.priceTwoAdultsOneChild));
            }
            if (criteria.getPriceOneAdultTwoChildren() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPriceOneAdultTwoChildren(), Room_.priceOneAdultTwoChildren));
            }
            if (criteria.getDefectsId() != null) {
                specification = specification.and(buildSpecification(criteria.getDefectsId(),
                    root -> root.join(Room_.defects, JoinType.LEFT).get(Defect_.id)));
            }
        }
        return specification;
    }
}
