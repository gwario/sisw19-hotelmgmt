package com.sisw19.hotelmgnt.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.sisw19.hotelmgnt.domain.Employee;
import com.sisw19.hotelmgnt.domain.*; // for static metamodels
import com.sisw19.hotelmgnt.repository.EmployeeRepository;
import com.sisw19.hotelmgnt.service.dto.EmployeeCriteria;
import com.sisw19.hotelmgnt.service.dto.EmployeeDTO;
import com.sisw19.hotelmgnt.service.mapper.EmployeeMapper;

/**
 * Service for executing complex queries for {@link Employee} entities in the database.
 * The main input is a {@link EmployeeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EmployeeDTO} or a {@link Page} of {@link EmployeeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EmployeeQueryService extends QueryService<Employee> {

    private final Logger log = LoggerFactory.getLogger(EmployeeQueryService.class);

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;

    public EmployeeQueryService(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
    }

    /**
     * Return a {@link List} of {@link EmployeeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EmployeeDTO> findByCriteria(EmployeeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Employee> specification = createSpecification(criteria);
        return employeeMapper.toDto(employeeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EmployeeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EmployeeDTO> findByCriteria(EmployeeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Employee> specification = createSpecification(criteria);
        return employeeRepository.findAll(specification, page)
            .map(employeeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EmployeeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Employee> specification = createSpecification(criteria);
        return employeeRepository.count(specification);
    }

    /**
     * Function to convert {@link EmployeeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Employee> createSpecification(EmployeeCriteria criteria) {
        Specification<Employee> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Employee_.id));
            }
            if (criteria.getEmployeeType() != null) {
                specification = specification.and(buildSpecification(criteria.getEmployeeType(), Employee_.employeeType));
            }
            if (criteria.getShortId() != null) {
                specification = specification.or(buildStringSpecification(criteria.getShortId(), Employee_.shortId));
            }
            if (criteria.getInsuranceNumber() != null) {
                specification = specification.or(buildStringSpecification(criteria.getInsuranceNumber(), Employee_.insuranceNumber));
            }
            if (criteria.getAccountData() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAccountData(), Employee_.accountData));
            }
            if (criteria.getContractType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContractType(), Employee_.contractType));
            }
            if (criteria.getTasks() != null) {
                specification = specification.or(buildStringSpecification(criteria.getTasks(), Employee_.tasks));
            }
            if (criteria.getName() != null) {
                specification = specification.or(buildStringSpecification(criteria.getName(), Employee_.name));
            }
            if (criteria.getBirthday() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBirthday(), Employee_.birthday));
            }
            if (criteria.getGender() != null) {
                specification = specification.and(buildSpecification(criteria.getGender(), Employee_.gender));
            }
            if (criteria.getTel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTel(), Employee_.tel));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), Employee_.email));
            }
            if (criteria.getAddress() != null) {
                specification = specification.or(buildStringSpecification(criteria.getAddress(), Employee_.address));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Employee_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getEmployeeId() != null) {
                specification = specification.and(buildSpecification(criteria.getEmployeeId(),
                    root -> root.join(Employee_.employees, JoinType.LEFT).get(Employee_.id)));
            }
            if (criteria.getVacationsId() != null) {
                specification = specification.and(buildSpecification(criteria.getVacationsId(),
                    root -> root.join(Employee_.vacations, JoinType.LEFT).get(Vacation_.id)));
            }
            if (criteria.getBossId() != null) {
                specification = specification.and(buildSpecification(criteria.getBossId(),
                    root -> root.join(Employee_.boss, JoinType.LEFT).get(Employee_.id)));
            }
        }
        return specification;
    }
}
