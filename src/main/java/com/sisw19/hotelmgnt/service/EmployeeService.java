package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.domain.Employee;
import com.sisw19.hotelmgnt.domain.User;
import com.sisw19.hotelmgnt.domain.Vacation;
import com.sisw19.hotelmgnt.repository.EmployeeRepository;
import com.sisw19.hotelmgnt.repository.UserRepository;
import com.sisw19.hotelmgnt.security.AuthoritiesConstants;
import com.sisw19.hotelmgnt.security.SecurityUtils;
import com.sisw19.hotelmgnt.service.dto.EmployeeDTO;
import com.sisw19.hotelmgnt.service.dto.VacationDTO;
import com.sisw19.hotelmgnt.service.mapper.EmployeeMapper;
import com.sisw19.hotelmgnt.service.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Employee}.
 */
@Service
@Transactional
public class EmployeeService {

    private final Logger log = LoggerFactory.getLogger(EmployeeService.class);

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;
    private final UserRepository userRepository;

    public EmployeeService(EmployeeRepository employeeRepository, EmployeeMapper employeeMapper, UserRepository userRepository) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
        this.userRepository = userRepository;
    }

    /**
     * Save a employee.
     *
     * @param employeeDTO the entity to save.
     * @return the persisted entity.
     */
    public EmployeeDTO save(EmployeeDTO employeeDTO) {
        log.debug("Request to save Employee : {}", employeeDTO);
        Employee employee = employeeMapper.toEntity(employeeDTO);
        if (this.isValidBoss(this.getCurrentUser(), employee)) {

            employee.setEmail("example@example.com");
            employee = employeeRepository.save(employee);
            employee.setEmail(employee.getUser().getEmail());
            employee = employeeRepository.save(employee);
            return employeeMapper.toDto(employee);
        }
        return null;
    }


    /**
     * Check if a user has permissions to accept or reject a vacation request.
     *
     * @param user
     * @param employee
     * @return
     */
    private boolean isValidBoss(User user, Employee employee) {

        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            return true;
        }

        if (employee.getBoss() != null) {
            if (employee.getBoss().getUser().getId() == user.getId()) {
                return true;
            } else {
                throw new ValidationException("You are not allowed to manage this vacation.", "vacation", "unauthorized");
            }
        }
        return false;
    }


    /**
     * Get all the employees.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<EmployeeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Employees");
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            log.debug("Not an admin. Will get only his subworkers");
            User user = getCurrentUser();
            if (user == null) {
                return new PageImpl<EmployeeDTO>(new ArrayList<>());
            }
            return employeeRepository.findAllByBoss(user.getId(), pageable).map(employeeMapper::toDto);
        }
        log.debug("An admin will get all workders");
        return employeeRepository.findAll(pageable)
            .map(employeeMapper::toDto);
    }


    private User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        User user = userRepository.findOneByLogin(authentication.getName()).orElse(null);

        if (user == null) {
            throw new ValidationException("Cannot find the user for current request", "workload", "no_user");
        }
        return user;
    }

    /**
     * Get one employee by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EmployeeDTO> findOne(Long id) {
        log.debug("Request to get Employee : {}", id);
        return employeeRepository.findById(id)
            .map(employeeMapper::toDto);
    }

    /**
     * Delete the employee by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Employee : {}", id);
        employeeRepository.deleteById(id);
    }
}
