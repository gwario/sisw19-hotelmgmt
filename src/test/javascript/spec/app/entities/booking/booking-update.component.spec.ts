import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HotelManagementTestModule } from '../../../test.module';
import { BookingUpdateComponent } from 'app/entities/booking/booking-update.component';
import { BookingService } from 'app/entities/booking/booking.service';
import { Booking } from 'app/shared/model/booking.model';
import { Customer } from 'app/shared/model/customer.model';

describe('Component Tests', () => {
  describe('Booking Management Update Component', () => {
    let comp: BookingUpdateComponent;
    let fixture: ComponentFixture<BookingUpdateComponent>;
    let service: BookingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HotelManagementTestModule],
        declarations: [BookingUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BookingUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BookingUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Booking(123);
        entity.price = 0;
        entity.customer = new Customer(456);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
