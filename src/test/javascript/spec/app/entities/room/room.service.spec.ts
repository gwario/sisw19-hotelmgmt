import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import { RoomService } from 'app/entities/room/room.service';
import { IRoom, Room } from 'app/shared/model/room.model';

describe('Service Tests', () => {
  describe('Room Service', () => {
    let injector: TestBed;
    let service: RoomService;
    let httpMock: HttpTestingController;
    let elemDefault: IRoom;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(RoomService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new Room(0, 'AAAAAAA', 0, 0, 0, 0, 0, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Room', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new Room(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Room', () => {
        const returnedFromService = Object.assign(
          {
            number: 'BBBBBB',
            maxCapacity: 1,
            priceOneAdult: 1,
            priceTwoAdults: 1,
            priceThreeAdults: 1,
            priceOneAdultOneChild: 1,
            priceTwoAdultsOneChild: 1,
            priceOneAdultTwoChildren: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Room', () => {
        const returnedFromService = Object.assign(
          {
            number: 'BBBBBB',
            maxCapacity: 1,
            priceOneAdult: 1,
            priceTwoAdults: 1,
            priceThreeAdults: 1,
            priceOneAdultOneChild: 1,
            priceTwoAdultsOneChild: 1,
            priceOneAdultTwoChildren: 1
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Room', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
