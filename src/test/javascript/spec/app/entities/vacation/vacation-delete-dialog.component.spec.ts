import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HotelManagementTestModule } from '../../../test.module';
import { VacationDeleteDialogComponent } from 'app/entities/vacation/vacation-delete-dialog.component';
import { VacationService } from 'app/entities/vacation/vacation.service';

describe('Component Tests', () => {
  describe('Vacation Management Delete Component', () => {
    let comp: VacationDeleteDialogComponent;
    let fixture: ComponentFixture<VacationDeleteDialogComponent>;
    let service: VacationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HotelManagementTestModule],
        declarations: [VacationDeleteDialogComponent]
      })
        .overrideTemplate(VacationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VacationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(VacationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
