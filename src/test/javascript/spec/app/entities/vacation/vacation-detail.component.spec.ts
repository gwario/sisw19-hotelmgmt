import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HotelManagementTestModule } from '../../../test.module';
import { VacationDetailComponent } from 'app/entities/vacation/vacation-detail.component';
import { Vacation } from 'app/shared/model/vacation.model';

describe('Component Tests', () => {
  describe('Vacation Management Detail Component', () => {
    let comp: VacationDetailComponent;
    let fixture: ComponentFixture<VacationDetailComponent>;
    const route = ({ data: of({ vacation: new Vacation(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HotelManagementTestModule],
        declarations: [VacationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(VacationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(VacationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.vacation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
