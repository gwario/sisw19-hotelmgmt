import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HotelManagementTestModule } from '../../../test.module';
import { DefectDeleteDialogComponent } from 'app/entities/defect/defect-delete-dialog.component';
import { DefectService } from 'app/entities/defect/defect.service';

describe('Component Tests', () => {
  describe('Defect Management Delete Component', () => {
    let comp: DefectDeleteDialogComponent;
    let fixture: ComponentFixture<DefectDeleteDialogComponent>;
    let service: DefectService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HotelManagementTestModule],
        declarations: [DefectDeleteDialogComponent]
      })
        .overrideTemplate(DefectDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DefectDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DefectService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
