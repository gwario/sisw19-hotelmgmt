import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { HotelManagementTestModule } from '../../../test.module';
import { InvoiceComponent } from 'app/entities/invoice/invoice.component';
import { BookingService } from 'app/entities/booking/booking.service';
import { Booking } from 'app/shared/model/booking.model';

describe('Component Tests', () => {
  describe('Invoice Management Component', () => {
    let comp: InvoiceComponent;
    let fixture: ComponentFixture<InvoiceComponent>;
    let service: BookingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HotelManagementTestModule],
        declarations: [InvoiceComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(InvoiceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InvoiceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BookingService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'queryWithInvoice').and.returnValue(
        of(
          new HttpResponse({
            body: [new Booking(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.queryWithInvoice).toHaveBeenCalled();
      expect(comp.bookings[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'queryWithInvoice').and.returnValue(
        of(
          new HttpResponse({
            body: [new Booking(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.queryWithInvoice).toHaveBeenCalled();
      expect(comp.bookings[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should not load a page is the page is the same as the previous page', () => {
      spyOn(service, 'queryWithInvoice').and.callThrough();

      // WHEN
      comp.loadPage(0);

      // THEN
      expect(service.queryWithInvoice).toHaveBeenCalledTimes(0);
    });

    it('should re-initialize the page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'queryWithInvoice').and.returnValue(
        of(
          new HttpResponse({
            body: [new Booking(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);
      comp.clear();

      // THEN
      expect(comp.page).toEqual(0);
      expect(service.queryWithInvoice).toHaveBeenCalledTimes(2);
      expect(comp.bookings[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
