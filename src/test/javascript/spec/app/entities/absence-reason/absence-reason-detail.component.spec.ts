import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HotelManagementTestModule } from '../../../test.module';
import { AbsenceReasonDetailComponent } from 'app/entities/absence-reason/absence-reason-detail.component';
import { AbsenceReason } from 'app/shared/model/absence-reason.model';

describe('Component Tests', () => {
  describe('AbsenceReason Management Detail Component', () => {
    let comp: AbsenceReasonDetailComponent;
    let fixture: ComponentFixture<AbsenceReasonDetailComponent>;
    const route = ({ data: of({ absenceReason: new AbsenceReason(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HotelManagementTestModule],
        declarations: [AbsenceReasonDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AbsenceReasonDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AbsenceReasonDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.absenceReason).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
