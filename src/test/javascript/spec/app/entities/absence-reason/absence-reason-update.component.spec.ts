import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { HotelManagementTestModule } from '../../../test.module';
import { AbsenceReasonUpdateComponent } from 'app/entities/absence-reason/absence-reason-update.component';
import { AbsenceReasonService } from 'app/entities/absence-reason/absence-reason.service';
import { AbsenceReason } from 'app/shared/model/absence-reason.model';

describe('Component Tests', () => {
  describe('AbsenceReason Management Update Component', () => {
    let comp: AbsenceReasonUpdateComponent;
    let fixture: ComponentFixture<AbsenceReasonUpdateComponent>;
    let service: AbsenceReasonService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HotelManagementTestModule],
        declarations: [AbsenceReasonUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AbsenceReasonUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AbsenceReasonUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AbsenceReasonService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AbsenceReason(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AbsenceReason();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
