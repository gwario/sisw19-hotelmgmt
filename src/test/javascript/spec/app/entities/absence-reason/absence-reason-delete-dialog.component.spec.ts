import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HotelManagementTestModule } from '../../../test.module';
import { AbsenceReasonDeleteDialogComponent } from 'app/entities/absence-reason/absence-reason-delete-dialog.component';
import { AbsenceReasonService } from 'app/entities/absence-reason/absence-reason.service';

describe('Component Tests', () => {
  describe('AbsenceReason Management Delete Component', () => {
    let comp: AbsenceReasonDeleteDialogComponent;
    let fixture: ComponentFixture<AbsenceReasonDeleteDialogComponent>;
    let service: AbsenceReasonService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HotelManagementTestModule],
        declarations: [AbsenceReasonDeleteDialogComponent]
      })
        .overrideTemplate(AbsenceReasonDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AbsenceReasonDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AbsenceReasonService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
