package com.sisw19.hotelmgnt.service;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.*;
import com.sisw19.hotelmgnt.domain.enumeration.DefectState;
import com.sisw19.hotelmgnt.domain.enumeration.Gender;
import com.sisw19.hotelmgnt.domain.enumeration.State;
import com.sisw19.hotelmgnt.repository.BookingRepository;
import com.sisw19.hotelmgnt.service.dto.BookingDTO;
import com.sisw19.hotelmgnt.service.exception.ValidationException;
import com.sisw19.hotelmgnt.service.mapper.BookingMapper;
import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Integration tests for {@link UserService}.
 */
@SpringBootTest(classes = HotelManagementApp.class)
@Transactional
public class BookingServiceIT {

    /* Booking */
    private static final Double DEFAULT_BOOKING_DISCOUNT_PERCENT = 0D;
    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(0);
    private static final ZonedDateTime DEFAULT_START_DATE = LocalDate.of(2020, 1, 1).atStartOfDay(ZoneOffset.UTC);
    private static final ZonedDateTime DEFAULT_END_DATE = LocalDate.of(2020, 1, 5).atStartOfDay(ZoneOffset.UTC);
    private static final ZonedDateTime OVERLAPPING_START_DATE = LocalDate.of(2020, 1, 3).atStartOfDay(ZoneOffset.UTC);
    private static final ZonedDateTime OVERLAPPING_END_DATE = LocalDate.of(2020, 1, 6).atStartOfDay(ZoneOffset.UTC);

    private static final Integer DEFAULT_CANCELLATION_NOTICE_NIGHTS = 0;
    private static final State DEFAULT_BOOKING_STATE = State.ACTIVE;

    /* Customer */
    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final Double DEFAULT_CUSTOMER_DISCOUNT_PERCENT = 10.0;
    private static final String DEFAULT_WEBPAGE = "AAAAAAAAAA";
    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final String DEFAULT_TEL = "AAAAAAAAAA";
    private static final String DEFAULT_EMAIL = "D@%b.(";
    private static final String DEFAULT_BILLING_ADDRESS = "AAAAAAAAAA";

    /* Room */
    private static final String DEFAULT_NUMBER = "01";
    private static final Integer DEFAULT_MAX_CAPACITY = 3;
    private static final BigDecimal DEFAULT_PRICE_ONE_ADULT = new BigDecimal(10);
    private static final BigDecimal DEFAULT_PRICE_TWO_ADULTS = new BigDecimal(20);
    private static final BigDecimal DEFAULT_PRICE_THREE_ADULTS = new BigDecimal(30);
    private static final BigDecimal DEFAULT_PRICE_ONE_ADULT_ONE_CHILD = new BigDecimal(11);
    private static final BigDecimal DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD = new BigDecimal(21);
    private static final BigDecimal DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN = new BigDecimal(12);

    /* Defect */
    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final DefectState DEFAULT_DEFECT_STATE = DefectState.IN_PROGRESS;
    private static final Double DEFAULT_DEFECT_DISCOUNT_PERCENT = 10.0;

    @Autowired
    private EntityManager em;

    @Autowired
    private BookingMapper bookingMapper;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingRepository bookingRepository;

    private Customer customer;
    private Room room1;
    private Room room2;
    private Defect defect1;
    private Defect defect2;
    private Defect defect3;
    private Defect defect4;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Booking createBookingEntity(EntityManager em) {
        Booking booking = new Booking()
            .discountPercent(DEFAULT_BOOKING_DISCOUNT_PERCENT)
            .price(DEFAULT_PRICE)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .cancellationNoticeNights(DEFAULT_CANCELLATION_NOTICE_NIGHTS)
            .bookingState(DEFAULT_BOOKING_STATE);
        return booking;
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Customer createCustomerEntity(EntityManager em) {
        Customer customer = new Customer()
            .companyName(DEFAULT_COMPANY_NAME)
            .notes(DEFAULT_NOTES)
            .discountPercent(DEFAULT_CUSTOMER_DISCOUNT_PERCENT)
            .webpage(DEFAULT_WEBPAGE)
            .fax(DEFAULT_FAX)
            .name(DEFAULT_NAME)
            .birthday(DEFAULT_BIRTHDAY)
            .gender(DEFAULT_GENDER)
            .tel(DEFAULT_TEL)
            .email(DEFAULT_EMAIL)
            .billingAddress(DEFAULT_BILLING_ADDRESS);
        return customer;
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Room createRoomEntity(EntityManager em) {
        Room room = new Room()
            .number(DEFAULT_NUMBER)
            .maxCapacity(DEFAULT_MAX_CAPACITY)
            .priceOneAdult(DEFAULT_PRICE_ONE_ADULT)
            .priceTwoAdults(DEFAULT_PRICE_TWO_ADULTS)
            .priceThreeAdults(DEFAULT_PRICE_THREE_ADULTS)
            .priceOneAdultOneChild(DEFAULT_PRICE_ONE_ADULT_ONE_CHILD)
            .priceTwoAdultsOneChild(DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD)
            .priceOneAdultTwoChildren(DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);
        return room;
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Defect createDefectEntity(EntityManager em) {
        Defect defect = new Defect()
            .description(DEFAULT_DESCRIPTION)
            .defectState(DEFAULT_DEFECT_STATE)
            .discountPercent(DEFAULT_DEFECT_DISCOUNT_PERCENT);
        return defect;
    }

    @BeforeEach
    public void init() {
        customer = createCustomerEntity(em);
        room1 = createRoomEntity(em);
        room1.setNumber("01");
        room2 = createRoomEntity(em);
        room2.setNumber("02");
        defect1 = createDefectEntity(em);
        defect2 = createDefectEntity(em);
        defect3 = createDefectEntity(em);
        defect4 = createDefectEntity(em);
    }

    @Test
    public void priceOneRoomWithoutAnyDiscounts() throws Exception {
        System.out.println("===priceOneRoomWithoutAnyDiscounts=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setNumberAdults(1);
        roomOccupancy.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(0.0));
        roomOccupancy.setRoom(room1);
        roomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(10*4), Percentage.withPercentage(0.001));
    }

    @Test
    public void priceOneRoomWithCustomerDiscount() throws Exception {
        System.out.println("===priceOneRoomWithCustomerDiscount=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(10.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setNumberAdults(1);
        roomOccupancy.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(0.0));
        roomOccupancy.setRoom(room1);
        roomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(10*4-4), Percentage.withPercentage(0.001));
    }

    @Test
    public void priceOneRoomWithBookingDiscount() throws Exception {
        System.out.println("===priceOneRoomWithBookingDiscount=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(10.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setNumberAdults(1);
        roomOccupancy.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(0.0));
        roomOccupancy.setRoom(room1);
        roomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(10*4-4), Percentage.withPercentage(0.001));
    }

    @Test
    public void priceOneRoomWithCustomerAndBookingDiscount() throws Exception {
        System.out.println("===priceOneRoomWithCustomerAndBookingDiscount=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(10.0);

        customer.setDiscountPercent(10.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setNumberAdults(1);
        roomOccupancy.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(0.0));
        roomOccupancy.setRoom(room1);
        roomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(10*4-4-(10*4-4)*0.1), Percentage.withPercentage(0.001));
    }

    @Test
    public void priceOneRoomWithOnlyNewDefectDiscount() throws Exception {
        System.out.println("===priceOneRoomWithOnlyNewDefectDiscount=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setNumberAdults(1);
        roomOccupancy.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(0.0));
        defect1.setDefectState(DefectState.NEW);
        defect1.discountPercent(10.0);
        defect1.setRoom(room1);
        room1.addDefects(defect1);
        roomOccupancy.setRoom(room1);
        roomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.persist(defect1);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(9*4), Percentage.withPercentage(0.001));
    }

    @Test
    public void priceOneRoomWithInProgressDefect() throws Exception {
        System.out.println("===priceOneRoomWithInProgressDefect=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setNumberAdults(1);
        roomOccupancy.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(0.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(0.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(0.0));
        defect1.setDefectState(DefectState.IN_PROGRESS);
        defect1.discountPercent(10.0);
        defect1.setRoom(room1);
        room1.addDefects(defect1);
        roomOccupancy.setRoom(room1);
        roomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.persist(defect1);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(9*4), Percentage.withPercentage(0.001));
    }

    @Test
    public void priceOneRoomWithResolvedDefect() throws Exception {
        System.out.println("===priceOneRoomWithResolvedDefect=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setNumberAdults(1);
        roomOccupancy.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(99.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(99.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(99.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(99.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(99.0));
        defect1.setDefectState(DefectState.RESOLVED);
        defect1.discountPercent(10.0);
        defect1.setRoom(room1);
        room1.addDefects(defect1);
        roomOccupancy.setRoom(room1);
        roomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.persist(defect1);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(10*4), Percentage.withPercentage(0.001));
    }

    @Test
    public void priceOneRoomWithSeveralDefects() throws Exception {
        System.out.println("===priceOneRoomWithSeveralDefects=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy = new RoomOccupancy();
        roomOccupancy.setNumberAdults(1);
        roomOccupancy.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(99.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(99.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(99.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(99.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(99.0));

        defect1.setDefectState(DefectState.NEW);
        defect1.discountPercent(3.0);
        defect1.setRoom(room1);
        room1.addDefects(defect1);

        defect2.setDefectState(DefectState.IN_PROGRESS);
        defect2.discountPercent(3.0);
        defect2.setRoom(room1);
        room1.addDefects(defect2);

        defect3.setDefectState(DefectState.NEW);
        defect3.discountPercent(4.0);
        defect3.setRoom(room1);
        room1.addDefects(defect3);

        roomOccupancy.setRoom(room1);
        roomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.persist(defect1);
        em.persist(defect2);
        em.persist(defect3);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(9*4), Percentage.withPercentage(0.001));
    }

    @Test
    public void priceTwoRoomsWithSeveralDefects() throws Exception {
        System.out.println("===priceTwoRoomsWithSeveralDefects=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy1 = new RoomOccupancy();
        roomOccupancy1.setNumberAdults(1);
        roomOccupancy1.setNumberChildren(0);
        room1.setPriceOneAdult(BigDecimal.valueOf(10.0));
        room1.setPriceOneAdultOneChild(BigDecimal.valueOf(99.0));
        room1.setPriceOneAdultTwoChildren(BigDecimal.valueOf(99.0));
        room1.setPriceTwoAdults(BigDecimal.valueOf(99.0));
        room1.setPriceTwoAdultsOneChild(BigDecimal.valueOf(99.0));
        room1.setPriceThreeAdults(BigDecimal.valueOf(99.0));

        defect1.setDefectState(DefectState.NEW);
        defect1.discountPercent(7.0);
        defect1.setRoom(room1);
        room1.addDefects(defect1);

        defect2.setDefectState(DefectState.IN_PROGRESS);
        defect2.discountPercent(3.0);
        defect2.setRoom(room1);
        room1.addDefects(defect2);

        roomOccupancy1.setRoom(room1);
        roomOccupancy1.setBooking(booking);

        RoomOccupancy roomOccupancy2 = new RoomOccupancy();
        roomOccupancy2.setNumberAdults(2);
        roomOccupancy2.setNumberChildren(1);

        room2.setPriceOneAdult(BigDecimal.valueOf(99.0));
        room2.setPriceOneAdultOneChild(BigDecimal.valueOf(99.0));
        room2.setPriceOneAdultTwoChildren(BigDecimal.valueOf(99.0));
        room2.setPriceTwoAdults(BigDecimal.valueOf(99.0));
        room2.setPriceTwoAdultsOneChild(BigDecimal.valueOf(10.0));
        room2.setPriceThreeAdults(BigDecimal.valueOf(99.0));

        defect3.setDefectState(DefectState.NEW);
        defect3.discountPercent(5.0);
        defect3.setRoom(room2);
        room2.addDefects(defect3);

        defect4.setDefectState(DefectState.NEW);
        defect4.discountPercent(5.0);
        defect4.setRoom(room2);
        room2.addDefects(defect4);

        roomOccupancy2.setRoom(room2);
        roomOccupancy2.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy1);
        rooms.add(roomOccupancy2);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.persist(room2);
        em.persist(defect1);
        em.persist(defect2);
        em.persist(defect3);
        em.persist(defect4);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        assertThat(bookingService.setCurrentPrice(bookingDTO).getPrice()).isCloseTo(BigDecimal.valueOf(9*4+9*4), Percentage.withPercentage(0.001));
    }

    @Test
    public void duplicateRoomInBookingShouldThrowException() throws Exception {
        System.out.println("===duplicateRoomInBookingShouldThrowException=====================");

        // Initialize the database
        Booking booking = createBookingEntity(em);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy roomOccupancy1 = new RoomOccupancy();
        roomOccupancy1.setNumberAdults(1);
        roomOccupancy1.setNumberChildren(0);
        roomOccupancy1.setRoom(room1);
        roomOccupancy1.setBooking(booking);

        RoomOccupancy roomOccupancy2 = new RoomOccupancy();
        roomOccupancy2.setNumberAdults(2);
        roomOccupancy2.setNumberChildren(1);
        roomOccupancy2.setRoom(room1);
        roomOccupancy2.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(roomOccupancy1);
        rooms.add(roomOccupancy2);
        booking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.flush();

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        ValidationException ex = assertThrows(ValidationException.class, () -> bookingService.save(bookingDTO));
        assertThat(ex.getTitle()).isEqualTo("Booking contains the same room multiple times!");
        assertThat(ex.getEntityName()).isEqualTo("RoomOccupancy");
        assertThat(ex.getErrorKey()).isEqualTo("business");
    }

    @Test
    public void occupiedRoomInBookingShouldThrowException() throws Exception {
        System.out.println("===occupiedRoomInBookingShouldThrowException=====================");

        // Initialize the database
        Booking existingBooking = createBookingEntity(em);
        existingBooking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        existingBooking.setCustomer(customer);

        RoomOccupancy existingRoomOccupancy = new RoomOccupancy();
        existingRoomOccupancy.setNumberAdults(1);
        existingRoomOccupancy.setNumberChildren(0);
        existingRoomOccupancy.setRoom(room1);
        existingRoomOccupancy.setBooking(existingBooking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(existingRoomOccupancy);
        existingBooking.setRooms(rooms);

        em.persist(customer);
        em.persist(room1);
        em.persist(existingBooking);
        em.flush();

        //Try to use same room in another overlapping booking
        Booking newBooking = createBookingEntity(em);
        newBooking.setStartDate(OVERLAPPING_START_DATE);
        newBooking.setEndDate(OVERLAPPING_END_DATE);
        newBooking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        newBooking.setCustomer(customer);

        RoomOccupancy newRoomOccupancy = new RoomOccupancy();
        newRoomOccupancy.setNumberAdults(1);
        newRoomOccupancy.setNumberChildren(0);
        newRoomOccupancy.setRoom(room1);
        newRoomOccupancy.setBooking(newBooking);

        Set<RoomOccupancy> newRooms = new HashSet<>();
        newRooms.add(newRoomOccupancy);
        newBooking.setRooms(newRooms);

        BookingDTO bookingDTO = bookingMapper.toDto(newBooking);

        ValidationException ex = assertThrows(ValidationException.class, () -> bookingService.save(bookingDTO));
        assertThat(ex.getTitle()).isEqualTo("Booking contains unavailable rooms!");
        assertThat(ex.getEntityName()).isEqualTo("RoomOccupancy");
        assertThat(ex.getErrorKey()).isEqualTo("business");
    }

    @Test
    public void endDateBeforeStartDateShouldThrowException() throws Exception {
        System.out.println("===endDateBeforeStartDateShouldThrowException=====================");

        // Initialize database
        em.persist(customer);
        em.persist(room1);
        em.flush();


        Booking booking = createBookingEntity(em);
        booking.setStartDate(DEFAULT_END_DATE);
        booking.setEndDate(DEFAULT_START_DATE);
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy existingRoomOccupancy = new RoomOccupancy();
        existingRoomOccupancy.setNumberAdults(1);
        existingRoomOccupancy.setNumberChildren(0);
        existingRoomOccupancy.setRoom(room1);
        existingRoomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(existingRoomOccupancy);
        booking.setRooms(rooms);

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        ValidationException ex = assertThrows(ValidationException.class, () -> bookingService.save(bookingDTO));
        assertThat(ex.getTitle()).isEqualTo("End date is before start date!");
        assertThat(ex.getEntityName()).isEqualTo("Booking");
        assertThat(ex.getErrorKey()).isEqualTo("business");
    }

    @Test
    public void durationShorterThanOneDayShouldThrowException() throws Exception {
        System.out.println("===durationShorterThanOneDayShouldThrowException=====================");

        // Initialize database
        em.persist(customer);
        em.persist(room1);
        em.flush();


        Booking booking = createBookingEntity(em);
        booking.setStartDate(LocalDate.of(2020, 1, 1).atStartOfDay(ZoneOffset.UTC));
        booking.setEndDate(LocalDate.of(2020, 1, 2).atStartOfDay(ZoneOffset.UTC).minusHours(5));
        booking.setDiscountPercent(0.0);

        customer.setDiscountPercent(0.0);
        booking.setCustomer(customer);

        RoomOccupancy existingRoomOccupancy = new RoomOccupancy();
        existingRoomOccupancy.setNumberAdults(1);
        existingRoomOccupancy.setNumberChildren(0);
        existingRoomOccupancy.setRoom(room1);
        existingRoomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(existingRoomOccupancy);
        booking.setRooms(rooms);

        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        ValidationException ex = assertThrows(ValidationException.class, () -> bookingService.save(bookingDTO));
        assertThat(ex.getTitle()).isEqualTo("Duration is less than a day!");
        assertThat(ex.getEntityName()).isEqualTo("Booking");
        assertThat(ex.getErrorKey()).isEqualTo("business");
    }

    @Test
    public void cancelAfterEndOfCancellationPeriodShouldThrowException() throws Exception {
        System.out.println("===cancelAfterEndOfCancellationPeriodShouldThrowException=====================");

        // Initialize database
        em.persist(customer);
        em.persist(room1);

        Booking booking = createBookingEntity(em);
        booking.setStartDate(LocalDate.now().atStartOfDay(ZoneOffset.UTC).plusDays(1)); //starts in one day
        booking.setEndDate(LocalDate.now().atStartOfDay(ZoneOffset.UTC).plusWeeks(2)); //ends in 2 weeks
        booking.setCancellationNoticeNights(2); //cancellation only possible until 2 days before start (until yesterday)

        booking.setCustomer(customer);
        RoomOccupancy existingRoomOccupancy = new RoomOccupancy();
        existingRoomOccupancy.setNumberAdults(1);
        existingRoomOccupancy.setNumberChildren(0);
        existingRoomOccupancy.setRoom(room1);
        existingRoomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(existingRoomOccupancy);
        booking.setRooms(rooms);

        em.persist(booking);
        em.flush();



        ValidationException ex = assertThrows(ValidationException.class, () -> bookingService.cancel(booking.getId()));
        assertThat(ex.getTitle()).isEqualTo("Cancellation not possible anymore!");
        assertThat(ex.getEntityName()).isEqualTo("Booking");
        assertThat(ex.getErrorKey()).isEqualTo("business");
    }

    @Test
    public void cancelWithinCancellationPeriodShouldSucceed() throws Exception {
        System.out.println("===cancelWithinCancellationPeriodShouldSucceed=====================");

        // Initialize database
        em.persist(customer);
        em.persist(room1);

        Booking booking = createBookingEntity(em);
        booking.setStartDate(LocalDate.now().atStartOfDay(ZoneOffset.UTC).plusWeeks(1)); //starts in one week
        booking.setEndDate(LocalDate.now().atStartOfDay(ZoneOffset.UTC).plusWeeks(2)); //ends in 2 weeks
        booking.setCancellationNoticeNights(2); //cancellation only possible until 2 days before start
        booking.setCustomer(customer);
        RoomOccupancy existingRoomOccupancy = new RoomOccupancy();
        existingRoomOccupancy.setNumberAdults(1);
        existingRoomOccupancy.setNumberChildren(0);
        existingRoomOccupancy.setRoom(room1);
        existingRoomOccupancy.setBooking(booking);

        Set<RoomOccupancy> rooms = new HashSet<>();
        rooms.add(existingRoomOccupancy);
        booking.setRooms(rooms);

        em.persist(booking);
        em.flush();

        bookingService.cancel(booking.getId());
    }
}
