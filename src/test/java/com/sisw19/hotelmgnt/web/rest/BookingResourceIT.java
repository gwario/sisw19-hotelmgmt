package com.sisw19.hotelmgnt.web.rest;

import static com.sisw19.hotelmgnt.web.rest.TestUtil.createFormattingConversionService;
import static com.sisw19.hotelmgnt.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.Booking;
import com.sisw19.hotelmgnt.domain.Customer;
import com.sisw19.hotelmgnt.domain.Room;
import com.sisw19.hotelmgnt.domain.enumeration.State;
import com.sisw19.hotelmgnt.repository.BookingRepository;
import com.sisw19.hotelmgnt.service.BookingQueryService;
import com.sisw19.hotelmgnt.service.BookingService;
import com.sisw19.hotelmgnt.service.dto.BookingDTO;
import com.sisw19.hotelmgnt.service.dto.CustomerDTO;
import com.sisw19.hotelmgnt.service.dto.RoomDTO;
import com.sisw19.hotelmgnt.service.dto.RoomOccupancyDTO;
import com.sisw19.hotelmgnt.service.mapper.BookingMapper;
import com.sisw19.hotelmgnt.web.rest.errors.ExceptionTranslator;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;
/**
 * Integration tests for the {@link BookingResource} REST controller.
 */
@SpringBootTest(classes = HotelManagementApp.class)
public class BookingResourceIT {

    private static final Double DEFAULT_DISCOUNT_PERCENT = 0D;
    private static final Double UPDATED_DISCOUNT_PERCENT = 1D;

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(1);

    private static final ZonedDateTime DEFAULT_START_DATE = LocalDate.of(2020, 1, 1).atStartOfDay(ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_DATE = LocalDate.of(2020, 2, 1).atStartOfDay(ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_END_DATE = LocalDate.of(2020, 1, 5).atStartOfDay(ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END_DATE = LocalDate.of(2020, 2, 5).atStartOfDay(ZoneOffset.UTC);

    private static final Integer DEFAULT_CANCELLATION_NOTICE_NIGHTS = 0;
    private static final Integer UPDATED_CANCELLATION_NOTICE_NIGHTS = 1;

    private static final State DEFAULT_BOOKING_STATE = State.ACTIVE;
    private static final State UPDATED_BOOKING_STATE = State.CANCELLED;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private BookingMapper bookingMapper;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingQueryService bookingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBookingMockMvc;

    private Booking booking;
    private Customer existingCustomer;
    private Room existingRoom;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BookingResource bookingResource = new BookingResource(bookingService, bookingQueryService);
        this.restBookingMockMvc = MockMvcBuilders.standaloneSetup(bookingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Booking createEntity(EntityManager em) {
        Booking booking = new Booking()
            .discountPercent(DEFAULT_DISCOUNT_PERCENT)
            .price(DEFAULT_PRICE)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .cancellationNoticeNights(DEFAULT_CANCELLATION_NOTICE_NIGHTS)
            .bookingState(DEFAULT_BOOKING_STATE);
        return booking;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Booking createUpdatedEntity(EntityManager em) {
        Booking booking = new Booking()
            .discountPercent(UPDATED_DISCOUNT_PERCENT)
            .price(UPDATED_PRICE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .cancellationNoticeNights(UPDATED_CANCELLATION_NOTICE_NIGHTS)
            .bookingState(UPDATED_BOOKING_STATE);
        return booking;
    }

    @BeforeEach
    public void initTest() {
        existingCustomer = CustomerResourceIT.createEntity(em);
        existingRoom = RoomResourceIT.createEntity(em);
        em.persist(existingCustomer);
        em.persist(existingRoom);

        booking = createEntity(em);
    }

    @Test
    @Transactional
    public void createBooking() throws Exception {
        int databaseSizeBeforeCreate = bookingRepository.findAll().size();

        // Create the Booking
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(existingCustomer.getId());
        bookingDTO.setCustomer(customerDTO);

        RoomOccupancyDTO roomOccupancyDTO = new RoomOccupancyDTO();
        roomOccupancyDTO.setRoom(new RoomDTO().id(existingRoom.getId()));
        roomOccupancyDTO.setNumberAdults(1);
        roomOccupancyDTO.setNumberChildren(0);
        Set<RoomOccupancyDTO> rooms = new HashSet<>();
        rooms.add(roomOccupancyDTO);
        bookingDTO.setRooms(rooms);

        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isCreated());

        // Validate the Booking in the database
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeCreate + 1);
        Booking testBooking = bookingList.get(bookingList.size() - 1);
        assertThat(testBooking.getDiscountPercent()).isEqualTo(DEFAULT_DISCOUNT_PERCENT);
        assertThat(testBooking.getPrice()).isCloseTo(DEFAULT_PRICE, Percentage.withPercentage(0.0001));
        assertThat(testBooking.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testBooking.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testBooking.getCancellationNoticeNights()).isEqualTo(DEFAULT_CANCELLATION_NOTICE_NIGHTS);
        assertThat(testBooking.getBookingState()).isEqualTo(DEFAULT_BOOKING_STATE);

        assertThat(testBooking.getCustomer()).isNotNull();
        assertThat(testBooking.getCustomer().getId()).isEqualTo(existingCustomer.getId());
        assertThat(testBooking.getRooms().size()).isEqualTo(1);
        assertThat(testBooking.getRooms().stream().findFirst().get().getRoom().getId()).isEqualTo(existingRoom.getId());
    }

    @Test
    @Transactional
    public void createBookingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bookingRepository.findAll().size();

        // Create the Booking with an existing ID
        booking.setId(1L);
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Booking in the database
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDiscountPercentIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setDiscountPercent(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setPrice(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setStartDate(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setEndDate(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCancellationNoticeNightsIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setCancellationNoticeNights(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBookingStateIsRequired() throws Exception {
        int databaseSizeBeforeTest = bookingRepository.findAll().size();
        // set the field null
        booking.setBookingState(null);

        // Create the Booking, which fails.
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        restBookingMockMvc.perform(post("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isBadRequest());

        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBookings() throws Exception {
        // Initialize the database
        booking.setCustomer(existingCustomer);
        bookingRepository.saveAndFlush(booking);

        // Get all the bookingList
        restBookingMockMvc.perform(get("/api/bookings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(booking.getId().intValue())))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.doubleValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(sameInstant(DEFAULT_START_DATE))))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(sameInstant(DEFAULT_END_DATE))))
            .andExpect(jsonPath("$.[*].cancellationNoticeNights").value(hasItem(DEFAULT_CANCELLATION_NOTICE_NIGHTS)))
            .andExpect(jsonPath("$.[*].bookingState").value(hasItem(DEFAULT_BOOKING_STATE.toString())));
    }

    @Test
    @Transactional
    public void getBooking() throws Exception {
        // Initialize the database
        booking.setCustomer(existingCustomer);
        bookingRepository.saveAndFlush(booking);

        // Get the booking
        restBookingMockMvc.perform(get("/api/bookings/{id}", booking.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(booking.getId().intValue()))
            .andExpect(jsonPath("$.discountPercent").value(DEFAULT_DISCOUNT_PERCENT.doubleValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()))
            .andExpect(jsonPath("$.startDate").value(sameInstant(DEFAULT_START_DATE)))
            .andExpect(jsonPath("$.endDate").value(sameInstant(DEFAULT_END_DATE)))
            .andExpect(jsonPath("$.cancellationNoticeNights").value(DEFAULT_CANCELLATION_NOTICE_NIGHTS))
            .andExpect(jsonPath("$.bookingState").value(DEFAULT_BOOKING_STATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBooking() throws Exception {
        // Get the booking
        restBookingMockMvc.perform(get("/api/bookings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBooking() throws Exception {
        // Initialize the database
        booking.setCustomer(existingCustomer);
        bookingRepository.saveAndFlush(booking);

        int databaseSizeBeforeUpdate = bookingRepository.findAll().size();

        // Update the booking
        Booking updatedBooking = bookingRepository.findById(booking.getId()).get();
        // Disconnect from session so that the updates on updatedBooking are not directly saved in db
        em.detach(updatedBooking);
        updatedBooking
            .discountPercent(UPDATED_DISCOUNT_PERCENT)
            .price(UPDATED_PRICE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .cancellationNoticeNights(UPDATED_CANCELLATION_NOTICE_NIGHTS)
            .bookingState(UPDATED_BOOKING_STATE);
        BookingDTO bookingDTO = bookingMapper.toDto(updatedBooking);

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(existingCustomer.getId());
        bookingDTO.setCustomer(customerDTO);
        RoomOccupancyDTO roomOccupancyDTO = new RoomOccupancyDTO();
        roomOccupancyDTO.setRoom(new RoomDTO().id(existingRoom.getId()));
        roomOccupancyDTO.setNumberAdults(1);
        roomOccupancyDTO.setNumberChildren(0);
        Set<RoomOccupancyDTO> rooms = new HashSet<>();
        rooms.add(roomOccupancyDTO);
        bookingDTO.setRooms(rooms);

        restBookingMockMvc.perform(put("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isOk());

        // Validate the Booking in the database
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeUpdate);
        Booking testBooking = bookingList.get(bookingList.size() - 1);
        assertThat(testBooking.getDiscountPercent()).isEqualTo(UPDATED_DISCOUNT_PERCENT);
//        assertThat(testBooking.getPrice()).isCloseTo(UPDATED_PRICE, Percentage.withPercentage(0.0001)); //covered in booking service tests
        assertThat(testBooking.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testBooking.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testBooking.getCancellationNoticeNights()).isEqualTo(UPDATED_CANCELLATION_NOTICE_NIGHTS);
        assertThat(testBooking.getBookingState()).isEqualTo(UPDATED_BOOKING_STATE);

        assertThat(testBooking.getCustomer()).isNotNull();
        assertThat(testBooking.getCustomer().getId()).isEqualTo(existingCustomer.getId());
        assertThat(testBooking.getRooms().size()).isEqualTo(1);
        assertThat(testBooking.getRooms().stream().findFirst().get().getRoom().equals(existingRoom)).isTrue();
    }

    @Test
    @Transactional
    public void updateBookingWithNotExistingCustomerReturns404() throws Exception {
        // Initialize the database
        booking.setCustomer(existingCustomer);
        bookingRepository.saveAndFlush(booking);

        // Update the booking
        Booking updatedBooking = bookingRepository.findById(booking.getId()).get();
        // Disconnect from session so that the updates on updatedBooking are not directly saved in db
        em.detach(updatedBooking);
        updatedBooking
            .discountPercent(UPDATED_DISCOUNT_PERCENT)
            .price(UPDATED_PRICE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .cancellationNoticeNights(UPDATED_CANCELLATION_NOTICE_NIGHTS)
            .bookingState(UPDATED_BOOKING_STATE);
        BookingDTO bookingDTO = bookingMapper.toDto(updatedBooking);

        CustomerDTO customer2 = new CustomerDTO();
        customer2.setId(999L);
        bookingDTO.setCustomer(customer2);

        RoomOccupancyDTO roomOccupancyDTO = new RoomOccupancyDTO();
        roomOccupancyDTO.setRoom(new RoomDTO().id(existingRoom.getId()));
        roomOccupancyDTO.setNumberAdults(1);
        roomOccupancyDTO.setNumberChildren(0);
        Set<RoomOccupancyDTO> rooms = new HashSet<>();
        rooms.add(roomOccupancyDTO);
        bookingDTO.setRooms(rooms);

        restBookingMockMvc.perform(put("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNonExistingBooking() throws Exception {
        int databaseSizeBeforeUpdate = bookingRepository.findAll().size();

        // Create the Booking
        BookingDTO bookingDTO = bookingMapper.toDto(booking);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBookingMockMvc.perform(put("/api/bookings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Booking in the database
        List<Booking> bookingList = bookingRepository.findAll();
        assertThat(bookingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void cancelBooking() throws Exception {
        // Initialize the database
        booking.setStartDate(LocalDate.now().atStartOfDay(ZoneOffset.UTC).plusDays(2)); //starts in 2 days
        booking.setEndDate(LocalDate.now().atStartOfDay(ZoneOffset.UTC).plusWeeks(1)); //ends in 1 weeks
        booking.setCancellationNoticeNights(1); //cancellation only possible until 1 days before start
        booking.setBookingState(State.ACTIVE);
        booking.setCustomer(existingCustomer);
        bookingRepository.saveAndFlush(booking);

        assertThat(bookingRepository.getOne(booking.getId()).getBookingState()).isEqualTo(State.ACTIVE);

        restBookingMockMvc.perform(delete("/api/bookings/{id}", booking.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        assertThat(bookingRepository.getOne(booking.getId()).getBookingState()).isEqualTo(State.CANCELLED);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Booking.class);
        Booking booking1 = new Booking();
        booking1.setId(1L);
        Booking booking2 = new Booking();
        booking2.setId(booking1.getId());
        assertThat(booking1).isEqualTo(booking2);
        booking2.setId(2L);
        assertThat(booking1).isNotEqualTo(booking2);
        booking1.setId(null);
        assertThat(booking1).isNotEqualTo(booking2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BookingDTO.class);
        BookingDTO bookingDTO1 = new BookingDTO();
        bookingDTO1.setId(1L);
        BookingDTO bookingDTO2 = new BookingDTO();
        assertThat(bookingDTO1).isNotEqualTo(bookingDTO2);
        bookingDTO2.setId(bookingDTO1.getId());
        assertThat(bookingDTO1).isEqualTo(bookingDTO2);
        bookingDTO2.setId(2L);
        assertThat(bookingDTO1).isNotEqualTo(bookingDTO2);
        bookingDTO1.setId(null);
        assertThat(bookingDTO1).isNotEqualTo(bookingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bookingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bookingMapper.fromId(null)).isNull();
    }
}
