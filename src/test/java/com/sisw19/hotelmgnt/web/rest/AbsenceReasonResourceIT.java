package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.AbsenceReason;
import com.sisw19.hotelmgnt.repository.AbsenceReasonRepository;
import com.sisw19.hotelmgnt.service.AbsenceReasonService;
import com.sisw19.hotelmgnt.service.dto.AbsenceReasonDTO;
import com.sisw19.hotelmgnt.service.mapper.AbsenceReasonMapper;
import com.sisw19.hotelmgnt.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.sisw19.hotelmgnt.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AbsenceReasonResource} REST controller.
 */
@SpringBootTest(classes = HotelManagementApp.class)
public class AbsenceReasonResourceIT {

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private AbsenceReasonRepository absenceReasonRepository;

    @Autowired
    private AbsenceReasonMapper absenceReasonMapper;

    @Autowired
    private AbsenceReasonService absenceReasonService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAbsenceReasonMockMvc;

    private AbsenceReason absenceReason;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AbsenceReasonResource absenceReasonResource = new AbsenceReasonResource(absenceReasonService);
        this.restAbsenceReasonMockMvc = MockMvcBuilders.standaloneSetup(absenceReasonResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AbsenceReason createEntity(EntityManager em) {
        AbsenceReason absenceReason = new AbsenceReason()
            .reason(DEFAULT_REASON);
        return absenceReason;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AbsenceReason createUpdatedEntity(EntityManager em) {
        AbsenceReason absenceReason = new AbsenceReason()
            .reason(UPDATED_REASON);
        return absenceReason;
    }

    @BeforeEach
    public void initTest() {
        absenceReason = createEntity(em);
    }

    @Test
    @Transactional
    public void createAbsenceReason() throws Exception {
        int databaseSizeBeforeCreate = absenceReasonRepository.findAll().size();

        // Create the AbsenceReason
        AbsenceReasonDTO absenceReasonDTO = absenceReasonMapper.toDto(absenceReason);
        restAbsenceReasonMockMvc.perform(post("/api/absence-reasons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(absenceReasonDTO)))
            .andExpect(status().isCreated());

        // Validate the AbsenceReason in the database
        List<AbsenceReason> absenceReasonList = absenceReasonRepository.findAll();
        assertThat(absenceReasonList).hasSize(databaseSizeBeforeCreate + 1);
        AbsenceReason testAbsenceReason = absenceReasonList.get(absenceReasonList.size() - 1);
        assertThat(testAbsenceReason.getReason()).isEqualTo(DEFAULT_REASON);
    }

    @Test
    @Transactional
    public void createAbsenceReasonWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = absenceReasonRepository.findAll().size();

        // Create the AbsenceReason with an existing ID
        absenceReason.setId(1L);
        AbsenceReasonDTO absenceReasonDTO = absenceReasonMapper.toDto(absenceReason);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAbsenceReasonMockMvc.perform(post("/api/absence-reasons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(absenceReasonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AbsenceReason in the database
        List<AbsenceReason> absenceReasonList = absenceReasonRepository.findAll();
        assertThat(absenceReasonList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkReasonIsRequired() throws Exception {
        int databaseSizeBeforeTest = absenceReasonRepository.findAll().size();
        // set the field null
        absenceReason.setReason(null);

        // Create the AbsenceReason, which fails.
        AbsenceReasonDTO absenceReasonDTO = absenceReasonMapper.toDto(absenceReason);

        restAbsenceReasonMockMvc.perform(post("/api/absence-reasons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(absenceReasonDTO)))
            .andExpect(status().isBadRequest());

        List<AbsenceReason> absenceReasonList = absenceReasonRepository.findAll();
        assertThat(absenceReasonList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAbsenceReasons() throws Exception {
        // Initialize the database
        absenceReasonRepository.saveAndFlush(absenceReason);

        // Get all the absenceReasonList
        restAbsenceReasonMockMvc.perform(get("/api/absence-reasons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(absenceReason.getId().intValue())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)));
    }

    @Test
    @Transactional
    public void getAbsenceReason() throws Exception {
        // Initialize the database
        absenceReasonRepository.saveAndFlush(absenceReason);

        // Get the absenceReason
        restAbsenceReasonMockMvc.perform(get("/api/absence-reasons/{id}", absenceReason.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(absenceReason.getId().intValue()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON));
    }

    @Test
    @Transactional
    public void getNonExistingAbsenceReason() throws Exception {
        // Get the absenceReason
        restAbsenceReasonMockMvc.perform(get("/api/absence-reasons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAbsenceReason() throws Exception {
        // Initialize the database
        absenceReasonRepository.saveAndFlush(absenceReason);

        int databaseSizeBeforeUpdate = absenceReasonRepository.findAll().size();

        // Update the absenceReason
        AbsenceReason updatedAbsenceReason = absenceReasonRepository.findById(absenceReason.getId()).get();
        // Disconnect from session so that the updates on updatedAbsenceReason are not directly saved in db
        em.detach(updatedAbsenceReason);
        updatedAbsenceReason
            .reason(UPDATED_REASON);
        AbsenceReasonDTO absenceReasonDTO = absenceReasonMapper.toDto(updatedAbsenceReason);

        restAbsenceReasonMockMvc.perform(put("/api/absence-reasons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(absenceReasonDTO)))
            .andExpect(status().isOk());

        // Validate the AbsenceReason in the database
        List<AbsenceReason> absenceReasonList = absenceReasonRepository.findAll();
        assertThat(absenceReasonList).hasSize(databaseSizeBeforeUpdate);
        AbsenceReason testAbsenceReason = absenceReasonList.get(absenceReasonList.size() - 1);
        assertThat(testAbsenceReason.getReason()).isEqualTo(UPDATED_REASON);
    }

    @Test
    @Transactional
    public void updateNonExistingAbsenceReason() throws Exception {
        int databaseSizeBeforeUpdate = absenceReasonRepository.findAll().size();

        // Create the AbsenceReason
        AbsenceReasonDTO absenceReasonDTO = absenceReasonMapper.toDto(absenceReason);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAbsenceReasonMockMvc.perform(put("/api/absence-reasons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(absenceReasonDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AbsenceReason in the database
        List<AbsenceReason> absenceReasonList = absenceReasonRepository.findAll();
        assertThat(absenceReasonList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAbsenceReason() throws Exception {
        // Initialize the database
        absenceReasonRepository.saveAndFlush(absenceReason);

        int databaseSizeBeforeDelete = absenceReasonRepository.findAll().size();

        // Delete the absenceReason
        restAbsenceReasonMockMvc.perform(delete("/api/absence-reasons/{id}", absenceReason.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AbsenceReason> absenceReasonList = absenceReasonRepository.findAll();
        assertThat(absenceReasonList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AbsenceReason.class);
        AbsenceReason absenceReason1 = new AbsenceReason();
        absenceReason1.setId(1L);
        AbsenceReason absenceReason2 = new AbsenceReason();
        absenceReason2.setId(absenceReason1.getId());
        assertThat(absenceReason1).isEqualTo(absenceReason2);
        absenceReason2.setId(2L);
        assertThat(absenceReason1).isNotEqualTo(absenceReason2);
        absenceReason1.setId(null);
        assertThat(absenceReason1).isNotEqualTo(absenceReason2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AbsenceReasonDTO.class);
        AbsenceReasonDTO absenceReasonDTO1 = new AbsenceReasonDTO();
        absenceReasonDTO1.setId(1L);
        AbsenceReasonDTO absenceReasonDTO2 = new AbsenceReasonDTO();
        assertThat(absenceReasonDTO1).isNotEqualTo(absenceReasonDTO2);
        absenceReasonDTO2.setId(absenceReasonDTO1.getId());
        assertThat(absenceReasonDTO1).isEqualTo(absenceReasonDTO2);
        absenceReasonDTO2.setId(2L);
        assertThat(absenceReasonDTO1).isNotEqualTo(absenceReasonDTO2);
        absenceReasonDTO1.setId(null);
        assertThat(absenceReasonDTO1).isNotEqualTo(absenceReasonDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(absenceReasonMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(absenceReasonMapper.fromId(null)).isNull();
    }
}
