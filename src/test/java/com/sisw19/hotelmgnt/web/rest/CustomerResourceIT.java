package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.Customer;
import com.sisw19.hotelmgnt.domain.User;
import com.sisw19.hotelmgnt.repository.CustomerRepository;
import com.sisw19.hotelmgnt.service.CustomerService;
import com.sisw19.hotelmgnt.service.dto.CustomerDTO;
import com.sisw19.hotelmgnt.service.mapper.CustomerMapper;
import com.sisw19.hotelmgnt.web.rest.errors.ExceptionTranslator;
import com.sisw19.hotelmgnt.service.dto.CustomerCriteria;
import com.sisw19.hotelmgnt.service.CustomerQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.sisw19.hotelmgnt.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sisw19.hotelmgnt.domain.enumeration.Gender;
/**
 * Integration tests for the {@link CustomerResource} REST controller.
 */
@SpringBootTest(classes = HotelManagementApp.class)
public class CustomerResourceIT {

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    private static final Double DEFAULT_DISCOUNT_PERCENT = 0D;
    private static final Double UPDATED_DISCOUNT_PERCENT = 1D;
    private static final Double SMALLER_DISCOUNT_PERCENT = 0D - 1D;

    private static final String DEFAULT_WEBPAGE = "AAAAAAAAAA";
    private static final String UPDATED_WEBPAGE = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_BIRTHDAY = LocalDate.ofEpochDay(-1L);

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final String DEFAULT_TEL = "AAAAAAAAAA";
    private static final String UPDATED_TEL = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "D@%b.(";
    private static final String UPDATED_EMAIL = "g%@/.I";

    private static final String DEFAULT_BILLING_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_BILLING_ADDRESS = "BBBBBBBBBB";

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerMapper customerMapper;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerQueryService customerQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCustomerMockMvc;

    private Customer customer;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CustomerResource customerResource = new CustomerResource(customerService, customerQueryService);
        this.restCustomerMockMvc = MockMvcBuilders.standaloneSetup(customerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Customer createEntity(EntityManager em) {
        Customer customer = new Customer()
            .companyName(DEFAULT_COMPANY_NAME)
            .notes(DEFAULT_NOTES)
            .discountPercent(DEFAULT_DISCOUNT_PERCENT)
            .webpage(DEFAULT_WEBPAGE)
            .fax(DEFAULT_FAX)
            .name(DEFAULT_NAME)
            .birthday(DEFAULT_BIRTHDAY)
            .gender(DEFAULT_GENDER)
            .tel(DEFAULT_TEL)
            .email(DEFAULT_EMAIL)
            .billingAddress(DEFAULT_BILLING_ADDRESS);
        return customer;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Customer createUpdatedEntity(EntityManager em) {
        Customer customer = new Customer()
            .companyName(UPDATED_COMPANY_NAME)
            .notes(UPDATED_NOTES)
            .discountPercent(UPDATED_DISCOUNT_PERCENT)
            .webpage(UPDATED_WEBPAGE)
            .fax(UPDATED_FAX)
            .name(UPDATED_NAME)
            .birthday(UPDATED_BIRTHDAY)
            .gender(UPDATED_GENDER)
            .tel(UPDATED_TEL)
            .email(UPDATED_EMAIL)
            .billingAddress(UPDATED_BILLING_ADDRESS);
        return customer;
    }

    @BeforeEach
    public void initTest() {
        customer = createEntity(em);
    }

    @Test
    @Transactional
    public void createCustomer() throws Exception {
        int databaseSizeBeforeCreate = customerRepository.findAll().size();

        // Create the Customer
        CustomerDTO customerDTO = customerMapper.toDto(customer);
        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isCreated());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeCreate + 1);
        Customer testCustomer = customerList.get(customerList.size() - 1);
        assertThat(testCustomer.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testCustomer.getNotes()).isEqualTo(DEFAULT_NOTES);
        assertThat(testCustomer.getDiscountPercent()).isEqualTo(DEFAULT_DISCOUNT_PERCENT);
        assertThat(testCustomer.getWebpage()).isEqualTo(DEFAULT_WEBPAGE);
        assertThat(testCustomer.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testCustomer.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCustomer.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
        assertThat(testCustomer.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testCustomer.getTel()).isEqualTo(DEFAULT_TEL);
        assertThat(testCustomer.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCustomer.getBillingAddress()).isEqualTo(DEFAULT_BILLING_ADDRESS);
    }

    @Test
    @Transactional
    public void createCustomerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customerRepository.findAll().size();

        // Create the Customer with an existing ID
        customer.setId(1L);
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNotesIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerRepository.findAll().size();
        // set the field null
        customer.setNotes(null);

        // Create the Customer, which fails.
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDiscountPercentIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerRepository.findAll().size();
        // set the field null
        customer.setDiscountPercent(null);

        // Create the Customer, which fails.
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerRepository.findAll().size();
        // set the field null
        customer.setName(null);

        // Create the Customer, which fails.
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBirthdayIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerRepository.findAll().size();
        // set the field null
        customer.setBirthday(null);

        // Create the Customer, which fails.
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerRepository.findAll().size();
        // set the field null
        customer.setGender(null);

        // Create the Customer, which fails.
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTelIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerRepository.findAll().size();
        // set the field null
        customer.setTel(null);

        // Create the Customer, which fails.
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerRepository.findAll().size();
        // set the field null
        customer.setEmail(null);

        // Create the Customer, which fails.
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBillingAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = customerRepository.findAll().size();
        // set the field null
        customer.setBillingAddress(null);

        // Create the Customer, which fails.
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        restCustomerMockMvc.perform(post("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCustomers() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customer.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.doubleValue())))
            .andExpect(jsonPath("$.[*].webpage").value(hasItem(DEFAULT_WEBPAGE)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].billingAddress").value(hasItem(DEFAULT_BILLING_ADDRESS)));
    }
    
    @Test
    @Transactional
    public void getCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", customer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(customer.getId().intValue()))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES))
            .andExpect(jsonPath("$.discountPercent").value(DEFAULT_DISCOUNT_PERCENT.doubleValue()))
            .andExpect(jsonPath("$.webpage").value(DEFAULT_WEBPAGE))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.tel").value(DEFAULT_TEL))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.billingAddress").value(DEFAULT_BILLING_ADDRESS));
    }

    @Test
    @Transactional
    public void getAllCustomersByCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where companyName equals to DEFAULT_COMPANY_NAME
        defaultCustomerShouldBeFound("companyName.equals=" + DEFAULT_COMPANY_NAME);

        // Get all the customerList where companyName equals to UPDATED_COMPANY_NAME
        defaultCustomerShouldNotBeFound("companyName.equals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where companyName not equals to DEFAULT_COMPANY_NAME
        defaultCustomerShouldNotBeFound("companyName.notEquals=" + DEFAULT_COMPANY_NAME);

        // Get all the customerList where companyName not equals to UPDATED_COMPANY_NAME
        defaultCustomerShouldBeFound("companyName.notEquals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where companyName in DEFAULT_COMPANY_NAME or UPDATED_COMPANY_NAME
        defaultCustomerShouldBeFound("companyName.in=" + DEFAULT_COMPANY_NAME + "," + UPDATED_COMPANY_NAME);

        // Get all the customerList where companyName equals to UPDATED_COMPANY_NAME
        defaultCustomerShouldNotBeFound("companyName.in=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where companyName is not null
        defaultCustomerShouldBeFound("companyName.specified=true");

        // Get all the customerList where companyName is null
        defaultCustomerShouldNotBeFound("companyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCustomersByCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where companyName contains DEFAULT_COMPANY_NAME
        defaultCustomerShouldBeFound("companyName.contains=" + DEFAULT_COMPANY_NAME);

        // Get all the customerList where companyName contains UPDATED_COMPANY_NAME
        defaultCustomerShouldNotBeFound("companyName.contains=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where companyName does not contain DEFAULT_COMPANY_NAME
        defaultCustomerShouldNotBeFound("companyName.doesNotContain=" + DEFAULT_COMPANY_NAME);

        // Get all the customerList where companyName does not contain UPDATED_COMPANY_NAME
        defaultCustomerShouldBeFound("companyName.doesNotContain=" + UPDATED_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllCustomersByNotesIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where notes equals to DEFAULT_NOTES
        defaultCustomerShouldBeFound("notes.equals=" + DEFAULT_NOTES);

        // Get all the customerList where notes equals to UPDATED_NOTES
        defaultCustomerShouldNotBeFound("notes.equals=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllCustomersByNotesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where notes not equals to DEFAULT_NOTES
        defaultCustomerShouldNotBeFound("notes.notEquals=" + DEFAULT_NOTES);

        // Get all the customerList where notes not equals to UPDATED_NOTES
        defaultCustomerShouldBeFound("notes.notEquals=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllCustomersByNotesIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where notes in DEFAULT_NOTES or UPDATED_NOTES
        defaultCustomerShouldBeFound("notes.in=" + DEFAULT_NOTES + "," + UPDATED_NOTES);

        // Get all the customerList where notes equals to UPDATED_NOTES
        defaultCustomerShouldNotBeFound("notes.in=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllCustomersByNotesIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where notes is not null
        defaultCustomerShouldBeFound("notes.specified=true");

        // Get all the customerList where notes is null
        defaultCustomerShouldNotBeFound("notes.specified=false");
    }
                @Test
    @Transactional
    public void getAllCustomersByNotesContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where notes contains DEFAULT_NOTES
        defaultCustomerShouldBeFound("notes.contains=" + DEFAULT_NOTES);

        // Get all the customerList where notes contains UPDATED_NOTES
        defaultCustomerShouldNotBeFound("notes.contains=" + UPDATED_NOTES);
    }

    @Test
    @Transactional
    public void getAllCustomersByNotesNotContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where notes does not contain DEFAULT_NOTES
        defaultCustomerShouldNotBeFound("notes.doesNotContain=" + DEFAULT_NOTES);

        // Get all the customerList where notes does not contain UPDATED_NOTES
        defaultCustomerShouldBeFound("notes.doesNotContain=" + UPDATED_NOTES);
    }


    @Test
    @Transactional
    public void getAllCustomersByDiscountPercentIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where discountPercent equals to DEFAULT_DISCOUNT_PERCENT
        defaultCustomerShouldBeFound("discountPercent.equals=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the customerList where discountPercent equals to UPDATED_DISCOUNT_PERCENT
        defaultCustomerShouldNotBeFound("discountPercent.equals=" + UPDATED_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void getAllCustomersByDiscountPercentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where discountPercent not equals to DEFAULT_DISCOUNT_PERCENT
        defaultCustomerShouldNotBeFound("discountPercent.notEquals=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the customerList where discountPercent not equals to UPDATED_DISCOUNT_PERCENT
        defaultCustomerShouldBeFound("discountPercent.notEquals=" + UPDATED_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void getAllCustomersByDiscountPercentIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where discountPercent in DEFAULT_DISCOUNT_PERCENT or UPDATED_DISCOUNT_PERCENT
        defaultCustomerShouldBeFound("discountPercent.in=" + DEFAULT_DISCOUNT_PERCENT + "," + UPDATED_DISCOUNT_PERCENT);

        // Get all the customerList where discountPercent equals to UPDATED_DISCOUNT_PERCENT
        defaultCustomerShouldNotBeFound("discountPercent.in=" + UPDATED_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void getAllCustomersByDiscountPercentIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where discountPercent is not null
        defaultCustomerShouldBeFound("discountPercent.specified=true");

        // Get all the customerList where discountPercent is null
        defaultCustomerShouldNotBeFound("discountPercent.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByDiscountPercentIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where discountPercent is greater than or equal to DEFAULT_DISCOUNT_PERCENT
        defaultCustomerShouldBeFound("discountPercent.greaterThanOrEqual=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the customerList where discountPercent is greater than or equal to (DEFAULT_DISCOUNT_PERCENT + 1)
        defaultCustomerShouldNotBeFound("discountPercent.greaterThanOrEqual=" + (DEFAULT_DISCOUNT_PERCENT + 1));
    }

    @Test
    @Transactional
    public void getAllCustomersByDiscountPercentIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where discountPercent is less than or equal to DEFAULT_DISCOUNT_PERCENT
        defaultCustomerShouldBeFound("discountPercent.lessThanOrEqual=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the customerList where discountPercent is less than or equal to SMALLER_DISCOUNT_PERCENT
        defaultCustomerShouldNotBeFound("discountPercent.lessThanOrEqual=" + SMALLER_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void getAllCustomersByDiscountPercentIsLessThanSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where discountPercent is less than DEFAULT_DISCOUNT_PERCENT
        defaultCustomerShouldNotBeFound("discountPercent.lessThan=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the customerList where discountPercent is less than (DEFAULT_DISCOUNT_PERCENT + 1)
        defaultCustomerShouldBeFound("discountPercent.lessThan=" + (DEFAULT_DISCOUNT_PERCENT + 1));
    }

    @Test
    @Transactional
    public void getAllCustomersByDiscountPercentIsGreaterThanSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where discountPercent is greater than DEFAULT_DISCOUNT_PERCENT
        defaultCustomerShouldNotBeFound("discountPercent.greaterThan=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the customerList where discountPercent is greater than SMALLER_DISCOUNT_PERCENT
        defaultCustomerShouldBeFound("discountPercent.greaterThan=" + SMALLER_DISCOUNT_PERCENT);
    }


    @Test
    @Transactional
    public void getAllCustomersByWebpageIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where webpage equals to DEFAULT_WEBPAGE
        defaultCustomerShouldBeFound("webpage.equals=" + DEFAULT_WEBPAGE);

        // Get all the customerList where webpage equals to UPDATED_WEBPAGE
        defaultCustomerShouldNotBeFound("webpage.equals=" + UPDATED_WEBPAGE);
    }

    @Test
    @Transactional
    public void getAllCustomersByWebpageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where webpage not equals to DEFAULT_WEBPAGE
        defaultCustomerShouldNotBeFound("webpage.notEquals=" + DEFAULT_WEBPAGE);

        // Get all the customerList where webpage not equals to UPDATED_WEBPAGE
        defaultCustomerShouldBeFound("webpage.notEquals=" + UPDATED_WEBPAGE);
    }

    @Test
    @Transactional
    public void getAllCustomersByWebpageIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where webpage in DEFAULT_WEBPAGE or UPDATED_WEBPAGE
        defaultCustomerShouldBeFound("webpage.in=" + DEFAULT_WEBPAGE + "," + UPDATED_WEBPAGE);

        // Get all the customerList where webpage equals to UPDATED_WEBPAGE
        defaultCustomerShouldNotBeFound("webpage.in=" + UPDATED_WEBPAGE);
    }

    @Test
    @Transactional
    public void getAllCustomersByWebpageIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where webpage is not null
        defaultCustomerShouldBeFound("webpage.specified=true");

        // Get all the customerList where webpage is null
        defaultCustomerShouldNotBeFound("webpage.specified=false");
    }
                @Test
    @Transactional
    public void getAllCustomersByWebpageContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where webpage contains DEFAULT_WEBPAGE
        defaultCustomerShouldBeFound("webpage.contains=" + DEFAULT_WEBPAGE);

        // Get all the customerList where webpage contains UPDATED_WEBPAGE
        defaultCustomerShouldNotBeFound("webpage.contains=" + UPDATED_WEBPAGE);
    }

    @Test
    @Transactional
    public void getAllCustomersByWebpageNotContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where webpage does not contain DEFAULT_WEBPAGE
        defaultCustomerShouldNotBeFound("webpage.doesNotContain=" + DEFAULT_WEBPAGE);

        // Get all the customerList where webpage does not contain UPDATED_WEBPAGE
        defaultCustomerShouldBeFound("webpage.doesNotContain=" + UPDATED_WEBPAGE);
    }


    @Test
    @Transactional
    public void getAllCustomersByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fax equals to DEFAULT_FAX
        defaultCustomerShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the customerList where fax equals to UPDATED_FAX
        defaultCustomerShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllCustomersByFaxIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fax not equals to DEFAULT_FAX
        defaultCustomerShouldNotBeFound("fax.notEquals=" + DEFAULT_FAX);

        // Get all the customerList where fax not equals to UPDATED_FAX
        defaultCustomerShouldBeFound("fax.notEquals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllCustomersByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultCustomerShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the customerList where fax equals to UPDATED_FAX
        defaultCustomerShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllCustomersByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fax is not null
        defaultCustomerShouldBeFound("fax.specified=true");

        // Get all the customerList where fax is null
        defaultCustomerShouldNotBeFound("fax.specified=false");
    }
                @Test
    @Transactional
    public void getAllCustomersByFaxContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fax contains DEFAULT_FAX
        defaultCustomerShouldBeFound("fax.contains=" + DEFAULT_FAX);

        // Get all the customerList where fax contains UPDATED_FAX
        defaultCustomerShouldNotBeFound("fax.contains=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllCustomersByFaxNotContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where fax does not contain DEFAULT_FAX
        defaultCustomerShouldNotBeFound("fax.doesNotContain=" + DEFAULT_FAX);

        // Get all the customerList where fax does not contain UPDATED_FAX
        defaultCustomerShouldBeFound("fax.doesNotContain=" + UPDATED_FAX);
    }


    @Test
    @Transactional
    public void getAllCustomersByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where name equals to DEFAULT_NAME
        defaultCustomerShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the customerList where name equals to UPDATED_NAME
        defaultCustomerShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where name not equals to DEFAULT_NAME
        defaultCustomerShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the customerList where name not equals to UPDATED_NAME
        defaultCustomerShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByNameIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCustomerShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the customerList where name equals to UPDATED_NAME
        defaultCustomerShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where name is not null
        defaultCustomerShouldBeFound("name.specified=true");

        // Get all the customerList where name is null
        defaultCustomerShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllCustomersByNameContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where name contains DEFAULT_NAME
        defaultCustomerShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the customerList where name contains UPDATED_NAME
        defaultCustomerShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCustomersByNameNotContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where name does not contain DEFAULT_NAME
        defaultCustomerShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the customerList where name does not contain UPDATED_NAME
        defaultCustomerShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllCustomersByBirthdayIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where birthday equals to DEFAULT_BIRTHDAY
        defaultCustomerShouldBeFound("birthday.equals=" + DEFAULT_BIRTHDAY);

        // Get all the customerList where birthday equals to UPDATED_BIRTHDAY
        defaultCustomerShouldNotBeFound("birthday.equals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllCustomersByBirthdayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where birthday not equals to DEFAULT_BIRTHDAY
        defaultCustomerShouldNotBeFound("birthday.notEquals=" + DEFAULT_BIRTHDAY);

        // Get all the customerList where birthday not equals to UPDATED_BIRTHDAY
        defaultCustomerShouldBeFound("birthday.notEquals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllCustomersByBirthdayIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where birthday in DEFAULT_BIRTHDAY or UPDATED_BIRTHDAY
        defaultCustomerShouldBeFound("birthday.in=" + DEFAULT_BIRTHDAY + "," + UPDATED_BIRTHDAY);

        // Get all the customerList where birthday equals to UPDATED_BIRTHDAY
        defaultCustomerShouldNotBeFound("birthday.in=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllCustomersByBirthdayIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where birthday is not null
        defaultCustomerShouldBeFound("birthday.specified=true");

        // Get all the customerList where birthday is null
        defaultCustomerShouldNotBeFound("birthday.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByBirthdayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where birthday is greater than or equal to DEFAULT_BIRTHDAY
        defaultCustomerShouldBeFound("birthday.greaterThanOrEqual=" + DEFAULT_BIRTHDAY);

        // Get all the customerList where birthday is greater than or equal to UPDATED_BIRTHDAY
        defaultCustomerShouldNotBeFound("birthday.greaterThanOrEqual=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllCustomersByBirthdayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where birthday is less than or equal to DEFAULT_BIRTHDAY
        defaultCustomerShouldBeFound("birthday.lessThanOrEqual=" + DEFAULT_BIRTHDAY);

        // Get all the customerList where birthday is less than or equal to SMALLER_BIRTHDAY
        defaultCustomerShouldNotBeFound("birthday.lessThanOrEqual=" + SMALLER_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllCustomersByBirthdayIsLessThanSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where birthday is less than DEFAULT_BIRTHDAY
        defaultCustomerShouldNotBeFound("birthday.lessThan=" + DEFAULT_BIRTHDAY);

        // Get all the customerList where birthday is less than UPDATED_BIRTHDAY
        defaultCustomerShouldBeFound("birthday.lessThan=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllCustomersByBirthdayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where birthday is greater than DEFAULT_BIRTHDAY
        defaultCustomerShouldNotBeFound("birthday.greaterThan=" + DEFAULT_BIRTHDAY);

        // Get all the customerList where birthday is greater than SMALLER_BIRTHDAY
        defaultCustomerShouldBeFound("birthday.greaterThan=" + SMALLER_BIRTHDAY);
    }


    @Test
    @Transactional
    public void getAllCustomersByGenderIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender equals to DEFAULT_GENDER
        defaultCustomerShouldBeFound("gender.equals=" + DEFAULT_GENDER);

        // Get all the customerList where gender equals to UPDATED_GENDER
        defaultCustomerShouldNotBeFound("gender.equals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllCustomersByGenderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender not equals to DEFAULT_GENDER
        defaultCustomerShouldNotBeFound("gender.notEquals=" + DEFAULT_GENDER);

        // Get all the customerList where gender not equals to UPDATED_GENDER
        defaultCustomerShouldBeFound("gender.notEquals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllCustomersByGenderIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender in DEFAULT_GENDER or UPDATED_GENDER
        defaultCustomerShouldBeFound("gender.in=" + DEFAULT_GENDER + "," + UPDATED_GENDER);

        // Get all the customerList where gender equals to UPDATED_GENDER
        defaultCustomerShouldNotBeFound("gender.in=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllCustomersByGenderIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where gender is not null
        defaultCustomerShouldBeFound("gender.specified=true");

        // Get all the customerList where gender is null
        defaultCustomerShouldNotBeFound("gender.specified=false");
    }

    @Test
    @Transactional
    public void getAllCustomersByTelIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel equals to DEFAULT_TEL
        defaultCustomerShouldBeFound("tel.equals=" + DEFAULT_TEL);

        // Get all the customerList where tel equals to UPDATED_TEL
        defaultCustomerShouldNotBeFound("tel.equals=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllCustomersByTelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel not equals to DEFAULT_TEL
        defaultCustomerShouldNotBeFound("tel.notEquals=" + DEFAULT_TEL);

        // Get all the customerList where tel not equals to UPDATED_TEL
        defaultCustomerShouldBeFound("tel.notEquals=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllCustomersByTelIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel in DEFAULT_TEL or UPDATED_TEL
        defaultCustomerShouldBeFound("tel.in=" + DEFAULT_TEL + "," + UPDATED_TEL);

        // Get all the customerList where tel equals to UPDATED_TEL
        defaultCustomerShouldNotBeFound("tel.in=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllCustomersByTelIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel is not null
        defaultCustomerShouldBeFound("tel.specified=true");

        // Get all the customerList where tel is null
        defaultCustomerShouldNotBeFound("tel.specified=false");
    }
                @Test
    @Transactional
    public void getAllCustomersByTelContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel contains DEFAULT_TEL
        defaultCustomerShouldBeFound("tel.contains=" + DEFAULT_TEL);

        // Get all the customerList where tel contains UPDATED_TEL
        defaultCustomerShouldNotBeFound("tel.contains=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllCustomersByTelNotContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where tel does not contain DEFAULT_TEL
        defaultCustomerShouldNotBeFound("tel.doesNotContain=" + DEFAULT_TEL);

        // Get all the customerList where tel does not contain UPDATED_TEL
        defaultCustomerShouldBeFound("tel.doesNotContain=" + UPDATED_TEL);
    }


    @Test
    @Transactional
    public void getAllCustomersByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where email equals to DEFAULT_EMAIL
        defaultCustomerShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the customerList where email equals to UPDATED_EMAIL
        defaultCustomerShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllCustomersByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where email not equals to DEFAULT_EMAIL
        defaultCustomerShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the customerList where email not equals to UPDATED_EMAIL
        defaultCustomerShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllCustomersByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultCustomerShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the customerList where email equals to UPDATED_EMAIL
        defaultCustomerShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllCustomersByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where email is not null
        defaultCustomerShouldBeFound("email.specified=true");

        // Get all the customerList where email is null
        defaultCustomerShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllCustomersByEmailContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where email contains DEFAULT_EMAIL
        defaultCustomerShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the customerList where email contains UPDATED_EMAIL
        defaultCustomerShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllCustomersByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where email does not contain DEFAULT_EMAIL
        defaultCustomerShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the customerList where email does not contain UPDATED_EMAIL
        defaultCustomerShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllCustomersByBillingAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where billingAddress equals to DEFAULT_BILLING_ADDRESS
        defaultCustomerShouldBeFound("billingAddress.equals=" + DEFAULT_BILLING_ADDRESS);

        // Get all the customerList where billingAddress equals to UPDATED_BILLING_ADDRESS
        defaultCustomerShouldNotBeFound("billingAddress.equals=" + UPDATED_BILLING_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllCustomersByBillingAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where billingAddress not equals to DEFAULT_BILLING_ADDRESS
        defaultCustomerShouldNotBeFound("billingAddress.notEquals=" + DEFAULT_BILLING_ADDRESS);

        // Get all the customerList where billingAddress not equals to UPDATED_BILLING_ADDRESS
        defaultCustomerShouldBeFound("billingAddress.notEquals=" + UPDATED_BILLING_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllCustomersByBillingAddressIsInShouldWork() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where billingAddress in DEFAULT_BILLING_ADDRESS or UPDATED_BILLING_ADDRESS
        defaultCustomerShouldBeFound("billingAddress.in=" + DEFAULT_BILLING_ADDRESS + "," + UPDATED_BILLING_ADDRESS);

        // Get all the customerList where billingAddress equals to UPDATED_BILLING_ADDRESS
        defaultCustomerShouldNotBeFound("billingAddress.in=" + UPDATED_BILLING_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllCustomersByBillingAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where billingAddress is not null
        defaultCustomerShouldBeFound("billingAddress.specified=true");

        // Get all the customerList where billingAddress is null
        defaultCustomerShouldNotBeFound("billingAddress.specified=false");
    }
                @Test
    @Transactional
    public void getAllCustomersByBillingAddressContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where billingAddress contains DEFAULT_BILLING_ADDRESS
        defaultCustomerShouldBeFound("billingAddress.contains=" + DEFAULT_BILLING_ADDRESS);

        // Get all the customerList where billingAddress contains UPDATED_BILLING_ADDRESS
        defaultCustomerShouldNotBeFound("billingAddress.contains=" + UPDATED_BILLING_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllCustomersByBillingAddressNotContainsSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        // Get all the customerList where billingAddress does not contain DEFAULT_BILLING_ADDRESS
        defaultCustomerShouldNotBeFound("billingAddress.doesNotContain=" + DEFAULT_BILLING_ADDRESS);

        // Get all the customerList where billingAddress does not contain UPDATED_BILLING_ADDRESS
        defaultCustomerShouldBeFound("billingAddress.doesNotContain=" + UPDATED_BILLING_ADDRESS);
    }


    @Test
    @Transactional
    public void getAllCustomersByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        customer.setUser(user);
        customerRepository.saveAndFlush(customer);
        Long userId = user.getId();

        // Get all the customerList where user equals to userId
        defaultCustomerShouldBeFound("userId.equals=" + userId);

        // Get all the customerList where user equals to userId + 1
        defaultCustomerShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCustomerShouldBeFound(String filter) throws Exception {
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customer.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.doubleValue())))
            .andExpect(jsonPath("$.[*].webpage").value(hasItem(DEFAULT_WEBPAGE)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].billingAddress").value(hasItem(DEFAULT_BILLING_ADDRESS)));

        // Check, that the count call also returns 1
        restCustomerMockMvc.perform(get("/api/customers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCustomerShouldNotBeFound(String filter) throws Exception {
        restCustomerMockMvc.perform(get("/api/customers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCustomerMockMvc.perform(get("/api/customers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCustomer() throws Exception {
        // Get the customer
        restCustomerMockMvc.perform(get("/api/customers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        int databaseSizeBeforeUpdate = customerRepository.findAll().size();

        // Update the customer
        Customer updatedCustomer = customerRepository.findById(customer.getId()).get();
        // Disconnect from session so that the updates on updatedCustomer are not directly saved in db
        em.detach(updatedCustomer);
        updatedCustomer
            .companyName(UPDATED_COMPANY_NAME)
            .notes(UPDATED_NOTES)
            .discountPercent(UPDATED_DISCOUNT_PERCENT)
            .webpage(UPDATED_WEBPAGE)
            .fax(UPDATED_FAX)
            .name(UPDATED_NAME)
            .birthday(UPDATED_BIRTHDAY)
            .gender(UPDATED_GENDER)
            .tel(UPDATED_TEL)
            .email(UPDATED_EMAIL)
            .billingAddress(UPDATED_BILLING_ADDRESS);
        CustomerDTO customerDTO = customerMapper.toDto(updatedCustomer);

        restCustomerMockMvc.perform(put("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isOk());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeUpdate);
        Customer testCustomer = customerList.get(customerList.size() - 1);
        assertThat(testCustomer.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testCustomer.getNotes()).isEqualTo(UPDATED_NOTES);
        assertThat(testCustomer.getDiscountPercent()).isEqualTo(UPDATED_DISCOUNT_PERCENT);
        assertThat(testCustomer.getWebpage()).isEqualTo(UPDATED_WEBPAGE);
        assertThat(testCustomer.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testCustomer.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCustomer.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
        assertThat(testCustomer.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testCustomer.getTel()).isEqualTo(UPDATED_TEL);
        assertThat(testCustomer.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCustomer.getBillingAddress()).isEqualTo(UPDATED_BILLING_ADDRESS);
    }

    @Test
    @Transactional
    public void updateNonExistingCustomer() throws Exception {
        int databaseSizeBeforeUpdate = customerRepository.findAll().size();

        // Create the Customer
        CustomerDTO customerDTO = customerMapper.toDto(customer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerMockMvc.perform(put("/api/customers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Customer in the database
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCustomer() throws Exception {
        // Initialize the database
        customerRepository.saveAndFlush(customer);

        int databaseSizeBeforeDelete = customerRepository.findAll().size();

        // Delete the customer
        restCustomerMockMvc.perform(delete("/api/customers/{id}", customer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Customer> customerList = customerRepository.findAll();
        assertThat(customerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Customer.class);
        Customer customer1 = new Customer();
        customer1.setId(1L);
        Customer customer2 = new Customer();
        customer2.setId(customer1.getId());
        assertThat(customer1).isEqualTo(customer2);
        customer2.setId(2L);
        assertThat(customer1).isNotEqualTo(customer2);
        customer1.setId(null);
        assertThat(customer1).isNotEqualTo(customer2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerDTO.class);
        CustomerDTO customerDTO1 = new CustomerDTO();
        customerDTO1.setId(1L);
        CustomerDTO customerDTO2 = new CustomerDTO();
        assertThat(customerDTO1).isNotEqualTo(customerDTO2);
        customerDTO2.setId(customerDTO1.getId());
        assertThat(customerDTO1).isEqualTo(customerDTO2);
        customerDTO2.setId(2L);
        assertThat(customerDTO1).isNotEqualTo(customerDTO2);
        customerDTO1.setId(null);
        assertThat(customerDTO1).isNotEqualTo(customerDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(customerMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(customerMapper.fromId(null)).isNull();
    }
}
