package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.Defect;
import com.sisw19.hotelmgnt.domain.Room;
import com.sisw19.hotelmgnt.domain.enumeration.DefectState;
import com.sisw19.hotelmgnt.repository.DefectRepository;
import com.sisw19.hotelmgnt.repository.RoomRepository;
import com.sisw19.hotelmgnt.service.DefectQueryService;
import com.sisw19.hotelmgnt.service.DefectService;
import com.sisw19.hotelmgnt.service.dto.DefectDTO;
import com.sisw19.hotelmgnt.service.mapper.DefectMapper;
import com.sisw19.hotelmgnt.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.sisw19.hotelmgnt.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Integration tests for the {@link DefectResource} REST controller.
 */
@SpringBootTest(classes = HotelManagementApp.class)
public class DefectResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final DefectState DEFAULT_DEFECT_STATE = DefectState.IN_PROGRESS;
    private static final DefectState UPDATED_DEFECT_STATE = DefectState.RESOLVED;

    private static final Double DEFAULT_DISCOUNT_PERCENT = 0D;
    private static final Double UPDATED_DISCOUNT_PERCENT = 1D;
    private static final Double SMALLER_DISCOUNT_PERCENT = 0D - 1D;

    @Autowired
    private DefectRepository defectRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private DefectMapper defectMapper;

    @Autowired
    private DefectService defectService;

    @Autowired
    private DefectQueryService defectQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDefectMockMvc;

    private Defect defect;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DefectResource defectResource = new DefectResource(defectService, defectQueryService);
        this.restDefectMockMvc = MockMvcBuilders.standaloneSetup(defectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Defect createEntity(EntityManager em) {
        Defect defect = new Defect()
            .description(DEFAULT_DESCRIPTION)
            .defectState(DEFAULT_DEFECT_STATE)
            .discountPercent(DEFAULT_DISCOUNT_PERCENT);
        return defect;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Defect createUpdatedEntity(EntityManager em) {
        Defect defect = new Defect()
            .description(UPDATED_DESCRIPTION)
            .defectState(UPDATED_DEFECT_STATE)
            .discountPercent(UPDATED_DISCOUNT_PERCENT);
        return defect;
    }

    @BeforeEach
    public void initTest() {
        defect = createEntity(em);
        Room room = RoomResourceIT.createEntity(em);
        roomRepository.save(room);
        defect.setRoom(room);
    }

    @Test
    @Transactional
    public void createDefect() throws Exception {
        int databaseSizeBeforeCreate = defectRepository.findAll().size();

        // Create the Defect
        DefectDTO defectDTO = defectMapper.toDto(defect);
        restDefectMockMvc.perform(post("/api/defects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(defectDTO)))
            .andExpect(status().isCreated());

        // Validate the Defect in the database
        List<Defect> defectList = defectRepository.findAll();
        assertThat(defectList).hasSize(databaseSizeBeforeCreate + 1);
        Defect testDefect = defectList.get(defectList.size() - 1);
        assertThat(testDefect.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDefect.getDefectState()).isEqualTo(DEFAULT_DEFECT_STATE);
        assertThat(testDefect.getDiscountPercent()).isEqualTo(DEFAULT_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void createDefectWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = defectRepository.findAll().size();

        // Create the Defect with an existing ID
        defect.setId(1L);
        DefectDTO defectDTO = defectMapper.toDto(defect);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDefectMockMvc.perform(post("/api/defects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(defectDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Defect in the database
        List<Defect> defectList = defectRepository.findAll();
        assertThat(defectList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = defectRepository.findAll().size();
        // set the field null
        defect.setDescription(null);

        // Create the Defect, which fails.
        DefectDTO defectDTO = defectMapper.toDto(defect);

        restDefectMockMvc.perform(post("/api/defects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(defectDTO)))
            .andExpect(status().isBadRequest());

        List<Defect> defectList = defectRepository.findAll();
        assertThat(defectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDefectStateIsRequired() throws Exception {
        int databaseSizeBeforeTest = defectRepository.findAll().size();
        // set the field null
        defect.setDefectState(null);

        // Create the Defect, which fails.
        DefectDTO defectDTO = defectMapper.toDto(defect);

        restDefectMockMvc.perform(post("/api/defects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(defectDTO)))
            .andExpect(status().isBadRequest());

        List<Defect> defectList = defectRepository.findAll();
        assertThat(defectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDiscountPercentIsRequired() throws Exception {
        int databaseSizeBeforeTest = defectRepository.findAll().size();
        // set the field null
        defect.setDiscountPercent(null);

        // Create the Defect, which fails.
        DefectDTO defectDTO = defectMapper.toDto(defect);

        restDefectMockMvc.perform(post("/api/defects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(defectDTO)))
            .andExpect(status().isBadRequest());

        List<Defect> defectList = defectRepository.findAll();
        assertThat(defectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDefects() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList
        restDefectMockMvc.perform(get("/api/defects?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(defect.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].defectState").value(hasItem(DEFAULT_DEFECT_STATE.toString())))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.doubleValue())));
    }

    @Test
    @Transactional
    public void getDefect() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get the defect
        restDefectMockMvc.perform(get("/api/defects/{id}", defect.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(defect.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.defectState").value(DEFAULT_DEFECT_STATE.toString()))
            .andExpect(jsonPath("$.discountPercent").value(DEFAULT_DISCOUNT_PERCENT.doubleValue()));
    }

    @Test
    @Transactional
    public void getAllDefectsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where description equals to DEFAULT_DESCRIPTION
        defaultDefectShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the defectList where description equals to UPDATED_DESCRIPTION
        defaultDefectShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllDefectsByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where description not equals to DEFAULT_DESCRIPTION
        defaultDefectShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the defectList where description not equals to UPDATED_DESCRIPTION
        defaultDefectShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllDefectsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultDefectShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the defectList where description equals to UPDATED_DESCRIPTION
        defaultDefectShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllDefectsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where description is not null
        defaultDefectShouldBeFound("description.specified=true");

        // Get all the defectList where description is null
        defaultDefectShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllDefectsByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where description contains DEFAULT_DESCRIPTION
        defaultDefectShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the defectList where description contains UPDATED_DESCRIPTION
        defaultDefectShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllDefectsByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where description does not contain DEFAULT_DESCRIPTION
        defaultDefectShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the defectList where description does not contain UPDATED_DESCRIPTION
        defaultDefectShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllDefectsByDefectStateIsEqualToSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where defectState equals to DEFAULT_DEFECT_STATE
        defaultDefectShouldBeFound("defectState.equals=" + DEFAULT_DEFECT_STATE);

        // Get all the defectList where defectState equals to UPDATED_DEFECT_STATE
        defaultDefectShouldNotBeFound("defectState.equals=" + UPDATED_DEFECT_STATE);
    }

    @Test
    @Transactional
    public void getAllDefectsByDefectStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where defectState not equals to DEFAULT_DEFECT_STATE
        defaultDefectShouldNotBeFound("defectState.notEquals=" + DEFAULT_DEFECT_STATE);

        // Get all the defectList where defectState not equals to UPDATED_DEFECT_STATE
        defaultDefectShouldBeFound("defectState.notEquals=" + UPDATED_DEFECT_STATE);
    }

    @Test
    @Transactional
    public void getAllDefectsByDefectStateIsInShouldWork() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where defectState in DEFAULT_DEFECT_STATE or UPDATED_DEFECT_STATE
        defaultDefectShouldBeFound("defectState.in=" + DEFAULT_DEFECT_STATE + "," + UPDATED_DEFECT_STATE);

        // Get all the defectList where defectState equals to UPDATED_DEFECT_STATE
        defaultDefectShouldNotBeFound("defectState.in=" + UPDATED_DEFECT_STATE);
    }

    @Test
    @Transactional
    public void getAllDefectsByDefectStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where defectState is not null
        defaultDefectShouldBeFound("defectState.specified=true");

        // Get all the defectList where defectState is null
        defaultDefectShouldNotBeFound("defectState.specified=false");
    }

    @Test
    @Transactional
    public void getAllDefectsByDiscountPercentIsEqualToSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where discountPercent equals to DEFAULT_DISCOUNT_PERCENT
        defaultDefectShouldBeFound("discountPercent.equals=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the defectList where discountPercent equals to UPDATED_DISCOUNT_PERCENT
        defaultDefectShouldNotBeFound("discountPercent.equals=" + UPDATED_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void getAllDefectsByDiscountPercentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where discountPercent not equals to DEFAULT_DISCOUNT_PERCENT
        defaultDefectShouldNotBeFound("discountPercent.notEquals=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the defectList where discountPercent not equals to UPDATED_DISCOUNT_PERCENT
        defaultDefectShouldBeFound("discountPercent.notEquals=" + UPDATED_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void getAllDefectsByDiscountPercentIsInShouldWork() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where discountPercent in DEFAULT_DISCOUNT_PERCENT or UPDATED_DISCOUNT_PERCENT
        defaultDefectShouldBeFound("discountPercent.in=" + DEFAULT_DISCOUNT_PERCENT + "," + UPDATED_DISCOUNT_PERCENT);

        // Get all the defectList where discountPercent equals to UPDATED_DISCOUNT_PERCENT
        defaultDefectShouldNotBeFound("discountPercent.in=" + UPDATED_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void getAllDefectsByDiscountPercentIsNullOrNotNull() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where discountPercent is not null
        defaultDefectShouldBeFound("discountPercent.specified=true");

        // Get all the defectList where discountPercent is null
        defaultDefectShouldNotBeFound("discountPercent.specified=false");
    }

    @Test
    @Transactional
    public void getAllDefectsByDiscountPercentIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where discountPercent is greater than or equal to DEFAULT_DISCOUNT_PERCENT
        defaultDefectShouldBeFound("discountPercent.greaterThanOrEqual=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the defectList where discountPercent is greater than or equal to (DEFAULT_DISCOUNT_PERCENT + 1)
        defaultDefectShouldNotBeFound("discountPercent.greaterThanOrEqual=" + (DEFAULT_DISCOUNT_PERCENT + 1));
    }

    @Test
    @Transactional
    public void getAllDefectsByDiscountPercentIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where discountPercent is less than or equal to DEFAULT_DISCOUNT_PERCENT
        defaultDefectShouldBeFound("discountPercent.lessThanOrEqual=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the defectList where discountPercent is less than or equal to SMALLER_DISCOUNT_PERCENT
        defaultDefectShouldNotBeFound("discountPercent.lessThanOrEqual=" + SMALLER_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void getAllDefectsByDiscountPercentIsLessThanSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where discountPercent is less than DEFAULT_DISCOUNT_PERCENT
        defaultDefectShouldNotBeFound("discountPercent.lessThan=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the defectList where discountPercent is less than (DEFAULT_DISCOUNT_PERCENT + 1)
        defaultDefectShouldBeFound("discountPercent.lessThan=" + (DEFAULT_DISCOUNT_PERCENT + 1));
    }

    @Test
    @Transactional
    public void getAllDefectsByDiscountPercentIsGreaterThanSomething() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        // Get all the defectList where discountPercent is greater than DEFAULT_DISCOUNT_PERCENT
        defaultDefectShouldNotBeFound("discountPercent.greaterThan=" + DEFAULT_DISCOUNT_PERCENT);

        // Get all the defectList where discountPercent is greater than SMALLER_DISCOUNT_PERCENT
        defaultDefectShouldBeFound("discountPercent.greaterThan=" + SMALLER_DISCOUNT_PERCENT);
    }


    @Test
    @Transactional
    public void getAllDefectsByRoomIsEqualToSomething() throws Exception {
        // Initialize the database
        Room room = RoomResourceIT.createEntity(em);
        room.setNumber("99999");
        em.persist(room);
        em.flush();
        defect.setRoom(room);
        defectRepository.saveAndFlush(defect);
        Long roomId = room.getId();

        // Get all the defectList where room equals to roomId
        defaultDefectShouldBeFound("roomId.equals=" + roomId);

        // Get all the defectList where room equals to roomId + 1
        defaultDefectShouldNotBeFound("roomId.equals=" + (roomId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDefectShouldBeFound(String filter) throws Exception {
        restDefectMockMvc.perform(get("/api/defects?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(defect.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].defectState").value(hasItem(DEFAULT_DEFECT_STATE.toString())))
            .andExpect(jsonPath("$.[*].discountPercent").value(hasItem(DEFAULT_DISCOUNT_PERCENT.doubleValue())));

        // Check, that the count call also returns 1
        restDefectMockMvc.perform(get("/api/defects/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDefectShouldNotBeFound(String filter) throws Exception {
        restDefectMockMvc.perform(get("/api/defects?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDefectMockMvc.perform(get("/api/defects/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingDefect() throws Exception {
        // Get the defect
        restDefectMockMvc.perform(get("/api/defects/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDefect() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        int databaseSizeBeforeUpdate = defectRepository.findAll().size();

        // Update the defect
        Defect updatedDefect = defectRepository.findById(defect.getId()).get();
        // Disconnect from session so that the updates on updatedDefect are not directly saved in db
        em.detach(updatedDefect);
        updatedDefect
            .description(UPDATED_DESCRIPTION)
            .defectState(UPDATED_DEFECT_STATE)
            .discountPercent(UPDATED_DISCOUNT_PERCENT);
        DefectDTO defectDTO = defectMapper.toDto(updatedDefect);

        restDefectMockMvc.perform(put("/api/defects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(defectDTO)))
            .andExpect(status().isOk());

        // Validate the Defect in the database
        List<Defect> defectList = defectRepository.findAll();
        assertThat(defectList).hasSize(databaseSizeBeforeUpdate);
        Defect testDefect = defectList.get(defectList.size() - 1);
        assertThat(testDefect.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDefect.getDefectState()).isEqualTo(UPDATED_DEFECT_STATE);
        assertThat(testDefect.getDiscountPercent()).isEqualTo(UPDATED_DISCOUNT_PERCENT);
    }

    @Test
    @Transactional
    public void updateNonExistingDefect() throws Exception {
        int databaseSizeBeforeUpdate = defectRepository.findAll().size();

        // Create the Defect
        DefectDTO defectDTO = defectMapper.toDto(defect);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDefectMockMvc.perform(put("/api/defects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(defectDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Defect in the database
        List<Defect> defectList = defectRepository.findAll();
        assertThat(defectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDefect() throws Exception {
        // Initialize the database
        defectRepository.saveAndFlush(defect);

        int databaseSizeBeforeDelete = defectRepository.findAll().size();

        // Delete the defect
        restDefectMockMvc.perform(delete("/api/defects/{id}", defect.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Defect> defectList = defectRepository.findAll();
        assertThat(defectList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Defect.class);
        Defect defect1 = new Defect();
        defect1.setId(1L);
        Defect defect2 = new Defect();
        defect2.setId(defect1.getId());
        assertThat(defect1).isEqualTo(defect2);
        defect2.setId(2L);
        assertThat(defect1).isNotEqualTo(defect2);
        defect1.setId(null);
        assertThat(defect1).isNotEqualTo(defect2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DefectDTO.class);
        DefectDTO defectDTO1 = new DefectDTO();
        defectDTO1.setId(1L);
        DefectDTO defectDTO2 = new DefectDTO();
        assertThat(defectDTO1).isNotEqualTo(defectDTO2);
        defectDTO2.setId(defectDTO1.getId());
        assertThat(defectDTO1).isEqualTo(defectDTO2);
        defectDTO2.setId(2L);
        assertThat(defectDTO1).isNotEqualTo(defectDTO2);
        defectDTO1.setId(null);
        assertThat(defectDTO1).isNotEqualTo(defectDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(defectMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(defectMapper.fromId(null)).isNull();
    }
}
