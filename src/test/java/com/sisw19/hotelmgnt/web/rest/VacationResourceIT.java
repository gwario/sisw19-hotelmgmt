package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.Vacation;
import com.sisw19.hotelmgnt.repository.VacationRepository;
import com.sisw19.hotelmgnt.service.VacationService;
import com.sisw19.hotelmgnt.service.dto.VacationDTO;
import com.sisw19.hotelmgnt.service.mapper.VacationMapper;
import com.sisw19.hotelmgnt.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.sisw19.hotelmgnt.web.rest.TestUtil.sameInstant;
import static com.sisw19.hotelmgnt.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sisw19.hotelmgnt.domain.enumeration.VacationStatus;
/**
 * Integration tests for the {@link VacationResource} REST controller.
 */
@SpringBootTest(classes = HotelManagementApp.class)
public class VacationResourceIT {

    private static final ZonedDateTime DEFAULT_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final VacationStatus DEFAULT_STATUS = VacationStatus.REQUESTED;
    private static final VacationStatus UPDATED_STATUS = VacationStatus.ACCEPTED;

    @Autowired
    private VacationRepository vacationRepository;

    @Autowired
    private VacationMapper vacationMapper;

    @Autowired
    private VacationService vacationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restVacationMockMvc;

    private Vacation vacation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VacationResource vacationResource = new VacationResource(vacationService);
        this.restVacationMockMvc = MockMvcBuilders.standaloneSetup(vacationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vacation createEntity(EntityManager em) {
        Vacation vacation = new Vacation()
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .status(DEFAULT_STATUS);
        return vacation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vacation createUpdatedEntity(EntityManager em) {
        Vacation vacation = new Vacation()
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .status(UPDATED_STATUS);
        return vacation;
    }

    @BeforeEach
    public void initTest() {
        vacation = createEntity(em);
    }

    @Test
    @Transactional
    public void createVacation() throws Exception {
        int databaseSizeBeforeCreate = vacationRepository.findAll().size();

        // Create the Vacation
        VacationDTO vacationDTO = vacationMapper.toDto(vacation);
        restVacationMockMvc.perform(post("/api/vacations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationDTO)))
            .andExpect(status().isCreated());

        // Validate the Vacation in the database
        List<Vacation> vacationList = vacationRepository.findAll();
        assertThat(vacationList).hasSize(databaseSizeBeforeCreate + 1);
        Vacation testVacation = vacationList.get(vacationList.size() - 1);
        assertThat(testVacation.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testVacation.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testVacation.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createVacationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vacationRepository.findAll().size();

        // Create the Vacation with an existing ID
        vacation.setId(1L);
        VacationDTO vacationDTO = vacationMapper.toDto(vacation);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVacationMockMvc.perform(post("/api/vacations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vacation in the database
        List<Vacation> vacationList = vacationRepository.findAll();
        assertThat(vacationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacationRepository.findAll().size();
        // set the field null
        vacation.setStartDate(null);

        // Create the Vacation, which fails.
        VacationDTO vacationDTO = vacationMapper.toDto(vacation);

        restVacationMockMvc.perform(post("/api/vacations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationDTO)))
            .andExpect(status().isBadRequest());

        List<Vacation> vacationList = vacationRepository.findAll();
        assertThat(vacationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacationRepository.findAll().size();
        // set the field null
        vacation.setEndDate(null);

        // Create the Vacation, which fails.
        VacationDTO vacationDTO = vacationMapper.toDto(vacation);

        restVacationMockMvc.perform(post("/api/vacations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationDTO)))
            .andExpect(status().isBadRequest());

        List<Vacation> vacationList = vacationRepository.findAll();
        assertThat(vacationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacationRepository.findAll().size();
        // set the field null
        vacation.setStatus(null);

        // Create the Vacation, which fails.
        VacationDTO vacationDTO = vacationMapper.toDto(vacation);

        restVacationMockMvc.perform(post("/api/vacations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationDTO)))
            .andExpect(status().isBadRequest());

        List<Vacation> vacationList = vacationRepository.findAll();
        assertThat(vacationList).hasSize(databaseSizeBeforeTest);
    }

//    @Test
//    @Transactional
//    public void getAllVacations() throws Exception {
//        // Initialize the database
//        vacationRepository.saveAndFlush(vacation);
//
//        // Get all the vacationList
//        restVacationMockMvc.perform(get("/api/vacations?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(vacation.getId().intValue())))
//            .andExpect(jsonPath("$.[*].startDate").value(hasItem(sameInstant(DEFAULT_START_DATE))))
//            .andExpect(jsonPath("$.[*].endDate").value(hasItem(sameInstant(DEFAULT_END_DATE))))
//            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
//    }

    @Test
    @Transactional
    public void getVacation() throws Exception {
        // Initialize the database
        vacationRepository.saveAndFlush(vacation);

        // Get the vacation
        restVacationMockMvc.perform(get("/api/vacations/{id}", vacation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vacation.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(sameInstant(DEFAULT_START_DATE)))
            .andExpect(jsonPath("$.endDate").value(sameInstant(DEFAULT_END_DATE)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVacation() throws Exception {
        // Get the vacation
        restVacationMockMvc.perform(get("/api/vacations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVacation() throws Exception {
        // Initialize the database
        vacationRepository.saveAndFlush(vacation);

        int databaseSizeBeforeUpdate = vacationRepository.findAll().size();

        // Update the vacation
        Vacation updatedVacation = vacationRepository.findById(vacation.getId()).get();
        // Disconnect from session so that the updates on updatedVacation are not directly saved in db
        em.detach(updatedVacation);
        updatedVacation
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .status(UPDATED_STATUS);
        VacationDTO vacationDTO = vacationMapper.toDto(updatedVacation);

        restVacationMockMvc.perform(put("/api/vacations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationDTO)))
            .andExpect(status().isOk());

        // Validate the Vacation in the database
        List<Vacation> vacationList = vacationRepository.findAll();
        assertThat(vacationList).hasSize(databaseSizeBeforeUpdate);
        Vacation testVacation = vacationList.get(vacationList.size() - 1);
        assertThat(testVacation.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testVacation.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testVacation.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingVacation() throws Exception {
        int databaseSizeBeforeUpdate = vacationRepository.findAll().size();

        // Create the Vacation
        VacationDTO vacationDTO = vacationMapper.toDto(vacation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVacationMockMvc.perform(put("/api/vacations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Vacation in the database
        List<Vacation> vacationList = vacationRepository.findAll();
        assertThat(vacationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVacation() throws Exception {
        // Initialize the database
        vacationRepository.saveAndFlush(vacation);

        int databaseSizeBeforeDelete = vacationRepository.findAll().size();

        // Delete the vacation
        restVacationMockMvc.perform(delete("/api/vacations/{id}", vacation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Vacation> vacationList = vacationRepository.findAll();
        assertThat(vacationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Vacation.class);
        Vacation vacation1 = new Vacation();
        vacation1.setId(1L);
        Vacation vacation2 = new Vacation();
        vacation2.setId(vacation1.getId());
        assertThat(vacation1).isEqualTo(vacation2);
        vacation2.setId(2L);
        assertThat(vacation1).isNotEqualTo(vacation2);
        vacation1.setId(null);
        assertThat(vacation1).isNotEqualTo(vacation2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VacationDTO.class);
        VacationDTO vacationDTO1 = new VacationDTO();
        vacationDTO1.setId(1L);
        VacationDTO vacationDTO2 = new VacationDTO();
        assertThat(vacationDTO1).isNotEqualTo(vacationDTO2);
        vacationDTO2.setId(vacationDTO1.getId());
        assertThat(vacationDTO1).isEqualTo(vacationDTO2);
        vacationDTO2.setId(2L);
        assertThat(vacationDTO1).isNotEqualTo(vacationDTO2);
        vacationDTO1.setId(null);
        assertThat(vacationDTO1).isNotEqualTo(vacationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(vacationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(vacationMapper.fromId(null)).isNull();
    }
}
