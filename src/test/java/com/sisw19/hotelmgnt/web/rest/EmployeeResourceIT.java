package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.Employee;
import com.sisw19.hotelmgnt.domain.User;
import com.sisw19.hotelmgnt.domain.Vacation;
import com.sisw19.hotelmgnt.domain.enumeration.EmployeeType;
import com.sisw19.hotelmgnt.domain.enumeration.Gender;
import com.sisw19.hotelmgnt.repository.EmployeeRepository;
import com.sisw19.hotelmgnt.service.EmployeeQueryService;
import com.sisw19.hotelmgnt.service.EmployeeService;
import com.sisw19.hotelmgnt.service.dto.EmployeeDTO;
import com.sisw19.hotelmgnt.service.mapper.EmployeeMapper;
import com.sisw19.hotelmgnt.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.sisw19.hotelmgnt.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Integration tests for the {@link EmployeeResource} REST controller.
 */
@SpringBootTest(classes = HotelManagementApp.class)
public class EmployeeResourceIT {

    private static final EmployeeType DEFAULT_EMPLOYEE_TYPE = EmployeeType.RECEPTION;
    private static final EmployeeType UPDATED_EMPLOYEE_TYPE = EmployeeType.KITCHEN;

    private static final String DEFAULT_SHORT_ID = "AAAAAAAAAA";
    private static final String UPDATED_SHORT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_INSURANCE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_INSURANCE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_DATA = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_DATA = "BBBBBBBBBB";

    private static final String DEFAULT_CONTRACT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_TASKS = "AAAAAAAAAA";
    private static final String UPDATED_TASKS = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_BIRTHDAY = LocalDate.ofEpochDay(-1L);

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final String DEFAULT_TEL = "AAAAAAAAAA";
    private static final String UPDATED_TEL = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "so@g.xW";
    private static final String UPDATED_EMAIL = ".@/).h2";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeQueryService employeeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEmployeeMockMvc;

    private Employee employee;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EmployeeResource employeeResource = new EmployeeResource(employeeService, employeeQueryService);
        this.restEmployeeMockMvc = MockMvcBuilders.standaloneSetup(employeeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Employee createEntity(EntityManager em) {
        Employee employee = new Employee()
            .employeeType(DEFAULT_EMPLOYEE_TYPE)
            .shortId(DEFAULT_SHORT_ID)
            .insuranceNumber(DEFAULT_INSURANCE_NUMBER)
            .accountData(DEFAULT_ACCOUNT_DATA)
            .contractType(DEFAULT_CONTRACT_TYPE)
            .tasks(DEFAULT_TASKS)
            .name(DEFAULT_NAME)
            .birthday(DEFAULT_BIRTHDAY)
            .gender(DEFAULT_GENDER)
            .tel(DEFAULT_TEL)
            .email(DEFAULT_EMAIL)
            .address(DEFAULT_ADDRESS);
        return employee;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Employee createUpdatedEntity(EntityManager em) {
        Employee employee = new Employee()
            .employeeType(UPDATED_EMPLOYEE_TYPE)
            .shortId(UPDATED_SHORT_ID)
            .insuranceNumber(UPDATED_INSURANCE_NUMBER)
            .accountData(UPDATED_ACCOUNT_DATA)
            .contractType(UPDATED_CONTRACT_TYPE)
            .tasks(UPDATED_TASKS)
            .name(UPDATED_NAME)
            .birthday(UPDATED_BIRTHDAY)
            .gender(UPDATED_GENDER)
            .tel(UPDATED_TEL)
            .email(UPDATED_EMAIL)
            .address(UPDATED_ADDRESS);
        return employee;
    }

    @BeforeEach
    public void initTest() {
        employee = createEntity(em);
    }

//    @Test
//    @Transactional
//    public void createEmployee() throws Exception {
//        int databaseSizeBeforeCreate = employeeRepository.findAll().size();
//
//        // Create the Employee
//        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);
//        restEmployeeMockMvc.perform(post("/api/employees")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
//            .andExpect(status().isCreated());
//
//        // Validate the Employee in the database
//        List<Employee> employeeList = employeeRepository.findAll();
//        assertThat(employeeList).hasSize(databaseSizeBeforeCreate + 1);
//        Employee testEmployee = employeeList.get(employeeList.size() - 1);
//        assertThat(testEmployee.getEmployeeType()).isEqualTo(DEFAULT_EMPLOYEE_TYPE);
//        assertThat(testEmployee.getShortId()).isEqualTo(DEFAULT_SHORT_ID);
//        assertThat(testEmployee.getInsuranceNumber()).isEqualTo(DEFAULT_INSURANCE_NUMBER);
//        assertThat(testEmployee.getAccountData()).isEqualTo(DEFAULT_ACCOUNT_DATA);
//        assertThat(testEmployee.getContractType()).isEqualTo(DEFAULT_CONTRACT_TYPE);
//        assertThat(testEmployee.getTasks()).isEqualTo(DEFAULT_TASKS);
//        assertThat(testEmployee.getName()).isEqualTo(DEFAULT_NAME);
//        assertThat(testEmployee.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
//        assertThat(testEmployee.getGender()).isEqualTo(DEFAULT_GENDER);
//        assertThat(testEmployee.getTel()).isEqualTo(DEFAULT_TEL);
//        assertThat(testEmployee.getEmail()).isEqualTo(DEFAULT_EMAIL);
//        assertThat(testEmployee.getAddress()).isEqualTo(DEFAULT_ADDRESS);
//    }

    @Test
    @Transactional
    public void createEmployeeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = employeeRepository.findAll().size();

        // Create the Employee with an existing ID
        employee.setId(1L);
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Employee in the database
        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEmployeeTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setEmployeeType(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkShortIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setShortId(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInsuranceNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setInsuranceNumber(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAccountDataIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setAccountData(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContractTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setContractType(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTasksIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setTasks(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setName(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBirthdayIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setBirthday(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setGender(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTelIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setTel(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = employeeRepository.findAll().size();
        // set the field null
        employee.setAddress(null);

        // Create the Employee, which fails.
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        restEmployeeMockMvc.perform(post("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeTest);
    }

    //      Obsolete, the definition of get all has changed
//    @Test
//    @Transactional
//    public void getAllEmployees() throws Exception {
//        // Initialize the database
//        employeeRepository.saveAndFlush(employee);
//
//        // Get all the employeeList
//        restEmployeeMockMvc.perform(get("/api/employees?sort=id,desc"))
//            .andExpect(status().isOk())
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
//            .andExpect(jsonPath("$.[*].id").value(hasItem(employee.getId().intValue())))
//            .andExpect(jsonPath("$.[*].employeeType").value(hasItem(DEFAULT_EMPLOYEE_TYPE.toString())))
//            .andExpect(jsonPath("$.[*].shortId").value(hasItem(DEFAULT_SHORT_ID)))
//            .andExpect(jsonPath("$.[*].insuranceNumber").value(hasItem(DEFAULT_INSURANCE_NUMBER)))
//            .andExpect(jsonPath("$.[*].accountData").value(hasItem(DEFAULT_ACCOUNT_DATA)))
//            .andExpect(jsonPath("$.[*].contractType").value(hasItem(DEFAULT_CONTRACT_TYPE)))
//            .andExpect(jsonPath("$.[*].tasks").value(hasItem(DEFAULT_TASKS)))
//            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
//            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
//            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
//            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL)))
//            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
//            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)));
//    }

    @Test
    @Transactional
    public void getEmployee() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get the employee
        restEmployeeMockMvc.perform(get("/api/employees/{id}", employee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(employee.getId().intValue()))
            .andExpect(jsonPath("$.employeeType").value(DEFAULT_EMPLOYEE_TYPE.toString()))
            .andExpect(jsonPath("$.shortId").value(DEFAULT_SHORT_ID))
            .andExpect(jsonPath("$.insuranceNumber").value(DEFAULT_INSURANCE_NUMBER))
            .andExpect(jsonPath("$.accountData").value(DEFAULT_ACCOUNT_DATA))
            .andExpect(jsonPath("$.contractType").value(DEFAULT_CONTRACT_TYPE))
            .andExpect(jsonPath("$.tasks").value(DEFAULT_TASKS))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.tel").value(DEFAULT_TEL))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS));
    }

    @Test
    @Transactional
    public void getAllEmployeesByEmployeeTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where employeeType equals to DEFAULT_EMPLOYEE_TYPE
        defaultEmployeeShouldBeFound("employeeType.equals=" + DEFAULT_EMPLOYEE_TYPE);

        // Get all the employeeList where employeeType equals to UPDATED_EMPLOYEE_TYPE
        defaultEmployeeShouldNotBeFound("employeeType.equals=" + UPDATED_EMPLOYEE_TYPE);
    }

    @Test
    @Transactional
    public void getAllEmployeesByEmployeeTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where employeeType not equals to DEFAULT_EMPLOYEE_TYPE
        defaultEmployeeShouldNotBeFound("employeeType.notEquals=" + DEFAULT_EMPLOYEE_TYPE);

        // Get all the employeeList where employeeType not equals to UPDATED_EMPLOYEE_TYPE
        defaultEmployeeShouldBeFound("employeeType.notEquals=" + UPDATED_EMPLOYEE_TYPE);
    }

    @Test
    @Transactional
    public void getAllEmployeesByEmployeeTypeIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where employeeType in DEFAULT_EMPLOYEE_TYPE or UPDATED_EMPLOYEE_TYPE
        defaultEmployeeShouldBeFound("employeeType.in=" + DEFAULT_EMPLOYEE_TYPE + "," + UPDATED_EMPLOYEE_TYPE);

        // Get all the employeeList where employeeType equals to UPDATED_EMPLOYEE_TYPE
        defaultEmployeeShouldNotBeFound("employeeType.in=" + UPDATED_EMPLOYEE_TYPE);
    }

    @Test
    @Transactional
    public void getAllEmployeesByEmployeeTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where employeeType is not null
        defaultEmployeeShouldBeFound("employeeType.specified=true");

        // Get all the employeeList where employeeType is null
        defaultEmployeeShouldNotBeFound("employeeType.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmployeesByShortIdIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where shortId equals to DEFAULT_SHORT_ID
        defaultEmployeeShouldBeFound("shortId.equals=" + DEFAULT_SHORT_ID);

        // Get all the employeeList where shortId equals to UPDATED_SHORT_ID
        defaultEmployeeShouldNotBeFound("shortId.equals=" + UPDATED_SHORT_ID);
    }

    @Test
    @Transactional
    public void getAllEmployeesByShortIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where shortId not equals to DEFAULT_SHORT_ID
        defaultEmployeeShouldNotBeFound("shortId.notEquals=" + DEFAULT_SHORT_ID);

        // Get all the employeeList where shortId not equals to UPDATED_SHORT_ID
        defaultEmployeeShouldBeFound("shortId.notEquals=" + UPDATED_SHORT_ID);
    }

    @Test
    @Transactional
    public void getAllEmployeesByShortIdIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where shortId in DEFAULT_SHORT_ID or UPDATED_SHORT_ID
        defaultEmployeeShouldBeFound("shortId.in=" + DEFAULT_SHORT_ID + "," + UPDATED_SHORT_ID);

        // Get all the employeeList where shortId equals to UPDATED_SHORT_ID
        defaultEmployeeShouldNotBeFound("shortId.in=" + UPDATED_SHORT_ID);
    }

    @Test
    @Transactional
    public void getAllEmployeesByShortIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where shortId is not null
        defaultEmployeeShouldBeFound("shortId.specified=true");

        // Get all the employeeList where shortId is null
        defaultEmployeeShouldNotBeFound("shortId.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByShortIdContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where shortId contains DEFAULT_SHORT_ID
        defaultEmployeeShouldBeFound("shortId.contains=" + DEFAULT_SHORT_ID);

        // Get all the employeeList where shortId contains UPDATED_SHORT_ID
        defaultEmployeeShouldNotBeFound("shortId.contains=" + UPDATED_SHORT_ID);
    }

    @Test
    @Transactional
    public void getAllEmployeesByShortIdNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where shortId does not contain DEFAULT_SHORT_ID
        defaultEmployeeShouldNotBeFound("shortId.doesNotContain=" + DEFAULT_SHORT_ID);

        // Get all the employeeList where shortId does not contain UPDATED_SHORT_ID
        defaultEmployeeShouldBeFound("shortId.doesNotContain=" + UPDATED_SHORT_ID);
    }


    @Test
    @Transactional
    public void getAllEmployeesByInsuranceNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where insuranceNumber equals to DEFAULT_INSURANCE_NUMBER
        defaultEmployeeShouldBeFound("insuranceNumber.equals=" + DEFAULT_INSURANCE_NUMBER);

        // Get all the employeeList where insuranceNumber equals to UPDATED_INSURANCE_NUMBER
        defaultEmployeeShouldNotBeFound("insuranceNumber.equals=" + UPDATED_INSURANCE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllEmployeesByInsuranceNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where insuranceNumber not equals to DEFAULT_INSURANCE_NUMBER
        defaultEmployeeShouldNotBeFound("insuranceNumber.notEquals=" + DEFAULT_INSURANCE_NUMBER);

        // Get all the employeeList where insuranceNumber not equals to UPDATED_INSURANCE_NUMBER
        defaultEmployeeShouldBeFound("insuranceNumber.notEquals=" + UPDATED_INSURANCE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllEmployeesByInsuranceNumberIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where insuranceNumber in DEFAULT_INSURANCE_NUMBER or UPDATED_INSURANCE_NUMBER
        defaultEmployeeShouldBeFound("insuranceNumber.in=" + DEFAULT_INSURANCE_NUMBER + "," + UPDATED_INSURANCE_NUMBER);

        // Get all the employeeList where insuranceNumber equals to UPDATED_INSURANCE_NUMBER
        defaultEmployeeShouldNotBeFound("insuranceNumber.in=" + UPDATED_INSURANCE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllEmployeesByInsuranceNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where insuranceNumber is not null
        defaultEmployeeShouldBeFound("insuranceNumber.specified=true");

        // Get all the employeeList where insuranceNumber is null
        defaultEmployeeShouldNotBeFound("insuranceNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByInsuranceNumberContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where insuranceNumber contains DEFAULT_INSURANCE_NUMBER
        defaultEmployeeShouldBeFound("insuranceNumber.contains=" + DEFAULT_INSURANCE_NUMBER);

        // Get all the employeeList where insuranceNumber contains UPDATED_INSURANCE_NUMBER
        defaultEmployeeShouldNotBeFound("insuranceNumber.contains=" + UPDATED_INSURANCE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllEmployeesByInsuranceNumberNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where insuranceNumber does not contain DEFAULT_INSURANCE_NUMBER
        defaultEmployeeShouldNotBeFound("insuranceNumber.doesNotContain=" + DEFAULT_INSURANCE_NUMBER);

        // Get all the employeeList where insuranceNumber does not contain UPDATED_INSURANCE_NUMBER
        defaultEmployeeShouldBeFound("insuranceNumber.doesNotContain=" + UPDATED_INSURANCE_NUMBER);
    }


    @Test
    @Transactional
    public void getAllEmployeesByAccountDataIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where accountData equals to DEFAULT_ACCOUNT_DATA
        defaultEmployeeShouldBeFound("accountData.equals=" + DEFAULT_ACCOUNT_DATA);

        // Get all the employeeList where accountData equals to UPDATED_ACCOUNT_DATA
        defaultEmployeeShouldNotBeFound("accountData.equals=" + UPDATED_ACCOUNT_DATA);
    }

    @Test
    @Transactional
    public void getAllEmployeesByAccountDataIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where accountData not equals to DEFAULT_ACCOUNT_DATA
        defaultEmployeeShouldNotBeFound("accountData.notEquals=" + DEFAULT_ACCOUNT_DATA);

        // Get all the employeeList where accountData not equals to UPDATED_ACCOUNT_DATA
        defaultEmployeeShouldBeFound("accountData.notEquals=" + UPDATED_ACCOUNT_DATA);
    }

    @Test
    @Transactional
    public void getAllEmployeesByAccountDataIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where accountData in DEFAULT_ACCOUNT_DATA or UPDATED_ACCOUNT_DATA
        defaultEmployeeShouldBeFound("accountData.in=" + DEFAULT_ACCOUNT_DATA + "," + UPDATED_ACCOUNT_DATA);

        // Get all the employeeList where accountData equals to UPDATED_ACCOUNT_DATA
        defaultEmployeeShouldNotBeFound("accountData.in=" + UPDATED_ACCOUNT_DATA);
    }

    @Test
    @Transactional
    public void getAllEmployeesByAccountDataIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where accountData is not null
        defaultEmployeeShouldBeFound("accountData.specified=true");

        // Get all the employeeList where accountData is null
        defaultEmployeeShouldNotBeFound("accountData.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByAccountDataContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where accountData contains DEFAULT_ACCOUNT_DATA
        defaultEmployeeShouldBeFound("accountData.contains=" + DEFAULT_ACCOUNT_DATA);

        // Get all the employeeList where accountData contains UPDATED_ACCOUNT_DATA
        defaultEmployeeShouldNotBeFound("accountData.contains=" + UPDATED_ACCOUNT_DATA);
    }

    @Test
    @Transactional
    public void getAllEmployeesByAccountDataNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where accountData does not contain DEFAULT_ACCOUNT_DATA
        defaultEmployeeShouldNotBeFound("accountData.doesNotContain=" + DEFAULT_ACCOUNT_DATA);

        // Get all the employeeList where accountData does not contain UPDATED_ACCOUNT_DATA
        defaultEmployeeShouldBeFound("accountData.doesNotContain=" + UPDATED_ACCOUNT_DATA);
    }


    @Test
    @Transactional
    public void getAllEmployeesByContractTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where contractType equals to DEFAULT_CONTRACT_TYPE
        defaultEmployeeShouldBeFound("contractType.equals=" + DEFAULT_CONTRACT_TYPE);

        // Get all the employeeList where contractType equals to UPDATED_CONTRACT_TYPE
        defaultEmployeeShouldNotBeFound("contractType.equals=" + UPDATED_CONTRACT_TYPE);
    }

    @Test
    @Transactional
    public void getAllEmployeesByContractTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where contractType not equals to DEFAULT_CONTRACT_TYPE
        defaultEmployeeShouldNotBeFound("contractType.notEquals=" + DEFAULT_CONTRACT_TYPE);

        // Get all the employeeList where contractType not equals to UPDATED_CONTRACT_TYPE
        defaultEmployeeShouldBeFound("contractType.notEquals=" + UPDATED_CONTRACT_TYPE);
    }

    @Test
    @Transactional
    public void getAllEmployeesByContractTypeIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where contractType in DEFAULT_CONTRACT_TYPE or UPDATED_CONTRACT_TYPE
        defaultEmployeeShouldBeFound("contractType.in=" + DEFAULT_CONTRACT_TYPE + "," + UPDATED_CONTRACT_TYPE);

        // Get all the employeeList where contractType equals to UPDATED_CONTRACT_TYPE
        defaultEmployeeShouldNotBeFound("contractType.in=" + UPDATED_CONTRACT_TYPE);
    }

    @Test
    @Transactional
    public void getAllEmployeesByContractTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where contractType is not null
        defaultEmployeeShouldBeFound("contractType.specified=true");

        // Get all the employeeList where contractType is null
        defaultEmployeeShouldNotBeFound("contractType.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByContractTypeContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where contractType contains DEFAULT_CONTRACT_TYPE
        defaultEmployeeShouldBeFound("contractType.contains=" + DEFAULT_CONTRACT_TYPE);

        // Get all the employeeList where contractType contains UPDATED_CONTRACT_TYPE
        defaultEmployeeShouldNotBeFound("contractType.contains=" + UPDATED_CONTRACT_TYPE);
    }

    @Test
    @Transactional
    public void getAllEmployeesByContractTypeNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where contractType does not contain DEFAULT_CONTRACT_TYPE
        defaultEmployeeShouldNotBeFound("contractType.doesNotContain=" + DEFAULT_CONTRACT_TYPE);

        // Get all the employeeList where contractType does not contain UPDATED_CONTRACT_TYPE
        defaultEmployeeShouldBeFound("contractType.doesNotContain=" + UPDATED_CONTRACT_TYPE);
    }


    @Test
    @Transactional
    public void getAllEmployeesByTasksIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tasks equals to DEFAULT_TASKS
        defaultEmployeeShouldBeFound("tasks.equals=" + DEFAULT_TASKS);

        // Get all the employeeList where tasks equals to UPDATED_TASKS
        defaultEmployeeShouldNotBeFound("tasks.equals=" + UPDATED_TASKS);
    }

    @Test
    @Transactional
    public void getAllEmployeesByTasksIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tasks not equals to DEFAULT_TASKS
        defaultEmployeeShouldNotBeFound("tasks.notEquals=" + DEFAULT_TASKS);

        // Get all the employeeList where tasks not equals to UPDATED_TASKS
        defaultEmployeeShouldBeFound("tasks.notEquals=" + UPDATED_TASKS);
    }

    @Test
    @Transactional
    public void getAllEmployeesByTasksIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tasks in DEFAULT_TASKS or UPDATED_TASKS
        defaultEmployeeShouldBeFound("tasks.in=" + DEFAULT_TASKS + "," + UPDATED_TASKS);

        // Get all the employeeList where tasks equals to UPDATED_TASKS
        defaultEmployeeShouldNotBeFound("tasks.in=" + UPDATED_TASKS);
    }

    @Test
    @Transactional
    public void getAllEmployeesByTasksIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tasks is not null
        defaultEmployeeShouldBeFound("tasks.specified=true");

        // Get all the employeeList where tasks is null
        defaultEmployeeShouldNotBeFound("tasks.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByTasksContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tasks contains DEFAULT_TASKS
        defaultEmployeeShouldBeFound("tasks.contains=" + DEFAULT_TASKS);

        // Get all the employeeList where tasks contains UPDATED_TASKS
        defaultEmployeeShouldNotBeFound("tasks.contains=" + UPDATED_TASKS);
    }

    @Test
    @Transactional
    public void getAllEmployeesByTasksNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tasks does not contain DEFAULT_TASKS
        defaultEmployeeShouldNotBeFound("tasks.doesNotContain=" + DEFAULT_TASKS);

        // Get all the employeeList where tasks does not contain UPDATED_TASKS
        defaultEmployeeShouldBeFound("tasks.doesNotContain=" + UPDATED_TASKS);
    }


    @Test
    @Transactional
    public void getAllEmployeesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where name equals to DEFAULT_NAME
        defaultEmployeeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the employeeList where name equals to UPDATED_NAME
        defaultEmployeeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllEmployeesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where name not equals to DEFAULT_NAME
        defaultEmployeeShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the employeeList where name not equals to UPDATED_NAME
        defaultEmployeeShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllEmployeesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultEmployeeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the employeeList where name equals to UPDATED_NAME
        defaultEmployeeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllEmployeesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where name is not null
        defaultEmployeeShouldBeFound("name.specified=true");

        // Get all the employeeList where name is null
        defaultEmployeeShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByNameContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where name contains DEFAULT_NAME
        defaultEmployeeShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the employeeList where name contains UPDATED_NAME
        defaultEmployeeShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllEmployeesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where name does not contain DEFAULT_NAME
        defaultEmployeeShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the employeeList where name does not contain UPDATED_NAME
        defaultEmployeeShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllEmployeesByBirthdayIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where birthday equals to DEFAULT_BIRTHDAY
        defaultEmployeeShouldBeFound("birthday.equals=" + DEFAULT_BIRTHDAY);

        // Get all the employeeList where birthday equals to UPDATED_BIRTHDAY
        defaultEmployeeShouldNotBeFound("birthday.equals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllEmployeesByBirthdayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where birthday not equals to DEFAULT_BIRTHDAY
        defaultEmployeeShouldNotBeFound("birthday.notEquals=" + DEFAULT_BIRTHDAY);

        // Get all the employeeList where birthday not equals to UPDATED_BIRTHDAY
        defaultEmployeeShouldBeFound("birthday.notEquals=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllEmployeesByBirthdayIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where birthday in DEFAULT_BIRTHDAY or UPDATED_BIRTHDAY
        defaultEmployeeShouldBeFound("birthday.in=" + DEFAULT_BIRTHDAY + "," + UPDATED_BIRTHDAY);

        // Get all the employeeList where birthday equals to UPDATED_BIRTHDAY
        defaultEmployeeShouldNotBeFound("birthday.in=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllEmployeesByBirthdayIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where birthday is not null
        defaultEmployeeShouldBeFound("birthday.specified=true");

        // Get all the employeeList where birthday is null
        defaultEmployeeShouldNotBeFound("birthday.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmployeesByBirthdayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where birthday is greater than or equal to DEFAULT_BIRTHDAY
        defaultEmployeeShouldBeFound("birthday.greaterThanOrEqual=" + DEFAULT_BIRTHDAY);

        // Get all the employeeList where birthday is greater than or equal to UPDATED_BIRTHDAY
        defaultEmployeeShouldNotBeFound("birthday.greaterThanOrEqual=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllEmployeesByBirthdayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where birthday is less than or equal to DEFAULT_BIRTHDAY
        defaultEmployeeShouldBeFound("birthday.lessThanOrEqual=" + DEFAULT_BIRTHDAY);

        // Get all the employeeList where birthday is less than or equal to SMALLER_BIRTHDAY
        defaultEmployeeShouldNotBeFound("birthday.lessThanOrEqual=" + SMALLER_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllEmployeesByBirthdayIsLessThanSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where birthday is less than DEFAULT_BIRTHDAY
        defaultEmployeeShouldNotBeFound("birthday.lessThan=" + DEFAULT_BIRTHDAY);

        // Get all the employeeList where birthday is less than UPDATED_BIRTHDAY
        defaultEmployeeShouldBeFound("birthday.lessThan=" + UPDATED_BIRTHDAY);
    }

    @Test
    @Transactional
    public void getAllEmployeesByBirthdayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where birthday is greater than DEFAULT_BIRTHDAY
        defaultEmployeeShouldNotBeFound("birthday.greaterThan=" + DEFAULT_BIRTHDAY);

        // Get all the employeeList where birthday is greater than SMALLER_BIRTHDAY
        defaultEmployeeShouldBeFound("birthday.greaterThan=" + SMALLER_BIRTHDAY);
    }


    @Test
    @Transactional
    public void getAllEmployeesByGenderIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where gender equals to DEFAULT_GENDER
        defaultEmployeeShouldBeFound("gender.equals=" + DEFAULT_GENDER);

        // Get all the employeeList where gender equals to UPDATED_GENDER
        defaultEmployeeShouldNotBeFound("gender.equals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllEmployeesByGenderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where gender not equals to DEFAULT_GENDER
        defaultEmployeeShouldNotBeFound("gender.notEquals=" + DEFAULT_GENDER);

        // Get all the employeeList where gender not equals to UPDATED_GENDER
        defaultEmployeeShouldBeFound("gender.notEquals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllEmployeesByGenderIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where gender in DEFAULT_GENDER or UPDATED_GENDER
        defaultEmployeeShouldBeFound("gender.in=" + DEFAULT_GENDER + "," + UPDATED_GENDER);

        // Get all the employeeList where gender equals to UPDATED_GENDER
        defaultEmployeeShouldNotBeFound("gender.in=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllEmployeesByGenderIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where gender is not null
        defaultEmployeeShouldBeFound("gender.specified=true");

        // Get all the employeeList where gender is null
        defaultEmployeeShouldNotBeFound("gender.specified=false");
    }

    @Test
    @Transactional
    public void getAllEmployeesByTelIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tel equals to DEFAULT_TEL
        defaultEmployeeShouldBeFound("tel.equals=" + DEFAULT_TEL);

        // Get all the employeeList where tel equals to UPDATED_TEL
        defaultEmployeeShouldNotBeFound("tel.equals=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllEmployeesByTelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tel not equals to DEFAULT_TEL
        defaultEmployeeShouldNotBeFound("tel.notEquals=" + DEFAULT_TEL);

        // Get all the employeeList where tel not equals to UPDATED_TEL
        defaultEmployeeShouldBeFound("tel.notEquals=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllEmployeesByTelIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tel in DEFAULT_TEL or UPDATED_TEL
        defaultEmployeeShouldBeFound("tel.in=" + DEFAULT_TEL + "," + UPDATED_TEL);

        // Get all the employeeList where tel equals to UPDATED_TEL
        defaultEmployeeShouldNotBeFound("tel.in=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllEmployeesByTelIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tel is not null
        defaultEmployeeShouldBeFound("tel.specified=true");

        // Get all the employeeList where tel is null
        defaultEmployeeShouldNotBeFound("tel.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByTelContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tel contains DEFAULT_TEL
        defaultEmployeeShouldBeFound("tel.contains=" + DEFAULT_TEL);

        // Get all the employeeList where tel contains UPDATED_TEL
        defaultEmployeeShouldNotBeFound("tel.contains=" + UPDATED_TEL);
    }

    @Test
    @Transactional
    public void getAllEmployeesByTelNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where tel does not contain DEFAULT_TEL
        defaultEmployeeShouldNotBeFound("tel.doesNotContain=" + DEFAULT_TEL);

        // Get all the employeeList where tel does not contain UPDATED_TEL
        defaultEmployeeShouldBeFound("tel.doesNotContain=" + UPDATED_TEL);
    }


    @Test
    @Transactional
    public void getAllEmployeesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where email equals to DEFAULT_EMAIL
        defaultEmployeeShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the employeeList where email equals to UPDATED_EMAIL
        defaultEmployeeShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllEmployeesByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where email not equals to DEFAULT_EMAIL
        defaultEmployeeShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the employeeList where email not equals to UPDATED_EMAIL
        defaultEmployeeShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllEmployeesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultEmployeeShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the employeeList where email equals to UPDATED_EMAIL
        defaultEmployeeShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllEmployeesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where email is not null
        defaultEmployeeShouldBeFound("email.specified=true");

        // Get all the employeeList where email is null
        defaultEmployeeShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByEmailContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where email contains DEFAULT_EMAIL
        defaultEmployeeShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the employeeList where email contains UPDATED_EMAIL
        defaultEmployeeShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllEmployeesByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where email does not contain DEFAULT_EMAIL
        defaultEmployeeShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the employeeList where email does not contain UPDATED_EMAIL
        defaultEmployeeShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllEmployeesByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where address equals to DEFAULT_ADDRESS
        defaultEmployeeShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the employeeList where address equals to UPDATED_ADDRESS
        defaultEmployeeShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllEmployeesByAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where address not equals to DEFAULT_ADDRESS
        defaultEmployeeShouldNotBeFound("address.notEquals=" + DEFAULT_ADDRESS);

        // Get all the employeeList where address not equals to UPDATED_ADDRESS
        defaultEmployeeShouldBeFound("address.notEquals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllEmployeesByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultEmployeeShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the employeeList where address equals to UPDATED_ADDRESS
        defaultEmployeeShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllEmployeesByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where address is not null
        defaultEmployeeShouldBeFound("address.specified=true");

        // Get all the employeeList where address is null
        defaultEmployeeShouldNotBeFound("address.specified=false");
    }
                @Test
    @Transactional
    public void getAllEmployeesByAddressContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where address contains DEFAULT_ADDRESS
        defaultEmployeeShouldBeFound("address.contains=" + DEFAULT_ADDRESS);

        // Get all the employeeList where address contains UPDATED_ADDRESS
        defaultEmployeeShouldNotBeFound("address.contains=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllEmployeesByAddressNotContainsSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        // Get all the employeeList where address does not contain DEFAULT_ADDRESS
        defaultEmployeeShouldNotBeFound("address.doesNotContain=" + DEFAULT_ADDRESS);

        // Get all the employeeList where address does not contain UPDATED_ADDRESS
        defaultEmployeeShouldBeFound("address.doesNotContain=" + UPDATED_ADDRESS);
    }


    @Test
    @Transactional
    public void getAllEmployeesByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        employee.setUser(user);
        employeeRepository.saveAndFlush(employee);
        Long userId = user.getId();

        // Get all the employeeList where user equals to userId
        defaultEmployeeShouldBeFound("userId.equals=" + userId);

        // Get all the employeeList where user equals to userId + 1
        defaultEmployeeShouldNotBeFound("userId.equals=" + (userId + 1));
    }


//    @Test
//    @Transactional
//    public void getAllEmployeesByEmployeeIsEqualToSomething() throws Exception {
//        // Initialize the database
//        employeeRepository.saveAndFlush(employee);
//        Employee employee = EmployeeResourceIT.createEntity(em);
//        em.persist(employee);
//        em.flush();
//        employee.addEmployee(employee);
//        employeeRepository.saveAndFlush(employee);
//        Long employeeId = employee.getId();
//
//        // Get all the employeeList where employee equals to employeeId
//        defaultEmployeeShouldBeFound("employeeId.equals=" + employeeId);
//
//        // Get all the employeeList where employee equals to employeeId + 1
//        defaultEmployeeShouldNotBeFound("employeeId.equals=" + (employeeId + 1));
//    }


    @Test
    @Transactional
    public void getAllEmployeesByVacationsIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);
        Vacation vacations = VacationResourceIT.createEntity(em);
        em.persist(vacations);
        em.flush();
        employee.addVacations(vacations);
        employeeRepository.saveAndFlush(employee);
        Long vacationsId = vacations.getId();

        // Get all the employeeList where vacations equals to vacationsId
        defaultEmployeeShouldBeFound("vacationsId.equals=" + vacationsId);

        // Get all the employeeList where vacations equals to vacationsId + 1
        defaultEmployeeShouldNotBeFound("vacationsId.equals=" + (vacationsId + 1));
    }


    @Test
    @Transactional
    public void getAllEmployeesByBossIsEqualToSomething() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);
        Employee boss = EmployeeResourceIT.createEntity(em);
        em.persist(boss);
        em.flush();
        employee.setBoss(boss);
        employeeRepository.saveAndFlush(employee);
        Long bossId = boss.getId();

        // Get all the employeeList where boss equals to bossId
        defaultEmployeeShouldBeFound("bossId.equals=" + bossId);

        // Get all the employeeList where boss equals to bossId + 1
        defaultEmployeeShouldNotBeFound("bossId.equals=" + (bossId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEmployeeShouldBeFound(String filter) throws Exception {
        restEmployeeMockMvc.perform(get("/api/employees?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(employee.getId().intValue())))
            .andExpect(jsonPath("$.[*].employeeType").value(hasItem(DEFAULT_EMPLOYEE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].shortId").value(hasItem(DEFAULT_SHORT_ID)))
            .andExpect(jsonPath("$.[*].insuranceNumber").value(hasItem(DEFAULT_INSURANCE_NUMBER)))
            .andExpect(jsonPath("$.[*].accountData").value(hasItem(DEFAULT_ACCOUNT_DATA)))
            .andExpect(jsonPath("$.[*].contractType").value(hasItem(DEFAULT_CONTRACT_TYPE)))
            .andExpect(jsonPath("$.[*].tasks").value(hasItem(DEFAULT_TASKS)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].tel").value(hasItem(DEFAULT_TEL)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)));

        // Check, that the count call also returns 1
        restEmployeeMockMvc.perform(get("/api/employees/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEmployeeShouldNotBeFound(String filter) throws Exception {
        restEmployeeMockMvc.perform(get("/api/employees?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEmployeeMockMvc.perform(get("/api/employees/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEmployee() throws Exception {
        // Get the employee
        restEmployeeMockMvc.perform(get("/api/employees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

//    @Test
//    @Transactional
//    public void updateEmployee() throws Exception {
//        // Initialize the database
//        employeeRepository.saveAndFlush(employee);
//
//        int databaseSizeBeforeUpdate = employeeRepository.findAll().size();
//
//        // Update the employee
//        Employee updatedEmployee = employeeRepository.findById(employee.getId()).get();
//        // Disconnect from session so that the updates on updatedEmployee are not directly saved in db
//        em.detach(updatedEmployee);
//        updatedEmployee
//            .employeeType(UPDATED_EMPLOYEE_TYPE)
//            .shortId(UPDATED_SHORT_ID)
//            .insuranceNumber(UPDATED_INSURANCE_NUMBER)
//            .accountData(UPDATED_ACCOUNT_DATA)
//            .contractType(UPDATED_CONTRACT_TYPE)
//            .tasks(UPDATED_TASKS)
//            .name(UPDATED_NAME)
//            .birthday(UPDATED_BIRTHDAY)
//            .gender(UPDATED_GENDER)
//            .tel(UPDATED_TEL)
//            .email(UPDATED_EMAIL)
//            .address(UPDATED_ADDRESS);
//        EmployeeDTO employeeDTO = employeeMapper.toDto(updatedEmployee);
//
//        restEmployeeMockMvc.perform(put("/api/employees")
//            .contentType(TestUtil.APPLICATION_JSON_UTF8)
//            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
//            .andExpect(status().isOk());
//
//        // Validate the Employee in the database
//        List<Employee> employeeList = employeeRepository.findAll();
//        assertThat(employeeList).hasSize(databaseSizeBeforeUpdate);
//        Employee testEmployee = employeeList.get(employeeList.size() - 1);
//        assertThat(testEmployee.getEmployeeType()).isEqualTo(UPDATED_EMPLOYEE_TYPE);
//        assertThat(testEmployee.getShortId()).isEqualTo(UPDATED_SHORT_ID);
//        assertThat(testEmployee.getInsuranceNumber()).isEqualTo(UPDATED_INSURANCE_NUMBER);
//        assertThat(testEmployee.getAccountData()).isEqualTo(UPDATED_ACCOUNT_DATA);
//        assertThat(testEmployee.getContractType()).isEqualTo(UPDATED_CONTRACT_TYPE);
//        assertThat(testEmployee.getTasks()).isEqualTo(UPDATED_TASKS);
//        assertThat(testEmployee.getName()).isEqualTo(UPDATED_NAME);
//        assertThat(testEmployee.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
//        assertThat(testEmployee.getGender()).isEqualTo(UPDATED_GENDER);
//        assertThat(testEmployee.getTel()).isEqualTo(UPDATED_TEL);
//        assertThat(testEmployee.getEmail()).isEqualTo(UPDATED_EMAIL);
//        assertThat(testEmployee.getAddress()).isEqualTo(UPDATED_ADDRESS);
//    }

    @Test
    @Transactional
    public void updateNonExistingEmployee() throws Exception {
        int databaseSizeBeforeUpdate = employeeRepository.findAll().size();

        // Create the Employee
        EmployeeDTO employeeDTO = employeeMapper.toDto(employee);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEmployeeMockMvc.perform(put("/api/employees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(employeeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Employee in the database
        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEmployee() throws Exception {
        // Initialize the database
        employeeRepository.saveAndFlush(employee);

        int databaseSizeBeforeDelete = employeeRepository.findAll().size();

        // Delete the employee
        restEmployeeMockMvc.perform(delete("/api/employees/{id}", employee.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Employee> employeeList = employeeRepository.findAll();
        assertThat(employeeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Employee.class);
        Employee employee1 = new Employee();
        employee1.setId(1L);
        Employee employee2 = new Employee();
        employee2.setId(employee1.getId());
        assertThat(employee1).isEqualTo(employee2);
        employee2.setId(2L);
        assertThat(employee1).isNotEqualTo(employee2);
        employee1.setId(null);
        assertThat(employee1).isNotEqualTo(employee2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EmployeeDTO.class);
        EmployeeDTO employeeDTO1 = new EmployeeDTO();
        employeeDTO1.setId(1L);
        EmployeeDTO employeeDTO2 = new EmployeeDTO();
        assertThat(employeeDTO1).isNotEqualTo(employeeDTO2);
        employeeDTO2.setId(employeeDTO1.getId());
        assertThat(employeeDTO1).isEqualTo(employeeDTO2);
        employeeDTO2.setId(2L);
        assertThat(employeeDTO1).isNotEqualTo(employeeDTO2);
        employeeDTO1.setId(null);
        assertThat(employeeDTO1).isNotEqualTo(employeeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(employeeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(employeeMapper.fromId(null)).isNull();
    }
}
