package com.sisw19.hotelmgnt.web.rest;

import static com.sisw19.hotelmgnt.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.Booking;
import com.sisw19.hotelmgnt.domain.Customer;
import com.sisw19.hotelmgnt.domain.Hotel;
import com.sisw19.hotelmgnt.domain.Invoice;
import com.sisw19.hotelmgnt.domain.enumeration.Gender;
import com.sisw19.hotelmgnt.domain.enumeration.State;
import com.sisw19.hotelmgnt.repository.BookingRepository;
import com.sisw19.hotelmgnt.repository.CustomerRepository;
import com.sisw19.hotelmgnt.repository.HotelRepository;
import com.sisw19.hotelmgnt.repository.InvoiceRepository;
import com.sisw19.hotelmgnt.service.BookingQueryService;
import com.sisw19.hotelmgnt.service.BookingService;
import com.sisw19.hotelmgnt.service.dto.InvoiceDTO;
import com.sisw19.hotelmgnt.service.mapper.InvoiceMapper;
import com.sisw19.hotelmgnt.web.rest.errors.ExceptionTranslator;

import java.math.BigDecimal;
import java.time.*;
import java.util.Arrays;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

/**
 * Integration tests for the {@link InvoiceResource} REST controller.
 */
@SpringBootTest(classes = HotelManagementApp.class)
public class InvoiceResourceIT {

    private static final ZonedDateTime DEFAULT_INVOICE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_INVOICE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final State DEFAULT_INVOICE_STATE = State.ACTIVE;
    private static final State UPDATED_INVOICE_STATE = State.CANCELLED;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private InvoiceMapper invoiceMapper;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingQueryService bookingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBookingMockMvc;

    private Invoice invoice;

    private Booking booking;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BookingResource bookingResource = new BookingResource(bookingService, bookingQueryService);
        this.restBookingMockMvc = MockMvcBuilders.standaloneSetup(bookingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an invoice entity for this test.
     */
    public Invoice createEntityInvoice(EntityManager em, Booking booking) {
        Hotel hotel = new Hotel();
        hotel.setDefaultCancellationNoticeNights(3);
        hotel.setHotelAddress("HotelAddress");
        hotelRepository.saveAndFlush(hotel);

        Invoice invoice = new Invoice()
            .invoiceDate(DEFAULT_INVOICE_DATE)
            .invoiceState(DEFAULT_INVOICE_STATE);
        invoice.setBooking(booking);
        invoice.setDiscountPercent(12d);
        invoice.setDurationNights(2);
        invoice.setStartDate(ZonedDateTime.now());
        invoice.setEndDate(ZonedDateTime.now().plusDays(1));
        invoice.setCustomerName("Customer");
        invoice.setCustomerGender(Gender.MALE);
        invoice.setCustomerAddress("CAddress");
        invoice.setHotelAddress(hotel.getHotelAddress());
        booking.getInvoices().add(invoice);
        bookingRepository.saveAndFlush(booking);
        return invoiceRepository.saveAndFlush(invoice);
    }

    /**
     * Create an invoice entity for this test.
     */
    private Booking createEntityBooking(EntityManager em) {
        Customer c = new Customer();
        c.setDiscountPercent(10d);
        c.setBillingAddress("Address");
        c.setBirthday(LocalDate.now());
        c.setEmail("email@email.com");
        c.setGender(Gender.MALE);
        c.setName("Name");
        c.setNotes("Notes");
        c.setTel("1223344");

        customerRepository.saveAndFlush(c);

        Booking booking = new Booking();
        booking.setPrice(new BigDecimal(0));
        booking.setCancellationNoticeNights(2);
        booking.setBookingState(State.ACTIVE);
        booking.setStartDate(DEFAULT_INVOICE_DATE);
        booking.setEndDate(ZonedDateTime.now().plusWeeks(1));
        booking.setDiscountPercent(13d);
        booking.setCustomer(c);

        return bookingRepository.saveAndFlush(booking);
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Invoice createUpdatedEntity(EntityManager em) {
        Invoice invoice = new Invoice()
            .invoiceDate(UPDATED_INVOICE_DATE)
            .invoiceState(UPDATED_INVOICE_STATE);
        return invoice;
    }

    @BeforeEach
    public void initTest() {
        booking = createEntityBooking(em);
        invoice = createEntityInvoice(em, booking);
    }

    @Test
    @Transactional
    public void createInvoice() throws Exception {
        int databaseSizeBeforeCreate = invoiceRepository.findAll().size();

        // Create the Invoice
        restBookingMockMvc.perform(post("/api/bookings/" + booking.getId() + "/createInvoice"))
            .andExpect(status().isCreated());

        // Validate the Invoice in the database
        List<Invoice> invoiceList = invoiceRepository.findAll();
        assertThat(invoiceList).hasSize(databaseSizeBeforeCreate + 1);
        Invoice testInvoice = invoiceList.get(invoiceList.size() - 1);
        assertThat(testInvoice.getBooking().getId()).isEqualTo(booking.getId());
        assertThat(testInvoice.getInvoiceState()).isEqualTo(DEFAULT_INVOICE_STATE);
    }

    @Test
    @Transactional
    public void getAllInvoices() throws Exception {
        // Initialize the database
        invoiceRepository.saveAndFlush(invoice);

        // Get all the invoiceList
        restBookingMockMvc.perform(get("/api/bookings/invoices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(booking.getId().intValue())))
            .andExpect(jsonPath("$..invoices").isArray())
            .andExpect(jsonPath("$..invoices.size())").value(is(Arrays.asList(1))));
    }

    @Test
    @Transactional
    public void getInvoice() throws Exception {
        // Initialize the database
        invoiceRepository.saveAndFlush(invoice);

        // Get the invoice
        restBookingMockMvc.perform(get("/api/bookings/invoice/{id}", invoice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(booking.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingInvoice() throws Exception {
        // Get the invoice
        restBookingMockMvc.perform(get("/api/invoices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Invoice.class);
        Invoice invoice1 = new Invoice();
        invoice1.setId(1L);
        Invoice invoice2 = new Invoice();
        invoice2.setId(invoice1.getId());
        assertThat(invoice1).isEqualTo(invoice2);
        invoice2.setId(2L);
        assertThat(invoice1).isNotEqualTo(invoice2);
        invoice1.setId(null);
        assertThat(invoice1).isNotEqualTo(invoice2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvoiceDTO.class);
        InvoiceDTO invoiceDTO1 = new InvoiceDTO();
        invoiceDTO1.setId(1L);
        InvoiceDTO invoiceDTO2 = new InvoiceDTO();
        assertThat(invoiceDTO1).isNotEqualTo(invoiceDTO2);
        invoiceDTO2.setId(invoiceDTO1.getId());
        assertThat(invoiceDTO1).isEqualTo(invoiceDTO2);
        invoiceDTO2.setId(2L);
        assertThat(invoiceDTO1).isNotEqualTo(invoiceDTO2);
        invoiceDTO1.setId(null);
        assertThat(invoiceDTO1).isNotEqualTo(invoiceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(invoiceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(invoiceMapper.fromId(null)).isNull();
    }
}
