package com.sisw19.hotelmgnt.web.rest;

import com.sisw19.hotelmgnt.HotelManagementApp;
import com.sisw19.hotelmgnt.domain.Room;
import com.sisw19.hotelmgnt.domain.Defect;
import com.sisw19.hotelmgnt.repository.RoomRepository;
import com.sisw19.hotelmgnt.service.RoomService;
import com.sisw19.hotelmgnt.service.dto.RoomDTO;
import com.sisw19.hotelmgnt.service.mapper.RoomMapper;
import com.sisw19.hotelmgnt.web.rest.errors.ExceptionTranslator;
import com.sisw19.hotelmgnt.service.dto.RoomCriteria;
import com.sisw19.hotelmgnt.service.RoomQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static com.sisw19.hotelmgnt.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RoomResource} REST controller.
 */
@SpringBootTest(classes = HotelManagementApp.class)
public class RoomResourceIT {

    private static final String DEFAULT_NUMBER = "01";
    private static final String UPDATED_NUMBER = "02";

    private static final Integer DEFAULT_MAX_CAPACITY = 1;
    private static final Integer UPDATED_MAX_CAPACITY = 2;
    private static final Integer SMALLER_MAX_CAPACITY = 1 - 1;

    private static final BigDecimal DEFAULT_PRICE_ONE_ADULT = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE_ONE_ADULT = new BigDecimal(1);
    private static final BigDecimal SMALLER_PRICE_ONE_ADULT = new BigDecimal(0 - 1);

    private static final BigDecimal DEFAULT_PRICE_TWO_ADULTS = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE_TWO_ADULTS = new BigDecimal(1);
    private static final BigDecimal SMALLER_PRICE_TWO_ADULTS = new BigDecimal(0 - 1);

    private static final BigDecimal DEFAULT_PRICE_THREE_ADULTS = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE_THREE_ADULTS = new BigDecimal(1);
    private static final BigDecimal SMALLER_PRICE_THREE_ADULTS = new BigDecimal(0 - 1);

    private static final BigDecimal DEFAULT_PRICE_ONE_ADULT_ONE_CHILD = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE_ONE_ADULT_ONE_CHILD = new BigDecimal(1);
    private static final BigDecimal SMALLER_PRICE_ONE_ADULT_ONE_CHILD = new BigDecimal(0 - 1);

    private static final BigDecimal DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE_TWO_ADULTS_ONE_CHILD = new BigDecimal(1);
    private static final BigDecimal SMALLER_PRICE_TWO_ADULTS_ONE_CHILD = new BigDecimal(0 - 1);

    private static final BigDecimal DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN = new BigDecimal(1);
    private static final BigDecimal SMALLER_PRICE_ONE_ADULT_TWO_CHILDREN = new BigDecimal(0 - 1);

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomQueryService roomQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRoomMockMvc;

    private Room room;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RoomResource roomResource = new RoomResource(roomService, roomQueryService);
        this.restRoomMockMvc = MockMvcBuilders.standaloneSetup(roomResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Room createEntity(EntityManager em) {
        Room room = new Room()
            .number(DEFAULT_NUMBER)
            .maxCapacity(DEFAULT_MAX_CAPACITY)
            .priceOneAdult(DEFAULT_PRICE_ONE_ADULT)
            .priceTwoAdults(DEFAULT_PRICE_TWO_ADULTS)
            .priceThreeAdults(DEFAULT_PRICE_THREE_ADULTS)
            .priceOneAdultOneChild(DEFAULT_PRICE_ONE_ADULT_ONE_CHILD)
            .priceTwoAdultsOneChild(DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD)
            .priceOneAdultTwoChildren(DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);
        return room;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Room createUpdatedEntity(EntityManager em) {
        Room room = new Room()
            .number(UPDATED_NUMBER)
            .maxCapacity(UPDATED_MAX_CAPACITY)
            .priceOneAdult(UPDATED_PRICE_ONE_ADULT)
            .priceTwoAdults(UPDATED_PRICE_TWO_ADULTS)
            .priceThreeAdults(UPDATED_PRICE_THREE_ADULTS)
            .priceOneAdultOneChild(UPDATED_PRICE_ONE_ADULT_ONE_CHILD)
            .priceTwoAdultsOneChild(UPDATED_PRICE_TWO_ADULTS_ONE_CHILD)
            .priceOneAdultTwoChildren(UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);
        return room;
    }

    @BeforeEach
    public void initTest() {
        room = createEntity(em);
    }

    @Test
    @Transactional
    public void createRoom() throws Exception {
        int databaseSizeBeforeCreate = roomRepository.findAll().size();

        // Create the Room
        RoomDTO roomDTO = roomMapper.toDto(room);
        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isCreated());

        // Validate the Room in the database
        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeCreate + 1);
        Room testRoom = roomList.get(roomList.size() - 1);
        assertThat(testRoom.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testRoom.getMaxCapacity()).isEqualTo(DEFAULT_MAX_CAPACITY);
        assertThat(testRoom.getPriceOneAdult()).isEqualTo(DEFAULT_PRICE_ONE_ADULT);
        assertThat(testRoom.getPriceTwoAdults()).isEqualTo(DEFAULT_PRICE_TWO_ADULTS);
        assertThat(testRoom.getPriceThreeAdults()).isEqualTo(DEFAULT_PRICE_THREE_ADULTS);
        assertThat(testRoom.getPriceOneAdultOneChild()).isEqualTo(DEFAULT_PRICE_ONE_ADULT_ONE_CHILD);
        assertThat(testRoom.getPriceTwoAdultsOneChild()).isEqualTo(DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD);
        assertThat(testRoom.getPriceOneAdultTwoChildren()).isEqualTo(DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);
    }

    @Test
    @Transactional
    public void createRoomWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = roomRepository.findAll().size();

        // Create the Room with an existing ID
        room.setId(1L);
        RoomDTO roomDTO = roomMapper.toDto(room);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Room in the database
        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = roomRepository.findAll().size();
        // set the field null
        room.setNumber(null);

        // Create the Room, which fails.
        RoomDTO roomDTO = roomMapper.toDto(room);

        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaxCapacityIsRequired() throws Exception {
        int databaseSizeBeforeTest = roomRepository.findAll().size();
        // set the field null
        room.setMaxCapacity(null);

        // Create the Room, which fails.
        RoomDTO roomDTO = roomMapper.toDto(room);

        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceOneAdultIsRequired() throws Exception {
        int databaseSizeBeforeTest = roomRepository.findAll().size();
        // set the field null
        room.setPriceOneAdult(null);

        // Create the Room, which fails.
        RoomDTO roomDTO = roomMapper.toDto(room);

        restRoomMockMvc.perform(post("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRooms() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList
        restRoomMockMvc.perform(get("/api/rooms?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(room.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].maxCapacity").value(hasItem(DEFAULT_MAX_CAPACITY)))
            .andExpect(jsonPath("$.[*].priceOneAdult").value(hasItem(DEFAULT_PRICE_ONE_ADULT.intValue())))
            .andExpect(jsonPath("$.[*].priceTwoAdults").value(hasItem(DEFAULT_PRICE_TWO_ADULTS.intValue())))
            .andExpect(jsonPath("$.[*].priceThreeAdults").value(hasItem(DEFAULT_PRICE_THREE_ADULTS.intValue())))
            .andExpect(jsonPath("$.[*].priceOneAdultOneChild").value(hasItem(DEFAULT_PRICE_ONE_ADULT_ONE_CHILD.intValue())))
            .andExpect(jsonPath("$.[*].priceTwoAdultsOneChild").value(hasItem(DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD.intValue())))
            .andExpect(jsonPath("$.[*].priceOneAdultTwoChildren").value(hasItem(DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN.intValue())));
    }

    @Test
    @Transactional
    public void getRoom() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get the room
        restRoomMockMvc.perform(get("/api/rooms/{id}", room.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(room.getId().intValue()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER))
            .andExpect(jsonPath("$.maxCapacity").value(DEFAULT_MAX_CAPACITY))
            .andExpect(jsonPath("$.priceOneAdult").value(DEFAULT_PRICE_ONE_ADULT.intValue()))
            .andExpect(jsonPath("$.priceTwoAdults").value(DEFAULT_PRICE_TWO_ADULTS.intValue()))
            .andExpect(jsonPath("$.priceThreeAdults").value(DEFAULT_PRICE_THREE_ADULTS.intValue()))
            .andExpect(jsonPath("$.priceOneAdultOneChild").value(DEFAULT_PRICE_ONE_ADULT_ONE_CHILD.intValue()))
            .andExpect(jsonPath("$.priceTwoAdultsOneChild").value(DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD.intValue()))
            .andExpect(jsonPath("$.priceOneAdultTwoChildren").value(DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN.intValue()));
    }

    @Test
    @Transactional
    public void getAllRoomsByNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where number equals to DEFAULT_NUMBER
        defaultRoomShouldBeFound("number.equals=" + DEFAULT_NUMBER);

        // Get all the roomList where number equals to UPDATED_NUMBER
        defaultRoomShouldNotBeFound("number.equals=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllRoomsByNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where number not equals to DEFAULT_NUMBER
        defaultRoomShouldNotBeFound("number.notEquals=" + DEFAULT_NUMBER);

        // Get all the roomList where number not equals to UPDATED_NUMBER
        defaultRoomShouldBeFound("number.notEquals=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllRoomsByNumberIsInShouldWork() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where number in DEFAULT_NUMBER or UPDATED_NUMBER
        defaultRoomShouldBeFound("number.in=" + DEFAULT_NUMBER + "," + UPDATED_NUMBER);

        // Get all the roomList where number equals to UPDATED_NUMBER
        defaultRoomShouldNotBeFound("number.in=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllRoomsByNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where number is not null
        defaultRoomShouldBeFound("number.specified=true");

        // Get all the roomList where number is null
        defaultRoomShouldNotBeFound("number.specified=false");
    }
                @Test
    @Transactional
    public void getAllRoomsByNumberContainsSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where number contains DEFAULT_NUMBER
        defaultRoomShouldBeFound("number.contains=" + DEFAULT_NUMBER);

        // Get all the roomList where number contains UPDATED_NUMBER
        defaultRoomShouldNotBeFound("number.contains=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllRoomsByNumberNotContainsSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where number does not contain DEFAULT_NUMBER
        defaultRoomShouldNotBeFound("number.doesNotContain=" + DEFAULT_NUMBER);

        // Get all the roomList where number does not contain UPDATED_NUMBER
        defaultRoomShouldBeFound("number.doesNotContain=" + UPDATED_NUMBER);
    }


    @Test
    @Transactional
    public void getAllRoomsByMaxCapacityIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where maxCapacity equals to DEFAULT_MAX_CAPACITY
        defaultRoomShouldBeFound("maxCapacity.equals=" + DEFAULT_MAX_CAPACITY);

        // Get all the roomList where maxCapacity equals to UPDATED_MAX_CAPACITY
        defaultRoomShouldNotBeFound("maxCapacity.equals=" + UPDATED_MAX_CAPACITY);
    }

    @Test
    @Transactional
    public void getAllRoomsByMaxCapacityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where maxCapacity not equals to DEFAULT_MAX_CAPACITY
        defaultRoomShouldNotBeFound("maxCapacity.notEquals=" + DEFAULT_MAX_CAPACITY);

        // Get all the roomList where maxCapacity not equals to UPDATED_MAX_CAPACITY
        defaultRoomShouldBeFound("maxCapacity.notEquals=" + UPDATED_MAX_CAPACITY);
    }

    @Test
    @Transactional
    public void getAllRoomsByMaxCapacityIsInShouldWork() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where maxCapacity in DEFAULT_MAX_CAPACITY or UPDATED_MAX_CAPACITY
        defaultRoomShouldBeFound("maxCapacity.in=" + DEFAULT_MAX_CAPACITY + "," + UPDATED_MAX_CAPACITY);

        // Get all the roomList where maxCapacity equals to UPDATED_MAX_CAPACITY
        defaultRoomShouldNotBeFound("maxCapacity.in=" + UPDATED_MAX_CAPACITY);
    }

    @Test
    @Transactional
    public void getAllRoomsByMaxCapacityIsNullOrNotNull() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where maxCapacity is not null
        defaultRoomShouldBeFound("maxCapacity.specified=true");

        // Get all the roomList where maxCapacity is null
        defaultRoomShouldNotBeFound("maxCapacity.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoomsByMaxCapacityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where maxCapacity is greater than or equal to DEFAULT_MAX_CAPACITY
        defaultRoomShouldBeFound("maxCapacity.greaterThanOrEqual=" + DEFAULT_MAX_CAPACITY);

        // Get all the roomList where maxCapacity is greater than or equal to (DEFAULT_MAX_CAPACITY + 1)
        defaultRoomShouldNotBeFound("maxCapacity.greaterThanOrEqual=" + (DEFAULT_MAX_CAPACITY + 1));
    }

    @Test
    @Transactional
    public void getAllRoomsByMaxCapacityIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where maxCapacity is less than or equal to DEFAULT_MAX_CAPACITY
        defaultRoomShouldBeFound("maxCapacity.lessThanOrEqual=" + DEFAULT_MAX_CAPACITY);

        // Get all the roomList where maxCapacity is less than or equal to SMALLER_MAX_CAPACITY
        defaultRoomShouldNotBeFound("maxCapacity.lessThanOrEqual=" + SMALLER_MAX_CAPACITY);
    }

    @Test
    @Transactional
    public void getAllRoomsByMaxCapacityIsLessThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where maxCapacity is less than DEFAULT_MAX_CAPACITY
        defaultRoomShouldNotBeFound("maxCapacity.lessThan=" + DEFAULT_MAX_CAPACITY);

        // Get all the roomList where maxCapacity is less than (DEFAULT_MAX_CAPACITY + 1)
        defaultRoomShouldBeFound("maxCapacity.lessThan=" + (DEFAULT_MAX_CAPACITY + 1));
    }

    @Test
    @Transactional
    public void getAllRoomsByMaxCapacityIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where maxCapacity is greater than DEFAULT_MAX_CAPACITY
        defaultRoomShouldNotBeFound("maxCapacity.greaterThan=" + DEFAULT_MAX_CAPACITY);

        // Get all the roomList where maxCapacity is greater than SMALLER_MAX_CAPACITY
        defaultRoomShouldBeFound("maxCapacity.greaterThan=" + SMALLER_MAX_CAPACITY);
    }


    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdult equals to DEFAULT_PRICE_ONE_ADULT
        defaultRoomShouldBeFound("priceOneAdult.equals=" + DEFAULT_PRICE_ONE_ADULT);

        // Get all the roomList where priceOneAdult equals to UPDATED_PRICE_ONE_ADULT
        defaultRoomShouldNotBeFound("priceOneAdult.equals=" + UPDATED_PRICE_ONE_ADULT);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultIsNotEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdult not equals to DEFAULT_PRICE_ONE_ADULT
        defaultRoomShouldNotBeFound("priceOneAdult.notEquals=" + DEFAULT_PRICE_ONE_ADULT);

        // Get all the roomList where priceOneAdult not equals to UPDATED_PRICE_ONE_ADULT
        defaultRoomShouldBeFound("priceOneAdult.notEquals=" + UPDATED_PRICE_ONE_ADULT);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultIsInShouldWork() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdult in DEFAULT_PRICE_ONE_ADULT or UPDATED_PRICE_ONE_ADULT
        defaultRoomShouldBeFound("priceOneAdult.in=" + DEFAULT_PRICE_ONE_ADULT + "," + UPDATED_PRICE_ONE_ADULT);

        // Get all the roomList where priceOneAdult equals to UPDATED_PRICE_ONE_ADULT
        defaultRoomShouldNotBeFound("priceOneAdult.in=" + UPDATED_PRICE_ONE_ADULT);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultIsNullOrNotNull() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdult is not null
        defaultRoomShouldBeFound("priceOneAdult.specified=true");

        // Get all the roomList where priceOneAdult is null
        defaultRoomShouldNotBeFound("priceOneAdult.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdult is greater than or equal to DEFAULT_PRICE_ONE_ADULT
        defaultRoomShouldBeFound("priceOneAdult.greaterThanOrEqual=" + DEFAULT_PRICE_ONE_ADULT);

        // Get all the roomList where priceOneAdult is greater than or equal to UPDATED_PRICE_ONE_ADULT
        defaultRoomShouldNotBeFound("priceOneAdult.greaterThanOrEqual=" + UPDATED_PRICE_ONE_ADULT);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdult is less than or equal to DEFAULT_PRICE_ONE_ADULT
        defaultRoomShouldBeFound("priceOneAdult.lessThanOrEqual=" + DEFAULT_PRICE_ONE_ADULT);

        // Get all the roomList where priceOneAdult is less than or equal to SMALLER_PRICE_ONE_ADULT
        defaultRoomShouldNotBeFound("priceOneAdult.lessThanOrEqual=" + SMALLER_PRICE_ONE_ADULT);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultIsLessThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdult is less than DEFAULT_PRICE_ONE_ADULT
        defaultRoomShouldNotBeFound("priceOneAdult.lessThan=" + DEFAULT_PRICE_ONE_ADULT);

        // Get all the roomList where priceOneAdult is less than UPDATED_PRICE_ONE_ADULT
        defaultRoomShouldBeFound("priceOneAdult.lessThan=" + UPDATED_PRICE_ONE_ADULT);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdult is greater than DEFAULT_PRICE_ONE_ADULT
        defaultRoomShouldNotBeFound("priceOneAdult.greaterThan=" + DEFAULT_PRICE_ONE_ADULT);

        // Get all the roomList where priceOneAdult is greater than SMALLER_PRICE_ONE_ADULT
        defaultRoomShouldBeFound("priceOneAdult.greaterThan=" + SMALLER_PRICE_ONE_ADULT);
    }


    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdults equals to DEFAULT_PRICE_TWO_ADULTS
        defaultRoomShouldBeFound("priceTwoAdults.equals=" + DEFAULT_PRICE_TWO_ADULTS);

        // Get all the roomList where priceTwoAdults equals to UPDATED_PRICE_TWO_ADULTS
        defaultRoomShouldNotBeFound("priceTwoAdults.equals=" + UPDATED_PRICE_TWO_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdults not equals to DEFAULT_PRICE_TWO_ADULTS
        defaultRoomShouldNotBeFound("priceTwoAdults.notEquals=" + DEFAULT_PRICE_TWO_ADULTS);

        // Get all the roomList where priceTwoAdults not equals to UPDATED_PRICE_TWO_ADULTS
        defaultRoomShouldBeFound("priceTwoAdults.notEquals=" + UPDATED_PRICE_TWO_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsIsInShouldWork() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdults in DEFAULT_PRICE_TWO_ADULTS or UPDATED_PRICE_TWO_ADULTS
        defaultRoomShouldBeFound("priceTwoAdults.in=" + DEFAULT_PRICE_TWO_ADULTS + "," + UPDATED_PRICE_TWO_ADULTS);

        // Get all the roomList where priceTwoAdults equals to UPDATED_PRICE_TWO_ADULTS
        defaultRoomShouldNotBeFound("priceTwoAdults.in=" + UPDATED_PRICE_TWO_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsIsNullOrNotNull() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdults is not null
        defaultRoomShouldBeFound("priceTwoAdults.specified=true");

        // Get all the roomList where priceTwoAdults is null
        defaultRoomShouldNotBeFound("priceTwoAdults.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdults is greater than or equal to DEFAULT_PRICE_TWO_ADULTS
        defaultRoomShouldBeFound("priceTwoAdults.greaterThanOrEqual=" + DEFAULT_PRICE_TWO_ADULTS);

        // Get all the roomList where priceTwoAdults is greater than or equal to UPDATED_PRICE_TWO_ADULTS
        defaultRoomShouldNotBeFound("priceTwoAdults.greaterThanOrEqual=" + UPDATED_PRICE_TWO_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdults is less than or equal to DEFAULT_PRICE_TWO_ADULTS
        defaultRoomShouldBeFound("priceTwoAdults.lessThanOrEqual=" + DEFAULT_PRICE_TWO_ADULTS);

        // Get all the roomList where priceTwoAdults is less than or equal to SMALLER_PRICE_TWO_ADULTS
        defaultRoomShouldNotBeFound("priceTwoAdults.lessThanOrEqual=" + SMALLER_PRICE_TWO_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsIsLessThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdults is less than DEFAULT_PRICE_TWO_ADULTS
        defaultRoomShouldNotBeFound("priceTwoAdults.lessThan=" + DEFAULT_PRICE_TWO_ADULTS);

        // Get all the roomList where priceTwoAdults is less than UPDATED_PRICE_TWO_ADULTS
        defaultRoomShouldBeFound("priceTwoAdults.lessThan=" + UPDATED_PRICE_TWO_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdults is greater than DEFAULT_PRICE_TWO_ADULTS
        defaultRoomShouldNotBeFound("priceTwoAdults.greaterThan=" + DEFAULT_PRICE_TWO_ADULTS);

        // Get all the roomList where priceTwoAdults is greater than SMALLER_PRICE_TWO_ADULTS
        defaultRoomShouldBeFound("priceTwoAdults.greaterThan=" + SMALLER_PRICE_TWO_ADULTS);
    }


    @Test
    @Transactional
    public void getAllRoomsByPriceThreeAdultsIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceThreeAdults equals to DEFAULT_PRICE_THREE_ADULTS
        defaultRoomShouldBeFound("priceThreeAdults.equals=" + DEFAULT_PRICE_THREE_ADULTS);

        // Get all the roomList where priceThreeAdults equals to UPDATED_PRICE_THREE_ADULTS
        defaultRoomShouldNotBeFound("priceThreeAdults.equals=" + UPDATED_PRICE_THREE_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceThreeAdultsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceThreeAdults not equals to DEFAULT_PRICE_THREE_ADULTS
        defaultRoomShouldNotBeFound("priceThreeAdults.notEquals=" + DEFAULT_PRICE_THREE_ADULTS);

        // Get all the roomList where priceThreeAdults not equals to UPDATED_PRICE_THREE_ADULTS
        defaultRoomShouldBeFound("priceThreeAdults.notEquals=" + UPDATED_PRICE_THREE_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceThreeAdultsIsInShouldWork() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceThreeAdults in DEFAULT_PRICE_THREE_ADULTS or UPDATED_PRICE_THREE_ADULTS
        defaultRoomShouldBeFound("priceThreeAdults.in=" + DEFAULT_PRICE_THREE_ADULTS + "," + UPDATED_PRICE_THREE_ADULTS);

        // Get all the roomList where priceThreeAdults equals to UPDATED_PRICE_THREE_ADULTS
        defaultRoomShouldNotBeFound("priceThreeAdults.in=" + UPDATED_PRICE_THREE_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceThreeAdultsIsNullOrNotNull() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceThreeAdults is not null
        defaultRoomShouldBeFound("priceThreeAdults.specified=true");

        // Get all the roomList where priceThreeAdults is null
        defaultRoomShouldNotBeFound("priceThreeAdults.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceThreeAdultsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceThreeAdults is greater than or equal to DEFAULT_PRICE_THREE_ADULTS
        defaultRoomShouldBeFound("priceThreeAdults.greaterThanOrEqual=" + DEFAULT_PRICE_THREE_ADULTS);

        // Get all the roomList where priceThreeAdults is greater than or equal to UPDATED_PRICE_THREE_ADULTS
        defaultRoomShouldNotBeFound("priceThreeAdults.greaterThanOrEqual=" + UPDATED_PRICE_THREE_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceThreeAdultsIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceThreeAdults is less than or equal to DEFAULT_PRICE_THREE_ADULTS
        defaultRoomShouldBeFound("priceThreeAdults.lessThanOrEqual=" + DEFAULT_PRICE_THREE_ADULTS);

        // Get all the roomList where priceThreeAdults is less than or equal to SMALLER_PRICE_THREE_ADULTS
        defaultRoomShouldNotBeFound("priceThreeAdults.lessThanOrEqual=" + SMALLER_PRICE_THREE_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceThreeAdultsIsLessThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceThreeAdults is less than DEFAULT_PRICE_THREE_ADULTS
        defaultRoomShouldNotBeFound("priceThreeAdults.lessThan=" + DEFAULT_PRICE_THREE_ADULTS);

        // Get all the roomList where priceThreeAdults is less than UPDATED_PRICE_THREE_ADULTS
        defaultRoomShouldBeFound("priceThreeAdults.lessThan=" + UPDATED_PRICE_THREE_ADULTS);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceThreeAdultsIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceThreeAdults is greater than DEFAULT_PRICE_THREE_ADULTS
        defaultRoomShouldNotBeFound("priceThreeAdults.greaterThan=" + DEFAULT_PRICE_THREE_ADULTS);

        // Get all the roomList where priceThreeAdults is greater than SMALLER_PRICE_THREE_ADULTS
        defaultRoomShouldBeFound("priceThreeAdults.greaterThan=" + SMALLER_PRICE_THREE_ADULTS);
    }


    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultOneChildIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultOneChild equals to DEFAULT_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldBeFound("priceOneAdultOneChild.equals=" + DEFAULT_PRICE_ONE_ADULT_ONE_CHILD);

        // Get all the roomList where priceOneAdultOneChild equals to UPDATED_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldNotBeFound("priceOneAdultOneChild.equals=" + UPDATED_PRICE_ONE_ADULT_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultOneChildIsNotEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultOneChild not equals to DEFAULT_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldNotBeFound("priceOneAdultOneChild.notEquals=" + DEFAULT_PRICE_ONE_ADULT_ONE_CHILD);

        // Get all the roomList where priceOneAdultOneChild not equals to UPDATED_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldBeFound("priceOneAdultOneChild.notEquals=" + UPDATED_PRICE_ONE_ADULT_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultOneChildIsInShouldWork() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultOneChild in DEFAULT_PRICE_ONE_ADULT_ONE_CHILD or UPDATED_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldBeFound("priceOneAdultOneChild.in=" + DEFAULT_PRICE_ONE_ADULT_ONE_CHILD + "," + UPDATED_PRICE_ONE_ADULT_ONE_CHILD);

        // Get all the roomList where priceOneAdultOneChild equals to UPDATED_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldNotBeFound("priceOneAdultOneChild.in=" + UPDATED_PRICE_ONE_ADULT_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultOneChildIsNullOrNotNull() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultOneChild is not null
        defaultRoomShouldBeFound("priceOneAdultOneChild.specified=true");

        // Get all the roomList where priceOneAdultOneChild is null
        defaultRoomShouldNotBeFound("priceOneAdultOneChild.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultOneChildIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultOneChild is greater than or equal to DEFAULT_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldBeFound("priceOneAdultOneChild.greaterThanOrEqual=" + DEFAULT_PRICE_ONE_ADULT_ONE_CHILD);

        // Get all the roomList where priceOneAdultOneChild is greater than or equal to UPDATED_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldNotBeFound("priceOneAdultOneChild.greaterThanOrEqual=" + UPDATED_PRICE_ONE_ADULT_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultOneChildIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultOneChild is less than or equal to DEFAULT_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldBeFound("priceOneAdultOneChild.lessThanOrEqual=" + DEFAULT_PRICE_ONE_ADULT_ONE_CHILD);

        // Get all the roomList where priceOneAdultOneChild is less than or equal to SMALLER_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldNotBeFound("priceOneAdultOneChild.lessThanOrEqual=" + SMALLER_PRICE_ONE_ADULT_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultOneChildIsLessThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultOneChild is less than DEFAULT_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldNotBeFound("priceOneAdultOneChild.lessThan=" + DEFAULT_PRICE_ONE_ADULT_ONE_CHILD);

        // Get all the roomList where priceOneAdultOneChild is less than UPDATED_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldBeFound("priceOneAdultOneChild.lessThan=" + UPDATED_PRICE_ONE_ADULT_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultOneChildIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultOneChild is greater than DEFAULT_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldNotBeFound("priceOneAdultOneChild.greaterThan=" + DEFAULT_PRICE_ONE_ADULT_ONE_CHILD);

        // Get all the roomList where priceOneAdultOneChild is greater than SMALLER_PRICE_ONE_ADULT_ONE_CHILD
        defaultRoomShouldBeFound("priceOneAdultOneChild.greaterThan=" + SMALLER_PRICE_ONE_ADULT_ONE_CHILD);
    }


    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsOneChildIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdultsOneChild equals to DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldBeFound("priceTwoAdultsOneChild.equals=" + DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD);

        // Get all the roomList where priceTwoAdultsOneChild equals to UPDATED_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldNotBeFound("priceTwoAdultsOneChild.equals=" + UPDATED_PRICE_TWO_ADULTS_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsOneChildIsNotEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdultsOneChild not equals to DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldNotBeFound("priceTwoAdultsOneChild.notEquals=" + DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD);

        // Get all the roomList where priceTwoAdultsOneChild not equals to UPDATED_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldBeFound("priceTwoAdultsOneChild.notEquals=" + UPDATED_PRICE_TWO_ADULTS_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsOneChildIsInShouldWork() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdultsOneChild in DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD or UPDATED_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldBeFound("priceTwoAdultsOneChild.in=" + DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD + "," + UPDATED_PRICE_TWO_ADULTS_ONE_CHILD);

        // Get all the roomList where priceTwoAdultsOneChild equals to UPDATED_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldNotBeFound("priceTwoAdultsOneChild.in=" + UPDATED_PRICE_TWO_ADULTS_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsOneChildIsNullOrNotNull() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdultsOneChild is not null
        defaultRoomShouldBeFound("priceTwoAdultsOneChild.specified=true");

        // Get all the roomList where priceTwoAdultsOneChild is null
        defaultRoomShouldNotBeFound("priceTwoAdultsOneChild.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsOneChildIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdultsOneChild is greater than or equal to DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldBeFound("priceTwoAdultsOneChild.greaterThanOrEqual=" + DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD);

        // Get all the roomList where priceTwoAdultsOneChild is greater than or equal to UPDATED_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldNotBeFound("priceTwoAdultsOneChild.greaterThanOrEqual=" + UPDATED_PRICE_TWO_ADULTS_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsOneChildIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdultsOneChild is less than or equal to DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldBeFound("priceTwoAdultsOneChild.lessThanOrEqual=" + DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD);

        // Get all the roomList where priceTwoAdultsOneChild is less than or equal to SMALLER_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldNotBeFound("priceTwoAdultsOneChild.lessThanOrEqual=" + SMALLER_PRICE_TWO_ADULTS_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsOneChildIsLessThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdultsOneChild is less than DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldNotBeFound("priceTwoAdultsOneChild.lessThan=" + DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD);

        // Get all the roomList where priceTwoAdultsOneChild is less than UPDATED_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldBeFound("priceTwoAdultsOneChild.lessThan=" + UPDATED_PRICE_TWO_ADULTS_ONE_CHILD);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceTwoAdultsOneChildIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceTwoAdultsOneChild is greater than DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldNotBeFound("priceTwoAdultsOneChild.greaterThan=" + DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD);

        // Get all the roomList where priceTwoAdultsOneChild is greater than SMALLER_PRICE_TWO_ADULTS_ONE_CHILD
        defaultRoomShouldBeFound("priceTwoAdultsOneChild.greaterThan=" + SMALLER_PRICE_TWO_ADULTS_ONE_CHILD);
    }


    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultTwoChildrenIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultTwoChildren equals to DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldBeFound("priceOneAdultTwoChildren.equals=" + DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);

        // Get all the roomList where priceOneAdultTwoChildren equals to UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldNotBeFound("priceOneAdultTwoChildren.equals=" + UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultTwoChildrenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultTwoChildren not equals to DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldNotBeFound("priceOneAdultTwoChildren.notEquals=" + DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);

        // Get all the roomList where priceOneAdultTwoChildren not equals to UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldBeFound("priceOneAdultTwoChildren.notEquals=" + UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultTwoChildrenIsInShouldWork() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultTwoChildren in DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN or UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldBeFound("priceOneAdultTwoChildren.in=" + DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN + "," + UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);

        // Get all the roomList where priceOneAdultTwoChildren equals to UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldNotBeFound("priceOneAdultTwoChildren.in=" + UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultTwoChildrenIsNullOrNotNull() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultTwoChildren is not null
        defaultRoomShouldBeFound("priceOneAdultTwoChildren.specified=true");

        // Get all the roomList where priceOneAdultTwoChildren is null
        defaultRoomShouldNotBeFound("priceOneAdultTwoChildren.specified=false");
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultTwoChildrenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultTwoChildren is greater than or equal to DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldBeFound("priceOneAdultTwoChildren.greaterThanOrEqual=" + DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);

        // Get all the roomList where priceOneAdultTwoChildren is greater than or equal to UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldNotBeFound("priceOneAdultTwoChildren.greaterThanOrEqual=" + UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultTwoChildrenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultTwoChildren is less than or equal to DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldBeFound("priceOneAdultTwoChildren.lessThanOrEqual=" + DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);

        // Get all the roomList where priceOneAdultTwoChildren is less than or equal to SMALLER_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldNotBeFound("priceOneAdultTwoChildren.lessThanOrEqual=" + SMALLER_PRICE_ONE_ADULT_TWO_CHILDREN);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultTwoChildrenIsLessThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultTwoChildren is less than DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldNotBeFound("priceOneAdultTwoChildren.lessThan=" + DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);

        // Get all the roomList where priceOneAdultTwoChildren is less than UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldBeFound("priceOneAdultTwoChildren.lessThan=" + UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);
    }

    @Test
    @Transactional
    public void getAllRoomsByPriceOneAdultTwoChildrenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        // Get all the roomList where priceOneAdultTwoChildren is greater than DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldNotBeFound("priceOneAdultTwoChildren.greaterThan=" + DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN);

        // Get all the roomList where priceOneAdultTwoChildren is greater than SMALLER_PRICE_ONE_ADULT_TWO_CHILDREN
        defaultRoomShouldBeFound("priceOneAdultTwoChildren.greaterThan=" + SMALLER_PRICE_ONE_ADULT_TWO_CHILDREN);
    }


    @Test
    @Transactional
    public void getAllRoomsByDefectsIsEqualToSomething() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);
        Defect defects = DefectResourceIT.createEntity(em);
        room.addDefects(defects);
        em.persist(defects);
        em.flush();
        roomRepository.saveAndFlush(room);
        Long defectsId = defects.getId();

        // Get all the roomList where defects equals to defectsId
        defaultRoomShouldBeFound("defectsId.equals=" + defectsId);

        // Get all the roomList where defects equals to defectsId + 1
        defaultRoomShouldNotBeFound("defectsId.equals=" + (defectsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultRoomShouldBeFound(String filter) throws Exception {
        restRoomMockMvc.perform(get("/api/rooms?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(room.getId().intValue())))
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].maxCapacity").value(hasItem(DEFAULT_MAX_CAPACITY)))
            .andExpect(jsonPath("$.[*].priceOneAdult").value(hasItem(DEFAULT_PRICE_ONE_ADULT.intValue())))
            .andExpect(jsonPath("$.[*].priceTwoAdults").value(hasItem(DEFAULT_PRICE_TWO_ADULTS.intValue())))
            .andExpect(jsonPath("$.[*].priceThreeAdults").value(hasItem(DEFAULT_PRICE_THREE_ADULTS.intValue())))
            .andExpect(jsonPath("$.[*].priceOneAdultOneChild").value(hasItem(DEFAULT_PRICE_ONE_ADULT_ONE_CHILD.intValue())))
            .andExpect(jsonPath("$.[*].priceTwoAdultsOneChild").value(hasItem(DEFAULT_PRICE_TWO_ADULTS_ONE_CHILD.intValue())))
            .andExpect(jsonPath("$.[*].priceOneAdultTwoChildren").value(hasItem(DEFAULT_PRICE_ONE_ADULT_TWO_CHILDREN.intValue())));

        // Check, that the count call also returns 1
        restRoomMockMvc.perform(get("/api/rooms/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultRoomShouldNotBeFound(String filter) throws Exception {
        restRoomMockMvc.perform(get("/api/rooms?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restRoomMockMvc.perform(get("/api/rooms/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingRoom() throws Exception {
        // Get the room
        restRoomMockMvc.perform(get("/api/rooms/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRoom() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        int databaseSizeBeforeUpdate = roomRepository.findAll().size();

        // Update the room
        Room updatedRoom = roomRepository.findById(room.getId()).get();
        // Disconnect from session so that the updates on updatedRoom are not directly saved in db
        em.detach(updatedRoom);
        updatedRoom
            .number(UPDATED_NUMBER)
            .maxCapacity(UPDATED_MAX_CAPACITY)
            .priceOneAdult(UPDATED_PRICE_ONE_ADULT)
            .priceTwoAdults(UPDATED_PRICE_TWO_ADULTS)
            .priceThreeAdults(UPDATED_PRICE_THREE_ADULTS)
            .priceOneAdultOneChild(UPDATED_PRICE_ONE_ADULT_ONE_CHILD)
            .priceTwoAdultsOneChild(UPDATED_PRICE_TWO_ADULTS_ONE_CHILD)
            .priceOneAdultTwoChildren(UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);
        RoomDTO roomDTO = roomMapper.toDto(updatedRoom);

        restRoomMockMvc.perform(put("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isOk());

        // Validate the Room in the database
        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeUpdate);
        Room testRoom = roomList.get(roomList.size() - 1);
        assertThat(testRoom.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testRoom.getMaxCapacity()).isEqualTo(UPDATED_MAX_CAPACITY);
        assertThat(testRoom.getPriceOneAdult()).isEqualTo(UPDATED_PRICE_ONE_ADULT);
        assertThat(testRoom.getPriceTwoAdults()).isEqualTo(UPDATED_PRICE_TWO_ADULTS);
        assertThat(testRoom.getPriceThreeAdults()).isEqualTo(UPDATED_PRICE_THREE_ADULTS);
        assertThat(testRoom.getPriceOneAdultOneChild()).isEqualTo(UPDATED_PRICE_ONE_ADULT_ONE_CHILD);
        assertThat(testRoom.getPriceTwoAdultsOneChild()).isEqualTo(UPDATED_PRICE_TWO_ADULTS_ONE_CHILD);
        assertThat(testRoom.getPriceOneAdultTwoChildren()).isEqualTo(UPDATED_PRICE_ONE_ADULT_TWO_CHILDREN);
    }

    @Test
    @Transactional
    public void updateNonExistingRoom() throws Exception {
        int databaseSizeBeforeUpdate = roomRepository.findAll().size();

        // Create the Room
        RoomDTO roomDTO = roomMapper.toDto(room);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRoomMockMvc.perform(put("/api/rooms")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roomDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Room in the database
        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRoom() throws Exception {
        // Initialize the database
        roomRepository.saveAndFlush(room);

        int databaseSizeBeforeDelete = roomRepository.findAll().size();

        // Delete the room
        restRoomMockMvc.perform(delete("/api/rooms/{id}", room.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Room> roomList = roomRepository.findAll();
        assertThat(roomList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Room.class);
        Room room1 = new Room();
        room1.setId(1L);
        Room room2 = new Room();
        room2.setId(room1.getId());
        assertThat(room1).isEqualTo(room2);
        room2.setId(2L);
        assertThat(room1).isNotEqualTo(room2);
        room1.setId(null);
        assertThat(room1).isNotEqualTo(room2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoomDTO.class);
        RoomDTO roomDTO1 = new RoomDTO();
        roomDTO1.setId(1L);
        RoomDTO roomDTO2 = new RoomDTO();
        assertThat(roomDTO1).isNotEqualTo(roomDTO2);
        roomDTO2.setId(roomDTO1.getId());
        assertThat(roomDTO1).isEqualTo(roomDTO2);
        roomDTO2.setId(2L);
        assertThat(roomDTO1).isNotEqualTo(roomDTO2);
        roomDTO1.setId(null);
        assertThat(roomDTO1).isNotEqualTo(roomDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(roomMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(roomMapper.fromId(null)).isNull();
    }
}
